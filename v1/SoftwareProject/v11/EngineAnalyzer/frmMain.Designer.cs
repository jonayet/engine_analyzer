﻿namespace EngineAnalyzer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblClock = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnSave = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnOpen = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnSetup = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnExit = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnPrint = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnBatteryAnalysis = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnVehicleDetails = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnOpacityAnalysis = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnCustomerDetails = new InterfaceLab.WinForm.Controls.GlassButton();
            this.btnEmissionAnalysis = new InterfaceLab.WinForm.Controls.GlassButton();
            this.a1Panel1 = new Owf.Controls.A1Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCompanyName = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.tmrClock = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.a1Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.lblClock);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnOpen);
            this.panel1.Controls.Add(this.btnSetup);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.btnBatteryAnalysis);
            this.panel1.Controls.Add(this.btnVehicleDetails);
            this.panel1.Controls.Add(this.btnOpacityAnalysis);
            this.panel1.Controls.Add(this.btnCustomerDetails);
            this.panel1.Controls.Add(this.btnEmissionAnalysis);
            this.panel1.Controls.Add(this.a1Panel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1018, 738);
            this.panel1.TabIndex = 0;
            // 
            // lblClock
            // 
            this.lblClock.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblClock.BackColor = System.Drawing.Color.Transparent;
            this.lblClock.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblClock.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblClock.BackShadowColor = System.Drawing.Color.Black;
            this.lblClock.BorderColor = System.Drawing.Color.Black;
            this.lblClock.Font = new System.Drawing.Font("Microsoft Sans Serif", 56F);
            this.lblClock.ForeColor = System.Drawing.Color.MediumSpringGreen;
            this.lblClock.ForeGradientAngle = 90;
            this.lblClock.ForeGradientColor2 = System.Drawing.Color.Aquamarine;
            this.lblClock.ForeShadowAmount = 4;
            this.lblClock.ForeShadowColor = System.Drawing.Color.Black;
            this.lblClock.Location = new System.Drawing.Point(259, 179);
            this.lblClock.Name = "lblClock";
            this.lblClock.Size = new System.Drawing.Size(466, 102);
            this.lblClock.TabIndex = 0;
            this.lblClock.Text = "00:00:00 AM";
            this.lblClock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblClock.TextBorderColor = System.Drawing.Color.Black;
            this.lblClock.TextBorderWidth = 1;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(296, 611);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(110, 35);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "   Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnOpen.Image")));
            this.btnOpen.Location = new System.Drawing.Point(162, 611);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(110, 35);
            this.btnOpen.TabIndex = 17;
            this.btnOpen.Text = "   Open";
            this.btnOpen.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOpen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnSetup
            // 
            this.btnSetup.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSetup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetup.Image = ((System.Drawing.Image)(resources.GetObject("btnSetup.Image")));
            this.btnSetup.Location = new System.Drawing.Point(618, 611);
            this.btnSetup.Name = "btnSetup";
            this.btnSetup.Size = new System.Drawing.Size(110, 35);
            this.btnSetup.TabIndex = 16;
            this.btnSetup.Text = "   Setup";
            this.btnSetup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSetup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSetup.Click += new System.EventHandler(this.btnSetup_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.GlowColor = System.Drawing.Color.Red;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.Location = new System.Drawing.Point(753, 611);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(110, 35);
            this.btnExit.TabIndex = 15;
            this.btnExit.Text = "   Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPrint.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnPrint.GlowColor = System.Drawing.Color.Cyan;
            this.btnPrint.Location = new System.Drawing.Point(544, 518);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(320, 70);
            this.btnPrint.TabIndex = 14;
            this.btnPrint.Text = "Print";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnBatteryAnalysis
            // 
            this.btnBatteryAnalysis.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBatteryAnalysis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBatteryAnalysis.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBatteryAnalysis.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnBatteryAnalysis.GlowColor = System.Drawing.Color.Cyan;
            this.btnBatteryAnalysis.Location = new System.Drawing.Point(162, 518);
            this.btnBatteryAnalysis.Name = "btnBatteryAnalysis";
            this.btnBatteryAnalysis.Size = new System.Drawing.Size(320, 70);
            this.btnBatteryAnalysis.TabIndex = 13;
            this.btnBatteryAnalysis.Text = "Battery and Alternator Analysis";
            this.btnBatteryAnalysis.Click += new System.EventHandler(this.btnBatteryAnalysis_Click);
            // 
            // btnVehicleDetails
            // 
            this.btnVehicleDetails.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnVehicleDetails.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVehicleDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVehicleDetails.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnVehicleDetails.GlowColor = System.Drawing.Color.Cyan;
            this.btnVehicleDetails.Location = new System.Drawing.Point(544, 415);
            this.btnVehicleDetails.Name = "btnVehicleDetails";
            this.btnVehicleDetails.Size = new System.Drawing.Size(320, 70);
            this.btnVehicleDetails.TabIndex = 12;
            this.btnVehicleDetails.Text = "Vehicle Details";
            this.btnVehicleDetails.Click += new System.EventHandler(this.btnVehicleDetails_Click);
            // 
            // btnOpacityAnalysis
            // 
            this.btnOpacityAnalysis.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOpacityAnalysis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpacityAnalysis.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpacityAnalysis.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnOpacityAnalysis.GlowColor = System.Drawing.Color.Cyan;
            this.btnOpacityAnalysis.Location = new System.Drawing.Point(162, 415);
            this.btnOpacityAnalysis.Name = "btnOpacityAnalysis";
            this.btnOpacityAnalysis.Size = new System.Drawing.Size(320, 70);
            this.btnOpacityAnalysis.TabIndex = 11;
            this.btnOpacityAnalysis.Text = "Diesel Opacity and Smoke Analysis";
            this.btnOpacityAnalysis.Click += new System.EventHandler(this.btnOpacityAnalysis_Click);
            // 
            // btnCustomerDetails
            // 
            this.btnCustomerDetails.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCustomerDetails.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCustomerDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomerDetails.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomerDetails.GlowColor = System.Drawing.Color.Cyan;
            this.btnCustomerDetails.Location = new System.Drawing.Point(544, 317);
            this.btnCustomerDetails.Name = "btnCustomerDetails";
            this.btnCustomerDetails.Size = new System.Drawing.Size(320, 70);
            this.btnCustomerDetails.TabIndex = 10;
            this.btnCustomerDetails.Text = "Customer Details";
            this.btnCustomerDetails.Click += new System.EventHandler(this.btnCustomerDetails_Click);
            // 
            // btnEmissionAnalysis
            // 
            this.btnEmissionAnalysis.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEmissionAnalysis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEmissionAnalysis.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmissionAnalysis.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnEmissionAnalysis.GlowColor = System.Drawing.Color.Cyan;
            this.btnEmissionAnalysis.Location = new System.Drawing.Point(162, 317);
            this.btnEmissionAnalysis.Name = "btnEmissionAnalysis";
            this.btnEmissionAnalysis.Size = new System.Drawing.Size(320, 70);
            this.btnEmissionAnalysis.TabIndex = 9;
            this.btnEmissionAnalysis.Text = "Emission Analysis";
            this.btnEmissionAnalysis.Click += new System.EventHandler(this.btnEmissionAnalysis_Click);
            // 
            // a1Panel1
            // 
            this.a1Panel1.BackColor = System.Drawing.Color.Black;
            this.a1Panel1.BorderColor = System.Drawing.Color.Gray;
            this.a1Panel1.Controls.Add(this.panel3);
            this.a1Panel1.Controls.Add(this.smartLabel1);
            this.a1Panel1.Controls.Add(this.lblCompanyName);
            this.a1Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.a1Panel1.GradientEndColor = System.Drawing.Color.Silver;
            this.a1Panel1.GradientStartColor = System.Drawing.Color.Gray;
            this.a1Panel1.Image = null;
            this.a1Panel1.ImageLocation = new System.Drawing.Point(4, 4);
            this.a1Panel1.Location = new System.Drawing.Point(0, 0);
            this.a1Panel1.Name = "a1Panel1";
            this.a1Panel1.ShadowOffSet = 0;
            this.a1Panel1.Size = new System.Drawing.Size(1018, 125);
            this.a1Panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel3.Location = new System.Drawing.Point(165, 30);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(235, 29);
            this.panel3.TabIndex = 7;
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.Location = new System.Drawing.Point(397, 30);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(450, 29);
            this.smartLabel1.TabIndex = 9;
            this.smartLabel1.Text = "COMPUTERIZED ENGINE ANALYZER   ";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel1.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.lblCompanyName.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCompanyName.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCompanyName.BackShadowColor = System.Drawing.Color.Black;
            this.lblCompanyName.BorderColor = System.Drawing.Color.Black;
            this.lblCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.lblCompanyName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCompanyName.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblCompanyName.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCompanyName.Location = new System.Drawing.Point(13, 76);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(993, 28);
            this.lblCompanyName.TabIndex = 8;
            this.lblCompanyName.Text = "[COMPANY NAME]";
            this.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCompanyName.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // tmrClock
            // 
            this.tmrClock.Enabled = true;
            this.tmrClock.Tick += new System.EventHandler(this.tmrClock_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 738);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Computerized Engine Aanalyzer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.a1Panel1.ResumeLayout(false);
            this.a1Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Owf.Controls.A1Panel a1Panel1;
        private System.Windows.Forms.Panel panel3;
        private InterfaceLab.WinForm.Controls.GlassButton btnPrint;
        private InterfaceLab.WinForm.Controls.GlassButton btnBatteryAnalysis;
        private InterfaceLab.WinForm.Controls.GlassButton btnVehicleDetails;
        private InterfaceLab.WinForm.Controls.GlassButton btnOpacityAnalysis;
        private InterfaceLab.WinForm.Controls.GlassButton btnCustomerDetails;
        private InterfaceLab.WinForm.Controls.GlassButton btnEmissionAnalysis;
        private InterfaceLab.WinForm.Controls.GlassButton btnExit;
        private InterfaceLab.WinForm.Controls.GlassButton btnSave;
        private InterfaceLab.WinForm.Controls.GlassButton btnOpen;
        private InterfaceLab.WinForm.Controls.GlassButton btnSetup;
        private System.Windows.Forms.Timer tmrClock;
        private InterfaceLab.WinForm.Controls.SmartLabel lblClock;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCompanyName;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
    }
}