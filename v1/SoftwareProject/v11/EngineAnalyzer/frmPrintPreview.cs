﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Forms;
using CoolPrintPreview;

namespace EngineAnalyzer
{
    public partial class frmPrintPreview : Form
    {
        CoolPrintPreviewControl _preview = new CoolPrintPreviewControl();

        public frmPrintPreview()
        {
            InitializeComponent();
        }

        private void btnPageSetup_Click(object sender, EventArgs e)
        {
            using (var dlg = new PageSetupDialog())
            {
                dlg.Document = _preview.Document;
                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    // to show new page layout
                    _preview.RefreshPreview();
                }
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            using (var dlg = new PrintDialog())
            {
                // configure dialog
                dlg.AllowSomePages = true;
                dlg.AllowSelection = true;
                dlg.AllowCurrentPage = true;
                dlg.UseEXDialog = true;
                dlg.Document = _preview.Document;

                // show allowed page range
                var ps = dlg.PrinterSettings;
                ps.MinimumPage = ps.FromPage = 1;
                ps.MaximumPage = ps.ToPage = _preview.PageCount;

                // show dialog
                if (dlg.ShowDialog(this) == DialogResult.OK)
                {
                    // print selected page range
                    _preview.Print();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdatePreview()
        {
            PrintPreviewControl cp;
            pnlPreview.Controls.Clear();

            int y = 0;
            for (int i = 0; i < _preview.PageCount; i++)
            {
                cp = new PrintPreviewControl();
                cp.Document = _preview.Document;
                cp.StartPage = i;

                cp.Height = (int)_preview.Document.DefaultPageSettings.PrintableArea.Height;
                cp.Width = (int)_preview.Document.DefaultPageSettings.PrintableArea.Width;

                int x = pnlPreview.Width - cp.Width;
                cp.Left = 10;
                if (x > 0)
                {
                    cp.Left += x / 2;
                }

                cp.Top = y;
                pnlPreview.Controls.Add(cp);
                y = y + (cp.Height);
            }
        }
    }
}
