﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // make settings portable
            PortableSettingsProvider PortableSettingsProvider = new PortableSettingsProvider();
            Properties.Settings.Default.Providers.Add(PortableSettingsProvider);
            foreach (System.Configuration.SettingsProperty property in Properties.Settings.Default.Properties)
            {
                property.Provider = PortableSettingsProvider;
            }
            // make settings portable

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new frmRegistrationChecker());
            //if (!RegistrationModule.IsRegistered) { return; }
            Application.Run(new frmMain());
        }
    }
}