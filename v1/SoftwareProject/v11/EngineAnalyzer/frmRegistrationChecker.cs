﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Properties = EngineAnalyzer.Properties;

public partial class frmRegistrationChecker : Form
{
    private BackgroundWorker bgWorker = new BackgroundWorker();
    delegate void SetTextCallback(Control ctrl, string text);

    public frmRegistrationChecker()
    {
        InitializeComponent();
        bgWorker.WorkerReportsProgress = true;
        bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
        bgWorker.ProgressChanged += new ProgressChangedEventHandler(bgWorker_ProgressChanged);
        bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
    }

    void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
        pbrProgress.Value = e.ProgressPercentage;
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        bgWorker.RunWorkerAsync();
    }

    void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        if (RegistrationModule.VerificationCode.Equals(Properties.Settings.Default.__RegCode))
        {
            this.Close();
        }
        else
        {
            this.Text = "Please register your product.";
            pbrProgress.Visible = false;
            tbxMachineId.Text = RegistrationModule.MachineCode;
            this.Width = 480;
            this.Height = 350;
            this.CenterToScreen();
            groupBox1.Visible = true;
        }
    }

    void bgWorker_DoWork(object sender, DoWorkEventArgs e)
    {
        bgWorker.ReportProgress(40);
        System.Threading.Thread.Sleep(100);
        RegistrationModule.GenerateID(tbxProductId.Text, Properties.Settings.Default.__RegCode);
        if (RegistrationModule.IsRegistered) { SetText(this, "Registered Product"); }
        bgWorker.ReportProgress(70);
        System.Threading.Thread.Sleep(1000);
        bgWorker.ReportProgress(100);
        System.Threading.Thread.Sleep(1000);
    }

    private void btnRegister_Click(object sender, EventArgs e)
    {
        Properties.Settings.Default.Save();
        if (RegistrationModule.CheckRegistrationCode(tbxProductId.Text, tbxRegCode.Text))
        {
            lblRegStatus.Text = "Registered";
            lblRegStatus.ForeColor = Color.Green;
            MessageBox.Show("This product has been registered successfully!", "Success");
            this.Close();
        }
        else
        {
            lblRegStatus.Text = "Unregistered";
            lblRegStatus.ForeColor = Color.Red;
            MessageBox.Show("Wrong registration code.", "Error");
        }
    }

    private void btnExit_Click(object sender, EventArgs e)
    {
        this.Close();
    }

    private void SetText(Control ctrl, string text)
    {
        if (ctrl.InvokeRequired)
        {
            ctrl.BeginInvoke(new SetTextCallback(SetText), ctrl, text);
        }
        else
        {
            ctrl.Text = text;
        }
    }
}
