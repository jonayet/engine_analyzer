﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using RTF;
using INFRA.USB;
using EngineAnalyzer.Properties;

namespace EngineAnalyzer
{
    public partial class frmBatteryAnalysis : Form
    {
        // PointPairList
        IPointListEdit plRPM;
        IPointListEdit plVoltage;
        IPointListEdit plCurrent;

        private RTFBuilderbase richTextBuilder = new RTFBuilder();
        private SerialComunication sCom;
        private int Async_Data_Length = 19;

        // create USB HID device
        UsbHidPort usbHid = new UsbHidPort(0x1FBD, 0x0003);

        // Starting time in milliseconds
        int rpmLastTick = 0, rpmTotalTick = 0;
        int vcLastTick = 0, vcTotalTick = 0;

        public frmBatteryAnalysis()
        {
            InitializeComponent();

            // settings for ZedGraph controls
            InitZedGraphControls();

            // if Emission Analyzer is used as RPM source
            if (Settings.Default.__RPM_Emission)
            {
                sCom = new SerialComunication(CommonResources.EmissionPortName, 9600);
                sCom.AsyncReceived += new EventHandler<AsyncReceived_EventArgs>(sCom_Emission_AsyncReceived);

                // load data length. vary on model number
                if (Settings.Default.__NHA505)
                {
                    Async_Data_Length = 19;
                }
                else if (Settings.Default.__NHA505_Touch)
                {
                    Async_Data_Length = 18;
                }
            }
            else if (Settings.Default.__RPM_Opacity)   // if Opacity Analyzer is used as RPM source
            {
                sCom = new SerialComunication(CommonResources.OpacityPortName, 9600);
                sCom.AsyncReceived += new EventHandler<AsyncReceived_EventArgs>(sCom_Opacity_AsyncReceived);
            }

            // init USB HID device
            try
            {
                usbHid.OnDeviceAttached += new EventHandler(usbHid_OnDeviceAttached);
                usbHid.OnDeviceRemoved += new EventHandler(usbHid_OnDeviceRemoved);
                usbHid.OnDataRecieved += new DataRecievedEventHandler(usbHid_OnDataRecieved);
                usbHid.CheckDevicePresent();
            }
            catch { }

            zgcRPM.MouseMove += new MouseEventHandler(zgcRPM_MouseMove);
            zgcVoltage.MouseMove += new MouseEventHandler(zgcVoltage_MouseMove);
            zgcCurrent.MouseMove +=new MouseEventHandler(zgcCurrent_MouseMove);
        }

        protected override void OnLoad(EventArgs e)
        {
            // load Total Capture
            if (CommonResources.BatteryTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.BatteryTotalCapture.ToString();
            }

            // load RichText
            richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.BatteryCapturedRichText))
            {
                // add Emission Title
                richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).AppendLine("Battery & Aletrnator Report");
                richTextBuilder.AppendLine();
            }
            else
            {
                richTextBuilder.AppendRTFDocument(CommonResources.BatteryCapturedRichText);
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // open COM port
            sCom.Open();

            // start graph updater timer
            tmrGraphUpdater.Enabled = true;

            // pass to base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            // save all settings
            Settings.Default.Save();

            if (Settings.Default.__RPM_Emission)
            {
                sCom.Send(0x02);
            }

            usbHid.OnDeviceAttached -= new EventHandler(usbHid_OnDeviceAttached);
            usbHid.OnDeviceRemoved -= new EventHandler(usbHid_OnDeviceRemoved);
            usbHid.OnDataRecieved -= new DataRecievedEventHandler(usbHid_OnDataRecieved);
            usbHid.UnregisterHandle();

            tmrMeasure.Enabled = false;
            tmrGraphUpdater.Enabled = false;
            sCom.Close();

            base.OnClosing(e);
        }

        void zgcRPM_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && btnMeasure.Text == "MEASURE")
            {
                zgcVoltage.GraphPane.XAxis.Scale.Max = zgcRPM.GraphPane.XAxis.Scale.Max;
                zgcVoltage.GraphPane.XAxis.Scale.Min = zgcRPM.GraphPane.XAxis.Scale.Min;
                zgcCurrent.GraphPane.XAxis.Scale.Max = zgcRPM.GraphPane.XAxis.Scale.Max;
                zgcCurrent.GraphPane.XAxis.Scale.Min = zgcRPM.GraphPane.XAxis.Scale.Min;
            }
        }

        void zgcVoltage_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && btnMeasure.Text == "MEASURE")
            {
                zgcRPM.GraphPane.XAxis.Scale.Max = zgcVoltage.GraphPane.XAxis.Scale.Max;
                zgcRPM.GraphPane.XAxis.Scale.Min = zgcVoltage.GraphPane.XAxis.Scale.Min;
                zgcCurrent.GraphPane.XAxis.Scale.Max = zgcVoltage.GraphPane.XAxis.Scale.Max;
                zgcCurrent.GraphPane.XAxis.Scale.Min = zgcVoltage.GraphPane.XAxis.Scale.Min;
            }
        }

        void zgcCurrent_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && btnMeasure.Text == "MEASURE")
            {
                zgcRPM.GraphPane.XAxis.Scale.Max = zgcCurrent.GraphPane.XAxis.Scale.Max;
                zgcRPM.GraphPane.XAxis.Scale.Min = zgcCurrent.GraphPane.XAxis.Scale.Min;
                zgcVoltage.GraphPane.XAxis.Scale.Max = zgcCurrent.GraphPane.XAxis.Scale.Max;
                zgcVoltage.GraphPane.XAxis.Scale.Min = zgcCurrent.GraphPane.XAxis.Scale.Min;
            }
        }

        private void InitZedGraphControls()
        {
            // RPM Graph Specific settiong
            zgcRPM.GraphPane.AddCurve("RPM", new RollingPointPairList(10000), Color.Red, SymbolType.None);
            zgcRPM.GraphPane.YAxis.Scale.FontSpec.FontColor = Color.Red;
            zgcRPM.GraphPane.YAxis.Title.FontSpec.FontColor = Color.Red;
            zgcRPM.GraphPane.YAxis.Scale.Min = 0;
            zgcRPM.GraphPane.YAxis.Scale.Max = 4000;
            zgcRPM.GraphPane.YAxis.Scale.MajorStep = 1000;
            zgcRPM.GraphPane.YAxis.Scale.MinorStep = 500;
            zgcRPM.GraphPane.Title.Text = " "; //"RPM vs. Time";
            zgcRPM.GraphPane.YAxis.Title.Text = "RPM";
            zgcRPM.GraphPane.XAxis.Title.Text = "Time";
            zgcRPM.AxisChange();

            // Voltage Graph Specific settiong
            zgcVoltage.GraphPane.AddCurve("Voltage", new RollingPointPairList(10000), Color.Green, SymbolType.None);
            zgcVoltage.GraphPane.YAxis.Scale.FontSpec.FontColor = Color.Green;
            zgcVoltage.GraphPane.YAxis.Title.FontSpec.FontColor = Color.Green;
            zgcVoltage.GraphPane.YAxis.Scale.Min = 0;
            zgcVoltage.GraphPane.YAxis.Scale.Max = 18;
            zgcVoltage.GraphPane.YAxis.Scale.MajorStep = 2;
            zgcVoltage.GraphPane.YAxis.Scale.MinorStep = 0;
            zgcVoltage.GraphPane.Title.Text = " ";//"Voltage vs. Time";
            zgcVoltage.GraphPane.YAxis.Title.Text = "Voltage";
            zgcVoltage.GraphPane.XAxis.Title.Text = "Time";
            zgcVoltage.AxisChange();

            // Current Graph Specific settiong
            zgcCurrent.GraphPane.AddCurve("Current", new RollingPointPairList(10000), Color.Blue, SymbolType.None);
            zgcCurrent.GraphPane.YAxis.Scale.FontSpec.FontColor = Color.Blue;
            zgcCurrent.GraphPane.YAxis.Title.FontSpec.FontColor = Color.Blue;
            zgcCurrent.GraphPane.YAxis.Scale.Min = 0;
            zgcCurrent.GraphPane.YAxis.Scale.Max = 100;
            zgcCurrent.GraphPane.YAxis.Scale.MajorStep = 20;
            zgcCurrent.GraphPane.YAxis.Scale.MinorStep = 0;
            zgcCurrent.GraphPane.Title.Text = " "; //"Current vs. Time";
            zgcCurrent.GraphPane.YAxis.Title.Text = "Current";
            zgcCurrent.GraphPane.XAxis.Title.Text = "Time";
            zgcCurrent.AxisChange();

            // common graph settings
            ZedGraphControl[] zGraphControls = new ZedGraphControl[3];
            zGraphControls[0] = zgcRPM;
            zGraphControls[1] = zgcVoltage;
            zGraphControls[2] = zgcCurrent;

            foreach (ZedGraphControl zGraph in zGraphControls)
            {
                // disable cursor tooltip values
                zGraph.IsShowPointValues = false;
                zGraph.IsShowCursorValues = false;

                // scrollbar
                zGraph.IsShowHScrollBar = false;
                zGraph.IsAutoScrollRange = true;
                zGraph.ScrollGrace = 0;

                // enable horizontal zoom and pan
                zGraph.IsEnableHPan = true;
                zGraph.IsEnableHZoom = true;
                zGraph.IsEnableVPan = false;
                zGraph.IsEnableVZoom = false;

                // set X axis
                zGraph.GraphPane.XAxis.Scale.Min = -30;
                zGraph.GraphPane.XAxis.Scale.Max = 0;
                zGraph.GraphPane.XAxis.Scale.MinorStep = 0;
                zGraph.GraphPane.XAxis.Scale.MajorStep = 5;
                zGraph.GraphPane.XAxis.Scale.Align = AlignP.Inside;
                zGraph.GraphPane.XAxis.Scale.FontSpec.Size = 20;
                zGraph.GraphPane.XAxis.Scale.FontSpec.FontColor = Color.Black;
                zGraph.GraphPane.XAxis.MajorGrid.IsVisible = false;
                zGraph.GraphPane.XAxis.MinorGrid.IsVisible = false;
                zGraph.GraphPane.XAxis.Title.FontSpec.FontColor = Color.Black;                
                zGraph.GraphPane.XAxis.Title.FontSpec.Size = 16;
                zGraph.GraphPane.XAxis.Title.IsVisible = false;
                
                // set Y axis
                zGraph.GraphPane.YAxis.Scale.Align = AlignP.Inside;
                zGraph.GraphPane.YAxis.Scale.FontSpec.Size = 24;
                zGraph.GraphPane.YAxis.MajorGrid.IsVisible = true;
                zGraph.GraphPane.YAxis.MajorGrid.DashOff = 10;
                zGraph.GraphPane.YAxis.MajorGrid.DashOn = 1;
                zGraph.GraphPane.YAxis.MajorGrid.IsZeroLine = false;
                zGraph.GraphPane.YAxis.MinorGrid.IsVisible = false;
                zGraph.GraphPane.YAxis.Title.FontSpec.Size = 24;
                zGraph.GraphPane.YAxis.Title.IsVisible = true;

                // hide curve label
                zGraph.GraphPane.CurveList[0].Label.IsVisible = false;

                // title
                zGraph.GraphPane.Title.FontSpec.Size = 10;
                zGraph.GraphPane.Title.IsVisible = true;

                // Scale the axes
                zGraph.AxisChange();
            }

            // Get the PointPairList
            plRPM = zgcRPM.GraphPane.CurveList[0].Points as IPointListEdit;
            plVoltage = zgcVoltage.GraphPane.CurveList[0].Points as IPointListEdit;
            plCurrent = zgcCurrent.GraphPane.CurveList[0].Points as IPointListEdit;
        }

        private void btnMeasure_Click(object sender, EventArgs e)
        {
            // set last tick
            rpmLastTick = Environment.TickCount;
            vcLastTick = Environment.TickCount;

            /////////////////////////////////////////
            
            if (btnMeasure.Text == "MEASURE")
            {
                btnMeasure.Text = "STOP";
                //tmrMeasure.Enabled = true;
            }
            else
            {
                btnMeasure.Text = "MEASURE";
                //tmrMeasure.Enabled = false;
            }
            return;
            
            /////////////////////////////////////////

            // Emission Analyzer as RPM source
            if (Settings.Default.__RPM_Emission)
            {
                if (btnMeasure.Text == "MEASURE")
                {
                    if (sCom.Send(0x01, 0x06, 2000))
                    {
                        btnMeasure.Text = "STOP";
                        tmrMeasure.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (sCom.Send(0x02, 0x06, 2000))
                    {
                        tmrMeasure.Enabled = false;
                        btnMeasure.Text = "MEASURE";
                    }
                    else
                    {

                        MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (Settings.Default.__RPM_Opacity)
            {
                byte[] Response = new byte[3];

                // stop the real time measurement
                if (btnMeasure.Text == "STOP")
                {
                    tmrMeasure.Enabled = false;
                    btnMeasure.Text = "MEASURE";
                    return;
                }

                // send A1H command
                bool IsSendSuccess = sCom.Send(CommonResources.Opacity_GetDataPacket(0xA1), Response, 3, 100);

                if (!IsSendSuccess)
                {
                    MessageBox.Show("Error occured while sending data. Please check conection.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // check Mode Code
                if (Response[1] == 0x00)
                {
                    MessageBox.Show("Device is under Warming up condition. Please start the test later.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // send A0H and RealTime Test mode command (01H)
                sCom.Send(CommonResources.Opacity_GetDataPacket(0xA0, 0x01), Response, 2, 100);

                // start the measuremnt through tmrMeasure tick
                btnMeasure.Text = "STOP";
                tmrMeasure.Enabled = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            richTextBuilder.Clear();
            CommonResources.BatteryTotalCapture = 0;
            CommonResources.BatteryCapturedRichText = "";
            btnCapture.Text = "CAPTURE";
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.BatteryTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.BatteryTotalCapture.ToString();

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            else
            {
                // richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            }

            // add RPM, Voltage and Current
            richTextBuilder.AppendPara();
            richTextBuilder.Reset();
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Engine speed", lblSpeed.Text + " RPM");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Battery Voltage", lblVoltage.Text + " V");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Battery Current (" + lblCharging.Text + ")", lblCurrent.Text + " A");
            richTextBuilder.AppendLine();

            if (cbxCaptureGraph.Checked)
            {
                //Stopwatch aStopwatch = new Stopwatch();
                //aStopwatch.Start();
                using (Bitmap bmp = new Bitmap(700, 200))
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;

                    // add RPM Graph
                    richTextBuilder.AppendPara();
                    richTextBuilder.Reset();
                    richTextBuilder.AppendLine();
                    g.DrawImage(zgcRPM.MasterPane.GetImage(), 0, 0, 699, 199);
                    richTextBuilder.InsertImage(bmp);

                    // add Voltage Graph
                    richTextBuilder.AppendPara();
                    richTextBuilder.Reset();
                    richTextBuilder.AppendLine();
                    g.DrawImage(zgcVoltage.MasterPane.GetImage(), 0, 0, 699, 199);
                    richTextBuilder.InsertImage(bmp);

                    // add Current Graph
                    richTextBuilder.AppendPara();
                    richTextBuilder.Reset();
                    richTextBuilder.AppendLine();
                    g.DrawImage(zgcCurrent.MasterPane.GetImage(), 0, 0, 699, 199);
                    richTextBuilder.InsertImage(bmp);
                }
                //aStopwatch.Stop();
                //MessageBox.Show(aStopwatch.ElapsedMilliseconds.ToString());
            }
            

            richTextBuilder.AppendPara();
            richTextBuilder.Reset();
            richTextBuilder.AppendLine();
            richTextBuilder.AppendLine();

            // save new values
            CommonResources.BatteryCapturedRichText = richTextBuilder.ToString();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            plRPM.Clear();
            plVoltage.Clear();
            plCurrent.Clear();
            zgcRPM.Invalidate();
            zgcVoltage.Invalidate();
            zgcCurrent.Invalidate();
            rpmTotalTick = 0;
            rpmLastTick = Environment.TickCount;
            vcTotalTick = 0;
            vcLastTick = Environment.TickCount;
        }

        #region serial data
        void sCom_Emission_AsyncReceived(object sender, AsyncReceived_EventArgs e)
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new EventHandler<AsyncReceived_EventArgs>(sCom_Emission_AsyncReceived), new object[] { sender, e });
                }
                catch { }
            }
            else
            {
                if (e.ReceivedSuccess)
                {
                    // get data from EventArgs
                    int Speed_Value = (e.ReceivedData[11] << 8) | e.ReceivedData[12];
                    if (Speed_Value > 10000) { Speed_Value = 0; }

                    // show the values
                    lblSpeed.Text = Speed_Value.ToString();

                    // tick calculation
                    rpmTotalTick += (Environment.TickCount - rpmLastTick);
                    rpmLastTick = Environment.TickCount;
                    double time = vcTotalTick / 1000.0;

                    // add value
                    plRPM.Add(time, Speed_Value);

                    Scale xScale = zgcRPM.GraphPane.XAxis.Scale;
                    double dX = xScale.Max - xScale.Min;
                    if (time > xScale.Max - (dX / 100))
                    {
                        xScale.Max = time + (dX / 100);
                        xScale.Min = xScale.Max - dX;
                    }
                }
            }
        }

        void sCom_Opacity_AsyncReceived(object sender, AsyncReceived_EventArgs e)
        {
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new EventHandler<AsyncReceived_EventArgs>(sCom_Emission_AsyncReceived), new object[] { sender, e });
                }
                catch { }
            }
            else
            {
                if (e.ReceivedSuccess)
                {
                    // get measurement
                    int Speed_S = ((e.ReceivedData[5] << 8) | e.ReceivedData[6]);

                    // show the values
                    lblSpeed.Text = Speed_S.ToString();

                    // tick calculation
                    rpmTotalTick += (Environment.TickCount - rpmLastTick);
                    rpmLastTick = Environment.TickCount;
                    double time = vcTotalTick / 1000.0;

                    // add value
                    plRPM.Add(time, Speed_S);

                    Scale xScale = zgcRPM.GraphPane.XAxis.Scale;
                    double dX = xScale.Max - xScale.Min;
                    if (time > xScale.Max - (dX / 100))
                    {
                        xScale.Max = time + (dX / 100);
                        xScale.Min = xScale.Max - dX;
                    }
                }
            }
        }

        private void usbHid_OnDataRecieved(object sender, DataRecievedEventArgs args)
        {           
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new DataRecievedEventHandler(usbHid_OnDataRecieved), new object[] { sender, args });
                }
                catch { }
            }
            else
            {
                try
                {
                    if (btnMeasure.Text == "STOP")
                    {
                        // tick calculation
                        vcTotalTick += Environment.TickCount - vcLastTick;
                        vcLastTick = Environment.TickCount;

                        // Time is measured in seconds
                        double time = vcTotalTick/1000.0;

                        // add value
                        //plVoltage.Add(time, Math.Sin(2.0 * Math.PI * time) * 5 + 13);
                        //plCurrent.Add(time, Math.Sin(1.0 * Math.PI * time) * 150 + 200);

                        int Voltage = (args.RawData[2] << 8) | args.RawData[3];
                        if ((Voltage & 0x8000) == 0x8000) // negative
                        {
                            Voltage = (int) (0xffff0000 | Voltage);
                        }

                        int Current = (args.RawData[4] << 8) | args.RawData[5];
                        if ((Current & 0x8000) == 0x8000) // negative
                        {
                            Current = (int) (0xffff0000 | Current);
                        }

                        lblVoltage.Text = Voltage.ToString();
                        lblCurrent.Text = Current.ToString();
                        plVoltage.Add(time, Math.Abs(Voltage)*5);
                        plCurrent.Add(time, Math.Abs(Current));

                        //return;

                        //////////////////////////////////////////////////////////////
                        //plRPM.Add(time, Math.Sin(5.0 * Math.PI * time) * 1500 + 2000);
                        //////////////////////////////////////////////////////////////

                        Scale xScale = zgcVoltage.GraphPane.XAxis.Scale;
                        double dX = xScale.Max - xScale.Min;
                        if (time > xScale.Max - (dX/100))
                        {
                            xScale.Max = time + (dX/100);
                            xScale.Min = xScale.Max - dX;
                        }

                        xScale = zgcCurrent.GraphPane.XAxis.Scale;
                        dX = xScale.Max - xScale.Min;
                        if (time > xScale.Max - (dX/100))
                        {
                            xScale.Max = time + (dX/100);
                            xScale.Min = xScale.Max - dX;
                        }

                        //////////////////////////////////////////////////////////////
                        xScale = zgcRPM.GraphPane.XAxis.Scale;
                        dX = xScale.Max - xScale.Min;
                        if (time > xScale.Max - (dX/100))
                        {
                            xScale.Max = time + (dX/100);
                            xScale.Min = xScale.Max - dX;
                        }
                        //////////////////////////////////////////////////////////////
                    }
                }
                catch
                {

                }
            }
        }
        #endregion

        #region USB HID Library
        private void usbHid_OnDeviceAttached(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(usbHid_OnDeviceAttached), new object[] { sender, e });
            }
            else
            {
                lblUsbConnected.Text = "       Connected";
                vcLastTick = Environment.TickCount;
            }
        }

        private void usbHid_OnDeviceRemoved(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(usbHid_OnDeviceRemoved), new object[] { sender, e });
            }
            else
            {
                lblUsbConnected.Text = "Not Connected";
            }
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            usbHid.RegisterHandle(Handle);
            base.OnHandleCreated(e);
        }

        protected override void WndProc(ref Message m)
        {
            // pass message on to base form
            usbHid.ParseMessages(ref m);
            base.WndProc(ref m);
        }
        #endregion

        private void tmrMeasure_Tick(object sender, EventArgs e)
        {
            if (Settings.Default.__RPM_Emission)
            {
                sCom.SendAsync(0x03, Async_Data_Length, 300);
            }
            else if (Settings.Default.__RPM_Opacity)
            {
                // send A5H command (real time data)
                sCom.SendAsync(CommonResources.Opacity_GetDataPacket(0xA5), 10, 50);
            }
        }

        private void tmrGraphUpdater_Tick(object sender, EventArgs e)
        {
            // Make sure the Y axis is rescaled to accommodate actual data
            zgcRPM.AxisChange();
            zgcVoltage.AxisChange();
            zgcCurrent.AxisChange();

            // Force a redraw
            zgcRPM.Invalidate();
            zgcVoltage.Invalidate();
            zgcCurrent.Invalidate();
        }
    }
}
