using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace CoolPrintPreview
{
    /// <summary>
    /// Represents a preview of one or two pages in a <see cref="PrintDocument"/>.
    /// </summary>
    /// <remarks>
    /// This control is similar to the standard <see cref="PrintPreviewControl"/> but
    /// it displays pages as they are rendered. By contrast, the standard control 
    /// waits until the entire document is rendered before it displays anything.
    /// </remarks>
    internal class CoolPrintPreviewControl : UserControl
    {
        //-------------------------------------------------------------
        #region ** fields

        PrintDocument _doc;
        int _startPage;
        Brush _backBrush;
        PointF _himm2pix = new PointF(-1, -1);
        PageImageList _img = new PageImageList();
        bool _cancel, _rendering;
        const int MARGIN = 4;

        #endregion

        //-------------------------------------------------------------
        #region ** ctor

        /// <summary>
        /// Initializes a new instance of a <see cref="CoolPrintPreviewControl"/> control.
        /// </summary>
        public CoolPrintPreviewControl()
        {
            BackColor = SystemColors.AppWorkspace;
            StartPage = 0;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        #endregion

        //-------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Gets or sets the <see cref="PrintDocument"/> being previewed.
        /// </summary>
        public PrintDocument Document
        {
            get { return _doc; }
            set
            {
                if (value != _doc)
                {
                    _doc = value;
                    RefreshPreview();
                }
            }
        }
        /// <summary>
        /// Regenerates the preview to reflect changes in the document layout.
        /// </summary>
        public void RefreshPreview()
        {
            // render into PrintController
            if (_doc != null)
            {
                // prepare to render preview document
                _img.Clear();
                PrintController savePC = _doc.PrintController;

                // render preview document
                try
                {
                    _cancel = false;
                    _rendering = true;
                    _doc.PrintController = new PreviewPrintController();
                    _doc.PrintPage += _doc_PrintPage;
                    _doc.EndPrint += _doc_EndPrint;
                    _doc.Print();
                }
                finally
                {
                    _cancel = false;
                    _rendering = false;
                    _doc.PrintPage -= _doc_PrintPage;
                    _doc.EndPrint -= _doc_EndPrint;
                    _doc.PrintController = savePC;
                }
            }

            // update
            OnPageCountChanged(EventArgs.Empty);
            UpdatePreview();
        }
        /// <summary>
        /// Stops rendering the <see cref="Document"/>.
        /// </summary>
        public void Cancel()
        {
            _cancel = true;
        }
        /// <summary>
        /// Gets a value that indicates whether the <see cref="Document"/> is being rendered.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsRendering
        {
            get { return _rendering; }
        }

        /// <summary>
        /// Gets or sets the first page being previewed.
        /// </summary>
        /// <remarks>
        /// There may be one or two pages visible depending on the setting of the
        /// <see cref="ZoomMode"/> property.
        /// </remarks>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
        ]
        public int StartPage
        {
            get { return _startPage; }
            set
            {
                // validate new setting
                if (value > PageCount - 1) value = PageCount - 1;
                if (value < 0) value = 0;

                // apply new setting
                if (value != _startPage)
                {
                    _startPage = value;
                    OnStartPageChanged(EventArgs.Empty);
                }
            }
        }
        /// <summary>
        /// Gets the number of pages available for preview.
        /// </summary>
        /// <remarks>
        /// This number increases as the document is rendered into the control.
        /// </remarks>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
        ]
        public int PageCount
        {
            get { return _img.Count; }
        }
        /// <summary>
        /// Gets or sets the control's background color.
        /// </summary>
        [DefaultValue(typeof(Color), "AppWorkspace")]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                base.BackColor = value;
                _backBrush = new SolidBrush(value);
            }
        }
        /// <summary>
        /// Gets a list containing the images of the pages in the document.
        /// </summary>
        [Browsable(false)]
        public PageImageList PageImages
        {
            get { return _img; }
        }
        /// <summary>
        /// Prints the current document honoring the selected page range.
        /// </summary>
        public void Print()
        {
            // select pages to print
            var ps = _doc.PrinterSettings;
            int first = ps.MinimumPage - 1;
            int last = ps.MaximumPage - 1;
            switch (ps.PrintRange)
            {
                case PrintRange.AllPages:
                    Document.PrintPage += Document_PrintPage;
                    Document.Print();
                    return;
                case PrintRange.CurrentPage:
                    first = last = StartPage;
                    break;
                case PrintRange.Selection:
                    first = last = StartPage;
                    break;
                case PrintRange.SomePages:
                    first = ps.FromPage - 1;
                    last = ps.ToPage - 1;
                    break;
            }

            // print using helper class
            var dp = new DocumentPrinter(this, first, last);
            dp.Print();
        }

        // print page number
        int _PageNumber = 1;
        void Document_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString("Page - " + (_PageNumber++).ToString() + "/" + PageCount.ToString(), new Font("Arial", 12), Brushes.Black, e.PageBounds.Size.Width / 2 - 5, e.PageBounds.Size.Height - 50);
        }

        #endregion

        //-------------------------------------------------------------
        #region ** events

        /// <summary>
        /// Occurs when the value of the <see cref="StartPage"/> property changes.
        /// </summary>
        public event EventHandler StartPageChanged;
        /// <summary>
        /// Raises the <see cref="StartPageChanged"/> event.
        /// </summary>
        /// <param name="e"><see cref="EventArgs"/> that provides the event data.</param>
        protected void OnStartPageChanged(EventArgs e)
        {
            if (StartPageChanged != null)
            {
                StartPageChanged(this, e);
            }
        }
        /// <summary>
        /// Occurs when the value of the <see cref="PageCount"/> property changes.
        /// </summary>
        public event EventHandler PageCountChanged;
        /// <summary>
        /// Raises the <see cref="PageCountChanged"/> event.
        /// </summary>
        /// <param name="e"><see cref="EventArgs"/> that provides the event data/</param>
        protected void OnPageCountChanged(EventArgs e)
        {
            if (PageCountChanged != null)
            {
                PageCountChanged(this, e);
            }
        }

        #endregion

        //-------------------------------------------------------------
        #region ** overrides
        // painting
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            // we're painting it all, so don't call base class
            //base.OnPaintBackground(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Image img = GetImage(StartPage);
            if (img != null)
            {
                Rectangle rc = GetImageRectangle(img);
                if (rc.Width > 2 && rc.Height > 2)
                {
                    // adjust for scrollbars
                    rc.Offset(AutoScrollPosition);

                    // render single page
                    RenderPage(e.Graphics, img, rc);

                }
            }

            // paint background
            e.Graphics.FillRectangle(_backBrush, ClientRectangle);
        }
        #endregion

        //-------------------------------------------------------------
        #region ** implementation

        void _doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            SyncPageImages(false);
            if (_cancel)
            {
                e.Cancel = true;
            }
        }

        void _doc_EndPrint(object sender, PrintEventArgs e)
        {
            SyncPageImages(true);
        }

        void SyncPageImages(bool lastPageReady)
        {
            var pv = (PreviewPrintController)_doc.PrintController;
            if (pv != null)
            {
                var pageInfo = pv.GetPreviewPageInfo();
                int count = lastPageReady ? pageInfo.Length : pageInfo.Length - 1;
                for (int i = _img.Count; i < count; i++)
                {
                    var img = pageInfo[i].Image;
                    _img.Add(img);

                    OnPageCountChanged(EventArgs.Empty);

                    if (StartPage < 0) StartPage = 0;
                    if (i == StartPage || i == StartPage + 1)
                    {
                        Refresh();
                    }
                    Application.DoEvents();
                }
            }
        }

        Image GetImage(int page)
        {
            return page > -1 && page < PageCount ? _img[page] : null;
        }

        Rectangle GetImageRectangle(Image img)
        {
            // start with regular image rectangle
            Size sz = GetImageSizeInPixels(img);
            Rectangle rc = new Rectangle(0, 0, sz.Width, sz.Height);

            // calculate zoom
            Rectangle rcCli = this.ClientRectangle;

            // center image
            int dx = (rcCli.Width - rc.Width) / 2;
            if (dx > 0) rc.X += dx;
            int dy = (rcCli.Height - rc.Height) / 2;
            if (dy > 0) rc.Y += dy;

            // add some extra space
            rc.Inflate(-MARGIN, -MARGIN);

            // done
            return rc;
        }

        Size GetImageSizeInPixels(Image img)
        {
            // get image size
            SizeF szf = img.PhysicalDimension;

            // if it is a metafile, convert to pixels
            if (img is Metafile)
            {
                // get screen resolution
                if (_himm2pix.X < 0)
                {
                    using (Graphics g = this.CreateGraphics())
                    {
                        _himm2pix.X = g.DpiX / 2540f;
                        _himm2pix.Y = g.DpiY / 2540f;
                    }
                }

                // convert himetric to pixels
                szf.Width *= _himm2pix.X;
                szf.Height *= _himm2pix.Y;
            }

            // done
            return Size.Truncate(szf);
        }

        void RenderPage(Graphics g, Image img, Rectangle rc)
        {
            // draw the page
            rc.Offset(1, 1);
            g.DrawRectangle(Pens.Black, rc);
            rc.Offset(-1, -1);
            g.FillRectangle(Brushes.White, rc);
            g.DrawImage(img, rc);
            g.DrawRectangle(Pens.Black, rc);

            // show page number
            g.DrawString("Page - " + (StartPage + 1).ToString() + "/" + PageCount.ToString(), new Font("Arial", 12), Brushes.Black, (rc.Width / 2) - 5, rc.Height - 50);

            // exclude cliprect to paint background later
            rc.Width++;
            rc.Height++;
            g.ExcludeClip(rc);
            rc.Offset(1, 1);
            g.ExcludeClip(rc);
        }

        void UpdatePreview()
        {
            // validate current page
            if (_startPage < 0) _startPage = 0;
            if (_startPage > PageCount - 1) _startPage = PageCount - 1;

            // repaint
            Invalidate();
        }
        #endregion

        //-------------------------------------------------------------
        #region ** nested class

        // helper class that prints the selected page range in a PrintDocument.
        internal class DocumentPrinter : PrintDocument
        {
            int _first, _last, _index;
            PageImageList _imgList;

            public DocumentPrinter(CoolPrintPreviewControl preview, int first, int last)
            {
                // save page range and image list
                _first = first;
                _last = last;
                _imgList = preview.PageImages;

                // copy page and printer settings from original document
                DefaultPageSettings = preview.Document.DefaultPageSettings;
                PrinterSettings = preview.Document.PrinterSettings;
            }

            protected override void OnBeginPrint(PrintEventArgs e)
            {
                // start from the first page
                _index = _first;
            }

            private int _pageCount = 1;
            protected override void OnPrintPage(PrintPageEventArgs e)
            {
                //e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                // render the current page and increment the index
                e.Graphics.PageUnit = GraphicsUnit.Display;
                e.Graphics.DrawImage(_imgList[_index++], e.PageBounds);

                // print page number
                e.Graphics.DrawString("Page - " + (_index).ToString() + "/" + _imgList.Count.ToString(), new Font("Arial", 12), Brushes.Black, e.PageBounds.Size.Width / 2 - 5, e.PageBounds.Size.Height - 50);

                // stop when we reach the last page in the range
                e.HasMorePages = _index <= _last;
                _pageCount++;
            }
        }

        #endregion
    }
}
