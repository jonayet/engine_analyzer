﻿namespace EngineAnalyzer
{
    partial class frmBatteryAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBatteryAnalysis));
            this.zgcRPM = new ZedGraph.ZedGraphControl();
            this.zgcVoltage = new ZedGraph.ZedGraphControl();
            this.zgcCurrent = new ZedGraph.ZedGraphControl();
            this.btnClear = new System.Windows.Forms.Button();
            this.smartLabel34 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.cmbxCaption = new System.Windows.Forms.ComboBox();
            this.btnMeasure = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCapture = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel40 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblVoltage = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCurrent = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel22 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel12 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblPeakCurrent = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblSpeed = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCharging = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.tmrMeasure = new System.Windows.Forms.Timer(this.components);
            this.lblUsbConnected = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.tmrGraphUpdater = new System.Windows.Forms.Timer(this.components);
            this.cbxCaptureGraph = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // zgcRPM
            // 
            this.zgcRPM.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.zgcRPM.Cursor = System.Windows.Forms.Cursors.Cross;
            this.zgcRPM.IsPrintKeepAspectRatio = false;
            this.zgcRPM.IsShowContextMenu = false;
            this.zgcRPM.IsShowCopyMessage = false;
            this.zgcRPM.Location = new System.Drawing.Point(12, 58);
            this.zgcRPM.Name = "zgcRPM";
            this.zgcRPM.PanButtons = System.Windows.Forms.MouseButtons.None;
            this.zgcRPM.PanButtons2 = System.Windows.Forms.MouseButtons.Left;
            this.zgcRPM.ScrollGrace = 0D;
            this.zgcRPM.ScrollMaxX = 0D;
            this.zgcRPM.ScrollMaxY = 0D;
            this.zgcRPM.ScrollMaxY2 = 0D;
            this.zgcRPM.ScrollMinX = 0D;
            this.zgcRPM.ScrollMinY = 0D;
            this.zgcRPM.ScrollMinY2 = 0D;
            this.zgcRPM.Size = new System.Drawing.Size(700, 200);
            this.zgcRPM.TabIndex = 0;
            this.zgcRPM.ZoomButtons = System.Windows.Forms.MouseButtons.None;
            // 
            // zgcVoltage
            // 
            this.zgcVoltage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.zgcVoltage.Cursor = System.Windows.Forms.Cursors.Cross;
            this.zgcVoltage.IsPrintKeepAspectRatio = false;
            this.zgcVoltage.Location = new System.Drawing.Point(12, 263);
            this.zgcVoltage.Name = "zgcVoltage";
            this.zgcVoltage.PanButtons = System.Windows.Forms.MouseButtons.None;
            this.zgcVoltage.PanButtons2 = System.Windows.Forms.MouseButtons.Left;
            this.zgcVoltage.ScrollGrace = 0D;
            this.zgcVoltage.ScrollMaxX = 0D;
            this.zgcVoltage.ScrollMaxY = 0D;
            this.zgcVoltage.ScrollMaxY2 = 0D;
            this.zgcVoltage.ScrollMinX = 0D;
            this.zgcVoltage.ScrollMinY = 0D;
            this.zgcVoltage.ScrollMinY2 = 0D;
            this.zgcVoltage.Size = new System.Drawing.Size(700, 200);
            this.zgcVoltage.TabIndex = 1;
            this.zgcVoltage.ZoomButtons = System.Windows.Forms.MouseButtons.None;
            // 
            // zgcCurrent
            // 
            this.zgcCurrent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.zgcCurrent.Cursor = System.Windows.Forms.Cursors.Cross;
            this.zgcCurrent.IsPrintKeepAspectRatio = false;
            this.zgcCurrent.Location = new System.Drawing.Point(12, 468);
            this.zgcCurrent.Name = "zgcCurrent";
            this.zgcCurrent.PanButtons = System.Windows.Forms.MouseButtons.None;
            this.zgcCurrent.PanButtons2 = System.Windows.Forms.MouseButtons.Left;
            this.zgcCurrent.ScrollGrace = 0D;
            this.zgcCurrent.ScrollMaxX = 0D;
            this.zgcCurrent.ScrollMaxY = 0D;
            this.zgcCurrent.ScrollMaxY2 = 0D;
            this.zgcCurrent.ScrollMinX = 0D;
            this.zgcCurrent.ScrollMinY = 0D;
            this.zgcCurrent.ScrollMinY2 = 0D;
            this.zgcCurrent.Size = new System.Drawing.Size(700, 200);
            this.zgcCurrent.TabIndex = 2;
            this.zgcCurrent.ZoomButtons = System.Windows.Forms.MouseButtons.None;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.AutoSize = true;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(861, 477);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(135, 48);
            this.btnClear.TabIndex = 225;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // smartLabel34
            // 
            this.smartLabel34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel34.AutoSize = true;
            this.smartLabel34.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel34.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel34.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel34.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel34.BorderColor = System.Drawing.Color.Black;
            this.smartLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel34.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel34.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel34.Location = new System.Drawing.Point(75, 690);
            this.smartLabel34.Name = "smartLabel34";
            this.smartLabel34.Size = new System.Drawing.Size(127, 20);
            this.smartLabel34.TabIndex = 224;
            this.smartLabel34.Text = "Test Conditions: ";
            this.smartLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel34.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // cmbxCaption
            // 
            this.cmbxCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxCaption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxCaption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxCaption.FormattingEnabled = true;
            this.cmbxCaption.Items.AddRange(new object[] {
            "",
            "With No Electric Load",
            "With AC Load",
            "With Headlight Load",
            "With AC and Headlight Load",
            "With Engine Cranking Load"});
            this.cmbxCaption.Location = new System.Drawing.Point(204, 687);
            this.cmbxCaption.Name = "cmbxCaption";
            this.cmbxCaption.Size = new System.Drawing.Size(395, 28);
            this.cmbxCaption.TabIndex = 223;
            // 
            // btnMeasure
            // 
            this.btnMeasure.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnMeasure.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeasure.Location = new System.Drawing.Point(718, 421);
            this.btnMeasure.Name = "btnMeasure";
            this.btnMeasure.Size = new System.Drawing.Size(277, 48);
            this.btnMeasure.TabIndex = 222;
            this.btnMeasure.Text = "MEASURE";
            this.btnMeasure.UseVisualStyleBackColor = true;
            this.btnMeasure.Click += new System.EventHandler(this.btnMeasure_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(718, 621);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(278, 48);
            this.btnClose.TabIndex = 220;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCapture.AutoSize = true;
            this.btnCapture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapture.ForeColor = System.Drawing.Color.Blue;
            this.btnCapture.Location = new System.Drawing.Point(718, 562);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(278, 48);
            this.btnCapture.TabIndex = 221;
            this.btnCapture.Text = "CAPTURE";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReset.AutoSize = true;
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(718, 477);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(135, 48);
            this.btnReset.TabIndex = 227;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel1.ForeShadowAmount = 1;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.ForeShadowOffset = 1;
            this.smartLabel1.Location = new System.Drawing.Point(778, 223);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(49, 37);
            this.smartLabel1.TabIndex = 234;
            this.smartLabel1.Text = "C:";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel1.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel40
            // 
            this.smartLabel40.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel40.AutoSize = true;
            this.smartLabel40.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel40.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel40.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel40.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel40.BorderColor = System.Drawing.Color.Black;
            this.smartLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel40.ForeColor = System.Drawing.Color.Black;
            this.smartLabel40.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel40.ForeShadowAmount = 1;
            this.smartLabel40.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel40.ForeShadowOffset = 1;
            this.smartLabel40.Location = new System.Drawing.Point(779, 148);
            this.smartLabel40.Name = "smartLabel40";
            this.smartLabel40.Size = new System.Drawing.Size(48, 37);
            this.smartLabel40.TabIndex = 236;
            this.smartLabel40.Text = "V:";
            this.smartLabel40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel40.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblVoltage
            // 
            this.lblVoltage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblVoltage.BackColor = System.Drawing.Color.Transparent;
            this.lblVoltage.BackGradientAngle = 150;
            this.lblVoltage.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblVoltage.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblVoltage.BackShadowColor = System.Drawing.Color.Black;
            this.lblVoltage.BorderColor = System.Drawing.Color.Black;
            this.lblVoltage.BorderCornerRadius = 10;
            this.lblVoltage.BorderWidth = 1;
            this.lblVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblVoltage.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblVoltage.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblVoltage.ForeShadowAmount = 3;
            this.lblVoltage.ForeShadowColor = System.Drawing.Color.Black;
            this.lblVoltage.ForeShadowOffset = 1;
            this.lblVoltage.Location = new System.Drawing.Point(833, 131);
            this.lblVoltage.Name = "lblVoltage";
            this.lblVoltage.Size = new System.Drawing.Size(163, 68);
            this.lblVoltage.TabIndex = 235;
            this.lblVoltage.Text = "0.0";
            this.lblVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblVoltage.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblCurrent
            // 
            this.lblCurrent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCurrent.BackColor = System.Drawing.Color.Transparent;
            this.lblCurrent.BackGradientAngle = 150;
            this.lblCurrent.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblCurrent.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblCurrent.BackShadowColor = System.Drawing.Color.Black;
            this.lblCurrent.BorderColor = System.Drawing.Color.Black;
            this.lblCurrent.BorderCornerRadius = 10;
            this.lblCurrent.BorderWidth = 1;
            this.lblCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblCurrent.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblCurrent.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblCurrent.ForeShadowAmount = 3;
            this.lblCurrent.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCurrent.ForeShadowOffset = 1;
            this.lblCurrent.Location = new System.Drawing.Point(833, 206);
            this.lblCurrent.Name = "lblCurrent";
            this.lblCurrent.Size = new System.Drawing.Size(163, 68);
            this.lblCurrent.TabIndex = 233;
            this.lblCurrent.Text = "0";
            this.lblCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCurrent.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel22
            // 
            this.smartLabel22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel22.AutoSize = true;
            this.smartLabel22.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel22.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel22.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel22.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel22.BorderColor = System.Drawing.Color.Black;
            this.smartLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel22.ForeColor = System.Drawing.Color.Black;
            this.smartLabel22.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel22.ForeShadowAmount = 1;
            this.smartLabel22.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel22.ForeShadowOffset = 1;
            this.smartLabel22.Location = new System.Drawing.Point(1478, 692);
            this.smartLabel22.Name = "smartLabel22";
            this.smartLabel22.Size = new System.Drawing.Size(63, 37);
            this.smartLabel22.TabIndex = 230;
            this.smartLabel22.Text = "HC";
            this.smartLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel22.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel12
            // 
            this.smartLabel12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel12.AutoSize = true;
            this.smartLabel12.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel12.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel12.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel12.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel12.BorderColor = System.Drawing.Color.Black;
            this.smartLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel12.ForeColor = System.Drawing.Color.Black;
            this.smartLabel12.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel12.ForeShadowAmount = 1;
            this.smartLabel12.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel12.ForeShadowOffset = 1;
            this.smartLabel12.Location = new System.Drawing.Point(739, 298);
            this.smartLabel12.Name = "smartLabel12";
            this.smartLabel12.Size = new System.Drawing.Size(88, 37);
            this.smartLabel12.TabIndex = 232;
            this.smartLabel12.Text = "P. C:";
            this.smartLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel12.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblPeakCurrent
            // 
            this.lblPeakCurrent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPeakCurrent.BackColor = System.Drawing.Color.Transparent;
            this.lblPeakCurrent.BackGradientAngle = 150;
            this.lblPeakCurrent.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblPeakCurrent.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblPeakCurrent.BackShadowColor = System.Drawing.Color.Black;
            this.lblPeakCurrent.BorderColor = System.Drawing.Color.Black;
            this.lblPeakCurrent.BorderCornerRadius = 10;
            this.lblPeakCurrent.BorderWidth = 1;
            this.lblPeakCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblPeakCurrent.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblPeakCurrent.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblPeakCurrent.ForeShadowAmount = 3;
            this.lblPeakCurrent.ForeShadowColor = System.Drawing.Color.Black;
            this.lblPeakCurrent.ForeShadowOffset = 1;
            this.lblPeakCurrent.Location = new System.Drawing.Point(833, 281);
            this.lblPeakCurrent.Name = "lblPeakCurrent";
            this.lblPeakCurrent.Size = new System.Drawing.Size(163, 68);
            this.lblPeakCurrent.TabIndex = 231;
            this.lblPeakCurrent.Text = "0";
            this.lblPeakCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPeakCurrent.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.Color.Black;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel2.ForeShadowAmount = 1;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.ForeShadowOffset = 1;
            this.smartLabel2.Location = new System.Drawing.Point(732, 73);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(95, 37);
            this.smartLabel2.TabIndex = 238;
            this.smartLabel2.Text = "RPM:";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel2.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblSpeed
            // 
            this.lblSpeed.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSpeed.BackColor = System.Drawing.Color.Transparent;
            this.lblSpeed.BackGradientAngle = 150;
            this.lblSpeed.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblSpeed.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblSpeed.BackShadowColor = System.Drawing.Color.Black;
            this.lblSpeed.BorderColor = System.Drawing.Color.Black;
            this.lblSpeed.BorderCornerRadius = 10;
            this.lblSpeed.BorderWidth = 1;
            this.lblSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblSpeed.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblSpeed.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblSpeed.ForeShadowAmount = 3;
            this.lblSpeed.ForeShadowColor = System.Drawing.Color.Black;
            this.lblSpeed.ForeShadowOffset = 1;
            this.lblSpeed.Location = new System.Drawing.Point(833, 56);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(163, 68);
            this.lblSpeed.TabIndex = 237;
            this.lblSpeed.Text = "0";
            this.lblSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSpeed.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.AutoSize = true;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.5F);
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(177, 9);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(626, 40);
            this.smartLabel10.TabIndex = 239;
            this.smartLabel10.Text = "BATTERY  & ALTERNATOR TEST       ";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel10.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblCharging
            // 
            this.lblCharging.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCharging.AutoSize = true;
            this.lblCharging.BackColor = System.Drawing.Color.Transparent;
            this.lblCharging.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCharging.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCharging.BackShadowColor = System.Drawing.Color.Black;
            this.lblCharging.BorderColor = System.Drawing.Color.Black;
            this.lblCharging.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCharging.ForeColor = System.Drawing.Color.Red;
            this.lblCharging.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblCharging.ForeShadowAmount = 1;
            this.lblCharging.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCharging.ForeShadowOffset = 1;
            this.lblCharging.Location = new System.Drawing.Point(782, 366);
            this.lblCharging.Name = "lblCharging";
            this.lblCharging.Size = new System.Drawing.Size(214, 37);
            this.lblCharging.TabIndex = 240;
            this.lblCharging.Text = "DISCHARGE ";
            this.lblCharging.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCharging.TextBorderColor = System.Drawing.Color.Black;
            // 
            // tmrMeasure
            // 
            this.tmrMeasure.Interval = 500;
            this.tmrMeasure.Tick += new System.EventHandler(this.tmrMeasure_Tick);
            // 
            // lblUsbConnected
            // 
            this.lblUsbConnected.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUsbConnected.AutoSize = true;
            this.lblUsbConnected.BackColor = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.BorderColor = System.Drawing.Color.Black;
            this.lblUsbConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsbConnected.ForeColor = System.Drawing.Color.White;
            this.lblUsbConnected.ForeGradientColor2 = System.Drawing.Color.White;
            this.lblUsbConnected.ForeShadowAmount = 1;
            this.lblUsbConnected.ForeShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.ForeShadowOffset = 1;
            this.lblUsbConnected.Location = new System.Drawing.Point(856, 687);
            this.lblUsbConnected.Name = "lblUsbConnected";
            this.lblUsbConnected.Size = new System.Drawing.Size(131, 24);
            this.lblUsbConnected.TabIndex = 241;
            this.lblUsbConnected.Text = " Not Conected";
            this.lblUsbConnected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblUsbConnected.TextBorderColor = System.Drawing.Color.Black;
            // 
            // tmrGraphUpdater
            // 
            this.tmrGraphUpdater.Enabled = true;
            this.tmrGraphUpdater.Interval = 5;
            this.tmrGraphUpdater.Tick += new System.EventHandler(this.tmrGraphUpdater_Tick);
            // 
            // cbxCaptureGraph
            // 
            this.cbxCaptureGraph.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxCaptureGraph.AutoSize = true;
            this.cbxCaptureGraph.Checked = global::EngineAnalyzer.Properties.Settings.Default.@__CaptureGraph;
            this.cbxCaptureGraph.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "__CaptureGraph", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCaptureGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCaptureGraph.Location = new System.Drawing.Point(723, 532);
            this.cbxCaptureGraph.Name = "cbxCaptureGraph";
            this.cbxCaptureGraph.Size = new System.Drawing.Size(152, 28);
            this.cbxCaptureGraph.TabIndex = 242;
            this.cbxCaptureGraph.Text = "Capture Graph";
            this.cbxCaptureGraph.UseVisualStyleBackColor = true;
            // 
            // frmBatteryAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.cbxCaptureGraph);
            this.Controls.Add(this.lblUsbConnected);
            this.Controls.Add(this.lblCharging);
            this.Controls.Add(this.smartLabel10);
            this.Controls.Add(this.smartLabel2);
            this.Controls.Add(this.lblSpeed);
            this.Controls.Add(this.smartLabel1);
            this.Controls.Add(this.smartLabel40);
            this.Controls.Add(this.lblVoltage);
            this.Controls.Add(this.lblCurrent);
            this.Controls.Add(this.smartLabel22);
            this.Controls.Add(this.smartLabel12);
            this.Controls.Add(this.lblPeakCurrent);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.smartLabel34);
            this.Controls.Add(this.cmbxCaption);
            this.Controls.Add(this.btnMeasure);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.zgcCurrent);
            this.Controls.Add(this.zgcVoltage);
            this.Controls.Add(this.zgcRPM);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "frmBatteryAnalysis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ZedGraph.ZedGraphControl zgcRPM;
        private ZedGraph.ZedGraphControl zgcVoltage;
        private ZedGraph.ZedGraphControl zgcCurrent;
        private System.Windows.Forms.Button btnClear;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel34;
        private System.Windows.Forms.ComboBox cmbxCaption;
        private System.Windows.Forms.Button btnMeasure;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.Button btnReset;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel40;
        private InterfaceLab.WinForm.Controls.SmartLabel lblVoltage;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCurrent;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel22;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel12;
        private InterfaceLab.WinForm.Controls.SmartLabel lblPeakCurrent;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        private InterfaceLab.WinForm.Controls.SmartLabel lblSpeed;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCharging;
        private System.Windows.Forms.Timer tmrMeasure;
        private InterfaceLab.WinForm.Controls.SmartLabel lblUsbConnected;
        private System.Windows.Forms.Timer tmrGraphUpdater;
        private System.Windows.Forms.CheckBox cbxCaptureGraph;
    }
}