﻿namespace EngineAnalyzer
{
    partial class frmStartUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bgwStartUp = new System.ComponentModel.BackgroundWorker();
            this.owfProgressControl1 = new Owf.Controls.OwfProgressControl(this.components);
            this.SuspendLayout();
            // 
            // bgwStartUp
            // 
            this.bgwStartUp.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwStartUp_RunWorkerCompleted);
            // 
            // owfProgressControl1
            // 
            this.owfProgressControl1.AnimationSpeed = ((short)(75));
            this.owfProgressControl1.BackColor = System.Drawing.Color.Transparent;
            this.owfProgressControl1.CirclesColor = System.Drawing.Color.Lime;
            this.owfProgressControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.owfProgressControl1.Location = new System.Drawing.Point(134, 3);
            this.owfProgressControl1.Name = "owfProgressControl1";
            this.owfProgressControl1.Size = new System.Drawing.Size(319, 153);
            this.owfProgressControl1.TabIndex = 0;
            this.owfProgressControl1.Text = "owfProgressControl1";
            this.owfProgressControl1.TitileText = "  Please wait...";
            // 
            // frmStartUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(546, 159);
            this.ControlBox = false;
            this.Controls.Add(this.owfProgressControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(552, 183);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(552, 183);
            this.Name = "frmStartUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Loading....";
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker bgwStartUp;
        private Owf.Controls.OwfProgressControl owfProgressControl1;
    }
}