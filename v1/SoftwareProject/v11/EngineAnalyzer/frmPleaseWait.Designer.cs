﻿namespace EngineAnalyzer
{
    partial class frmPleaseWait
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPleaseWait));
            this.owfProgressControl1 = new Owf.Controls.OwfProgressControl(this.components);
            this.SuspendLayout();
            // 
            // owfProgressControl1
            // 
            this.owfProgressControl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.owfProgressControl1.AnimationSpeed = ((short)(75));
            this.owfProgressControl1.BackColor = System.Drawing.Color.Transparent;
            this.owfProgressControl1.CirclesColor = System.Drawing.Color.Cyan;
            this.owfProgressControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.owfProgressControl1.Location = new System.Drawing.Point(44, 5);
            this.owfProgressControl1.Name = "owfProgressControl1";
            this.owfProgressControl1.Size = new System.Drawing.Size(405, 199);
            this.owfProgressControl1.TabIndex = 0;
            this.owfProgressControl1.TitileText = "    Pleae wait for a while ....";
            // 
            // frmPleaseWait
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(478, 200);
            this.ControlBox = false;
            this.Controls.Add(this.owfProgressControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPleaseWait";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Please Wait......";
            this.ResumeLayout(false);

        }

        #endregion

        private Owf.Controls.OwfProgressControl owfProgressControl1;
    }
}