﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    public partial class frmEmissionMain : Form
    {
        public frmEmissionMain()
        {
            InitializeComponent();
            lblCompanyName.Text = Properties.Settings.Default.__CompanyName;
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRealTimeTest_Click(object sender, EventArgs e)
        {
            // show the form
            frmEmissionRealTimeTest emissionRT = new frmEmissionRealTimeTest();
            emissionRT.WindowState = FormWindowState.Maximized;
            emissionRT.ShowDialog();
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            new frmEmissionSettings().ShowDialog();
        }
    }
}
