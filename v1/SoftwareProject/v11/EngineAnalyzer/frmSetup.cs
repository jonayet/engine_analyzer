﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using InterfaceLab.WinForm.Tools;
using EngineAnalyzer.Properties;

namespace EngineAnalyzer
{
    public partial class frmSetup : Form
    {
        // our object for tracking the "dirty" status of the form
        private DirtyTracker _dirtyTracker;

        public frmSetup()
        {
            InitializeComponent();

            // add all avilable comports in the combo box
            string[] sPorts = SerialPort.GetPortNames();
            foreach (string sPort in sPorts)
            {
                cmbxEmissionComPort.Items.Add(sPort);
                cmbxOpacityComPort.Items.Add(sPort);
            }
        }

        private void frmSetup_Load(object sender, EventArgs e)
        {
            // load saved settings
            if (cmbxEmissionComPort.Items.Count > Settings.Default.EmissionComPortIndex)
            {
                cmbxEmissionComPort.SelectedIndex = Settings.Default.EmissionComPortIndex;
                Settings.Default.EmissionComPortName = (string)cmbxEmissionComPort.SelectedItem;
            }
            if (cmbxOpacityComPort.Items.Count > Settings.Default.OpacityComPortIndex)
            {
                cmbxOpacityComPort.SelectedIndex = Settings.Default.OpacityComPortIndex;
                Settings.Default.EmissionComPortName = (string)cmbxEmissionComPort.SelectedItem;
            }
            ckbxEmissionPublishLimits.Checked = Settings.Default.EmissionPublishLimits;
            ckbxOpacityPublishLimits.Checked = Settings.Default.OpacityPublishLimits;
            ckbxPublishFooter.Checked = Settings.Default.SetupPublishFooter;
            ckbxPublishHeader.Checked = Settings.Default.SetupPublishHeader;
            cmbxOpacityComPort.SelectedItem = Settings.Default.OpacityComPortName;
            rtbFooter.Rtf = Settings.Default.SetupFooter;
            rtbHeader.Rtf = Settings.Default.SetupHeader;

            // load Emission settings
            tbxEmissionCo2Max.Text = Settings.Default.EmissionCO2Max.ToString("0.00");
            tbxEmissionCo2Min.Text = Settings.Default.EmissionCO2Min.ToString("0.00");
            tbxEmissionCoMax.Text = Settings.Default.EmissionCOMax.ToString("0.00");
            tbxEmissionCoMin.Text = Settings.Default.EmissionCOMin.ToString("0.00");
            tbxEmissionHcMax.Text = Settings.Default.EmissionHCMax.ToString();
            tbxEmissionHcMin.Text = Settings.Default.EmissionHCMin.ToString();
            tbxEmissionNoMax.Text = Settings.Default.EmissionNOMax.ToString();
            tbxEmissionNoMin.Text = Settings.Default.EmissionNOMin.ToString();
            tbxEmissionO2Max.Text = Settings.Default.EmissionO2Max.ToString("0.00");
            tbxEmissionO2Min.Text = Settings.Default.EmissionO2Min.ToString("0.00");
            tbxEmissionSMax.Text = Settings.Default.EmissionSMax.ToString();
            tbxEmissionSMin.Text = Settings.Default.EmissionSMin.ToString();
            tbxEmissionTMax.Text = Settings.Default.EmissionTMax.ToString();
            tbxEmissionTMin.Text = Settings.Default.EmissionTMin.ToString();

            // load Opacity settings
            tbxOpacityTMax.Text = Settings.Default.OpacityTMax.ToString();
            tbxOpacityTMin.Text = Settings.Default.OpacityTMin.ToString();
            tbxOpacitySMax.Text = Settings.Default.OpacitySMax.ToString();
            tbxOpacitySMin.Text = Settings.Default.OpacitySMin.ToString();
            tbxOpacityKMax.Text = Settings.Default.OpacityKMax.ToString();
            tbxOpacityKMin.Text = Settings.Default.OpacityKMin.ToString();
            tbxOpacityNMax.Text = Settings.Default.OpacityNMax.ToString();
            tbxOpacityNMin.Text = Settings.Default.OpacityNMin.ToString();

            // load radio button state
            rbtNHA505.Checked = Settings.Default.__NHA505;
            rbtNHA505_Touch.Checked = Settings.Default.__NHA505_Touch;
            rbtEmissionRPM.Checked = Settings.Default.__RPM_Emission;
            rbtOpacityRPM.Checked = Settings.Default.__RPM_Opacity;

            // in the Load event initialize our tracking object
            _dirtyTracker = new DirtyTracker(this);
            _dirtyTracker.SetAsClean();
            _dirtyTracker.DirtyTracked += new EventHandler(_dirtyTracker_DirtyTracked);
            _dirtyTracker.TrackCleaned += new EventHandler(_dirtyTracker_TrackCleaned);
        }

        private void frmSetup_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_dirtyTracker.IsDirty)
            {
                DialogResult dRes = MessageBox.Show("Would you like to save changes before closing?", "Warning!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                // cancel the closing
                if (dRes == DialogResult.Cancel) { e.Cancel = true; return; }

                if (dRes == DialogResult.Yes)
                {
                    // save all the new values
                    btnSave_Click(this, EventArgs.Empty);
                }
            }
        }

        void _dirtyTracker_DirtyTracked(object sender, EventArgs e)
        {
            this.Text += "  * (unsaved)";
        }

        void _dirtyTracker_TrackCleaned(object sender, EventArgs e)
        {
            this.Text = this.Text.Replace("  * (unsaved)", "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                // save the new settings
                Settings.Default["EmissionPublishLimits"] = ckbxEmissionPublishLimits.Checked;
                Settings.Default["OpacityPublishLimits"] = ckbxOpacityPublishLimits.Checked;
                Settings.Default["SetupPublishFooter"] = ckbxPublishFooter.Checked;
                Settings.Default["SetupPublishHeader"] = ckbxPublishHeader.Checked;
                Settings.Default["EmissionComPortIndex"] = cmbxEmissionComPort.SelectedIndex;
                Settings.Default["OpacityComPortIndex"] = cmbxOpacityComPort.SelectedIndex;
                Settings.Default["EmissionComPortName"] = (string)cmbxEmissionComPort.SelectedItem;
                Settings.Default["OpacityComPortName"] = (string) cmbxOpacityComPort.SelectedItem;
                Settings.Default["SetupFooter"] = rtbFooter.Rtf;
                Settings.Default["SetupHeader"] = rtbHeader.Rtf;

                // save Emission settings
                Settings.Default["EmissionCO2Max"] = Convert.ToSingle(tbxEmissionCo2Max.Text);
                Settings.Default["EmissionCO2Min"] = Convert.ToSingle(tbxEmissionCo2Min.Text);
                Settings.Default["EmissionCOMax"] = Convert.ToSingle(tbxEmissionCoMax.Text);
                Settings.Default["EmissionCOMin"] = Convert.ToSingle(tbxEmissionCoMin.Text);
                Settings.Default["EmissionHCMax"] = Convert.ToInt32(tbxEmissionHcMax.Text);
                Settings.Default["EmissionHCMin"] = Convert.ToInt32(tbxEmissionHcMin.Text);
                Settings.Default["EmissionNOMax"] = Convert.ToInt32(tbxEmissionNoMax.Text);
                Settings.Default["EmissionNOMin"] = Convert.ToInt32(tbxEmissionNoMin.Text);
                Settings.Default["EmissionO2Max"] = Convert.ToSingle(tbxEmissionO2Max.Text);
                Settings.Default["EmissionO2Min"] = Convert.ToSingle(tbxEmissionO2Min.Text);
                Settings.Default["EmissionSMax"] = Convert.ToInt32(tbxEmissionSMax.Text);
                Settings.Default["EmissionSMin"] = Convert.ToInt32(tbxEmissionSMin.Text);
                Settings.Default["EmissionTMax"] = Convert.ToInt32(tbxEmissionTMax.Text);
                Settings.Default["EmissionTMin"] = Convert.ToInt32(tbxEmissionTMin.Text);

                // save Opacity settings
                Settings.Default["OpacityTMax"] = Convert.ToInt32(tbxOpacityTMax.Text);
                Settings.Default["OpacityTMin"] = Convert.ToInt32(tbxOpacityTMin.Text);
                Settings.Default["OpacitySMax"] = Convert.ToInt32(tbxOpacitySMax.Text);
                Settings.Default["OpacitySMin"] = Convert.ToInt32(tbxOpacitySMin.Text);
                Settings.Default["OpacityKMax"] = Convert.ToSingle(tbxOpacityKMax.Text);
                Settings.Default["OpacityKMin"] = Convert.ToSingle(tbxOpacityKMin.Text);
                Settings.Default["OpacityNMax"] = Convert.ToSingle(tbxOpacityNMax.Text);
                Settings.Default["OpacityNMin"] = Convert.ToSingle(tbxOpacityNMin.Text);

                // save radio button state
                Settings.Default["__NHA505"] = rbtNHA505.Checked;
                Settings.Default["__NHA505_Touch"] = rbtNHA505_Touch.Checked;
                Settings.Default["__RPM_Emission"] = rbtEmissionRPM.Checked;
                Settings.Default["__RPM_Opacity"] = rbtOpacityRPM.Checked;
                Settings.Default.Save();

                // update common resources
                CommonResources.EmissionPortName = Settings.Default.EmissionComPortName;
                CommonResources.OpacityPortName = Settings.Default.OpacityComPortName;

                // clear all tarcks
                _dirtyTracker.SetAsClean();
            }
            catch
            {
                MessageBox.Show("Number format error!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            // default Common settings
            ckbxEmissionPublishLimits.Checked = true;
            ckbxOpacityPublishLimits.Checked = true;
            ckbxPublishFooter.Checked = true;
            ckbxPublishHeader.Checked = true;
            rtbFooter.Rtf = "";
            rtbHeader.Rtf = "";

            // default Emission settings
            tbxEmissionCo2Max.Text = (2.0f).ToString();
            tbxEmissionCo2Min.Text = (0.0f).ToString();
            tbxEmissionCoMax.Text = (2.0f).ToString();
            tbxEmissionCoMin.Text = (0.0f).ToString();
            tbxEmissionHcMax.Text = (400).ToString();
            tbxEmissionHcMin.Text = (0).ToString();
            tbxEmissionNoMax.Text = (10).ToString();
            tbxEmissionNoMin.Text = (0).ToString();
            tbxEmissionO2Max.Text = (2.0f).ToString();
            tbxEmissionO2Min.Text = (0.0f).ToString();
            tbxEmissionSMax.Text = (800).ToString();
            tbxEmissionSMin.Text = (700).ToString();
            tbxEmissionTMax.Text = (95).ToString();
            tbxEmissionTMin.Text = (25).ToString();

            // default Opacity settings
            tbxOpacityTMax.Text = (95).ToString();
            tbxOpacityTMin.Text = (25).ToString();
            tbxOpacitySMax.Text = (800).ToString();
            tbxOpacitySMin.Text = (700).ToString();
            tbxOpacityKMax.Text = (10.0f).ToString();
            tbxOpacityKMin.Text = (0.0f).ToString();
            tbxOpacityNMax.Text = (40.0f).ToString();
            tbxOpacityNMin.Text = (0.0f).ToString();

            // default emission model
            rbtNHA505.Checked = true;
            rbtEmissionRPM.Checked = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            // close this form
            this.Close();
        }
    }
}
