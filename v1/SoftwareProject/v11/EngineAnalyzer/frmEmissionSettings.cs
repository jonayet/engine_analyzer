﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using EngineAnalyzer.Properties;

namespace EngineAnalyzer
{
    public partial class frmEmissionSettings : Form
    {
        private SerialComunication sCom = new SerialComunication(CommonResources.EmissionPortName, 9600);

        public frmEmissionSettings()
        {
            InitializeComponent();
        }

        protected override void  OnLoad(EventArgs e)
        {
            cmbxCycle.SelectedIndex = Settings.Default.EmissionSettingsCycleIndex;
            cmbxFuel.SelectedIndex = Settings.Default.EmissionSettingsFuelIndex;
            cmbxSparkCoil.SelectedIndex = Settings.Default.EmissionSettingsSparkCoilIndex;
            base.OnLoad(e);
            sCom.Open();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            sCom.Close();
            base.OnClosing(e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            bool success = true;
            byte Command = 0;

            // set Meas Mode
            if (cmbxCycle.SelectedIndex == 0) { Command = 0x04; } else { Command = 0x05; }
            success &= sCom.Send(Command, Command, 100);

            // set Fuel Type
            if (cmbxFuel.SelectedIndex == 0) { Command = 0x06; } else { Command = 0x07; }
            success &= sCom.Send(Command, Command, 100);

            // set SparkCoil
            if (cmbxSparkCoil.SelectedIndex == 0) { Command = 0x0A; } else { Command = 0x0B; }
            success &= sCom.Send(Command, Command, 100);

            if (success)
            {
                Settings.Default["EmissionSettingsCycleIndex"] = cmbxCycle.SelectedIndex;
                Settings.Default["EmissionSettingsFuelIndex"] = cmbxFuel.SelectedIndex;
                Settings.Default["EmissionSettingsSparkCoilIndex"] = cmbxSparkCoil.SelectedIndex;
                Settings.Default.Save();
                MessageBox.Show("Saved successfully!");
            }
            else
            {
                MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
