﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using InterfaceLab.WinForm.Tools;

namespace EngineAnalyzer
{
    public partial class frmCustomerDetails : Form
    {
        // our object for tracking the "dirty" status of the form
        private DirtyTracker _dirtyTracker;

        public frmCustomerDetails()
        {
            InitializeComponent();
        }

        private void frmCustomerDetails_Load(object sender, EventArgs e)
        {
            // load previous settings at startup
            tbxCustomerAddress.Text = Properties.Settings.Default.CustomerAddress;
            tbxCustomerEmail.Text = Properties.Settings.Default.CustomerEmail;
            tbxCustomerJobRef.Text = Properties.Settings.Default.CustomerJobRef;
            tbxCustomerName.Text = Properties.Settings.Default.CustomerName;
            tbxCustomerTelephone.Text = Properties.Settings.Default.CustomerTelephone;

            // in the Load event initialize our tracking object
            _dirtyTracker = new DirtyTracker(this);
            _dirtyTracker.SetAsClean();
            _dirtyTracker.DirtyTracked += new EventHandler(_dirtyTracker_DirtyTracked);
            _dirtyTracker.TrackCleaned += new EventHandler(_dirtyTracker_TrackCleaned);
        }

        private void frmCustomerDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_dirtyTracker.IsDirty)
            {
                DialogResult dRes = MessageBox.Show("Would you like to save changes before closing?", "Warning!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                // cancel the closing
                if (dRes == DialogResult.Cancel) { e.Cancel = true; return; }

                if (dRes == DialogResult.Yes)
                {
                    // save the new settings
                    btnSave_Click(this, EventArgs.Empty);
                }
            }
        }

        void _dirtyTracker_DirtyTracked(object sender, EventArgs e)
        {
            this.Text += "  * (unsaved)";
        }

        void _dirtyTracker_TrackCleaned(object sender, EventArgs e)
        {
            this.Text = this.Text.Replace("  * (unsaved)", "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // save the new settings
            Properties.Settings.Default["CustomerAddress"] = tbxCustomerAddress.Text;
            Properties.Settings.Default["CustomerEmail"] = tbxCustomerEmail.Text;
            Properties.Settings.Default["CustomerJobRef"] = tbxCustomerJobRef.Text;
            Properties.Settings.Default["CustomerName"] = tbxCustomerName.Text;
            Properties.Settings.Default["CustomerTelephone"] = tbxCustomerTelephone.Text;
            Properties.Settings.Default.Save();

            // clear all text values
            _dirtyTracker.SetAsClean();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            // clear all text values
            tbxCustomerAddress.Text = "";
            tbxCustomerEmail.Text = "";
            tbxCustomerJobRef.Text = "";
            tbxCustomerName.Text = "";
            tbxCustomerTelephone.Text = "";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            // close the form
            this.Close();
        }
    }
}
