﻿using System;
using System.Threading;
using EngineAnalyzer.Utilities;
using INFRA.USB;
using INFRA.USB.HidHelper;

// ReSharper disable InconsistentNaming
namespace EngineAnalyzer.LogicLayer
{
    public class HidBatteryAnalyzerManager
    {
        public event AnalogDataReceivedEventHandler OnAnalogDataReceived;
        public float RawAverageVoltageReading;
        public float VoltageConstant;
        public float VoltageOffset;
        public float MaximumVoltage;
        public float MinimumVoltage;
        public float AverageVoltage;
        public float RawAverageCurrentReading;
        public float CurrentConstant;
        public float CurrentOffset;
        public float MaximumCurrent;
        public float MinimumCurrent;
        public float AverageCurrent;
        public const int VOLTAGE_DATA_PER_PACKET = 12;
        public const int CURRENT_DATA_PER_PACKET = 12;

        private const int READ_INDEX_OF_VOLTAGE_DATA = 0;
        private const int SIZE_OF_VOLTAGE_DATA = 24;
        private const int READ_INDEX_OF_CURRENT_DATA = 24;
        private const int SIZE_OF_CURRENT_DATA = 24;
        private const int READ_INDEX_OF_VOLTAGE_CONSTANT = 48;
        private const int READ_INDEX_OF_VOLTAGE_OFFSET = 52;
        private const int READ_INDEX_OF_CURRENT_CONSTANT = 56;
        private const int READ_INDEX_OF_CURRENT_OFFSET = 60;

        private const int CMD_SET_CALIBRATION = 0xA3;
        private const int WRITE_INDEX_OF_CMD_SET_CALIBRATION = 0;
        private const int WRITE_INDEX_OF_CALIBRATION_MUSK = 1;
        private const int WRITE_INDEX_OF_VOLTAGE_CONSTANT = 2;
        private const int SIZE_OF_VOLTAGE_CONSTANT = 4;
        private const int WRITE_INDEX_OF_VOLTAGE_OFFSET = 6;
        private const int SIZE_OF_VOLTAGE_OFFSET = 4;
        private const int WRITE_INDEX_OF_CURRENT_CONSTANT = 10;
        private const int SIZE_OF_CURRENT_CONSTANT = 4;
        private const int WRITE_INDEX_OF_CURRENT_OFFSET = 14;
        private const int SIZE_OF_CURRENT_OFFSET = 4;
        private const int MUSK_VOLTAGE_CONSTANT = 0x01;
        private const int MUSK_VOLTAGE_OFFSET = 0x02;
        private const int MUSK_CURRENT_CONSTANT = 0x04;
        private const int MUSK_CURRENT_OFFSET = 0x08;

        private readonly HidDevice _hidDevice;
        private readonly int _voltageAvgSize;
        private readonly int _currentAvgSize;
        private readonly RingBuffer<int> _voltageBuffer;
        private readonly RingBuffer<int> _currentBuffer;
        private readonly int _requiredNoOfBufferPacketToFireEvent;
        private int _packetCounter;
        private Thread dataProviderThread;
        private readonly  object _lockObject = new object();

        public HidBatteryAnalyzerManager(HidDevice hidDevice, int noOfBufferPacket = 100, int voltageAvgSize = 10, int currentAvgSize = 10)
        {
            _hidDevice = hidDevice;
            _voltageAvgSize = voltageAvgSize;
            _currentAvgSize = currentAvgSize;
            _hidDevice.OnReportReceived += _hidDevice_OnReportReceived;

            _requiredNoOfBufferPacketToFireEvent = noOfBufferPacket;
            var sizeOfVoltageBuffer = noOfBufferPacket * VOLTAGE_DATA_PER_PACKET * 3;
            var sizeOfCurrentBuffer = noOfBufferPacket * CURRENT_DATA_PER_PACKET * 3;

            _voltageBuffer = new RingBuffer<int>(sizeOfVoltageBuffer);
            _currentBuffer = new RingBuffer<int>(sizeOfCurrentBuffer);

            dataProviderThread = new Thread(DataDequeueThread) { IsBackground = true };
            dataProviderThread.Start();
        }

        public void Close()
        {
            if (_hidDevice != null)
                _hidDevice.Disconnect();
        }

        public void CalibrateVoltage(float actualVoltage1, double deviceVoltageData1, float actualVoltage2, double deviceVoltageData2)
        {
            // slope = (y1-y2)/(x1-x2)
            // offset = y-(slope*x)
            // y = offset + slope*x
            float vConstant = (float)((actualVoltage1 - actualVoltage2) / (deviceVoltageData1 - deviceVoltageData2));
            WriteVoltageConstant(vConstant);
        }

        public void CalibrateCurrent(float actualCurrent1, double deviceCurrentData1, float actualCurrent2, double deviceCurrentData2)
        {
            // slope = (y1-y2)/(x1-x2)
            // offset = y-(slope*x)
            // y = offset + slope*x
            float cConstant = (float)((actualCurrent1 - actualCurrent2) / (deviceCurrentData1 - deviceCurrentData2));
            WriteCurrentConstant(cConstant);
        }

        public void WriteVoltageConstant(float constant)
        {
            var calibrationData = new byte[64];

            // set command
            calibrationData[WRITE_INDEX_OF_CMD_SET_CALIBRATION] = CMD_SET_CALIBRATION;

            // set calibration musk
            calibrationData[WRITE_INDEX_OF_CALIBRATION_MUSK] = MUSK_VOLTAGE_CONSTANT;

            // set voltage constatnt
            Array.Copy(BitConverter.GetBytes(constant), 0, calibrationData, WRITE_INDEX_OF_VOLTAGE_CONSTANT, SIZE_OF_VOLTAGE_CONSTANT);

            // write to the device
            _hidDevice.Write(calibrationData);
        }

        public void WriteVoltageOffset(float offset)
        {
            var calibrationData = new byte[64];

            // set command
            calibrationData[WRITE_INDEX_OF_CMD_SET_CALIBRATION] = CMD_SET_CALIBRATION;

            // set calibration musk
            calibrationData[WRITE_INDEX_OF_CALIBRATION_MUSK] = MUSK_VOLTAGE_OFFSET;

            // set voltage offset
            Array.Copy(BitConverter.GetBytes(offset), 0, calibrationData, WRITE_INDEX_OF_VOLTAGE_OFFSET, SIZE_OF_VOLTAGE_OFFSET);

            // write to the device
            _hidDevice.Write(calibrationData);
        }

        public void WriteCurrentConstant(float constant)
        {
            var calibrationData = new byte[64];

            // set command
            calibrationData[WRITE_INDEX_OF_CMD_SET_CALIBRATION] = CMD_SET_CALIBRATION;
            calibrationData[WRITE_INDEX_OF_CALIBRATION_MUSK] = MUSK_CURRENT_CONSTANT;

            // set current constatnt
            Array.Copy(BitConverter.GetBytes(constant), 0, calibrationData, WRITE_INDEX_OF_CURRENT_CONSTANT, SIZE_OF_CURRENT_CONSTANT);

            // write to the device
            _hidDevice.Write(calibrationData);
        }

        public void WriteCurrentOffset(float offset)
        {
            var calibrationData = new byte[64];

            // set command
            calibrationData[WRITE_INDEX_OF_CMD_SET_CALIBRATION] = CMD_SET_CALIBRATION;
            calibrationData[WRITE_INDEX_OF_CALIBRATION_MUSK] = MUSK_CURRENT_OFFSET;

            // set current offset
            Array.Copy(BitConverter.GetBytes(offset), 0, calibrationData, WRITE_INDEX_OF_CURRENT_OFFSET, SIZE_OF_CURRENT_OFFSET);

            // write to the device
            _hidDevice.Write(calibrationData);
        }

        private void _hidDevice_OnReportReceived(object sender, ReportRecievedEventArgs e)
        {
            // read calibration data
            ExtractCalibrationData(e.Report.UserData, out VoltageConstant, out VoltageOffset, out CurrentConstant, out CurrentOffset);

            // get 12 voltage reading samples
            int[] voltageRawData = GetIntArray(e.Report.UserData, READ_INDEX_OF_VOLTAGE_DATA, SIZE_OF_VOLTAGE_DATA);

            // get 12 current reading sample
            int[] currentRawData = GetIntArray(e.Report.UserData, READ_INDEX_OF_CURRENT_DATA, SIZE_OF_CURRENT_DATA);

            // set data to buffer
            _voltageBuffer.PutBlocking(voltageRawData, 0, voltageRawData.Length);
            _currentBuffer.PutBlocking(currentRawData, 0, currentRawData.Length);

            lock (_lockObject) { _packetCounter++; }

            /*
            if (_packetCounter < _requiredNoOfBufferPacketToFireEvent) { return; }
            _packetCounter = 0;
            BuildAnalogDataFireEvent();
            */
        }

        private void DataDequeueThread()
        {
            while (true)
            {
                if (_packetCounter < _requiredNoOfBufferPacketToFireEvent) { continue; }
                lock (_lockObject) { _packetCounter = 0; }
                BuildAnalogDataFireEvent();
            }
        }

        private void BuildAnalogDataFireEvent()
        {
            if (OnAnalogDataReceived == null) { return; }

            // read all avilable data
            int[] voltageRawData = new int[_requiredNoOfBufferPacketToFireEvent * VOLTAGE_DATA_PER_PACKET];
            int[] currentRawData = new int[_requiredNoOfBufferPacketToFireEvent * CURRENT_DATA_PER_PACKET];
            _voltageBuffer.GetBlocking(voltageRawData, voltageRawData.Length);
            _currentBuffer.GetBlocking(currentRawData, currentRawData.Length);

            // convert to meaningful data
            float[] voltages, currents;
            ProcessAndBuildAnalogData(voltageRawData, ref RawAverageVoltageReading, VoltageOffset, VoltageConstant, out voltages, out MaximumVoltage, out MinimumVoltage, out AverageVoltage);
            ProcessAndBuildAnalogData(currentRawData, ref RawAverageCurrentReading, CurrentOffset, CurrentConstant, out currents, out MaximumCurrent, out MinimumCurrent, out AverageCurrent);

            // get average values
            float[] avgVoltages;
            float[] avgCurrents;
            GetAvgValues(voltages, _voltageAvgSize, out avgVoltages);
            GetAvgValues(currents, _currentAvgSize, out avgCurrents);

            var analogData = new AnalogDataReceivedEventArgs
            {
                Voltages = avgVoltages,
                MaximumVoltage = this.MaximumVoltage,
                MinimumVoltage = this.MinimumVoltage,
                AverageVoltage = this.AverageVoltage,
                Currents = avgCurrents,
                MaximumCurrent = this.MaximumCurrent,
                MinimumCurrent = this.MinimumCurrent,
                AverageCurrent = this.AverageCurrent,
            };

            OnAnalogDataReceived(this, analogData);
        }

        private void GetAvgValues(float[] data, int avgSize, out float[] avgData)
        {
            if (avgSize < 2)
            {
                avgData = data;
                return;
            }

            avgData = new float[data.Length / avgSize];
            float sum = 0;
            int index = 0;
            for (int i = 0; i < data.Length; i++)
            {
                sum += data[i];
                if ((i + 1) % avgSize != 0) continue;
                var avg = sum / avgSize;
                avgData[index] = avg;
                index++;
                sum = 0;
            }
        }

        private void ProcessAndBuildAnalogData(int[] packetData, ref float rawAverageReading, float offset, float constant, out float[] analogData, out float max, out float min, out float average)
        {
            analogData = new float[packetData.Length];
            int sumOfRawData = 0;
            float maxValue = float.MinValue;
            float minValue = float.MaxValue;
            float sumOfValues = 0;
            for (int i = 0; i < packetData.Length; i++)
            {
                int reading = packetData[i];

                if (rawAverageReading != 0 && Math.Abs(reading) > Math.Abs(rawAverageReading) + 3)
                {
                    reading = (reading + (int)Math.Round(rawAverageReading)) / 2;
                }

                sumOfRawData += reading;
                float offsetClippedValue = reading + offset;
                float value = offsetClippedValue * constant;
                analogData[i] = value;

                if (value > maxValue) { maxValue = value; }
                if (value < minValue) { minValue = value; }
                sumOfValues += value;
            }

            max = maxValue;
            min = minValue;
            average = sumOfValues/packetData.Length;
            rawAverageReading = (float)sumOfRawData / packetData.Length;
        }

        private void ExtractCalibrationData(byte[] rawData, out float voltageConstant, out float voltageOffset, out float currentConstant, out float currentOffset)
        {
            // get voltage constant & offfset
            voltageConstant = BitConverter.ToSingle(rawData, READ_INDEX_OF_VOLTAGE_CONSTANT);
            voltageOffset = BitConverter.ToSingle(rawData, READ_INDEX_OF_VOLTAGE_OFFSET);

            // get current constant & offfset
            currentConstant = BitConverter.ToSingle(rawData, READ_INDEX_OF_CURRENT_CONSTANT);
            currentOffset = BitConverter.ToSingle(rawData, READ_INDEX_OF_CURRENT_OFFSET);
        }

        private int[] GetIntArray(byte[] bytes, int startIndex, int length)
        {
            var result = new int[length / 2];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = BitConverter.ToInt16(bytes, startIndex + (i * 2));
            }
            return result;
        }
    }

    public delegate void AnalogDataReceivedEventHandler(object sender, AnalogDataReceivedEventArgs e);
    public class AnalogDataReceivedEventArgs : EventArgs
    {
        public float[] Voltages;
        public float MaximumVoltage, MinimumVoltage, AverageVoltage;
        public float[] Currents;
        public float MaximumCurrent, MinimumCurrent, AverageCurrent;

        public AnalogDataReceivedEventArgs()
        {
            Voltages = new float[0];
            Currents = new float[0];
        }
    }
}