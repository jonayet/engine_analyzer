﻿using System;
using System.ComponentModel;
using System.Threading;
using EngineAnalyzer.Utilities;

// ReSharper disable CSharpWarnings::CS1591
// ReSharper disable SuggestUseVarKeywordEvident
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local
// ReSharper disable RedundantExplicitArrayCreation
// ReSharper disable RedundantDefaultFieldInitializer
// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable RedundantDelegateCreation
// ReSharper disable LocalizableElement

namespace EngineAnalyzer.LogicLayer
{
    public delegate void OpacityRealtimeTestAbortedEventHandler(object sender, OpacityRealtimeTestAbortedEventArgs e);
    public delegate void OpacityRealtimeTestDataReceivedEventHandler(object sender, OpacityRealtimeTestDataEventArgs e);
    public delegate void OpacityAcclerationTestStateChangedEventHandler(object sender, OpacityAcclerationTestStateChangedEventArgs e);
    public delegate void OpacityAcclerationTestCompletedEventHandler(object sender, OpacityAcclerationTestCompletedEventArgs e);
    public delegate void OpacityAcclerationTestAbortedEventHandler(object sender, OpacityAcclerationTestAbortedEventArgs e);
        
    class OpacityAnalyzerManager
    {
        private readonly SerialComunication _serCom;
        private readonly HidCommunication _hidCom;
        private int _asynReadInterval;
        private bool _isPortOpen;
        private readonly object _lockObject = new object();
        private BackgroundWorker _bgWorker;
        private int _numberOfAccelerationTestTest;

        private const int SELECT_TEST_MODE = 0xA0;
        private const int GET_CURRENT_TEST_MODE = 0xA1;
        private const int FORCE_EXIT_WARM_UP = 0xA2;
        private const int GET_ALARM_INFO = 0xA3;
        private const int START_CALIBRATION = 0xA4;
        private const int GET_REALTIME_DATA = 0xA5;
        private const int GET_REALTIME_MAX_DATA = 0xA6;
        private const int CLEAR_MAX_REALTIME_TEST_DATA = 0xA7;
        private const int START_ACCLERATION_TEST = 0xA8;
        private const int GET_ACCLERATION_TEST_STATUS = 0xA9;
        private const int CONFIRM_PROBE_INSERTION = 0xAA;
        private const int STOP_ACCLERATION_TEST = 0xAB;
        private const int GET_ACCLERATION_TEST_DATA = 0xAC;

        private const int MODE_CODE_WARMING_UP = 0x00;
        private const int MODE_CODE_REALTIME_TEST = 0x01;
        private const int MODE_CODE_ACCLERATION_TEST = 0x02;
        private const int MODE_CODE_INVALID_COMMAND = 0x15;
        private const int MODE_CODE_OTHER_OPERATION = 0xFF;

        private const int STATUS_CODE_READY_FOR_CALIBRATION = 0x01;
        private const int STATUS_CODE_BEING_CALIBRATED = 0x02;
        private const int STATUS_CODE_CALIBRATION_COMPLETE = 0x03;
        private const int STATUS_CODE_SAMPLING_DATA = 0x04;
        private const int STATUS_CODE_PEAK_SAMPLING_COMPLETED = 0x05;
        private const int STATUS_CODE_ACCELERATION_TEST_COMPLETED = 0x06;
        private const int STATUS_CODE_ACCELERATION_TEST_INVALID = 0x07;
        private const int STATUS_CODE_ACCELERATION_TEST_FAILED = 0x08;
        private const int STATUS_CODE_INVALID = 0xEE;

        private const int GENERIC_RESP_LENGTH = 2;
        private const int CURRENT_TEST_MODE_RESP_LENGTH = 3;
        private const int ALARM_INFO_RESP_LENGTH = 4;
        private const int ACCELERATION_TEST_STATUS_RESP_LENGTH = 3;
        private const int REALTIME_TEST_DATA_LENGTH = 8;
        private const int ACCELERATION_TEST_DATA_LENGTH = 12;

        private const int NO_OF_TEST = 4;
        private const int GENERIC_RESP_TIMEOUT = 100;

        public event OpacityRealtimeTestDataReceivedEventHandler RealtimeTestDataReceived;
        public event OpacityRealtimeTestAbortedEventHandler RealtimeTestAborted;
        public event OpacityAcclerationTestStateChangedEventHandler AccelerationTestStateChanged;
        public event OpacityAcclerationTestCompletedEventHandler AccelerationTestCompleted;
        public event OpacityAcclerationTestAbortedEventHandler AccelerationTestAborted;

        public OpacityAnalyzerManager(HidCommunication hidCom)
        {
            _hidCom = hidCom;
            _isPortOpen = false;
        }

        public OpacityAnalyzerManager(SerialComunication serCom)
        {
            _serCom = serCom;
            _isPortOpen = false;
        }

        public bool Open()
        {
            if (_hidCom != null)
            {
                _hidCom.Open();
                _isPortOpen = true;
                return true;
            }
            
            if(_serCom != null)
            {
                _isPortOpen = _serCom.Open();
                return _isPortOpen;
            }
            return false;
        }

        public void Close()
        {
            if (_hidCom != null)
            {
                _hidCom.Close();
            }

            if (_serCom != null)
            {
                _serCom.Close();
            }
            _isPortOpen = false;
        }

        public bool GetAlarmInformation(out OpacityAlarmInfo alarmInfo)
        {
            alarmInfo = new OpacityAlarmInfo();
            if (_isPortOpen == false) { return false; }
            byte[] response = new byte[ALARM_INFO_RESP_LENGTH];
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(GET_ALARM_INFO), ref response, ALARM_INFO_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(GET_ALARM_INFO), response, ALARM_INFO_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
            }

            // get the alarm flag
            byte AlarmFlag_Hi = response[1];
            byte AlarmFlag_Lo = response[2];

            // Full Light Intensity
            if ((AlarmFlag_Hi & 0x02) != 0)
            {
                alarmInfo.IsFullLightIntensityOutOfRange = true;
            }

            // Ambient Light Intensity
            if ((AlarmFlag_Hi & 0x04) != 0)
            {
                alarmInfo.IsAmbientLightIntensityOutOfRange = true;
            }

            // EEPROM
            if ((AlarmFlag_Hi & 0x80) != 0)
            {
                alarmInfo.IsEepromError = true;
            }

            // Board Temp
            if ((AlarmFlag_Lo & 0x01) != 0)
            {
                alarmInfo.IsBroadTemperatureOutOfRange = true;
            }

            // Detector Temp
            if ((AlarmFlag_Lo & 0x02) != 0)
            {
                alarmInfo.IsDetectorTemperatureOutOfRange = true;
            }

            // Tube Temp
            if ((AlarmFlag_Lo & 0x04) != 0)
            {
                alarmInfo.IsTubeTemperatureOutOfRange = true;
            }

            // Power Voltage
            if ((AlarmFlag_Lo & 0x08) != 0)
            {
                alarmInfo.IsPowerVoltageOutOfRange = true;
            }

            // LED Temp
            if ((AlarmFlag_Lo & 0x10) != 0)
            {
                alarmInfo.IsLedTemperatureOutOfRange = true;
            }

            // Opacity
            if ((AlarmFlag_Lo & 0x20) != 0)
            {
                alarmInfo.IsOpacityOutOfRange = true;
            }

            // Fan Over Current
            if ((AlarmFlag_Lo & 0x40) != 0)
            {
                alarmInfo.IsFanCurrentOutOfRange = true;
            }

            // Fan Current Imbalance
            if ((AlarmFlag_Lo & 0x80) != 0)
            {
                alarmInfo.IsFanCurrentImbalance = true;
            }
            return isSuccess;
        }

        public bool IsDeviceWarmingUp(out string errorMsg)
        {
            if (_isPortOpen == false) { errorMsg = "port_open"; return false; }
            byte[] response = new byte[CURRENT_TEST_MODE_RESP_LENGTH];
            bool IsSendSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    IsSendSuccess = _hidCom.Send(GetDataPacket(GET_CURRENT_TEST_MODE), ref response,
                        CURRENT_TEST_MODE_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    IsSendSuccess = _serCom.Send(GetDataPacket(GET_CURRENT_TEST_MODE), response,
                        CURRENT_TEST_MODE_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }

            if (!IsSendSuccess) { errorMsg = "no_response"; return false; }
            if (response[1] == MODE_CODE_WARMING_UP) { errorMsg = "warming_up"; return true; }
            if (response[1] == MODE_CODE_OTHER_OPERATION) { errorMsg = "other_operation"; return false; }
            errorMsg = "normal";
            return false;
        }

        public bool DoForceExitWarmingUp()
        {
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            if (_isPortOpen == false) { return false; }
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(FORCE_EXIT_WARM_UP), ref response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(FORCE_EXIT_WARM_UP), response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public bool CalibrateDevice()
        {
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            if (_isPortOpen == false) { return false; }
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(START_CALIBRATION), ref response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(START_CALIBRATION), response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public bool StartRealtimeTest()
        {
            if (_isPortOpen == false) { return false; }
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(SELECT_TEST_MODE, MODE_CODE_REALTIME_TEST), ref response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(SELECT_TEST_MODE, MODE_CODE_REALTIME_TEST), response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public bool GetRealtimeData(out byte[] realTimeData)
        {
            realTimeData = new byte[REALTIME_TEST_DATA_LENGTH];
            if (_isPortOpen == false) { return false; }
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(GET_REALTIME_DATA), ref realTimeData,
                        REALTIME_TEST_DATA_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(GET_REALTIME_DATA), realTimeData, REALTIME_TEST_DATA_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public bool GetRealtimeMaxData(out byte[] realTimeMaxData)
        {
            realTimeMaxData = new byte[REALTIME_TEST_DATA_LENGTH];
            if (_isPortOpen == false) { return false; }
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(GET_REALTIME_MAX_DATA), ref realTimeMaxData,
                        REALTIME_TEST_DATA_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(GET_REALTIME_MAX_DATA), realTimeMaxData,
                        REALTIME_TEST_DATA_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public bool ClearRealtimeMaxData()
        {
            if (_isPortOpen == false) { return false; }
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(CLEAR_MAX_REALTIME_TEST_DATA), ref response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(CLEAR_MAX_REALTIME_TEST_DATA), response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        private bool StartAccelerationTest()
        {
            if (_isPortOpen == false) { return false; }
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            bool isSuccess = false;
            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(SELECT_TEST_MODE, MODE_CODE_ACCLERATION_TEST), ref response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(SELECT_TEST_MODE, MODE_CODE_ACCLERATION_TEST), response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        private bool StopAccelerationTest()
        {
            if (_isPortOpen == false) { return false; }
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(STOP_ACCLERATION_TEST), ref response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(STOP_ACCLERATION_TEST), response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public int GetAcclerationTestStatus()
        {
            if (_isPortOpen == false) { return 0xEE; }
            byte[] response = new byte[ACCELERATION_TEST_DATA_LENGTH];
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(GET_ACCLERATION_TEST_STATUS), ref response,
                        ACCELERATION_TEST_DATA_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(GET_ACCLERATION_TEST_STATUS), response,
                        ACCELERATION_TEST_DATA_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }
            return !isSuccess ? 0xEE : response[1];
        }

        public bool GetAcclerationTestData(out byte[] accelerationTestData)
        {
            accelerationTestData = new byte[ACCELERATION_TEST_DATA_LENGTH];
            if (_isPortOpen == false) { return false; }
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(GET_ACCLERATION_TEST_DATA), ref accelerationTestData,
                        ACCELERATION_TEST_DATA_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(GET_ACCLERATION_TEST_DATA), accelerationTestData,
                        ACCELERATION_TEST_DATA_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public bool SetNumberOfAcclerationTest(int numberOfTest)
        {
            if (_isPortOpen == false) { return false; }
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(START_ACCLERATION_TEST, (byte) numberOfTest), ref response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(START_ACCLERATION_TEST, (byte) numberOfTest), response,
                        GENERIC_RESP_LENGTH, GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public bool ConfirmProbeInsertion()
        {
            if (_isPortOpen == false) { return false; }
            byte[] response = new byte[GENERIC_RESP_LENGTH];
            bool isSuccess = false;

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    isSuccess = _hidCom.Send(GetDataPacket(CONFIRM_PROBE_INSERTION), ref response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
                else if (_serCom != null)
                {
                    isSuccess = _serCom.Send(GetDataPacket(CONFIRM_PROBE_INSERTION), response, GENERIC_RESP_LENGTH,
                        GENERIC_RESP_TIMEOUT);
                }
            }
            return isSuccess;
        }

        public string ReadAsync_RealtimeTestData(int readInterval)
        {
            if (_isPortOpen == false) { return "port_closed"; }
            _asynReadInterval = readInterval;
            _bgWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _bgWorker.DoWork += _bgWorker_DoWork_RealtimeTest;
            _bgWorker.RunWorkerAsync();
            return "";
        }

        public void StopAsync_RealtimeTestData()
        {
            if (_bgWorker.IsBusy)
            {
                _bgWorker.CancelAsync();
            }
        }

        public string StartAsync_AcclerationTest()
        {
            if (_isPortOpen == false) { return "port_closed"; }
            _bgWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _bgWorker.DoWork += _bgWorker_DoWork_AcclerationTest;
            _bgWorker.RunWorkerAsync();
            return "";
        }

        public void StopAsync_AcclerationTest()
        {
            if (_bgWorker.IsBusy)
            {
                _bgWorker.CancelAsync();
            }
        }

        void _bgWorker_DoWork_RealtimeTest(object sender, DoWorkEventArgs e)
        {
            //// check warming up condition, A1H command
            //string error;
            //if (IsDeviceWarmingUp(out error))
            //{
            //    if (RealtimeTestAborted != null)
            //    {
            //        RealtimeTestAborted(this, new OpacityRealtimeTestAbortedEventArgs("Device is under Warming up condition."));
            //    }
            //    return;
            //}

            //StartRealtimeTest();
            //Thread.Sleep(2000);

            while (true)
            {
                byte[] data;
                byte[] maxData;
                bool IsSuccess = GetRealtimeData(out data);
                IsSuccess &= GetRealtimeMaxData(out maxData);
                data[0] = 1;
                data[1] = 1;
                data[2] = 1;
                IsSuccess = true;
                if (IsSuccess && RealtimeTestDataReceived != null)
                {
                    OpacityRealtimeTestDataEventArgs args;
                    BuildRealtimeDataFromResponse(data, maxData, out args);
                    RealtimeTestDataReceived(this, args);
                }

                Thread.Sleep(_asynReadInterval);
                if (_bgWorker == null) { return; }
                if (_bgWorker.CancellationPending)
                {
                    if (RealtimeTestAborted != null)
                    {
                        RealtimeTestAborted(this, new OpacityRealtimeTestAbortedEventArgs("Realtime test aborted."));
                    }
                    e.Cancel = true;
                    _bgWorker.Dispose();
                    _bgWorker = null;
                    return;
                }
            }
        }

        void _bgWorker_DoWork_AcclerationTest(object sender, DoWorkEventArgs e)
        {
            int statusCode;

            // check warming up condition, A1H command
            string error;
            if (IsDeviceWarmingUp(out error))
            {
                if (AccelerationTestAborted != null)
                {
                    AccelerationTestAborted(this, new OpacityAcclerationTestAbortedEventArgs("Device is under Warming up condition."));
                }
                return;
            }
            
            StartAccelerationTest();
            _numberOfAccelerationTestTest = 0;

            // wait 2 second
            if (AccelerationTestStateChanged != null)
            {
                AccelerationTestStateChanged(this, new OpacityAcclerationTestStateChangedEventArgs("Accleration test started. Please wait....")); 
            }
            Thread.Sleep(2000);

            while (true)
            {
                // get Status code, A9H command
                statusCode = GetAcclerationTestStatus();

                // check Status Code
                if (statusCode == STATUS_CODE_ACCELERATION_TEST_COMPLETED || statusCode == STATUS_CODE_ACCELERATION_TEST_INVALID || statusCode == STATUS_CODE_ACCELERATION_TEST_FAILED) { break; }

                // send Stop test, ABH command
                StopAccelerationTest();

                // wait 1 second
                if (AccelerationTestStateChanged != null)
                {
                    AccelerationTestStateChanged(this, new OpacityAcclerationTestStateChangedEventArgs("Waiting for device response. Please wait...."));
                }
                Thread.Sleep(1000);

                // is cancel process?
                if (_bgWorker == null || _bgWorker.CancellationPending)
                {
                    e.Cancel = true;
                    if (_bgWorker != null) { _bgWorker.Dispose(); }
                    _bgWorker = null;
                    return;
                }
            }

            // set number of test, A8H command
            SetNumberOfAcclerationTest(NO_OF_TEST);
            Thread.Sleep(100);

            int LastStatusCode = 0;
            while (true)
            {
                // get Status code, A9H command
                statusCode = GetAcclerationTestStatus();

                if (statusCode != LastStatusCode)
                {
                    byte[] AccelerationData;
                    switch (statusCode)
                    {
                        case 0x01:
                            // prompt to take out the probe
                            if (AccelerationTestStateChanged != null)
                            {
                                AccelerationTestStateChanged(this, new OpacityAcclerationTestStateChangedEventArgs("Please take out the Probe."));
                            }
                            Thread.Sleep(5000);
                            break;

                        case 0x02:
                            // show calibration message
                            if (AccelerationTestStateChanged != null)
                            {
                                AccelerationTestStateChanged(this, new OpacityAcclerationTestStateChangedEventArgs("Calibrating...."));
                            }
                            Thread.Sleep(3000);
                            break;

                        case 0x03:
                            // prompt to insert the probe
                            if (AccelerationTestStateChanged != null)
                            {
                                AccelerationTestStateChanged(this, new OpacityAcclerationTestStateChangedEventArgs("Please insert the Probe, keep engine into Idle speed."));
                            }
                            Thread.Sleep(5000);

                            // confirm probe insertion, AAH command
                            ConfirmProbeInsertion();
                            break;

                        case 0x04:
                            _numberOfAccelerationTestTest++;
                            // prompt to accelerate
                            if (AccelerationTestStateChanged != null)
                            {
                                AccelerationTestStateChanged(this, new OpacityAcclerationTestStateChangedEventArgs("Please accelerate."));
                            }
                            Thread.Sleep(5000);
                            break;

                        case 0x05:
                            // prompt to deccelerate
                            if (AccelerationTestStateChanged != null)
                            {
                                AccelerationTestStateChanged(this, new OpacityAcclerationTestStateChangedEventArgs("Please resume to Idle speed."));
                            }
                            Thread.Sleep(5000);
                            break;

                        case 0x06:
                            // get test result, ACH command
                            GetAcclerationTestData(out AccelerationData);
                            StopAccelerationTest();
                            if (AccelerationTestCompleted != null)
                            {
                                OpacityAcclerationTestCompletedEventArgs args;
                                BuildAccelerationTestDataFromResponse(AccelerationData, out args);
                                args.IsConsistent = true;
                                args.NumberOfTest = _numberOfAccelerationTestTest;
                                AccelerationTestCompleted(this, args);
                            }
                            return;

                        case 0x07:
                            // get test result, ACH command
                            GetAcclerationTestData(out AccelerationData);
                            StopAccelerationTest();
                            if (AccelerationTestCompleted != null)
                            {
                                OpacityAcclerationTestCompletedEventArgs args;
                                BuildAccelerationTestDataFromResponse(AccelerationData, out args);
                                args.IsConsistent = false;
                                args.NumberOfTest = _numberOfAccelerationTestTest;
                                AccelerationTestCompleted(this, args);
                            }
                            return;

                        case 0x08:
                            // process error
                            StopAccelerationTest();
                            if (AccelerationTestAborted != null)
                            {
                                AccelerationTestAborted(this, new OpacityAcclerationTestAbortedEventArgs("Process error."));
                            }
                            return;

                        default:
                            // send Stop test, ABH command
                            StopAccelerationTest();
                            if (AccelerationTestAborted != null)
                            {
                                AccelerationTestAborted(this, new OpacityAcclerationTestAbortedEventArgs("Unexpected operation occured."));
                            }
                            return;
                    }

                    // update last command
                    LastStatusCode = statusCode;
                }

                // is cancel process?
                if (_bgWorker == null) { return; }
                if (_bgWorker.CancellationPending)
                {
                    e.Cancel = true;
                    StopAccelerationTest();
                    _bgWorker.Dispose();
                    _bgWorker = null;
                    return;
                }

                // wait sometimes
                Thread.Sleep(200);
            }
        }

        private void BuildRealtimeDataFromResponse(byte[] data, byte[] maxData, out OpacityRealtimeTestDataEventArgs args)
        {
            // get current measurement
            float opacity = Converter.GetInt16Value(data, 1);
            float lightCoefficient = Converter.GetInt16Value(data, 3);
            int speed = Converter.GetInt16Value(data, 5);
            int temperature = Converter.GetInt16Value(data, 7) - 273;

            // get current measurement
            float maxOpacity = Converter.GetInt16Value(maxData, 1);
            float maxLightCoefficient = Converter.GetInt16Value(maxData, 3);
            int maxSpeed = Converter.GetInt16Value(maxData, 5);
            int maxTemperature = Converter.GetInt16Value(maxData, 7) - 273;

            // shape the result
            opacity /= 10;
            lightCoefficient /= 100;
            maxOpacity /= 10;
            maxLightCoefficient /= 100;

            // shape the results
            args = new OpacityRealtimeTestDataEventArgs
            {
                Opacity = opacity,
                LightCoefficient = lightCoefficient,
                Speed = speed,
                Temperature = temperature,
                MaxOpacity = maxOpacity,
                MaxLightCoefficient = maxLightCoefficient,
                MaxSpeed = maxSpeed,
                MaxTemperature = maxTemperature
            };
        }

        private void BuildAccelerationTestDataFromResponse(byte[] data, out OpacityAcclerationTestCompletedEventArgs args)
        {
            // get measurement
            float lightCoefficient1 = Converter.GetInt16Value(data, 1);
            float lightCoefficient2 = Converter.GetInt16Value(data, 3);
            float lightCoefficient3 = Converter.GetInt16Value(data, 5);
            float lightCoefficient4 = Converter.GetInt16Value(data, 7);
            float averageLightCoefficient = Converter.GetInt16Value(data, 9);

            // shape the result
            lightCoefficient1 /= 100;
            lightCoefficient2 /= 100;
            lightCoefficient3 /= 100;
            lightCoefficient4 /= 100;
            averageLightCoefficient /= 100;

            args = new OpacityAcclerationTestCompletedEventArgs
            {
                LightCoefficient1 = lightCoefficient1,
                LightCoefficient2 = lightCoefficient2,
                LightCoefficient3 = lightCoefficient3,
                LightCoefficient4 = lightCoefficient4,
                AverageLightCoefficient = averageLightCoefficient
            };
        }

        public byte[] GetDataPacket(params byte[] Bytes)
        {
            int i;
            byte sum = 0;
            byte[] Packet = new byte[Bytes.Length + 1];
            for (i = 0; i < Bytes.Length; i++)
            {
                Packet[i] = Bytes[i];
                sum += Packet[i];
            }

            // set the check code
            Packet[i] = (byte)(~sum + 1);
            return Packet;
        }
    }

    public class OpacityRealtimeTestAbortedEventArgs : EventArgs
    {
        public string ErrorMessage;
        public OpacityRealtimeTestAbortedEventArgs(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }

    public class OpacityAcclerationTestAbortedEventArgs : EventArgs
    {
        public string ErrorMessage;
        public OpacityAcclerationTestAbortedEventArgs(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }

    public class OpacityAcclerationTestStateChangedEventArgs : EventArgs
    {
        public string StateMessage;
        public OpacityAcclerationTestStateChangedEventArgs(string stateMessage)
        {
            StateMessage = stateMessage;
        }
    }

    public class OpacityRealtimeTestDataEventArgs : EventArgs
    {
        public float Opacity;
        public float LightCoefficient;
        public int Speed;
        public int Temperature;
        public float MaxOpacity;
        public float MaxLightCoefficient;
        public int MaxSpeed;
        public int MaxTemperature;
    }

    public class OpacityAcclerationTestCompletedEventArgs : EventArgs
    {
        public bool IsConsistent;
        public int NumberOfTest;
        public float LightCoefficient1;
        public float LightCoefficient2;
        public float LightCoefficient3;
        public float LightCoefficient4;
        public float AverageLightCoefficient;
    }

    public class OpacityAlarmInfo
    {
        public bool IsFullLightIntensityOutOfRange;
        public bool IsAmbientLightIntensityOutOfRange;
        public bool IsEepromError;
        public bool IsBroadTemperatureOutOfRange;
        public bool IsDetectorTemperatureOutOfRange;
        public bool IsTubeTemperatureOutOfRange;
        public bool IsPowerVoltageOutOfRange;
        public bool IsLedTemperatureOutOfRange;
        public bool IsOpacityOutOfRange;
        public bool IsFanCurrentOutOfRange;
        public bool IsFanCurrentImbalance;

        public OpacityAlarmInfo()
        {
            IsFullLightIntensityOutOfRange = false;
            IsAmbientLightIntensityOutOfRange = false;
            IsEepromError = false;
            IsBroadTemperatureOutOfRange = false;
            IsDetectorTemperatureOutOfRange = false;
            IsTubeTemperatureOutOfRange = false;
            IsPowerVoltageOutOfRange = false;
            IsLedTemperatureOutOfRange = false;
            IsOpacityOutOfRange = false;
            IsFanCurrentOutOfRange = false;
            IsFanCurrentImbalance = false;
        }
    }
}