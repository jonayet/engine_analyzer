﻿using System;
using System.ComponentModel;
using System.Threading;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;

// ReSharper disable CSharpWarnings::CS1591
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local

namespace EngineAnalyzer.LogicLayer
{
    public delegate void RealtimeEmissionDataReceivedEventHandler(object sender, RealtimeEmissionDataEventArgs args);
        
    public class EmissionAnalyzerManager
    {
        private readonly SerialComunication _serCom;
        private readonly HidCommunication _hidCom;
        private int _asynDataLength;
        private int _asynReadInterval;
        private bool _isPortOpen;
        private readonly object _lockObject = new object();
        private BackgroundWorker _bgWorker;

        private const int TURN_PUMP_ON = 0x01;
        private const int TURN_PUMP_OFF = 0x02;
        private const int ACCESS_REALTIME_DATA = 0x03;
        private const int SET_FOUR_STROKE = 0x04;
        private const int SET_TWO_STROKE = 0x05;
        private const int SET_AS_GAS = 0x06;
        private const int SET_AS_LPG = 0x07;
        private const int SET_IGNITION_SINGLE = 0x0A;
        private const int SET_IGNITION_TWIN = 0x0B;
        private const int DEV_ACK = 0x06;
        private const int DEV_BUSY = 0x05;
        private const int DEV_NACK = 0x15;
        private const int REAL_TIME_DATA_ACCESS_TIME = 200;
        private const int PUMP_ON_OFF_TIME = 4000;
        private const int SETTINGS_SAVE_TIME = 2000;

        public event RealtimeEmissionDataReceivedEventHandler RealtimeDataReceived;

        public EmissionAnalyzerManager(HidCommunication hidCom)
        {
            _hidCom = hidCom;
            _isPortOpen = false;
        }

        public EmissionAnalyzerManager(SerialComunication serCom)
        {
            _serCom = serCom;
            _isPortOpen = false;
        }

        public bool Open()
        {
            if (_hidCom != null)
            {
                _hidCom.Open();
                _isPortOpen = true;
                return true;
            }
            
            if(_serCom != null)
            {
                _isPortOpen = _serCom.Open();
                return _isPortOpen;
            }
            return false;
        }

        public void Close()
        {
            if (_hidCom != null)
            {
                _hidCom.Close();
                _isPortOpen = false;
            }

            if (_serCom != null)
            {
                _serCom.Close();
                _isPortOpen = false;
            }
        }

        public string TurnPumpOn()
        {
            if(!_isPortOpen) { return "port_closed"; }

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    if (_hidCom.Send(TURN_PUMP_ON, DEV_ACK, PUMP_ON_OFF_TIME))
                    {
                        return "";
                    }
                    if (_hidCom.ReceivedDataBytes.Length > 0 && _hidCom.ReceivedDataBytes[0] == DEV_BUSY)
                    {
                        return "device_busy";
                    }
                    return "unknown_error";
                }

                if (_serCom != null)
                {
                    if (_serCom.Send(TURN_PUMP_ON, DEV_ACK, PUMP_ON_OFF_TIME))
                    {
                        return "";
                    }
                    return "unknown_error";
                }
            }
            return "com_undefined";
        }

        public string TurnPumpOff()
        {
            if (!_isPortOpen) { return "port_closed"; }

            lock (_lockObject)
            {
                if (_hidCom != null)
                {
                    if (_hidCom.Send(TURN_PUMP_OFF, DEV_ACK, PUMP_ON_OFF_TIME))
                    {
                        return "";
                    }
                    if (_hidCom.ReceivedDataBytes.Length > 0 && _hidCom.ReceivedDataBytes[0] == DEV_BUSY)
                    {
                        return "device_busy";
                    }
                    return "unknown_error";
                }

                if (_serCom != null)
                {
                    if (_serCom.Send(TURN_PUMP_OFF, DEV_ACK, PUMP_ON_OFF_TIME))
                    {
                        return "";
                    }
                    return "unknown_error";
                }
            }

            return "unknown_error";
        }

        public string SetNewSettings(byte engineStrokeType, byte fuelType, byte sparkCoilType)
        {
            if (!_isPortOpen) { return "port_closed"; }

            lock (_lockObject)
            {
                bool success = true;
                if (_hidCom != null)
                {
                    success &= _hidCom.Send(engineStrokeType, DEV_ACK, SETTINGS_SAVE_TIME);
                    success &= _hidCom.Send(fuelType, DEV_ACK, SETTINGS_SAVE_TIME);
                    success &= _hidCom.Send(sparkCoilType, DEV_ACK, SETTINGS_SAVE_TIME);
                    if (success)
                    {
                        return "";
                    }
                    if (_hidCom.ReceivedDataBytes.Length > 0 && _hidCom.ReceivedDataBytes[0] == DEV_BUSY)
                    {
                        return "device_busy";
                    }
                    return "unknown_error";
                }

                if (_serCom != null)
                {
                    success &= _serCom.Send(engineStrokeType, DEV_ACK, SETTINGS_SAVE_TIME);
                    success &= _serCom.Send(fuelType, DEV_ACK, SETTINGS_SAVE_TIME);
                    success &= _serCom.Send(sparkCoilType, DEV_ACK, SETTINGS_SAVE_TIME);
                    if (success)
                    {
                        return "";
                    }
                    return "unknown_error";
                }
            }

            return "com_undefined";
        }

        public string StartAsyncRealTimeData(int readInterval)
        {
            if (_isPortOpen == false) { return "port_closed"; }

            // load data length. vary on model number
            if (Settings.Default.__NHA505)
            {
                _asynDataLength = 19;
            }
            else if (Settings.Default.__NHA505_Touch)
            {
                _asynDataLength = 18;
            }
            else
            {
                return "undefined_model";
            }

            _asynReadInterval = readInterval;
            _bgWorker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _bgWorker.DoWork += _bgWorker_DoWork;
            _bgWorker.RunWorkerAsync();
            return "";
        }

        public void StopAsyncRealTimeData()
        {
            if (_bgWorker.IsBusy)
            {
                _bgWorker.CancelAsync();
            }
        }

        void _bgWorker_DoWork(object sender, DoWorkEventArgs args)
        {
            var response = new byte[_asynDataLength];

            while (true)
            {
                if (_isPortOpen)
                {
                    lock (_lockObject)
                    {
                        if (_serCom != null)
                        {
                            _serCom.Send(ACCESS_REALTIME_DATA, ref response, _asynDataLength, REAL_TIME_DATA_ACCESS_TIME);
                        }
                        else if (_hidCom != null)
                        {
                            _hidCom.Send(ACCESS_REALTIME_DATA, ref response, _asynDataLength, REAL_TIME_DATA_ACCESS_TIME);
                        }
                    }

                    RealtimeEmissionDataEventArgs e;
                    if (RealtimeDataReceived != null && BuildDataFromResponse(response, out e))
                    {
                        RealtimeDataReceived(this, e);
                    }
                }

                Thread.Sleep(_asynReadInterval);
                if (_bgWorker == null || _bgWorker.CancellationPending)
                {
                    args.Cancel = true;
                    if (_bgWorker != null)
                        _bgWorker.Dispose();
                    _bgWorker = null;
                    return;
                }
            }
        }

        private bool BuildDataFromResponse(byte[] response, out RealtimeEmissionDataEventArgs args)
        {
            int Ack = response[0];
            if (Ack != DEV_ACK)
            {
                args = null;
                return false; 
            }

            // get data from EventArgs
            int HC_Value = Converter.GetInt16Value(response, 1);
            float CO_Value = Converter.GetInt16Value(response, 3);
            float CO2_Value = Converter.GetInt16Value(response, 5);
            float O2_Value = Converter.GetInt16Value(response, 7);
            int NO_Value = Converter.GetInt16Value(response, 9);
            int Speed_Value = Converter.GetInt16Value(response, 11);
            int Temperature_Value = Converter.GetInt16Value(response, 13);
            float Lamda_Value = Converter.GetInt16Value(response, 15);
            // int CheckSum_Value = (response[17] << 8) | response[18]; /**** DON'T USE ****/

            // check limits
            if (HC_Value > 10000) { HC_Value = 0; }
            if (CO_Value > 10000) { CO2_Value = 0; }
            if (CO2_Value > 10000) { CO2_Value = 0; }
            if (O2_Value > 10000) { O2_Value = 0; }
            if (NO_Value > 10000) { NO_Value = 0; }
            if (Speed_Value > 10000) { Speed_Value = 0; }
            if (Temperature_Value > 10000) { Temperature_Value = 0; }
            if (Lamda_Value > 10000) { Lamda_Value = 0; }

            // shape the results
            args = new RealtimeEmissionDataEventArgs
            {
                Ack = Ack,
                HC = HC_Value,
                CO = CO_Value/100,
                CO2 = CO2_Value/100,
                O2 = O2_Value/100,
                NO = NO_Value,
                RPM = Speed_Value,
                Temperature = Temperature_Value,
                Lamda = Lamda_Value/100
            };
            return true;
        }
    }

    public class RealtimeEmissionDataEventArgs : EventArgs
    {
        public int Ack = 0;
        public int HC = 0;
        public float CO = 0;
        public float CO2 = 0;
        public float O2 = 0;
        public int NO = 0;
        public int RPM = 0;
        public int Temperature = 0;
        public float Lamda = 0;
    }
}
