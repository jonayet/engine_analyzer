﻿using INFRA.USB;
using INFRA.USB.HidToSerialHelper;

namespace EngineAnalyzer.LogicLayer
{
    public class HidCommunication
    {
        public byte[] ReceivedDataBytes { get; private set; }
        private readonly HidToSerialDevice _hidToSerialDevice;

        public HidCommunication(HidToSerialDevice hidToSerialDevice)
        {
            _hidToSerialDevice = hidToSerialDevice;
            ReceivedDataBytes = new byte[] { 0 };
        }

        /// <summary>
        /// Open the HID communication.
        /// </summary>
        public void Open()
        {
            _hidToSerialDevice.Connect();
        }

        /// <summary>
        /// Close HID communication.
        /// </summary>
        public void Close()
        {
            if (_hidToSerialDevice != null)
                _hidToSerialDevice.Close();
        }

        /// <summary>
        /// Send a Byte array through the port..
        /// </summary>
        /// <param name="data">Byte Array to send.</param>
        /// <returns>true: if succeed, false: if error occured.</returns>
        public bool Send(byte[] data)
        {
            if (!_hidToSerialDevice.HidDevice.IsConnected) { return false; }

            // send the data byte
            SingleResponse_FromDevice resp;
            var success = _hidToSerialDevice.SingleQuery(data, 0, 0, out resp);
            ReceivedDataBytes = resp.Data;
            return success;
        }

        /// <summary>
        /// Send a single byte and wait until Response byte found or Timeout occured.
        /// </summary>
        /// <param name="command">Byte to send.</param>
        /// <param name="responseToMatch">Response to be matched.</param>
        /// <param name="waitTime">waiting time before timeout occured in milliseconds.</param>
        /// <returns>true: if sent success and response matches, false: Response doesn't match or Timeout occured.</returns>
        public bool Send(byte command, byte responseToMatch, int waitTime)
        {
            if (!_hidToSerialDevice.HidDevice.IsConnected) { return false; }

            // send the data byte
            SingleResponse_FromDevice receivedData;
            var success = _hidToSerialDevice.SingleQuery(new[] { command }, 1, waitTime, out receivedData);
            if (!success) { return false; }
            ReceivedDataBytes = receivedData.Data;
            if (receivedData.ThisSegmentDataLength >= 1)
            {
                if (receivedData.Data[0] == responseToMatch)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Send a Byte array and wait for a Response byte array.
        /// </summary>
        /// <param name="data">Byte arry to send.</param>
        /// <param name="response">Response Byte array.</param>
        /// <param name="responseLength">Length of the Response byte array.</param>
        /// <param name="waitTime">Time in milliseconds before timeout occured.</param>
        /// <returns>true: send success, false: send was not successfull.</returns>
        public bool Send(byte[] data, ref byte[] response, int responseLength, int waitTime)
        {
            if (!_hidToSerialDevice.HidDevice.IsConnected) { return false; }
            SingleResponse_FromDevice receivedData;
            var success = _hidToSerialDevice.SingleQuery(data, responseLength, waitTime, out receivedData);
            if (!success) { return false; }
            ReceivedDataBytes = receivedData.Data;
            if (receivedData.ThisSegmentDataLength >= responseLength)
            {
                response = receivedData.Data;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Send a Byte array and wait for a Response byte array.
        /// </summary>
        /// <param name="command">command byte to send.</param>
        /// <param name="response">Response Byte array.</param>
        /// <param name="responseLength">Length of the Response byte array.</param>
        /// <param name="waitTime">Time in milliseconds before timeout occured.</param>
        /// <returns>true: send success, false: send was not successfull.</returns>
        public bool Send(byte command, ref byte[] response, int responseLength, int waitTime)
        {
            if (!_hidToSerialDevice.HidDevice.IsConnected) { return false; }
            SingleResponse_FromDevice receivedData;
            var success = _hidToSerialDevice.SingleQuery(new[] { command }, responseLength, waitTime, out receivedData);
            if (!success) { return false; }
            ReceivedDataBytes = receivedData.Data;
            if (receivedData.ThisSegmentDataLength >= responseLength)
            {
                response = receivedData.Data;
                return true;
            }
            return false;
        }
    }
}
