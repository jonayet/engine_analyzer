﻿using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.Properties;
using RTF;

namespace EngineAnalyzer.LogicLayer
{
    public class CommonResources
    {
        // Common Public
        public static string OwnerName = "";
        public static string OwnerAddress = "";

        // Emission Public
        public static int EmissionTotalCapture = 0;
        public static string EmissionCapturedRichText = "";
        public static string EmissionPortName = Settings.Default.EmissionComPortName;

        // Opaity Public
        public static int OpacityRealtimeTotalCapture = 0;
        public static string OpacityRealtimeCapturedRichText = "";
        public static int OpacityAccelerationTotalCapture = 0;
        public static string OpacityAccelerationCapturedRichText = "";
        public static string OpacityPortName = Settings.Default.OpacityComPortName;

        // Physical Inspection
        public static string PhysicalInspectionRichText = "";

        // Battery & Alternator
        public static int BatteryTotalCapture = 0;
        public static string BatteryCapturedRichText = "";
        public const int MIN_VOLTAGE_12V_BATTERY = 8;
        public const int MAX_VOLTAGE_12V_BATTERY = 16;
        public const int MIN_VOLTAGE_24V_BATTERY = 18;
        public const int MAX_VOLTAGE_24V_BATTERY = 30;

        public static void RTF_AddRow(RTFBuilderbase sb, int RowWidth, params string[] cellContents)
        {
            RTFRowDefinition rd = new RTFRowDefinition(RowWidth, RTFAlignment.TopLeft, RTFBorderSide.Left, 5, Color.White, Padding.Empty);
            RTFCellDefinition[] cds = new RTFCellDefinition[cellContents.Length];

            for (int i = 0; i < cellContents.Length; i++)
            {
                if ((RowWidth % cellContents.Length) > i)
                {
                    cds[i] = new RTFCellDefinition(RowWidth / cellContents.Length + 1, RTFAlignment.TopLeft, RTFBorderSide.Left, 5, Color.White, Padding.Empty);
                }
                else
                {
                    cds[i] = new RTFCellDefinition(RowWidth / cellContents.Length, RTFAlignment.TopLeft, RTFBorderSide.Left, 5, Color.White, Padding.Empty);
                }
            }

            int pos = 0;
            foreach (RTFBuilderbase item in sb.EnumerateCells(rd, cds))
            {
                item.FontSize(24).Append(cellContents[pos++]);
            }
        }

        public static void RTF_AddRow(RTFBuilderbase sb, int[] CellWidths, params string[] cellContents)
        {
            int RowWidth = 0;
            foreach (int i in CellWidths) { RowWidth += i; }
            RTFRowDefinition rd = new RTFRowDefinition(RowWidth, RTFAlignment.TopLeft, RTFBorderSide.Left, 5, Color.White, Padding.Empty);
            RTFCellDefinition[] cds = new RTFCellDefinition[cellContents.Length];

            for (int i = 0; i < cellContents.Length; i++)
            {
                cds[i] = new RTFCellDefinition(CellWidths[i], RTFAlignment.TopLeft, RTFBorderSide.Left, 5, Color.White, Padding.Empty);
            }

            int pos = 0;
            foreach (RTFBuilderbase item in sb.EnumerateCells(rd, cds))
            {
                item.FontSize(24).Append(cellContents[pos++]);
            }
        }

        public static byte[] Opacity_GetDataPacket(params byte[] Bytes)
        {
            int i = 0;
            byte sum = 0;
            byte[] Packet = new byte[Bytes.Length + 1];

            for (i = 0; i < Bytes.Length; i++)
            {
                Packet[i] = Bytes[i];
                sum += Packet[i];
            }

            // set the check code
            Packet[i] = (byte)(~sum + 1);
            return Packet;
        }
    }
}