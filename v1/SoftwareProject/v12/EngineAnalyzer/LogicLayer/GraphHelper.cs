﻿using System;
using System.Collections.Generic;
using System.Text;
using ZedGraph;

namespace EngineAnalyzer.LogicLayer
{
    class GraphHelper
    {
        private readonly ZedGraphControl _rpmGraph;
        private readonly ZedGraphControl _voltageGraph;
        private readonly ZedGraphControl _currentGraph;
        private readonly IPointListEdit _rpmPoinList;
        private readonly IPointListEdit _voltagePoinList;
        private readonly IPointListEdit _currentPoinList;
        private readonly object _graphSync;
        private double _voltageGraphX = 0;
        private double _currentGraphX = 0;
        private double _rpmGraphX = 0;
        private double _graphLastX = 0;
        private double _xAxisZoomLevel = 0;

        public GraphHelper(ZedGraphControl rpmGraph, IPointListEdit rpmPoinList, ZedGraphControl voltageGraph, IPointListEdit voltagePoinList, ZedGraphControl currentGraph, IPointListEdit currentPoinList, object graphSync)
        {
            _rpmGraph = rpmGraph;
            _rpmPoinList = rpmPoinList;
            _voltageGraph = voltageGraph;
            _voltagePoinList = voltagePoinList;
            _currentGraph = currentGraph;
            _currentPoinList = currentPoinList;
            SetXAxisZoomLevel(_rpmGraph.GraphPane.XAxis.Scale.Max, _rpmGraph.GraphPane.XAxis.Scale.Min);
            _graphSync = graphSync;
        }

        public void SetGraphDta(float[] voltageData, float[] currentData)
        {
            try
            {
                double vMaxY = 0, cMaxY = 0;
                _voltageGraphX = _graphLastX;
                foreach (float v in voltageData)
                {
                    float vAbs = Math.Abs(v);
                    _voltagePoinList.Add(_voltageGraphX, vAbs);
                    _voltageGraphX += 0.5;
                    if (vMaxY < vAbs) { vMaxY = vAbs; }
                }

                _currentGraphX = _graphLastX;
                foreach (float c in currentData)
                {
                    float cAbs = Math.Abs(c);
                    _currentPoinList.Add(_currentGraphX, cAbs);
                    _currentGraphX += 0.5;
                    if (cMaxY < cAbs) { cMaxY = cAbs; }
                }
                _graphLastX = _voltageGraphX > _currentGraphX ? _voltageGraphX : _currentGraphX;

                AdjustYScale(0, vMaxY, cMaxY);
            }
            catch { }
        }

        public void SetRpmDta(float rpm)
        {
            _rpmGraphX = _graphLastX;
            _rpmPoinList.Add(_rpmGraphX, rpm);
            if (_voltageGraphX == 0 && _currentGraphX == 0)
                _rpmGraphX += 5;
            else
                _rpmGraphX += 0.2;
            _graphLastX = _rpmGraphX;
        }

        private void AdjustYScale(double rpmMaxY, double voltageMaxY, double currentMaxY)
        {
            if (voltageMaxY <= CommonResources.MIN_VOLTAGE_24V_BATTERY)
            {
                _voltageGraph.GraphPane.YAxis.Scale.Min = CommonResources.MIN_VOLTAGE_12V_BATTERY;
                _voltageGraph.GraphPane.YAxis.Scale.Max = CommonResources.MAX_VOLTAGE_12V_BATTERY;
            }
            else
            {
                _voltageGraph.GraphPane.YAxis.Scale.Min = CommonResources.MIN_VOLTAGE_24V_BATTERY;
                _voltageGraph.GraphPane.YAxis.Scale.Max = CommonResources.MAX_VOLTAGE_24V_BATTERY;
            }

            if (currentMaxY <= 30)
            {
                _currentGraph.GraphPane.YAxis.Scale.Min = 0;
                _currentGraph.GraphPane.YAxis.Scale.Max = 30;
            }
            else
            {
                _currentGraph.GraphPane.YAxis.Scale.Min = 31;
                _currentGraph.GraphPane.YAxis.Scale.Max = 150;
            }
        }

        public void SetXScale(double max, double min)
        {
            lock (_graphSync)
            {
                try
                {
                    SetXAxisZoomLevel(max, min);
                    _rpmGraph.GraphPane.XAxis.Scale.Max = max;
                    _rpmGraph.GraphPane.XAxis.Scale.Min = min;
                    _voltageGraph.GraphPane.XAxis.Scale.Max = max;
                    _voltageGraph.GraphPane.XAxis.Scale.Min = min;
                    _currentGraph.GraphPane.XAxis.Scale.Max = max;
                    _currentGraph.GraphPane.XAxis.Scale.Min = min;
                }
                catch{}
            }
        }

        public void ScrollGraph()
        {
            lock (_graphSync)
            {
                try
                {
                    var xScale = _rpmGraph.GraphPane.XAxis.Scale;
                    double dX = xScale.Max - xScale.Min;

                    if (GetLargestX() > xScale.Max - (dX/100))
                    {
                        xScale.Max = GetLargestX() + (dX/100);
                        xScale.Min = xScale.Max - dX;
                    }

                    xScale = _voltageGraph.GraphPane.XAxis.Scale;
                    dX = xScale.Max - xScale.Min;
                    if (GetLargestX() > xScale.Max - (dX / 100))
                    {
                        xScale.Max = GetLargestX() + (dX / 100);
                        xScale.Min = xScale.Max - dX;
                    }

                    xScale = _currentGraph.GraphPane.XAxis.Scale;
                    dX = xScale.Max - xScale.Min;
                    if (GetLargestX() > xScale.Max - (dX / 100))
                    {
                        xScale.Max = GetLargestX() + (dX / 100);
                        xScale.Min = xScale.Max - dX;
                    }
                }
                catch { }
            }
        }

        public void UpdateGraph()
        {
            // Make sure the Y axis is rescaled to accommodate actual data
            _rpmGraph.AxisChange();
            _voltageGraph.AxisChange();
            _currentGraph.AxisChange();

            // Force a redraw
            _rpmGraph.Invalidate();
            _voltageGraph.Invalidate();
            _currentGraph.Invalidate();
        }

        private double GetLargestX()
        {
            double largeX = 0;
            largeX = _rpmGraphX > largeX ? _rpmGraphX : largeX;
            largeX = _voltageGraphX > largeX ? _voltageGraphX : largeX;
            largeX = _currentGraphX > largeX ? _currentGraphX : largeX;
            return largeX;
        }

        private double GetSmallestX()
        {
            double smallX = 0;
            smallX = _rpmGraphX < smallX ? _rpmGraphX : smallX;
            smallX = _voltageGraphX < smallX ? _voltageGraphX : smallX;
            smallX = _currentGraphX < smallX ? _currentGraphX : smallX;
            return smallX;
        }

        public void SetXAxisZoomLevel(double xMax, double xMin)
        {
            _xAxisZoomLevel = (_rpmGraph.GraphPane.XAxis.Scale.Max - _rpmGraph.GraphPane.XAxis.Scale.Min) / 500;
        }
    }
}
