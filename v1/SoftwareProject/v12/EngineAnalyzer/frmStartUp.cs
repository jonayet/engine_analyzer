﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace EngineAnalyzer
{
    public partial class frmStartUp : Form
    {
        public string _CompanyName = "";
        public string _CompanyAddress = "";
        public string _InstallDate = "";
        public string _CurrentComputerID = "";
        public string _LicenseComputerID = "";
        private string LicenseFile = "License.eal";
        string SecretKey = "1FBDE5D6";

        public frmStartUp()
        {
            InitializeComponent();

            // start background task
            bgwStartUp.DoWork += bgwStartUp_DoWork;
            bgwStartUp.RunWorkerAsync();
        }

        void bgwStartUp_DoWork(object sender, DoWorkEventArgs e)
        {
            _CurrentComputerID = ComputerID.GetUniqueID();
        }

        private void bgwStartUp_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // is there any License file?
            if (!ReadLicense())
            {
                MessageBox.Show("There is no License registered. This Program will be close now.");
                Application.ExitThread();
            }

            // is machine id is correct
            if (_CurrentComputerID != _LicenseComputerID)
            {
                MessageBox.Show("License number is not matched. This Program will be close now.");
                Application.ExitThread();
            }
            else
            {
                CommonResources.CompanyName = _CompanyName;
                CommonResources.CompanyAddress = _CompanyAddress;
                this.Hide();
                new frmMain().Show();
            }
        }

        private bool ReadLicense()
        {
            if (!File.Exists(LicenseFile)) { return false; }

            // Create an XML reader for this file.
            using (XmlTextReader reader = new XmlTextReader(LicenseFile))
            {
                while (reader.Read())
                {
                    // Only detect start elements.
                    if (reader.IsStartElement())
                    {
                        switch (reader.Name)
                        {
                            case "CompanyName":
                                reader.Read();
                                _CompanyName = reader.Value;
                                break;
                            case "CompanyAddress":
                                reader.Read();
                                _CompanyAddress = reader.Value;
                                break;
                            case "InstallDate":
                                reader.Read();
                                _InstallDate = Encryption.Decrypt(reader.Value, SecretKey);
                                break;
                            case "License":
                                reader.Read();
                                _LicenseComputerID = Encryption.Decrypt(reader.Value, SecretKey);
                                break;
                        }
                    }
                }
            }

            return true;
        }
    }
}