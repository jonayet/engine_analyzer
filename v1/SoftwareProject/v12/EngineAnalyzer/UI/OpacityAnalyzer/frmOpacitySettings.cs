﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using EngineAnalyzer.Properties;

namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    public partial class frmOpacitySettings : Form
    {
        public frmOpacitySettings()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            // load saved settings
            cmbxEngineStoke.SelectedIndex = Settings.Default.OpacitySettingsEngineStrokeIndex;
            cmbxGasComp.SelectedIndex = Settings.Default.OpacitySettingsGasCompIndex;
            cmbxSpeedSensor.SelectedIndex = Settings.Default.OpacitySettingsSpeedSensorIndex;

            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Settings.Default["OpacitySettingsEngineStrokeIndex"] = cmbxEngineStoke.SelectedIndex;
            Settings.Default["OpacitySettingsGasCompIndex"] = cmbxGasComp.SelectedIndex;
            Settings.Default["OpacitySettingsSpeedSensorIndex"] = cmbxSpeedSensor.SelectedIndex;
            Settings.Default.Save();
            MessageBox.Show("Saved successfully!");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
