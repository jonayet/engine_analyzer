﻿namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    partial class frmOpacityMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOpacityMain));
            this.btnRealTimeTest = new System.Windows.Forms.Button();
            this.btnAccelerationTest = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblDate = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTime = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCompanyName = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.tmrTime = new System.Windows.Forms.Timer(this.components);
            this.btnAlarmInfo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRealTimeTest
            // 
            this.btnRealTimeTest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRealTimeTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRealTimeTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealTimeTest.Location = new System.Drawing.Point(288, 155);
            this.btnRealTimeTest.Name = "btnRealTimeTest";
            this.btnRealTimeTest.Size = new System.Drawing.Size(300, 113);
            this.btnRealTimeTest.TabIndex = 0;
            this.btnRealTimeTest.Text = "Real Time Test";
            this.btnRealTimeTest.UseVisualStyleBackColor = true;
            this.btnRealTimeTest.Click += new System.EventHandler(this.btnRealTimeTest_Click);
            // 
            // btnAccelerationTest
            // 
            this.btnAccelerationTest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAccelerationTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAccelerationTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccelerationTest.Location = new System.Drawing.Point(288, 308);
            this.btnAccelerationTest.Name = "btnAccelerationTest";
            this.btnAccelerationTest.Size = new System.Drawing.Size(300, 113);
            this.btnAccelerationTest.TabIndex = 1;
            this.btnAccelerationTest.Text = "Acceleration Test";
            this.btnAccelerationTest.UseVisualStyleBackColor = true;
            this.btnAccelerationTest.Click += new System.EventHandler(this.btnAccelerationTest_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(768, 639);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 48);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblDate.BackShadowColor = System.Drawing.Color.Black;
            this.lblDate.BorderColor = System.Drawing.Color.Black;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 31F);
            this.lblDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeShadowColor = System.Drawing.Color.Black;
            this.lblDate.Location = new System.Drawing.Point(771, 73);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(235, 52);
            this.lblDate.TabIndex = 15;
            this.lblDate.Text = "01/01/2013";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDate.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTime.BackShadowColor = System.Drawing.Color.Black;
            this.lblTime.BorderColor = System.Drawing.Color.Black;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.lblTime.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTime.Location = new System.Drawing.Point(12, 74);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(249, 52);
            this.lblTime.TabIndex = 14;
            this.lblTime.Text = "00:00:00 AM";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTime.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.lblCompanyName.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCompanyName.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCompanyName.BackShadowColor = System.Drawing.Color.Black;
            this.lblCompanyName.BorderColor = System.Drawing.Color.Black;
            this.lblCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 19F);
            this.lblCompanyName.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCompanyName.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblCompanyName.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCompanyName.Location = new System.Drawing.Point(12, 49);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(994, 32);
            this.lblCompanyName.TabIndex = 13;
            this.lblCompanyName.Text = "[COMPANY NAME]";
            this.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCompanyName.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F);
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(12, 10);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(994, 45);
            this.smartLabel10.TabIndex = 12;
            this.smartLabel10.Text = "COMPUTERIZED ENGINE ANALYZER - DISEL SMOKE TEST";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel10.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // tmrTime
            // 
            this.tmrTime.Enabled = true;
            this.tmrTime.Tick += new System.EventHandler(this.tmrTime_Tick);
            // 
            // btnAlarmInfo
            // 
            this.btnAlarmInfo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAlarmInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlarmInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlarmInfo.Location = new System.Drawing.Point(288, 461);
            this.btnAlarmInfo.Name = "btnAlarmInfo";
            this.btnAlarmInfo.Size = new System.Drawing.Size(300, 113);
            this.btnAlarmInfo.TabIndex = 17;
            this.btnAlarmInfo.Text = "Alarm Information";
            this.btnAlarmInfo.UseVisualStyleBackColor = true;
            this.btnAlarmInfo.Click += new System.EventHandler(this.btnAlarmInfo_Click);
            // 
            // frmOpacityMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1018, 739);
            this.Controls.Add(this.btnAlarmInfo);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblCompanyName);
            this.Controls.Add(this.smartLabel10);
            this.Controls.Add(this.btnAccelerationTest);
            this.Controls.Add(this.btnRealTimeTest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "frmOpacityMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmOpacityMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRealTimeTest;
        private System.Windows.Forms.Button btnAccelerationTest;
        private System.Windows.Forms.Button btnClose;
        private InterfaceLab.WinForm.Controls.SmartLabel lblDate;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTime;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCompanyName;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private System.Windows.Forms.Timer tmrTime;
        private System.Windows.Forms.Button btnAlarmInfo;
    }
}