﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    public partial class frmPleaseWait : Form
    {
        private int _DelayTime = 1000;
        BackgroundWorker bgw = new BackgroundWorker();

        public frmPleaseWait(int DelayTime)
        {
            InitializeComponent();
            _DelayTime = DelayTime;
        }

        public frmPleaseWait(int DelayTime, string Caption, string InfoText)
        {
            InitializeComponent();
            _DelayTime = DelayTime;
            Text = Caption;
            owfProgressControl1.TitileText = InfoText;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            // configure bgw
            bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
            bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);
            bgw.RunWorkerAsync();
        }

        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < _DelayTime; i++)
            {
                System.Threading.Thread.Sleep(1);
                if (bgw.CancellationPending) { break; }
            }
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }
    }
}
