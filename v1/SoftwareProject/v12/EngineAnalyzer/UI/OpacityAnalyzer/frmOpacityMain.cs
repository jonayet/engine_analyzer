﻿using System;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;

namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    public partial class frmOpacityMain : Form
    {
        public frmOpacityMain()
        {
            InitializeComponent();
        }

        private void frmOpacityMain_Load(object sender, EventArgs e)
        {
            lblCompanyName.Text = CommonResources.OwnerName;
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }        

        private void btnRealTimeTest_Click(object sender, EventArgs e)
        {
            (new frmOpacityRealTimeTest()).ShowDialog();
        }

        private void btnAccelerationTest_Click(object sender, EventArgs e)
        {
            (new frmOpacityAccelerationTest()).ShowDialog();
        }

        private void btnAlarmInfo_Click(object sender, EventArgs e)
        {
            new frmOpacityAlarmInfo().ShowDialog();
        }
    }
}
