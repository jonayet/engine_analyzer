﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;
using INFRA.USB;
using RTF;

// ReSharper disable CSharpWarnings::CS1591
// ReSharper disable SuggestUseVarKeywordEvident
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local
// ReSharper disable RedundantExplicitArrayCreation
// ReSharper disable RedundantDefaultFieldInitializer
// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable RedundantDelegateCreation
// ReSharper disable LocalizableElement
// ReSharper disable SpecifyACultureInStringConversionExplicitly

namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    public partial class frmOpacityAccelerationTest : Form
    {
        private readonly OpacityAnalyzerManager _opacityAnalyzerManager;
        private readonly RTFBuilderbase _richTextBuilder;
        delegate void ShowInfo(string text, Color color);

        public frmOpacityAccelerationTest()
        {
            InitializeComponent();

            // choose communication port
            if (Settings.Default.__OpacityUseHid)
            {
                HidToSerialDevice hidToSerialDevice = new HidToSerialDevice(Settings.Default.__OpacityHidVendorId, Settings.Default.__OpacityHidProductId);
                hidToSerialDevice.HidDevice.OnDeviceAttached += new EventHandler(hidToSerialDevice_OnDeviceAttached);
                hidToSerialDevice.HidDevice.OnDeviceRemoved += new EventHandler(hidToSerialDevice_OnDeviceRemoved);
                HidCommunication hidCom = new HidCommunication(hidToSerialDevice);
                _opacityAnalyzerManager = new OpacityAnalyzerManager(hidCom);
            }
            else
            {
                SerialComunication serCom = new SerialComunication(CommonResources.OpacityPortName, 9600);
                _opacityAnalyzerManager = new OpacityAnalyzerManager(serCom);
            }

            // asign event handler
            _opacityAnalyzerManager.AccelerationTestStateChanged += _opacityAnalyzerManager_AccelerationTestStateChanged;
            _opacityAnalyzerManager.AccelerationTestCompleted += _opacityAnalyzerManager_AccelerationTestCompleted;
            _opacityAnalyzerManager.AccelerationTestAborted += _opacityAnalyzerManager_AccelerationTestAborted;

            _richTextBuilder = new RTFBuilder();
        }

        protected override void OnLoad(EventArgs e)
        {
            lblKMax.Text = Settings.Default.OpacityKMax.ToString("0.00");
            lblKMin.Text = Settings.Default.OpacityKMin.ToString("0.00");
            if (Settings.Default.OpacityPublishLimits == false)
            {
                pnlKLimits.Visible = false;
            }

            // load saved data
            _richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.OpacityAccelerationCapturedRichText))
            {
                // add Opacity Title
                _richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).Alignment(StringAlignment.Center).AppendLine("Diesel Opacity and Smoke Analysis  Report (Acceleration)");
                _richTextBuilder.AppendLine();
                _richTextBuilder.AppendLine();

                // add Opacity Limits
                if (Settings.Default.OpacityPublishLimits)
                {
                    _richTextBuilder.FontStyle(FontStyle.Bold).Append("Limits");
                    _richTextBuilder.AppendPara();
                    _richTextBuilder.Reset();
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Diagnosis Limit", "Min", "Max");
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Idle RPM", Settings.Default.OpacitySMin.ToString(), Settings.Default.OpacitySMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Opacity %", Settings.Default.OpacityNMin.ToString(), Settings.Default.OpacityNMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "K /M", Settings.Default.OpacityKMin.ToString(), Settings.Default.OpacityKMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Engine Temp", Settings.Default.OpacityTMin.ToString(), Settings.Default.OpacityTMax.ToString());
                    _richTextBuilder.Reset();
                    _richTextBuilder.AppendLine();
                    _richTextBuilder.AppendLine();
                }
            }
            else
            {
                _richTextBuilder.AppendRTFDocument(CommonResources.OpacityAccelerationCapturedRichText);
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // load capture number
            if (CommonResources.OpacityAccelerationTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.OpacityAccelerationTotalCapture.ToString();
            }

            // open communication port
            _opacityAnalyzerManager.Open();
            
            // pass to base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _opacityAnalyzerManager.Close();
            base.OnClosing(e);
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClearCapture_Click(object sender, EventArgs e)
        {
            _richTextBuilder.Clear();
            CommonResources.OpacityAccelerationTotalCapture = 0;
            CommonResources.OpacityAccelerationCapturedRichText = "";
            lblK_Peak1.Text = "0.00";
            lblK_Peak2.Text = "0.00";
            lblK_Peak3.Text = "0.00";
            lblK_Peak4.Text = "0.00";
            lblK_Average.Text = "0.00";
            lblTimes.Text = "0";
            btnCapture.Text = "CAPTURE";
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.OpacityAccelerationTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.OpacityAccelerationTotalCapture.ToString();

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                _richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            else
            {
//                _richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            }

            // add S, N, K and T
            _richTextBuilder.AppendPara();
            _richTextBuilder.Reset();
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 1", lblK_Peak1.Text + "  /M");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 2", lblK_Peak2.Text + "  /M");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 3", lblK_Peak3.Text + "  /M");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 4", lblK_Peak4.Text + "  /M");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - Average", lblK_Average.Text + "  /M");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Times", lblTimes.Text);
            _richTextBuilder.Reset();
            _richTextBuilder.AppendLine();
            _richTextBuilder.AppendLine();

            // save new values
            CommonResources.OpacityAccelerationCapturedRichText = _richTextBuilder.ToString();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // start the accleration test
            btnStart.Text = "STOP";

            // start Network test
            _opacityAnalyzerManager.StartAsync_AcclerationTest();
        }

        void _opacityAnalyzerManager_AccelerationTestStateChanged(object sender, OpacityAcclerationTestStateChangedEventArgs e)
        {
            ShowInfoText(e.StateMessage, Color.Blue);
        }

        void _opacityAnalyzerManager_AccelerationTestCompleted(object sender, OpacityAcclerationTestCompletedEventArgs e)
        {
            ShowInfoText("Acceleration Test completed successfully!", Color.Green);

            // show the peak values
            lblK_Peak1.Text = e.LightCoefficient1.ToString("0.00");
            lblK_Peak2.Text = e.LightCoefficient2.ToString("0.00");
            lblK_Peak3.Text = e.LightCoefficient3.ToString("0.00");
            lblK_Peak4.Text = e.LightCoefficient4.ToString("0.00");

            // show average value
            lblK_Average.Text = e.AverageLightCoefficient.ToString("0.00");

            // update counter
            lblTimes.Text = e.NumberOfTest.ToString();
            btnStart.Text = "START";
        }

        void _opacityAnalyzerManager_AccelerationTestAborted(object sender, OpacityAcclerationTestAbortedEventArgs e)
        {
            btnStart.Text = "START";
            ShowInfoText(e.ErrorMessage, Color.Red);
        }

        // for cross thread operation
        private void ShowInfoText(string text, Color color)
        {
            if (lblInfoBox.InvokeRequired)
            {
                lblInfoBox.BeginInvoke(new ShowInfo(ShowInfoText), text, color);
            }
            else
            {
                lblInfoBox.Text = text;
                lblInfoBox.ForeColor = color;
                lblInfoBox.ForeGradientColor2 = color;
            }
        }

        void hidToSerialDevice_OnDeviceRemoved(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Not Connected");
        }

        void hidToSerialDevice_OnDeviceAttached(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Connected");
        }
    }
}