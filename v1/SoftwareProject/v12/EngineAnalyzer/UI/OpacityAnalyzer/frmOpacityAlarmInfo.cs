﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;
using INFRA.USB;

// ReSharper disable LocalizableElement
// ReSharper disable InconsistentNaming
// ReSharper disable RedundantDelegateCreation

namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    public partial class frmOpacityAlarmInfo : Form
    {
        private readonly OpacityAnalyzerManager _opacityAnalyzerManager;

        public frmOpacityAlarmInfo()
        {
            InitializeComponent();

            if (Settings.Default.__OpacityUseHid)
            {
                HidToSerialDevice hidToSerialDevice = new HidToSerialDevice(Settings.Default.__OpacityHidVendorId, Settings.Default.__OpacityHidProductId);
                hidToSerialDevice.HidDevice.OnDeviceAttached += new EventHandler(hidToSerialDevice_OnDeviceAttached);
                hidToSerialDevice.HidDevice.OnDeviceRemoved += new EventHandler(hidToSerialDevice_OnDeviceRemoved);
                HidCommunication hidCom = new HidCommunication(hidToSerialDevice);
                _opacityAnalyzerManager = new OpacityAnalyzerManager(hidCom);
            }
            else
            {
                SerialComunication serCom = new SerialComunication(CommonResources.OpacityPortName, 9600);
                _opacityAnalyzerManager = new OpacityAnalyzerManager(serCom);
            }
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            // open communication port
            _opacityAnalyzerManager.Open();

            OpacityAlarmInfo alarmInfo;
            if (_opacityAnalyzerManager.GetAlarmInformation(out alarmInfo))
            {
                ShowAlarmInfo(alarmInfo);
            }
            else
            {
                MessageBox.Show("Device is not responding. Please chek the connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _opacityAnalyzerManager.Close();
            base.OnClosing(e);
        }

        private void btnCheckAgain_Click(object sender, EventArgs e)
        {
            OpacityAlarmInfo alarmInfo;
            if (_opacityAnalyzerManager.GetAlarmInformation(out alarmInfo))
            {
                ShowAlarmInfo(alarmInfo);
            }
            else
            {
                MessageBox.Show("Device is not responding. Please chek the connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ShowAlarmInfo(OpacityAlarmInfo alarmInfo)
        {
            // Full Light Intensity
            if (alarmInfo.IsFullLightIntensityOutOfRange)
            {
                lblFullLightIntensity.Text = "ERROR";
                lblFullLightIntensity.ForeColor = Color.Red;
                lblFullLightIntensity.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblFullLightIntensity.Text = "OK";
                lblFullLightIntensity.ForeColor = Color.Green;
                lblFullLightIntensity.ForeGradientColor2 = Color.Green;
            }

            // Ambient Light Intensity
            if (alarmInfo.IsAmbientLightIntensityOutOfRange)
            {
                lblAmbLightIntensity.Text = "ERROR";
                lblAmbLightIntensity.ForeColor = Color.Red;
                lblAmbLightIntensity.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblAmbLightIntensity.Text = "OK";
                lblAmbLightIntensity.ForeColor = Color.Green;
                lblAmbLightIntensity.ForeGradientColor2 = Color.Green;
            }

            // EEPROM
            if (alarmInfo.IsEepromError)
            {
                lblEeprom.Text = "ERROR";
                lblEeprom.ForeColor = Color.Red;
                lblEeprom.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblEeprom.Text = "OK";
                lblEeprom.ForeColor = Color.Green;
                lblEeprom.ForeGradientColor2 = Color.Green;
            }

            // Broad Temp
            if (alarmInfo.IsBroadTemperatureOutOfRange)
            {
                lblBoardTemp.Text = "ERROR";
                lblBoardTemp.ForeColor = Color.Red;
                lblBoardTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblBoardTemp.Text = "OK";
                lblBoardTemp.ForeColor = Color.Green;
                lblBoardTemp.ForeGradientColor2 = Color.Green;
            }

            // Detector Temp
            if (alarmInfo.IsDetectorTemperatureOutOfRange)
            {
                lblDetectorTemp.Text = "ERROR";
                lblDetectorTemp.ForeColor = Color.Red;
                lblDetectorTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblDetectorTemp.Text = "OK";
                lblDetectorTemp.ForeColor = Color.Green;
                lblDetectorTemp.ForeGradientColor2 = Color.Green;
            }

            // Tube Temp
            if (alarmInfo.IsTubeTemperatureOutOfRange)
            {
                lblTubeTemp.Text = "ERROR";
                lblTubeTemp.ForeColor = Color.Red;
                lblTubeTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblTubeTemp.Text = "OK";
                lblTubeTemp.ForeColor = Color.Green;
                lblTubeTemp.ForeGradientColor2 = Color.Green;
            }

            // Power Voltage
            if (alarmInfo.IsPowerVoltageOutOfRange)
            {
                lblPowerVoltage.Text = "ERROR";
                lblPowerVoltage.ForeColor = Color.Red;
                lblPowerVoltage.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblPowerVoltage.Text = "OK";
                lblPowerVoltage.ForeColor = Color.Green;
                lblPowerVoltage.ForeGradientColor2 = Color.Green;
            }

            // LED Temp
            if (alarmInfo.IsLedTemperatureOutOfRange)
            {
                lblLedTemp.Text = "ERROR";
                lblLedTemp.ForeColor = Color.Red;
                lblLedTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblLedTemp.Text = "OK";
                lblLedTemp.ForeColor = Color.Green;
                lblLedTemp.ForeGradientColor2 = Color.Green;
            }

            // Opacity
            if (alarmInfo.IsOpacityOutOfRange)
            {
                lblOpaity.Text = "ERROR";
                lblOpaity.ForeColor = Color.Red;
                lblOpaity.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblOpaity.Text = "OK";
                lblOpaity.ForeColor = Color.Green;
                lblOpaity.ForeGradientColor2 = Color.Green;
            }

            // Fan Over Current
            if (alarmInfo.IsFanCurrentOutOfRange)
            {
                lblFanOverCurrent.Text = "ERROR";
                lblFanOverCurrent.ForeColor = Color.Red;
                lblFanOverCurrent.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblFanOverCurrent.Text = "OK";
                lblFanOverCurrent.ForeColor = Color.Green;
                lblFanOverCurrent.ForeGradientColor2 = Color.Green;
            }

            // Fan Current Imbalance
            if (alarmInfo.IsFanCurrentImbalance)
            {
                lblFanCurrentImbalance.Text = "ERROR";
                lblFanCurrentImbalance.ForeColor = Color.Red;
                lblFanCurrentImbalance.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblFanCurrentImbalance.Text = "OK";
                lblFanCurrentImbalance.ForeColor = Color.Green;
                lblFanCurrentImbalance.ForeGradientColor2 = Color.Green;
            }
        }

        void hidToSerialDevice_OnDeviceRemoved(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Not Connected");
        }

        void hidToSerialDevice_OnDeviceAttached(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Connected");
        }
    }
}
