﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;
using INFRA.USB;
using RTF;

// ReSharper disable CSharpWarnings::CS1591
// ReSharper disable SuggestUseVarKeywordEvident
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local
// ReSharper disable RedundantExplicitArrayCreation
// ReSharper disable RedundantDefaultFieldInitializer
// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable RedundantDelegateCreation
// ReSharper disable LocalizableElement
// ReSharper disable SpecifyACultureInStringConversionExplicitly                                                     

namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    public partial class frmOpacityRealTimeTest : Form
    {
        private readonly OpacityAnalyzerManager _opacityAnalyzerManager;
        private readonly RTFBuilderbase _richTextBuilder;

        public frmOpacityRealTimeTest()
        {
            InitializeComponent();

            // choose communication port
            if (Settings.Default.__OpacityUseHid)
            {
                HidToSerialDevice hidToSerialDevice = new HidToSerialDevice(Settings.Default.__OpacityHidVendorId, Settings.Default.__OpacityHidProductId);
                hidToSerialDevice.HidDevice.OnDeviceAttached += new EventHandler(hidToSerialDevice_OnDeviceAttached);
                hidToSerialDevice.HidDevice.OnDeviceRemoved += new EventHandler(hidToSerialDevice_OnDeviceRemoved);
                HidCommunication hidCom = new HidCommunication(hidToSerialDevice);
                _opacityAnalyzerManager = new OpacityAnalyzerManager(hidCom);
            }
            else
            {
                SerialComunication serCom = new SerialComunication(CommonResources.OpacityPortName, 9600);
                _opacityAnalyzerManager = new OpacityAnalyzerManager(serCom);
            }

            // asign event handler
            _opacityAnalyzerManager.RealtimeTestDataReceived += _opacityAnalyzerManager_RealtimeTestDataReceived;
            _opacityAnalyzerManager.RealtimeTestAborted += _opacityAnalyzerManager_RealtimeTestAborted;

            _richTextBuilder = new RTFBuilder();
        }

        protected override void OnLoad(EventArgs e)
        {
            // load limits
            lblKMax.Text = Settings.Default.OpacityKMax.ToString("0.00");
            lblKMin.Text = Settings.Default.OpacityKMin.ToString("0.00");
            lblNMax.Text = Settings.Default.OpacityNMax.ToString("0.0");
            lblNMin.Text = Settings.Default.OpacityNMin.ToString("0.0");
            lblSMax.Text = Settings.Default.OpacitySMax.ToString("0");
            lblSMin.Text = Settings.Default.OpacitySMin.ToString("0");
            lblTMax.Text = Settings.Default.OpacityTMax.ToString("0");
            lblTMin.Text = Settings.Default.OpacityTMin.ToString("0");
            if (Settings.Default.OpacityPublishLimits == false)
            {
                pnlKLimits.Visible = false;
                pnlNLimits.Visible = false;
                pnlSLimits.Visible = false;
                pnlTLimits.Visible = false;
            }

            // load saved data
            _richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.OpacityRealtimeCapturedRichText))
            {
                // add Opacity Title
                _richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).Alignment(StringAlignment.Center).AppendLine("Diesel Opacity and Smoke Analysis  Report (Real Time)");
                _richTextBuilder.AppendLine();
                _richTextBuilder.AppendLine();

                // add Opacity Limits
                if (Settings.Default.OpacityPublishLimits)
                {
                    _richTextBuilder.FontStyle(FontStyle.Bold).Append("Limits");
                    _richTextBuilder.AppendPara();
                    _richTextBuilder.Reset();
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Diagnosis Limit", "Min", "Max");
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Idle RPM", Settings.Default.OpacitySMin.ToString(), Settings.Default.OpacitySMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Opacity %", Settings.Default.OpacityNMin.ToString(), Settings.Default.OpacityNMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Opacity %", Settings.Default.OpacityKMin.ToString(), Settings.Default.OpacityKMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 10, 10 }, "Engine Temp", Settings.Default.OpacityTMin.ToString(), Settings.Default.OpacityTMax.ToString());
                    _richTextBuilder.Reset();
                    _richTextBuilder.AppendLine();
                    _richTextBuilder.AppendLine();
                }
            }
            else
            {
                _richTextBuilder.AppendRTFDocument(CommonResources.OpacityRealtimeCapturedRichText);
            }

            // get capture number
            if (CommonResources.OpacityRealtimeTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.OpacityRealtimeTotalCapture.ToString();
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // open communication port
            _opacityAnalyzerManager.Open();

            // pass the base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _opacityAnalyzerManager.Close();
            base.OnClosing(e);
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.OpacityRealtimeTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.OpacityRealtimeTotalCapture.ToString();

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                _richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            else
            {
//                richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            }

            // add S, N, K and T
            _richTextBuilder.AppendPara();
            _richTextBuilder.Reset();
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Engine speed, S", lblSpeed.Text + " RPM");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Opacity, N", lblMaxN.Text + " %");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient, K", lblMaxK.Text + " /M");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Engine Temperature, T", lblT.Text + " °C");
            _richTextBuilder.Reset();
            _richTextBuilder.AppendLine();
            _richTextBuilder.AppendLine();

            // save new values
            CommonResources.OpacityRealtimeCapturedRichText = _richTextBuilder.ToString();
        }

        private void btnClearMax_Click(object sender, EventArgs e)
        {
            // send A7H command (clear real-time max data)
            _opacityAnalyzerManager.ClearRealtimeMaxData();

            // clear Max value
            lblMaxK.Text = "0.00";
            lblMaxN.Text = "0.0";
            lblMaxS.Text = "0";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            // stop the real time measurement
            if(btnStart.Text == "STOP")
            {
                _opacityAnalyzerManager.StopAsync_RealtimeTestData();
                btnStart.Text = "START";
                return;
            }

            // check Warming up status
            string errorMessage;
            if (_opacityAnalyzerManager.IsDeviceWarmingUp(out errorMessage))
            {
                MessageBox.Show("Device is under Warming up condition. Please start the test later.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // send A0H and RealTime Test mode command (01H)
            _opacityAnalyzerManager.StartRealtimeTest();

            // wait 2 second
            frmPleaseWait wait = new frmPleaseWait(2000, "Please wait....", "Starting Realtime test....");
            wait.ShowDialog();

            // need calibration?
            DialogResult dRes = MessageBox.Show("Do you want to Calibrate the instrument?", "Calibration", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == DialogResult.Yes)
            {
                // prompt to take out the probe
                MessageBox.Show("Please take out the Probe and then click OK button.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                // send A4H command (calibration)
                _opacityAnalyzerManager.CalibrateDevice();

                // wait 3 second
                wait = new frmPleaseWait(3000, "Calibrating...", "Please wait....");
                wait.ShowDialog();
            }

            // prompt to insert the probe
            MessageBox.Show("Please insert the Probe and then click OK button.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // wait 2 second
            wait = new frmPleaseWait(2000, "Please wait....", "Please wait for a while....");
            wait.ShowDialog();

            // start the measuremnt async
            _opacityAnalyzerManager.ReadAsync_RealtimeTestData(500);
            btnStart.Text = "STOP";
        }

        void _opacityAnalyzerManager_RealtimeTestDataReceived(object sender, OpacityRealtimeTestDataEventArgs e)
        {
            // show the values
            lblN.Text = e.Opacity.ToString("0.0");
            lblK.Text = e.LightCoefficient.ToString("0.00");
            lblS.Text = e.Speed.ToString();
            lblSpeed.Text = e.Speed.ToString();
            lblT.Text = e.Temperature.ToString();

            // update progressbar
            int pos = e.Speed / 40;
            if (pos > 100) { pos = 100; }
            pbrSpped.Position = pos;

            // show the values
            lblMaxN.Text = e.MaxOpacity.ToString("0.0");
            lblMaxK.Text = e.MaxLightCoefficient.ToString("0.00");
            lblMaxS.Text = e.MaxSpeed.ToString();
        }

        void _opacityAnalyzerManager_RealtimeTestAborted(object sender, OpacityRealtimeTestAbortedEventArgs e)
        {
            MessageBox.Show("Realtime tes aborted.");
        }

        private void btnClearCapture_Click(object sender, EventArgs e)
        {
            _richTextBuilder.Clear();
            CommonResources.OpacityRealtimeTotalCapture = 0;
            CommonResources.OpacityRealtimeCapturedRichText = "";
            btnCapture.Text = "CAPTURE";
        }

        void hidToSerialDevice_OnDeviceRemoved(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Not Connected");
        }

        void hidToSerialDevice_OnDeviceAttached(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Connected");
        }
    }
}
