﻿namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    partial class frmOpacityAccelerationTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOpacityAccelerationTest));
            this.btnClose = new System.Windows.Forms.Button();
            this.smartLabel13 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel11 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel5 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblDate = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTime = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblK_Peak4 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblK_Peak3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblK_Peak2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblK_Peak1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblK_Average = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTimes = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel27 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlKLimits = new System.Windows.Forms.Panel();
            this.lblKMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblKMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel23 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.tmrTime = new System.Windows.Forms.Timer(this.components);
            this.btnClearCapture = new System.Windows.Forms.Button();
            this.smartLabel14 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.cmbxCaption = new System.Windows.Forms.ComboBox();
            this.btnCapture = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblInfoBox = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblUsbConnected = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlKLimits.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClose.AutoSize = true;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(857, 526);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(128, 48);
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // smartLabel13
            // 
            this.smartLabel13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel13.AutoSize = true;
            this.smartLabel13.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel13.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel13.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel13.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel13.BorderColor = System.Drawing.Color.Black;
            this.smartLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel13.ForeColor = System.Drawing.Color.Black;
            this.smartLabel13.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel13.ForeShadowAmount = 1;
            this.smartLabel13.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel13.ForeShadowOffset = 1;
            this.smartLabel13.Location = new System.Drawing.Point(285, 273);
            this.smartLabel13.Name = "smartLabel13";
            this.smartLabel13.Size = new System.Drawing.Size(137, 29);
            this.smartLabel13.TabIndex = 40;
            this.smartLabel13.Text = "AVERAGE: ";
            this.smartLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel13.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel11
            // 
            this.smartLabel11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel11.AutoSize = true;
            this.smartLabel11.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel11.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel11.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel11.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel11.BorderColor = System.Drawing.Color.Black;
            this.smartLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel11.ForeColor = System.Drawing.Color.Black;
            this.smartLabel11.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel11.ForeShadowAmount = 1;
            this.smartLabel11.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel11.ForeShadowOffset = 1;
            this.smartLabel11.Location = new System.Drawing.Point(10, 504);
            this.smartLabel11.Name = "smartLabel11";
            this.smartLabel11.Size = new System.Drawing.Size(64, 55);
            this.smartLabel11.TabIndex = 36;
            this.smartLabel11.Text = "4.";
            this.smartLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel11.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel5
            // 
            this.smartLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel5.AutoSize = true;
            this.smartLabel5.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.BorderColor = System.Drawing.Color.Black;
            this.smartLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel5.ForeColor = System.Drawing.Color.Black;
            this.smartLabel5.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel5.ForeShadowAmount = 1;
            this.smartLabel5.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.ForeShadowOffset = 1;
            this.smartLabel5.Location = new System.Drawing.Point(10, 382);
            this.smartLabel5.Name = "smartLabel5";
            this.smartLabel5.Size = new System.Drawing.Size(64, 55);
            this.smartLabel5.TabIndex = 36;
            this.smartLabel5.Text = "3.";
            this.smartLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel5.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.Color.Black;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel1.ForeShadowAmount = 1;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.ForeShadowOffset = 1;
            this.smartLabel1.Location = new System.Drawing.Point(28, 75);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(261, 33);
            this.smartLabel1.TabIndex = 35;
            this.smartLabel1.Text = "K-Peak Value (/m):";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel1.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.Color.Black;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel2.ForeShadowAmount = 1;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.ForeShadowOffset = 1;
            this.smartLabel2.Location = new System.Drawing.Point(10, 138);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(64, 55);
            this.smartLabel2.TabIndex = 36;
            this.smartLabel2.Text = "1.";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel2.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblDate.BackShadowColor = System.Drawing.Color.Black;
            this.lblDate.BorderColor = System.Drawing.Color.Black;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 31F);
            this.lblDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeShadowAmount = 2;
            this.lblDate.ForeShadowColor = System.Drawing.Color.Black;
            this.lblDate.ForeShadowOffset = 1;
            this.lblDate.Location = new System.Drawing.Point(757, 93);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(228, 48);
            this.lblDate.TabIndex = 29;
            this.lblDate.Text = "01/01/2013";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDate.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTime.BackShadowColor = System.Drawing.Color.Black;
            this.lblTime.BorderColor = System.Drawing.Color.Black;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 29.5F);
            this.lblTime.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeShadowAmount = 2;
            this.lblTime.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTime.ForeShadowOffset = 1;
            this.lblTime.Location = new System.Drawing.Point(740, 155);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(245, 46);
            this.lblTime.TabIndex = 28;
            this.lblTime.Text = "00:00:00 AM";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTime.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.AutoSize = true;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.5F);
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(196, 9);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(572, 40);
            this.smartLabel10.TabIndex = 26;
            this.smartLabel10.Text = "OPACITY - ACCLERATION TEST ";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel10.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel3
            // 
            this.smartLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel3.AutoSize = true;
            this.smartLabel3.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.BorderColor = System.Drawing.Color.Black;
            this.smartLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel3.ForeColor = System.Drawing.Color.Black;
            this.smartLabel3.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel3.ForeShadowAmount = 1;
            this.smartLabel3.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.ForeShadowOffset = 1;
            this.smartLabel3.Location = new System.Drawing.Point(10, 260);
            this.smartLabel3.Name = "smartLabel3";
            this.smartLabel3.Size = new System.Drawing.Size(64, 55);
            this.smartLabel3.TabIndex = 36;
            this.smartLabel3.Text = "2.";
            this.smartLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel3.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblK_Peak4
            // 
            this.lblK_Peak4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblK_Peak4.BackColor = System.Drawing.Color.Transparent;
            this.lblK_Peak4.BackGradientAngle = 150;
            this.lblK_Peak4.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblK_Peak4.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblK_Peak4.BackShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak4.BorderColor = System.Drawing.Color.Black;
            this.lblK_Peak4.BorderCornerRadius = 10;
            this.lblK_Peak4.BorderWidth = 1;
            this.lblK_Peak4.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblK_Peak4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak4.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak4.ForeShadowAmount = 3;
            this.lblK_Peak4.ForeShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak4.ForeShadowOffset = 1;
            this.lblK_Peak4.Location = new System.Drawing.Point(78, 491);
            this.lblK_Peak4.Name = "lblK_Peak4";
            this.lblK_Peak4.Size = new System.Drawing.Size(190, 80);
            this.lblK_Peak4.TabIndex = 71;
            this.lblK_Peak4.Text = "0.00";
            this.lblK_Peak4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblK_Peak4.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblK_Peak3
            // 
            this.lblK_Peak3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblK_Peak3.BackColor = System.Drawing.Color.Transparent;
            this.lblK_Peak3.BackGradientAngle = 150;
            this.lblK_Peak3.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblK_Peak3.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblK_Peak3.BackShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak3.BorderColor = System.Drawing.Color.Black;
            this.lblK_Peak3.BorderCornerRadius = 10;
            this.lblK_Peak3.BorderWidth = 1;
            this.lblK_Peak3.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblK_Peak3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak3.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak3.ForeShadowAmount = 3;
            this.lblK_Peak3.ForeShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak3.ForeShadowOffset = 1;
            this.lblK_Peak3.Location = new System.Drawing.Point(78, 369);
            this.lblK_Peak3.Name = "lblK_Peak3";
            this.lblK_Peak3.Size = new System.Drawing.Size(190, 80);
            this.lblK_Peak3.TabIndex = 70;
            this.lblK_Peak3.Text = "0.00";
            this.lblK_Peak3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblK_Peak3.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblK_Peak2
            // 
            this.lblK_Peak2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblK_Peak2.BackColor = System.Drawing.Color.Transparent;
            this.lblK_Peak2.BackGradientAngle = 150;
            this.lblK_Peak2.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblK_Peak2.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblK_Peak2.BackShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak2.BorderColor = System.Drawing.Color.Black;
            this.lblK_Peak2.BorderCornerRadius = 10;
            this.lblK_Peak2.BorderWidth = 1;
            this.lblK_Peak2.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblK_Peak2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak2.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak2.ForeShadowAmount = 3;
            this.lblK_Peak2.ForeShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak2.ForeShadowOffset = 1;
            this.lblK_Peak2.Location = new System.Drawing.Point(78, 247);
            this.lblK_Peak2.Name = "lblK_Peak2";
            this.lblK_Peak2.Size = new System.Drawing.Size(190, 80);
            this.lblK_Peak2.TabIndex = 69;
            this.lblK_Peak2.Text = "0.00";
            this.lblK_Peak2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblK_Peak2.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblK_Peak1
            // 
            this.lblK_Peak1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblK_Peak1.BackColor = System.Drawing.Color.Transparent;
            this.lblK_Peak1.BackGradientAngle = 150;
            this.lblK_Peak1.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblK_Peak1.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblK_Peak1.BackShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak1.BorderColor = System.Drawing.Color.Black;
            this.lblK_Peak1.BorderCornerRadius = 10;
            this.lblK_Peak1.BorderWidth = 1;
            this.lblK_Peak1.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblK_Peak1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak1.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblK_Peak1.ForeShadowAmount = 3;
            this.lblK_Peak1.ForeShadowColor = System.Drawing.Color.Black;
            this.lblK_Peak1.ForeShadowOffset = 1;
            this.lblK_Peak1.Location = new System.Drawing.Point(78, 125);
            this.lblK_Peak1.Name = "lblK_Peak1";
            this.lblK_Peak1.Size = new System.Drawing.Size(190, 80);
            this.lblK_Peak1.TabIndex = 68;
            this.lblK_Peak1.Text = "0.00";
            this.lblK_Peak1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblK_Peak1.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblK_Average
            // 
            this.lblK_Average.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblK_Average.BackColor = System.Drawing.Color.Transparent;
            this.lblK_Average.BackGradientAngle = 150;
            this.lblK_Average.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblK_Average.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblK_Average.BackShadowColor = System.Drawing.Color.Black;
            this.lblK_Average.BorderColor = System.Drawing.Color.Black;
            this.lblK_Average.BorderCornerRadius = 10;
            this.lblK_Average.BorderWidth = 1;
            this.lblK_Average.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblK_Average.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblK_Average.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblK_Average.ForeShadowAmount = 3;
            this.lblK_Average.ForeShadowColor = System.Drawing.Color.Black;
            this.lblK_Average.ForeShadowOffset = 1;
            this.lblK_Average.Location = new System.Drawing.Point(427, 247);
            this.lblK_Average.Name = "lblK_Average";
            this.lblK_Average.Size = new System.Drawing.Size(190, 80);
            this.lblK_Average.TabIndex = 72;
            this.lblK_Average.Text = "0.00";
            this.lblK_Average.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblK_Average.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblTimes
            // 
            this.lblTimes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTimes.BackColor = System.Drawing.Color.Transparent;
            this.lblTimes.BackGradientAngle = 150;
            this.lblTimes.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblTimes.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblTimes.BackShadowColor = System.Drawing.Color.Black;
            this.lblTimes.BorderColor = System.Drawing.Color.Black;
            this.lblTimes.BorderCornerRadius = 10;
            this.lblTimes.BorderWidth = 1;
            this.lblTimes.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblTimes.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblTimes.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblTimes.ForeShadowAmount = 3;
            this.lblTimes.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTimes.ForeShadowOffset = 1;
            this.lblTimes.Location = new System.Drawing.Point(427, 369);
            this.lblTimes.Name = "lblTimes";
            this.lblTimes.Size = new System.Drawing.Size(190, 80);
            this.lblTimes.TabIndex = 74;
            this.lblTimes.Text = "0";
            this.lblTimes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTimes.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel27
            // 
            this.smartLabel27.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel27.AutoSize = true;
            this.smartLabel27.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel27.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel27.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel27.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel27.BorderColor = System.Drawing.Color.Black;
            this.smartLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel27.ForeColor = System.Drawing.Color.Black;
            this.smartLabel27.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel27.ForeShadowAmount = 1;
            this.smartLabel27.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel27.ForeShadowOffset = 1;
            this.smartLabel27.Location = new System.Drawing.Point(328, 395);
            this.smartLabel27.Name = "smartLabel27";
            this.smartLabel27.Size = new System.Drawing.Size(93, 29);
            this.smartLabel27.TabIndex = 73;
            this.smartLabel27.Text = "TIMES:";
            this.smartLabel27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel27.TextBorderColor = System.Drawing.Color.Black;
            // 
            // pnlKLimits
            // 
            this.pnlKLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlKLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlKLimits.Controls.Add(this.lblKMin);
            this.pnlKLimits.Controls.Add(this.lblKMax);
            this.pnlKLimits.Controls.Add(this.smartLabel23);
            this.pnlKLimits.Location = new System.Drawing.Point(427, 125);
            this.pnlKLimits.Name = "pnlKLimits";
            this.pnlKLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlKLimits.TabIndex = 76;
            // 
            // lblKMin
            // 
            this.lblKMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblKMin.BackColor = System.Drawing.Color.Transparent;
            this.lblKMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblKMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblKMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblKMin.BorderColor = System.Drawing.Color.Black;
            this.lblKMin.BorderWidth = 1;
            this.lblKMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblKMin.ForeColor = System.Drawing.Color.Red;
            this.lblKMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblKMin.ForeShadowAmount = 1;
            this.lblKMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblKMin.ForeShadowOffset = 1;
            this.lblKMin.Location = new System.Drawing.Point(5, 46);
            this.lblKMin.Name = "lblKMin";
            this.lblKMin.Size = new System.Drawing.Size(80, 32);
            this.lblKMin.TabIndex = 7;
            this.lblKMin.Text = "0";
            this.lblKMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblKMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblKMax
            // 
            this.lblKMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblKMax.BackColor = System.Drawing.Color.Transparent;
            this.lblKMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblKMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblKMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblKMax.BorderColor = System.Drawing.Color.Black;
            this.lblKMax.BorderWidth = 1;
            this.lblKMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblKMax.ForeColor = System.Drawing.Color.Red;
            this.lblKMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblKMax.ForeShadowAmount = 1;
            this.lblKMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblKMax.ForeShadowOffset = 1;
            this.lblKMax.Location = new System.Drawing.Point(5, 15);
            this.lblKMax.Name = "lblKMax";
            this.lblKMax.Size = new System.Drawing.Size(80, 32);
            this.lblKMax.TabIndex = 9;
            this.lblKMax.Text = "0";
            this.lblKMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblKMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel23
            // 
            this.smartLabel23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel23.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.BorderColor = System.Drawing.Color.Black;
            this.smartLabel23.BorderWidth = 1;
            this.smartLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.Location = new System.Drawing.Point(5, 2);
            this.smartLabel23.Name = "smartLabel23";
            this.smartLabel23.Size = new System.Drawing.Size(80, 14);
            this.smartLabel23.TabIndex = 8;
            this.smartLabel23.Text = "LIMITS";
            this.smartLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel23.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // tmrTime
            // 
            this.tmrTime.Enabled = true;
            this.tmrTime.Tick += new System.EventHandler(this.tmrTime_Tick);
            // 
            // btnClearCapture
            // 
            this.btnClearCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearCapture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearCapture.Location = new System.Drawing.Point(355, 528);
            this.btnClearCapture.Name = "btnClearCapture";
            this.btnClearCapture.Size = new System.Drawing.Size(133, 48);
            this.btnClearCapture.TabIndex = 248;
            this.btnClearCapture.Text = "CLEAR CAP";
            this.btnClearCapture.UseVisualStyleBackColor = true;
            this.btnClearCapture.Click += new System.EventHandler(this.btnClearCapture_Click);
            // 
            // smartLabel14
            // 
            this.smartLabel14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel14.AutoSize = true;
            this.smartLabel14.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel14.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel14.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel14.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel14.BorderColor = System.Drawing.Color.Black;
            this.smartLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel14.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel14.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel14.Location = new System.Drawing.Point(355, 489);
            this.smartLabel14.Name = "smartLabel14";
            this.smartLabel14.Size = new System.Drawing.Size(119, 20);
            this.smartLabel14.TabIndex = 247;
            this.smartLabel14.Text = "Test Condition: ";
            this.smartLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel14.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // cmbxCaption
            // 
            this.cmbxCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxCaption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxCaption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxCaption.FormattingEnabled = true;
            this.cmbxCaption.Items.AddRange(new object[] {
            "",
            "With AC Load",
            "Without AC Load",
            "With Acceleration",
            "Before Adjustment",
            "After Adjustment"});
            this.cmbxCaption.Location = new System.Drawing.Point(480, 483);
            this.cmbxCaption.Name = "cmbxCaption";
            this.cmbxCaption.Size = new System.Drawing.Size(505, 32);
            this.cmbxCaption.TabIndex = 246;
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCapture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapture.ForeColor = System.Drawing.Color.Blue;
            this.btnCapture.Location = new System.Drawing.Point(511, 528);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(167, 48);
            this.btnCapture.TabIndex = 245;
            this.btnCapture.Text = "CAPTURE";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(701, 526);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(133, 48);
            this.btnStart.TabIndex = 249;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblInfoBox
            // 
            this.lblInfoBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblInfoBox.BackColor = System.Drawing.Color.Transparent;
            this.lblInfoBox.BackGradientAngle = 90;
            this.lblInfoBox.BackGredientColor1 = System.Drawing.Color.Gainsboro;
            this.lblInfoBox.BackGredientColor2 = System.Drawing.Color.DarkGray;
            this.lblInfoBox.BackShadowColor = System.Drawing.Color.Black;
            this.lblInfoBox.BorderColor = System.Drawing.Color.Black;
            this.lblInfoBox.BorderCornerRadius = 10;
            this.lblInfoBox.BorderWidth = 1;
            this.lblInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoBox.ForeColor = System.Drawing.Color.Red;
            this.lblInfoBox.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblInfoBox.ForeShadowAmount = 2;
            this.lblInfoBox.ForeShadowColor = System.Drawing.Color.Black;
            this.lblInfoBox.ForeShadowOffset = 1;
            this.lblInfoBox.Location = new System.Drawing.Point(12, 618);
            this.lblInfoBox.Name = "lblInfoBox";
            this.lblInfoBox.Size = new System.Drawing.Size(994, 77);
            this.lblInfoBox.TabIndex = 250;
            this.lblInfoBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblInfoBox.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblUsbConnected
            // 
            this.lblUsbConnected.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUsbConnected.BackColor = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.BorderColor = System.Drawing.Color.Black;
            this.lblUsbConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblUsbConnected.ForeColor = System.Drawing.Color.White;
            this.lblUsbConnected.ForeGradientColor2 = System.Drawing.Color.White;
            this.lblUsbConnected.ForeShadowAmount = 1;
            this.lblUsbConnected.ForeShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.ForeShadowOffset = 1;
            this.lblUsbConnected.Location = new System.Drawing.Point(849, 456);
            this.lblUsbConnected.Name = "lblUsbConnected";
            this.lblUsbConnected.Size = new System.Drawing.Size(136, 24);
            this.lblUsbConnected.TabIndex = 261;
            this.lblUsbConnected.Text = " Not Conected";
            this.lblUsbConnected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUsbConnected.TextBorderColor = System.Drawing.Color.Black;
            // 
            // frmOpacityAccelerationTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1018, 730);
            this.Controls.Add(this.lblUsbConnected);
            this.Controls.Add(this.lblInfoBox);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnClearCapture);
            this.Controls.Add(this.smartLabel14);
            this.Controls.Add(this.cmbxCaption);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.lblTimes);
            this.Controls.Add(this.lblK_Average);
            this.Controls.Add(this.pnlKLimits);
            this.Controls.Add(this.smartLabel27);
            this.Controls.Add(this.lblK_Peak4);
            this.Controls.Add(this.lblK_Peak3);
            this.Controls.Add(this.lblK_Peak2);
            this.Controls.Add(this.lblK_Peak1);
            this.Controls.Add(this.smartLabel11);
            this.Controls.Add(this.smartLabel5);
            this.Controls.Add(this.smartLabel3);
            this.Controls.Add(this.smartLabel2);
            this.Controls.Add(this.smartLabel13);
            this.Controls.Add(this.smartLabel1);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.smartLabel10);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "frmOpacityAccelerationTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "OPACITY - ACCLERATION TEST";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlKLimits.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private InterfaceLab.WinForm.Controls.SmartLabel lblDate;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTime;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private System.Windows.Forms.Button btnClose;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel5;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel11;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel13;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel3;
        private InterfaceLab.WinForm.Controls.SmartLabel lblK_Peak4;
        private InterfaceLab.WinForm.Controls.SmartLabel lblK_Peak3;
        private InterfaceLab.WinForm.Controls.SmartLabel lblK_Peak2;
        private InterfaceLab.WinForm.Controls.SmartLabel lblK_Peak1;
        private InterfaceLab.WinForm.Controls.SmartLabel lblK_Average;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTimes;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel27;
        private System.Windows.Forms.Panel pnlKLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblKMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblKMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel23;
        private System.Windows.Forms.Timer tmrTime;
        private System.Windows.Forms.Button btnClearCapture;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel14;
        private System.Windows.Forms.ComboBox cmbxCaption;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.Button btnStart;
        private InterfaceLab.WinForm.Controls.SmartLabel lblInfoBox;
        private InterfaceLab.WinForm.Controls.SmartLabel lblUsbConnected;
    }
}