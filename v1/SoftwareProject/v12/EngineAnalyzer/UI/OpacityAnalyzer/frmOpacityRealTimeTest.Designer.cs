﻿namespace EngineAnalyzer.UI.OpacityAnalyzer
{
    partial class frmOpacityRealTimeTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOpacityRealTimeTest));
            this.btnClose = new System.Windows.Forms.Button();
            this.tmrTime = new System.Windows.Forms.Timer(this.components);
            this.btnCapture = new System.Windows.Forms.Button();
            this.lblSpeed = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel9 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlTLimits = new System.Windows.Forms.Panel();
            this.lblTMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel26 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlKLimits = new System.Windows.Forms.Panel();
            this.lblKMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblKMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel23 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlNLimits = new System.Windows.Forms.Panel();
            this.lblNMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblNMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel16 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlSLimits = new System.Windows.Forms.Panel();
            this.lblSMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblSMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel6 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel19 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel20 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel15 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel17 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel8 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel12 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblT = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblK = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblN = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblS = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel22 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel4 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblMaxK = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblMaxN = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblMaxS = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel11 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnClearMax = new System.Windows.Forms.Button();
            this.lblDate = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTime = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnStart = new System.Windows.Forms.Button();
            this.cmbxCaption = new System.Windows.Forms.ComboBox();
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pbrSpped = new AdvancedProgressBarControl.AdvancedProgressBar();
            this.smartLabel35 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel33 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel32 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel31 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel30 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel29 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel28 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel25 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel24 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel21 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel18 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnClearCapture = new System.Windows.Forms.Button();
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel5 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel7 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel13 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel14 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblUsbConnected = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlTLimits.SuspendLayout();
            this.pnlKLimits.SuspendLayout();
            this.pnlNLimits.SuspendLayout();
            this.pnlSLimits.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(682, 648);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(108, 48);
            this.btnClose.TabIndex = 21;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tmrTime
            // 
            this.tmrTime.Enabled = true;
            this.tmrTime.Tick += new System.EventHandler(this.tmrTime_Tick);
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCapture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapture.ForeColor = System.Drawing.Color.Blue;
            this.btnCapture.Location = new System.Drawing.Point(184, 648);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(167, 48);
            this.btnCapture.TabIndex = 138;
            this.btnCapture.Text = "CAPTURE";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // lblSpeed
            // 
            this.lblSpeed.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSpeed.BackColor = System.Drawing.Color.Transparent;
            this.lblSpeed.BackGradientAngle = 150;
            this.lblSpeed.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblSpeed.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblSpeed.BackShadowColor = System.Drawing.Color.Black;
            this.lblSpeed.BorderColor = System.Drawing.Color.Black;
            this.lblSpeed.BorderCornerRadius = 10;
            this.lblSpeed.BorderWidth = 1;
            this.lblSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.5F, System.Drawing.FontStyle.Bold);
            this.lblSpeed.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblSpeed.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblSpeed.ForeShadowAmount = 3;
            this.lblSpeed.ForeShadowColor = System.Drawing.Color.Black;
            this.lblSpeed.ForeShadowOffset = 1;
            this.lblSpeed.Location = new System.Drawing.Point(697, 584);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(93, 45);
            this.lblSpeed.TabIndex = 127;
            this.lblSpeed.Text = "0";
            this.lblSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblSpeed.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel9
            // 
            this.smartLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel9.AutoSize = true;
            this.smartLabel9.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.BorderColor = System.Drawing.Color.Black;
            this.smartLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel9.ForeColor = System.Drawing.Color.Black;
            this.smartLabel9.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel9.ForeShadowAmount = 1;
            this.smartLabel9.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.ForeShadowOffset = 1;
            this.smartLabel9.Location = new System.Drawing.Point(606, 590);
            this.smartLabel9.Name = "smartLabel9";
            this.smartLabel9.Size = new System.Drawing.Size(86, 37);
            this.smartLabel9.TabIndex = 126;
            this.smartLabel9.Text = "RPM";
            this.smartLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel9.TextBorderColor = System.Drawing.Color.Black;
            // 
            // pnlTLimits
            // 
            this.pnlTLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlTLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlTLimits.Controls.Add(this.lblTMin);
            this.pnlTLimits.Controls.Add(this.lblTMax);
            this.pnlTLimits.Controls.Add(this.smartLabel26);
            this.pnlTLimits.Location = new System.Drawing.Point(676, 446);
            this.pnlTLimits.Name = "pnlTLimits";
            this.pnlTLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlTLimits.TabIndex = 124;
            // 
            // lblTMin
            // 
            this.lblTMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTMin.BackColor = System.Drawing.Color.Transparent;
            this.lblTMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblTMin.BorderColor = System.Drawing.Color.Black;
            this.lblTMin.BorderWidth = 1;
            this.lblTMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblTMin.ForeColor = System.Drawing.Color.Red;
            this.lblTMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblTMin.ForeShadowAmount = 1;
            this.lblTMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTMin.ForeShadowOffset = 1;
            this.lblTMin.Location = new System.Drawing.Point(5, 46);
            this.lblTMin.Name = "lblTMin";
            this.lblTMin.Size = new System.Drawing.Size(80, 32);
            this.lblTMin.TabIndex = 7;
            this.lblTMin.Text = "0";
            this.lblTMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblTMax
            // 
            this.lblTMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTMax.BackColor = System.Drawing.Color.Transparent;
            this.lblTMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblTMax.BorderColor = System.Drawing.Color.Black;
            this.lblTMax.BorderWidth = 1;
            this.lblTMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblTMax.ForeColor = System.Drawing.Color.Red;
            this.lblTMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblTMax.ForeShadowAmount = 1;
            this.lblTMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTMax.ForeShadowOffset = 1;
            this.lblTMax.Location = new System.Drawing.Point(5, 15);
            this.lblTMax.Name = "lblTMax";
            this.lblTMax.Size = new System.Drawing.Size(80, 32);
            this.lblTMax.TabIndex = 9;
            this.lblTMax.Text = "0";
            this.lblTMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel26
            // 
            this.smartLabel26.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel26.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel26.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel26.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel26.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel26.BorderColor = System.Drawing.Color.Black;
            this.smartLabel26.BorderWidth = 1;
            this.smartLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel26.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel26.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel26.Location = new System.Drawing.Point(5, 2);
            this.smartLabel26.Name = "smartLabel26";
            this.smartLabel26.Size = new System.Drawing.Size(80, 14);
            this.smartLabel26.TabIndex = 8;
            this.smartLabel26.Text = "LIMITS";
            this.smartLabel26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel26.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // pnlKLimits
            // 
            this.pnlKLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlKLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlKLimits.Controls.Add(this.lblKMin);
            this.pnlKLimits.Controls.Add(this.lblKMax);
            this.pnlKLimits.Controls.Add(this.smartLabel23);
            this.pnlKLimits.Location = new System.Drawing.Point(676, 341);
            this.pnlKLimits.Name = "pnlKLimits";
            this.pnlKLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlKLimits.TabIndex = 123;
            // 
            // lblKMin
            // 
            this.lblKMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblKMin.BackColor = System.Drawing.Color.Transparent;
            this.lblKMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblKMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblKMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblKMin.BorderColor = System.Drawing.Color.Black;
            this.lblKMin.BorderWidth = 1;
            this.lblKMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblKMin.ForeColor = System.Drawing.Color.Red;
            this.lblKMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblKMin.ForeShadowAmount = 1;
            this.lblKMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblKMin.ForeShadowOffset = 1;
            this.lblKMin.Location = new System.Drawing.Point(5, 46);
            this.lblKMin.Name = "lblKMin";
            this.lblKMin.Size = new System.Drawing.Size(80, 32);
            this.lblKMin.TabIndex = 7;
            this.lblKMin.Text = "0";
            this.lblKMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblKMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblKMax
            // 
            this.lblKMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblKMax.BackColor = System.Drawing.Color.Transparent;
            this.lblKMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblKMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblKMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblKMax.BorderColor = System.Drawing.Color.Black;
            this.lblKMax.BorderWidth = 1;
            this.lblKMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblKMax.ForeColor = System.Drawing.Color.Red;
            this.lblKMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblKMax.ForeShadowAmount = 1;
            this.lblKMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblKMax.ForeShadowOffset = 1;
            this.lblKMax.Location = new System.Drawing.Point(5, 15);
            this.lblKMax.Name = "lblKMax";
            this.lblKMax.Size = new System.Drawing.Size(80, 32);
            this.lblKMax.TabIndex = 9;
            this.lblKMax.Text = "0";
            this.lblKMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblKMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel23
            // 
            this.smartLabel23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel23.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.BorderColor = System.Drawing.Color.Black;
            this.smartLabel23.BorderWidth = 1;
            this.smartLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.Location = new System.Drawing.Point(5, 2);
            this.smartLabel23.Name = "smartLabel23";
            this.smartLabel23.Size = new System.Drawing.Size(80, 14);
            this.smartLabel23.TabIndex = 8;
            this.smartLabel23.Text = "LIMITS";
            this.smartLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel23.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // pnlNLimits
            // 
            this.pnlNLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlNLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlNLimits.Controls.Add(this.lblNMin);
            this.pnlNLimits.Controls.Add(this.lblNMax);
            this.pnlNLimits.Controls.Add(this.smartLabel16);
            this.pnlNLimits.Location = new System.Drawing.Point(676, 241);
            this.pnlNLimits.Name = "pnlNLimits";
            this.pnlNLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlNLimits.TabIndex = 122;
            // 
            // lblNMin
            // 
            this.lblNMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNMin.BackColor = System.Drawing.Color.Transparent;
            this.lblNMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblNMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblNMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblNMin.BorderColor = System.Drawing.Color.Black;
            this.lblNMin.BorderWidth = 1;
            this.lblNMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblNMin.ForeColor = System.Drawing.Color.Red;
            this.lblNMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblNMin.ForeShadowAmount = 1;
            this.lblNMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblNMin.ForeShadowOffset = 1;
            this.lblNMin.Location = new System.Drawing.Point(5, 46);
            this.lblNMin.Name = "lblNMin";
            this.lblNMin.Size = new System.Drawing.Size(80, 32);
            this.lblNMin.TabIndex = 7;
            this.lblNMin.Text = "0";
            this.lblNMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblNMax
            // 
            this.lblNMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNMax.BackColor = System.Drawing.Color.Transparent;
            this.lblNMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblNMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblNMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblNMax.BorderColor = System.Drawing.Color.Black;
            this.lblNMax.BorderWidth = 1;
            this.lblNMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblNMax.ForeColor = System.Drawing.Color.Red;
            this.lblNMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblNMax.ForeShadowAmount = 1;
            this.lblNMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblNMax.ForeShadowOffset = 1;
            this.lblNMax.Location = new System.Drawing.Point(5, 15);
            this.lblNMax.Name = "lblNMax";
            this.lblNMax.Size = new System.Drawing.Size(80, 32);
            this.lblNMax.TabIndex = 9;
            this.lblNMax.Text = "0";
            this.lblNMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel16
            // 
            this.smartLabel16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel16.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel16.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel16.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel16.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel16.BorderColor = System.Drawing.Color.Black;
            this.smartLabel16.BorderWidth = 1;
            this.smartLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel16.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel16.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel16.Location = new System.Drawing.Point(5, 2);
            this.smartLabel16.Name = "smartLabel16";
            this.smartLabel16.Size = new System.Drawing.Size(80, 14);
            this.smartLabel16.TabIndex = 8;
            this.smartLabel16.Text = "LIMITS";
            this.smartLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel16.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // pnlSLimits
            // 
            this.pnlSLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlSLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlSLimits.Controls.Add(this.lblSMin);
            this.pnlSLimits.Controls.Add(this.lblSMax);
            this.pnlSLimits.Controls.Add(this.smartLabel6);
            this.pnlSLimits.Location = new System.Drawing.Point(676, 138);
            this.pnlSLimits.Name = "pnlSLimits";
            this.pnlSLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlSLimits.TabIndex = 121;
            // 
            // lblSMin
            // 
            this.lblSMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSMin.BackColor = System.Drawing.Color.Transparent;
            this.lblSMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblSMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblSMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblSMin.BorderColor = System.Drawing.Color.Black;
            this.lblSMin.BorderWidth = 1;
            this.lblSMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblSMin.ForeColor = System.Drawing.Color.Red;
            this.lblSMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblSMin.ForeShadowAmount = 1;
            this.lblSMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblSMin.ForeShadowOffset = 1;
            this.lblSMin.Location = new System.Drawing.Point(5, 46);
            this.lblSMin.Name = "lblSMin";
            this.lblSMin.Size = new System.Drawing.Size(80, 32);
            this.lblSMin.TabIndex = 7;
            this.lblSMin.Text = "0";
            this.lblSMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblSMax
            // 
            this.lblSMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSMax.BackColor = System.Drawing.Color.Transparent;
            this.lblSMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblSMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblSMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblSMax.BorderColor = System.Drawing.Color.Black;
            this.lblSMax.BorderWidth = 1;
            this.lblSMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblSMax.ForeColor = System.Drawing.Color.Red;
            this.lblSMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblSMax.ForeShadowAmount = 1;
            this.lblSMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblSMax.ForeShadowOffset = 1;
            this.lblSMax.Location = new System.Drawing.Point(5, 15);
            this.lblSMax.Name = "lblSMax";
            this.lblSMax.Size = new System.Drawing.Size(80, 32);
            this.lblSMax.TabIndex = 9;
            this.lblSMax.Text = "0";
            this.lblSMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel6
            // 
            this.smartLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel6.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.BorderColor = System.Drawing.Color.Black;
            this.smartLabel6.BorderWidth = 1;
            this.smartLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.Location = new System.Drawing.Point(5, 2);
            this.smartLabel6.Name = "smartLabel6";
            this.smartLabel6.Size = new System.Drawing.Size(80, 14);
            this.smartLabel6.TabIndex = 8;
            this.smartLabel6.Text = "LIMITS";
            this.smartLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel6.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel19
            // 
            this.smartLabel19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel19.AutoSize = true;
            this.smartLabel19.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel19.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel19.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel19.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel19.BorderColor = System.Drawing.Color.Black;
            this.smartLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel19.ForeColor = System.Drawing.Color.Black;
            this.smartLabel19.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel19.ForeShadowAmount = 1;
            this.smartLabel19.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel19.ForeShadowOffset = 1;
            this.smartLabel19.Location = new System.Drawing.Point(52, 490);
            this.smartLabel19.Name = "smartLabel19";
            this.smartLabel19.Size = new System.Drawing.Size(63, 31);
            this.smartLabel19.TabIndex = 113;
            this.smartLabel19.Text = "(°C)";
            this.smartLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel19.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel20
            // 
            this.smartLabel20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel20.AutoSize = true;
            this.smartLabel20.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel20.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel20.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel20.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel20.BorderColor = System.Drawing.Color.Black;
            this.smartLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel20.ForeColor = System.Drawing.Color.Black;
            this.smartLabel20.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel20.ForeShadowAmount = 1;
            this.smartLabel20.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel20.ForeShadowOffset = 1;
            this.smartLabel20.Location = new System.Drawing.Point(59, 444);
            this.smartLabel20.Name = "smartLabel20";
            this.smartLabel20.Size = new System.Drawing.Size(48, 51);
            this.smartLabel20.TabIndex = 112;
            this.smartLabel20.Tag = "";
            this.smartLabel20.Text = "T";
            this.smartLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel20.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel15
            // 
            this.smartLabel15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel15.AutoSize = true;
            this.smartLabel15.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel15.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel15.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel15.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel15.BorderColor = System.Drawing.Color.Black;
            this.smartLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel15.ForeColor = System.Drawing.Color.Black;
            this.smartLabel15.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel15.ForeShadowAmount = 1;
            this.smartLabel15.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel15.ForeShadowOffset = 1;
            this.smartLabel15.Location = new System.Drawing.Point(52, 387);
            this.smartLabel15.Name = "smartLabel15";
            this.smartLabel15.Size = new System.Drawing.Size(62, 31);
            this.smartLabel15.TabIndex = 111;
            this.smartLabel15.Text = "(/m)";
            this.smartLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel15.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel17
            // 
            this.smartLabel17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel17.AutoSize = true;
            this.smartLabel17.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel17.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel17.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel17.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel17.BorderColor = System.Drawing.Color.Black;
            this.smartLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel17.ForeColor = System.Drawing.Color.Black;
            this.smartLabel17.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel17.ForeShadowAmount = 1;
            this.smartLabel17.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel17.ForeShadowOffset = 1;
            this.smartLabel17.Location = new System.Drawing.Point(58, 344);
            this.smartLabel17.Name = "smartLabel17";
            this.smartLabel17.Size = new System.Drawing.Size(51, 51);
            this.smartLabel17.TabIndex = 110;
            this.smartLabel17.Text = "K";
            this.smartLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel17.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel8
            // 
            this.smartLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel8.AutoSize = true;
            this.smartLabel8.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.BorderColor = System.Drawing.Color.Black;
            this.smartLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel8.ForeColor = System.Drawing.Color.Black;
            this.smartLabel8.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel8.ForeShadowAmount = 1;
            this.smartLabel8.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.ForeShadowOffset = 1;
            this.smartLabel8.Location = new System.Drawing.Point(55, 286);
            this.smartLabel8.Name = "smartLabel8";
            this.smartLabel8.Size = new System.Drawing.Size(56, 31);
            this.smartLabel8.TabIndex = 109;
            this.smartLabel8.Text = "(%)";
            this.smartLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel8.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel12
            // 
            this.smartLabel12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel12.AutoSize = true;
            this.smartLabel12.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel12.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel12.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel12.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel12.BorderColor = System.Drawing.Color.Black;
            this.smartLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel12.ForeColor = System.Drawing.Color.Black;
            this.smartLabel12.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel12.ForeShadowAmount = 1;
            this.smartLabel12.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel12.ForeShadowOffset = 1;
            this.smartLabel12.Location = new System.Drawing.Point(57, 242);
            this.smartLabel12.Name = "smartLabel12";
            this.smartLabel12.Size = new System.Drawing.Size(53, 51);
            this.smartLabel12.TabIndex = 108;
            this.smartLabel12.Text = "N";
            this.smartLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel12.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblT
            // 
            this.lblT.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblT.BackColor = System.Drawing.Color.Transparent;
            this.lblT.BackGradientAngle = 150;
            this.lblT.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblT.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblT.BackShadowColor = System.Drawing.Color.Black;
            this.lblT.BorderColor = System.Drawing.Color.Black;
            this.lblT.BorderCornerRadius = 10;
            this.lblT.BorderWidth = 1;
            this.lblT.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblT.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblT.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblT.ForeShadowAmount = 3;
            this.lblT.ForeShadowColor = System.Drawing.Color.Black;
            this.lblT.ForeShadowOffset = 1;
            this.lblT.Location = new System.Drawing.Point(160, 446);
            this.lblT.Name = "lblT";
            this.lblT.Size = new System.Drawing.Size(190, 80);
            this.lblT.TabIndex = 106;
            this.lblT.Text = "0";
            this.lblT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblT.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblK
            // 
            this.lblK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblK.BackColor = System.Drawing.Color.Transparent;
            this.lblK.BackGradientAngle = 150;
            this.lblK.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblK.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblK.BackShadowColor = System.Drawing.Color.Black;
            this.lblK.BorderColor = System.Drawing.Color.Black;
            this.lblK.BorderCornerRadius = 10;
            this.lblK.BorderWidth = 1;
            this.lblK.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblK.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblK.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblK.ForeShadowAmount = 3;
            this.lblK.ForeShadowColor = System.Drawing.Color.Black;
            this.lblK.ForeShadowOffset = 1;
            this.lblK.Location = new System.Drawing.Point(160, 343);
            this.lblK.Name = "lblK";
            this.lblK.Size = new System.Drawing.Size(190, 80);
            this.lblK.TabIndex = 105;
            this.lblK.Text = "0.00";
            this.lblK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblK.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblN
            // 
            this.lblN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblN.BackColor = System.Drawing.Color.Transparent;
            this.lblN.BackGradientAngle = 150;
            this.lblN.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblN.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblN.BackShadowColor = System.Drawing.Color.Black;
            this.lblN.BorderColor = System.Drawing.Color.Black;
            this.lblN.BorderCornerRadius = 10;
            this.lblN.BorderWidth = 1;
            this.lblN.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblN.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblN.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblN.ForeShadowAmount = 3;
            this.lblN.ForeShadowColor = System.Drawing.Color.Black;
            this.lblN.ForeShadowOffset = 1;
            this.lblN.Location = new System.Drawing.Point(160, 241);
            this.lblN.Name = "lblN";
            this.lblN.Size = new System.Drawing.Size(190, 80);
            this.lblN.TabIndex = 104;
            this.lblN.Text = "0.0";
            this.lblN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblN.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblS
            // 
            this.lblS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblS.BackColor = System.Drawing.Color.Transparent;
            this.lblS.BackGradientAngle = 150;
            this.lblS.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblS.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblS.BackShadowColor = System.Drawing.Color.Black;
            this.lblS.BorderColor = System.Drawing.Color.Black;
            this.lblS.BorderCornerRadius = 10;
            this.lblS.BorderWidth = 1;
            this.lblS.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblS.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblS.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblS.ForeShadowAmount = 3;
            this.lblS.ForeShadowColor = System.Drawing.Color.Black;
            this.lblS.ForeShadowOffset = 1;
            this.lblS.Location = new System.Drawing.Point(160, 140);
            this.lblS.Name = "lblS";
            this.lblS.Size = new System.Drawing.Size(190, 80);
            this.lblS.TabIndex = 94;
            this.lblS.Text = "0";
            this.lblS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblS.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel22
            // 
            this.smartLabel22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel22.AutoSize = true;
            this.smartLabel22.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel22.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel22.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel22.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel22.BorderColor = System.Drawing.Color.Black;
            this.smartLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel22.ForeColor = System.Drawing.Color.Black;
            this.smartLabel22.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel22.ForeShadowAmount = 1;
            this.smartLabel22.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel22.ForeShadowOffset = 1;
            this.smartLabel22.Location = new System.Drawing.Point(24, 153);
            this.smartLabel22.Name = "smartLabel22";
            this.smartLabel22.Size = new System.Drawing.Size(118, 51);
            this.smartLabel22.TabIndex = 103;
            this.smartLabel22.Text = "RPM";
            this.smartLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel22.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel4
            // 
            this.smartLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel4.AutoSize = true;
            this.smartLabel4.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.BorderColor = System.Drawing.Color.Black;
            this.smartLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.5F);
            this.smartLabel4.ForeColor = System.Drawing.Color.Black;
            this.smartLabel4.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel4.ForeShadowAmount = 1;
            this.smartLabel4.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.ForeShadowOffset = 1;
            this.smartLabel4.Location = new System.Drawing.Point(199, 89);
            this.smartLabel4.Name = "smartLabel4";
            this.smartLabel4.Size = new System.Drawing.Size(112, 30);
            this.smartLabel4.TabIndex = 102;
            this.smartLabel4.Text = "ACTIVE ";
            this.smartLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel4.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblMaxK
            // 
            this.lblMaxK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblMaxK.BackColor = System.Drawing.Color.Transparent;
            this.lblMaxK.BackGradientAngle = 150;
            this.lblMaxK.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblMaxK.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblMaxK.BackShadowColor = System.Drawing.Color.Black;
            this.lblMaxK.BorderColor = System.Drawing.Color.Black;
            this.lblMaxK.BorderCornerRadius = 10;
            this.lblMaxK.BorderWidth = 1;
            this.lblMaxK.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblMaxK.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblMaxK.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblMaxK.ForeShadowAmount = 3;
            this.lblMaxK.ForeShadowColor = System.Drawing.Color.Black;
            this.lblMaxK.ForeShadowOffset = 1;
            this.lblMaxK.Location = new System.Drawing.Point(427, 341);
            this.lblMaxK.Name = "lblMaxK";
            this.lblMaxK.Size = new System.Drawing.Size(190, 80);
            this.lblMaxK.TabIndex = 143;
            this.lblMaxK.Text = "0.00";
            this.lblMaxK.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblMaxK.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblMaxN
            // 
            this.lblMaxN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblMaxN.BackColor = System.Drawing.Color.Transparent;
            this.lblMaxN.BackGradientAngle = 150;
            this.lblMaxN.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblMaxN.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblMaxN.BackShadowColor = System.Drawing.Color.Black;
            this.lblMaxN.BorderColor = System.Drawing.Color.Black;
            this.lblMaxN.BorderCornerRadius = 10;
            this.lblMaxN.BorderWidth = 1;
            this.lblMaxN.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblMaxN.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblMaxN.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblMaxN.ForeShadowAmount = 3;
            this.lblMaxN.ForeShadowColor = System.Drawing.Color.Black;
            this.lblMaxN.ForeShadowOffset = 1;
            this.lblMaxN.Location = new System.Drawing.Point(427, 239);
            this.lblMaxN.Name = "lblMaxN";
            this.lblMaxN.Size = new System.Drawing.Size(190, 80);
            this.lblMaxN.TabIndex = 142;
            this.lblMaxN.Text = "0.0";
            this.lblMaxN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblMaxN.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblMaxS
            // 
            this.lblMaxS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblMaxS.BackColor = System.Drawing.Color.Transparent;
            this.lblMaxS.BackGradientAngle = 150;
            this.lblMaxS.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblMaxS.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblMaxS.BackShadowColor = System.Drawing.Color.Black;
            this.lblMaxS.BorderColor = System.Drawing.Color.Black;
            this.lblMaxS.BorderCornerRadius = 10;
            this.lblMaxS.BorderWidth = 1;
            this.lblMaxS.Font = new System.Drawing.Font("Microsoft Sans Serif", 47F, System.Drawing.FontStyle.Bold);
            this.lblMaxS.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblMaxS.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblMaxS.ForeShadowAmount = 3;
            this.lblMaxS.ForeShadowColor = System.Drawing.Color.Black;
            this.lblMaxS.ForeShadowOffset = 1;
            this.lblMaxS.Location = new System.Drawing.Point(427, 138);
            this.lblMaxS.Name = "lblMaxS";
            this.lblMaxS.Size = new System.Drawing.Size(190, 80);
            this.lblMaxS.TabIndex = 140;
            this.lblMaxS.Text = "0";
            this.lblMaxS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblMaxS.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel11
            // 
            this.smartLabel11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel11.AutoSize = true;
            this.smartLabel11.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel11.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel11.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel11.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel11.BorderColor = System.Drawing.Color.Black;
            this.smartLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.5F);
            this.smartLabel11.ForeColor = System.Drawing.Color.Black;
            this.smartLabel11.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel11.ForeShadowAmount = 1;
            this.smartLabel11.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel11.ForeShadowOffset = 1;
            this.smartLabel11.Location = new System.Drawing.Point(451, 87);
            this.smartLabel11.Name = "smartLabel11";
            this.smartLabel11.Size = new System.Drawing.Size(145, 30);
            this.smartLabel11.TabIndex = 141;
            this.smartLabel11.Text = "MAXIMUM ";
            this.smartLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel11.TextBorderColor = System.Drawing.Color.Black;
            // 
            // btnClearMax
            // 
            this.btnClearMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearMax.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearMax.ForeColor = System.Drawing.Color.Black;
            this.btnClearMax.Location = new System.Drawing.Point(374, 648);
            this.btnClearMax.Name = "btnClearMax";
            this.btnClearMax.Size = new System.Drawing.Size(145, 48);
            this.btnClearMax.TabIndex = 144;
            this.btnClearMax.Text = "CLEAR MAX";
            this.btnClearMax.UseVisualStyleBackColor = true;
            this.btnClearMax.Click += new System.EventHandler(this.btnClearMax_Click);
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblDate.BackShadowColor = System.Drawing.Color.Black;
            this.lblDate.BorderColor = System.Drawing.Color.Black;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 31F);
            this.lblDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeShadowAmount = 2;
            this.lblDate.ForeShadowColor = System.Drawing.Color.Black;
            this.lblDate.ForeShadowOffset = 1;
            this.lblDate.Location = new System.Drawing.Point(624, 47);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(228, 48);
            this.lblDate.TabIndex = 148;
            this.lblDate.Text = "01/01/2013";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDate.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            this.lblDate.Visible = false;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTime.BackShadowColor = System.Drawing.Color.Black;
            this.lblTime.BorderColor = System.Drawing.Color.Black;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 29.5F);
            this.lblTime.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeShadowAmount = 2;
            this.lblTime.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTime.ForeShadowOffset = 1;
            this.lblTime.Location = new System.Drawing.Point(616, 90);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(245, 46);
            this.lblTime.TabIndex = 147;
            this.lblTime.Text = "00:00:00 AM";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTime.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            this.lblTime.Visible = false;
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.AutoSize = true;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.5F);
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(209, 9);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(506, 40);
            this.smartLabel10.TabIndex = 146;
            this.smartLabel10.Text = "OPACITY - REAL TIME TEST ";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel10.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // btnStart
            // 
            this.btnStart.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Black;
            this.btnStart.Location = new System.Drawing.Point(542, 648);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(117, 48);
            this.btnStart.TabIndex = 151;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cmbxCaption
            // 
            this.cmbxCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxCaption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxCaption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxCaption.FormattingEnabled = true;
            this.cmbxCaption.Items.AddRange(new object[] {
            "",
            "With AC Load",
            "Without AC Load",
            "With Acceleration",
            "Before Adjustment",
            "After Adjustment"});
            this.cmbxCaption.Location = new System.Drawing.Point(28, 597);
            this.cmbxCaption.Name = "cmbxCaption";
            this.cmbxCaption.Size = new System.Drawing.Size(311, 32);
            this.cmbxCaption.TabIndex = 152;
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.Location = new System.Drawing.Point(28, 574);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(119, 20);
            this.smartLabel1.TabIndex = 153;
            this.smartLabel1.Text = "Test Condition: ";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel1.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // pbrSpped
            // 
            this.pbrSpped.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbrSpped.BackColor = System.Drawing.Color.Transparent;
            this.pbrSpped.BorderColor = System.Drawing.Color.Black;
            this.pbrSpped.BorderDisplay = true;
            this.pbrSpped.BorderSize = 1;
            this.pbrSpped.Color1 = System.Drawing.Color.DeepSkyBlue;
            this.pbrSpped.Color2 = System.Drawing.Color.Blue;
            this.pbrSpped.FillingMethod = AdvancedProgressBarControl.AdvancedProgressBar.FillingType.BottomToTop;
            this.pbrSpped.Font = new System.Drawing.Font("Tahoma", 8F);
            this.pbrSpped.ForeColor = System.Drawing.Color.Green;
            this.pbrSpped.GradientMethod = AdvancedProgressBarControl.AdvancedProgressBar.FillDirection.Vertical;
            this.pbrSpped.Location = new System.Drawing.Point(867, 12);
            this.pbrSpped.Name = "pbrSpped";
            this.pbrSpped.PercentDisplay = false;
            this.pbrSpped.Position = 0;
            this.pbrSpped.ProgressBarBackColor = System.Drawing.Color.Gainsboro;
            this.pbrSpped.Shape = AdvancedProgressBarControl.AdvancedProgressBar.ProgressBarShape.Rectangle;
            this.pbrSpped.Size = new System.Drawing.Size(47, 706);
            this.pbrSpped.TabIndex = 232;
            // 
            // smartLabel35
            // 
            this.smartLabel35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel35.AutoSize = true;
            this.smartLabel35.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel35.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel35.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel35.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel35.BorderColor = System.Drawing.Color.Black;
            this.smartLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel35.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel35.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel35.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel35.Location = new System.Drawing.Point(923, 610);
            this.smartLabel35.Name = "smartLabel35";
            this.smartLabel35.Size = new System.Drawing.Size(61, 25);
            this.smartLabel35.TabIndex = 231;
            this.smartLabel35.Text = "- 500";
            this.smartLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel35.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel33
            // 
            this.smartLabel33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel33.AutoSize = true;
            this.smartLabel33.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel33.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel33.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel33.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel33.BorderColor = System.Drawing.Color.Black;
            this.smartLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel33.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel33.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel33.Location = new System.Drawing.Point(923, 223);
            this.smartLabel33.Name = "smartLabel33";
            this.smartLabel33.Size = new System.Drawing.Size(19, 25);
            this.smartLabel33.TabIndex = 230;
            this.smartLabel33.Text = "-";
            this.smartLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel33.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel32
            // 
            this.smartLabel32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel32.AutoSize = true;
            this.smartLabel32.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel32.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel32.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel32.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel32.BorderColor = System.Drawing.Color.Black;
            this.smartLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel32.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel32.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel32.Location = new System.Drawing.Point(923, 567);
            this.smartLabel32.Name = "smartLabel32";
            this.smartLabel32.Size = new System.Drawing.Size(19, 25);
            this.smartLabel32.TabIndex = 229;
            this.smartLabel32.Text = "-";
            this.smartLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel32.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel31
            // 
            this.smartLabel31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel31.AutoSize = true;
            this.smartLabel31.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel31.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel31.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel31.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel31.BorderColor = System.Drawing.Color.Black;
            this.smartLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel31.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel31.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel31.Location = new System.Drawing.Point(923, 524);
            this.smartLabel31.Name = "smartLabel31";
            this.smartLabel31.Size = new System.Drawing.Size(73, 25);
            this.smartLabel31.TabIndex = 228;
            this.smartLabel31.Text = "- 1000";
            this.smartLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel31.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel30
            // 
            this.smartLabel30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel30.AutoSize = true;
            this.smartLabel30.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel30.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel30.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel30.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel30.BorderColor = System.Drawing.Color.Black;
            this.smartLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel30.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel30.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel30.Location = new System.Drawing.Point(923, 481);
            this.smartLabel30.Name = "smartLabel30";
            this.smartLabel30.Size = new System.Drawing.Size(19, 25);
            this.smartLabel30.TabIndex = 227;
            this.smartLabel30.Text = "-";
            this.smartLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel30.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel29
            // 
            this.smartLabel29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel29.AutoSize = true;
            this.smartLabel29.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel29.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel29.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel29.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel29.BorderColor = System.Drawing.Color.Black;
            this.smartLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel29.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel29.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel29.Location = new System.Drawing.Point(923, 438);
            this.smartLabel29.Name = "smartLabel29";
            this.smartLabel29.Size = new System.Drawing.Size(73, 25);
            this.smartLabel29.TabIndex = 226;
            this.smartLabel29.Text = "- 1500";
            this.smartLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel29.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel28
            // 
            this.smartLabel28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel28.AutoSize = true;
            this.smartLabel28.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel28.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel28.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel28.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel28.BorderColor = System.Drawing.Color.Black;
            this.smartLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel28.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel28.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel28.Location = new System.Drawing.Point(923, 395);
            this.smartLabel28.Name = "smartLabel28";
            this.smartLabel28.Size = new System.Drawing.Size(19, 25);
            this.smartLabel28.TabIndex = 225;
            this.smartLabel28.Text = "-";
            this.smartLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel28.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel25
            // 
            this.smartLabel25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel25.AutoSize = true;
            this.smartLabel25.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel25.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel25.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel25.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel25.BorderColor = System.Drawing.Color.Black;
            this.smartLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel25.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel25.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel25.Location = new System.Drawing.Point(923, 352);
            this.smartLabel25.Name = "smartLabel25";
            this.smartLabel25.Size = new System.Drawing.Size(73, 25);
            this.smartLabel25.TabIndex = 224;
            this.smartLabel25.Text = "- 2000";
            this.smartLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel25.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel24
            // 
            this.smartLabel24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel24.AutoSize = true;
            this.smartLabel24.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel24.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel24.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel24.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel24.BorderColor = System.Drawing.Color.Black;
            this.smartLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel24.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel24.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel24.Location = new System.Drawing.Point(923, 309);
            this.smartLabel24.Name = "smartLabel24";
            this.smartLabel24.Size = new System.Drawing.Size(19, 25);
            this.smartLabel24.TabIndex = 223;
            this.smartLabel24.Text = "-";
            this.smartLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel24.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel21
            // 
            this.smartLabel21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel21.AutoSize = true;
            this.smartLabel21.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel21.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel21.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel21.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel21.BorderColor = System.Drawing.Color.Black;
            this.smartLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel21.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel21.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel21.Location = new System.Drawing.Point(923, 266);
            this.smartLabel21.Name = "smartLabel21";
            this.smartLabel21.Size = new System.Drawing.Size(73, 25);
            this.smartLabel21.TabIndex = 222;
            this.smartLabel21.Text = "- 2500";
            this.smartLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel21.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel18
            // 
            this.smartLabel18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel18.AutoSize = true;
            this.smartLabel18.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel18.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel18.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel18.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel18.BorderColor = System.Drawing.Color.Black;
            this.smartLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel18.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel18.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel18.Location = new System.Drawing.Point(923, 180);
            this.smartLabel18.Name = "smartLabel18";
            this.smartLabel18.Size = new System.Drawing.Size(73, 25);
            this.smartLabel18.TabIndex = 221;
            this.smartLabel18.Text = "- 3000";
            this.smartLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel18.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // btnClearCapture
            // 
            this.btnClearCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClearCapture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearCapture.Location = new System.Drawing.Point(28, 648);
            this.btnClearCapture.Name = "btnClearCapture";
            this.btnClearCapture.Size = new System.Drawing.Size(133, 48);
            this.btnClearCapture.TabIndex = 233;
            this.btnClearCapture.Text = "CLEAR CAP";
            this.btnClearCapture.UseVisualStyleBackColor = true;
            this.btnClearCapture.Click += new System.EventHandler(this.btnClearCapture_Click);
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.Location = new System.Drawing.Point(923, 137);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(19, 25);
            this.smartLabel2.TabIndex = 234;
            this.smartLabel2.Text = "-";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel2.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel3
            // 
            this.smartLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel3.AutoSize = true;
            this.smartLabel3.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.BorderColor = System.Drawing.Color.Black;
            this.smartLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.Location = new System.Drawing.Point(923, 94);
            this.smartLabel3.Name = "smartLabel3";
            this.smartLabel3.Size = new System.Drawing.Size(73, 25);
            this.smartLabel3.TabIndex = 235;
            this.smartLabel3.Text = "- 3500";
            this.smartLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel3.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel5
            // 
            this.smartLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel5.AutoSize = true;
            this.smartLabel5.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.BorderColor = System.Drawing.Color.Black;
            this.smartLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.Location = new System.Drawing.Point(923, 6);
            this.smartLabel5.Name = "smartLabel5";
            this.smartLabel5.Size = new System.Drawing.Size(73, 25);
            this.smartLabel5.TabIndex = 236;
            this.smartLabel5.Text = "- 4000";
            this.smartLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel5.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel7
            // 
            this.smartLabel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel7.AutoSize = true;
            this.smartLabel7.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.BorderColor = System.Drawing.Color.Black;
            this.smartLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel7.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel7.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.Location = new System.Drawing.Point(923, 653);
            this.smartLabel7.Name = "smartLabel7";
            this.smartLabel7.Size = new System.Drawing.Size(19, 25);
            this.smartLabel7.TabIndex = 237;
            this.smartLabel7.Text = "-";
            this.smartLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel7.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel13
            // 
            this.smartLabel13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel13.AutoSize = true;
            this.smartLabel13.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel13.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel13.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel13.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel13.BorderColor = System.Drawing.Color.Black;
            this.smartLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel13.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel13.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel13.Location = new System.Drawing.Point(923, 698);
            this.smartLabel13.Name = "smartLabel13";
            this.smartLabel13.Size = new System.Drawing.Size(37, 25);
            this.smartLabel13.TabIndex = 238;
            this.smartLabel13.Text = "- 0";
            this.smartLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel13.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel14
            // 
            this.smartLabel14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel14.AutoSize = true;
            this.smartLabel14.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel14.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel14.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel14.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel14.BorderColor = System.Drawing.Color.Black;
            this.smartLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel14.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel14.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel14.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel14.Location = new System.Drawing.Point(923, 51);
            this.smartLabel14.Name = "smartLabel14";
            this.smartLabel14.Size = new System.Drawing.Size(19, 25);
            this.smartLabel14.TabIndex = 239;
            this.smartLabel14.Text = "-";
            this.smartLabel14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel14.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblUsbConnected
            // 
            this.lblUsbConnected.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUsbConnected.BackColor = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.BorderColor = System.Drawing.Color.Black;
            this.lblUsbConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblUsbConnected.ForeColor = System.Drawing.Color.White;
            this.lblUsbConnected.ForeGradientColor2 = System.Drawing.Color.White;
            this.lblUsbConnected.ForeShadowAmount = 1;
            this.lblUsbConnected.ForeShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.ForeShadowOffset = 1;
            this.lblUsbConnected.Location = new System.Drawing.Point(383, 605);
            this.lblUsbConnected.Name = "lblUsbConnected";
            this.lblUsbConnected.Size = new System.Drawing.Size(136, 24);
            this.lblUsbConnected.TabIndex = 261;
            this.lblUsbConnected.Text = " Not Conected";
            this.lblUsbConnected.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUsbConnected.TextBorderColor = System.Drawing.Color.Black;
            // 
            // frmOpacityRealTimeTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1018, 730);
            this.Controls.Add(this.lblUsbConnected);
            this.Controls.Add(this.smartLabel14);
            this.Controls.Add(this.smartLabel13);
            this.Controls.Add(this.smartLabel7);
            this.Controls.Add(this.smartLabel5);
            this.Controls.Add(this.smartLabel3);
            this.Controls.Add(this.smartLabel2);
            this.Controls.Add(this.btnClearCapture);
            this.Controls.Add(this.pbrSpped);
            this.Controls.Add(this.smartLabel35);
            this.Controls.Add(this.smartLabel33);
            this.Controls.Add(this.smartLabel32);
            this.Controls.Add(this.smartLabel31);
            this.Controls.Add(this.smartLabel30);
            this.Controls.Add(this.smartLabel29);
            this.Controls.Add(this.smartLabel28);
            this.Controls.Add(this.smartLabel25);
            this.Controls.Add(this.smartLabel24);
            this.Controls.Add(this.smartLabel21);
            this.Controls.Add(this.smartLabel18);
            this.Controls.Add(this.smartLabel1);
            this.Controls.Add(this.cmbxCaption);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.smartLabel10);
            this.Controls.Add(this.btnClearMax);
            this.Controls.Add(this.lblMaxK);
            this.Controls.Add(this.lblMaxN);
            this.Controls.Add(this.lblMaxS);
            this.Controls.Add(this.smartLabel11);
            this.Controls.Add(this.smartLabel22);
            this.Controls.Add(this.lblSpeed);
            this.Controls.Add(this.smartLabel9);
            this.Controls.Add(this.pnlTLimits);
            this.Controls.Add(this.pnlKLimits);
            this.Controls.Add(this.pnlNLimits);
            this.Controls.Add(this.pnlSLimits);
            this.Controls.Add(this.smartLabel19);
            this.Controls.Add(this.smartLabel20);
            this.Controls.Add(this.smartLabel15);
            this.Controls.Add(this.smartLabel17);
            this.Controls.Add(this.smartLabel8);
            this.Controls.Add(this.smartLabel12);
            this.Controls.Add(this.lblT);
            this.Controls.Add(this.lblK);
            this.Controls.Add(this.lblN);
            this.Controls.Add(this.lblS);
            this.Controls.Add(this.smartLabel4);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "frmOpacityRealTimeTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlTLimits.ResumeLayout(false);
            this.pnlKLimits.ResumeLayout(false);
            this.pnlNLimits.ResumeLayout(false);
            this.pnlSLimits.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Timer tmrTime;
        private System.Windows.Forms.Button btnCapture;
        private InterfaceLab.WinForm.Controls.SmartLabel lblSpeed;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel9;
        private System.Windows.Forms.Panel pnlTLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel26;
        private System.Windows.Forms.Panel pnlKLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblKMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblKMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel23;
        private System.Windows.Forms.Panel pnlNLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblNMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblNMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel16;
        private System.Windows.Forms.Panel pnlSLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblSMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblSMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel6;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel19;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel20;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel15;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel17;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel8;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel12;
        private InterfaceLab.WinForm.Controls.SmartLabel lblT;
        private InterfaceLab.WinForm.Controls.SmartLabel lblK;
        private InterfaceLab.WinForm.Controls.SmartLabel lblN;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel22;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel4;
        private InterfaceLab.WinForm.Controls.SmartLabel lblMaxK;
        private InterfaceLab.WinForm.Controls.SmartLabel lblMaxN;
        private InterfaceLab.WinForm.Controls.SmartLabel lblMaxS;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel11;
        private System.Windows.Forms.Button btnClearMax;
        private InterfaceLab.WinForm.Controls.SmartLabel lblDate;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTime;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ComboBox cmbxCaption;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
        private AdvancedProgressBarControl.AdvancedProgressBar pbrSpped;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel35;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel33;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel32;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel31;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel30;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel29;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel28;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel25;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel24;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel21;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel18;
        private System.Windows.Forms.Button btnClearCapture;
        private InterfaceLab.WinForm.Controls.SmartLabel lblS;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel3;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel5;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel7;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel13;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel14;
        private InterfaceLab.WinForm.Controls.SmartLabel lblUsbConnected;
    }
}