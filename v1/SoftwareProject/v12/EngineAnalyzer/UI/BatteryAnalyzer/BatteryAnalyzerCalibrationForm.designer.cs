﻿namespace EngineAnalyzer.UI.BatteryAnalyzer
{
    partial class BatteryAnalyzerCalibrationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.voltageLabel = new System.Windows.Forms.Label();
            this.currentLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.voltageConstantlabel = new System.Windows.Forms.Label();
            this.voltageOffsetlabel = new System.Windows.Forms.Label();
            this.currentOffsetLabel = new System.Windows.Forms.Label();
            this.currentConstantLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deviceConnectedLabel = new System.Windows.Forms.Label();
            this.setVoltageConstantButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.voltageValueTextBox = new System.Windows.Forms.TextBox();
            this.calibrationGroupBox = new System.Windows.Forms.GroupBox();
            this.setVoltageOffsetButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.setCurrentConstantButton = new System.Windows.Forms.Button();
            this.setCurrentOffsetButton = new System.Windows.Forms.Button();
            this.currentValueTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.currentRawDataLabel = new System.Windows.Forms.Label();
            this.voltageRawDataLabel = new System.Windows.Forms.Label();
            this.label1112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.voltageSetConstantCheckBox = new System.Windows.Forms.CheckBox();
            this.currentSetConstantCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.calibrationGroupBox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Voltage:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Current:";
            // 
            // voltageLabel
            // 
            this.voltageLabel.AutoSize = true;
            this.voltageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltageLabel.Location = new System.Drawing.Point(7, 44);
            this.voltageLabel.Name = "voltageLabel";
            this.voltageLabel.Size = new System.Drawing.Size(51, 20);
            this.voltageLabel.TabIndex = 2;
            this.voltageLabel.Text = "0.0 V";
            // 
            // currentLabel
            // 
            this.currentLabel.AutoSize = true;
            this.currentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentLabel.Location = new System.Drawing.Point(7, 102);
            this.currentLabel.Name = "currentLabel";
            this.currentLabel.Size = new System.Drawing.Size(51, 20);
            this.currentLabel.TabIndex = 3;
            this.currentLabel.Text = "0.0 A";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(200, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Constant:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(359, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Offset:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(359, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Offset:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(200, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Constant:";
            // 
            // voltageConstantlabel
            // 
            this.voltageConstantlabel.AutoSize = true;
            this.voltageConstantlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltageConstantlabel.Location = new System.Drawing.Point(200, 44);
            this.voltageConstantlabel.Name = "voltageConstantlabel";
            this.voltageConstantlabel.Size = new System.Drawing.Size(84, 20);
            this.voltageConstantlabel.TabIndex = 8;
            this.voltageConstantlabel.Text = "1.000000";
            // 
            // voltageOffsetlabel
            // 
            this.voltageOffsetlabel.AutoSize = true;
            this.voltageOffsetlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltageOffsetlabel.Location = new System.Drawing.Point(359, 44);
            this.voltageOffsetlabel.Name = "voltageOffsetlabel";
            this.voltageOffsetlabel.Size = new System.Drawing.Size(54, 20);
            this.voltageOffsetlabel.TabIndex = 9;
            this.voltageOffsetlabel.Text = "0.000";
            // 
            // currentOffsetLabel
            // 
            this.currentOffsetLabel.AutoSize = true;
            this.currentOffsetLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentOffsetLabel.Location = new System.Drawing.Point(359, 102);
            this.currentOffsetLabel.Name = "currentOffsetLabel";
            this.currentOffsetLabel.Size = new System.Drawing.Size(54, 20);
            this.currentOffsetLabel.TabIndex = 11;
            this.currentOffsetLabel.Text = "0.000";
            // 
            // currentConstantLabel
            // 
            this.currentConstantLabel.AutoSize = true;
            this.currentConstantLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentConstantLabel.Location = new System.Drawing.Point(200, 102);
            this.currentConstantLabel.Name = "currentConstantLabel";
            this.currentConstantLabel.Size = new System.Drawing.Size(84, 20);
            this.currentConstantLabel.TabIndex = 10;
            this.currentConstantLabel.Text = "1.000000";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.currentRawDataLabel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.currentOffsetLabel);
            this.groupBox1.Controls.Add(this.voltageRawDataLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.currentConstantLabel);
            this.groupBox1.Controls.Add(this.label1112);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label111);
            this.groupBox1.Controls.Add(this.voltageOffsetlabel);
            this.groupBox1.Controls.Add(this.voltageLabel);
            this.groupBox1.Controls.Add(this.voltageConstantlabel);
            this.groupBox1.Controls.Add(this.currentLabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(466, 130);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Readings";
            // 
            // deviceConnectedLabel
            // 
            this.deviceConnectedLabel.AutoSize = true;
            this.deviceConnectedLabel.Location = new System.Drawing.Point(12, 319);
            this.deviceConnectedLabel.Name = "deviceConnectedLabel";
            this.deviceConnectedLabel.Size = new System.Drawing.Size(79, 13);
            this.deviceConnectedLabel.TabIndex = 13;
            this.deviceConnectedLabel.Text = "Not Connected";
            // 
            // setVoltageConstantButton
            // 
            this.setVoltageConstantButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.setVoltageConstantButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setVoltageConstantButton.ForeColor = System.Drawing.Color.Red;
            this.setVoltageConstantButton.Location = new System.Drawing.Point(81, 21);
            this.setVoltageConstantButton.Name = "setVoltageConstantButton";
            this.setVoltageConstantButton.Size = new System.Drawing.Size(159, 43);
            this.setVoltageConstantButton.TabIndex = 15;
            this.setVoltageConstantButton.Text = "Set";
            this.setVoltageConstantButton.UseVisualStyleBackColor = true;
            this.setVoltageConstantButton.Click += new System.EventHandler(this.setVoltageConstantButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(246, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 16);
            this.label7.TabIndex = 17;
            this.label7.Text = "Value:";
            // 
            // voltageValueTextBox
            // 
            this.voltageValueTextBox.Location = new System.Drawing.Point(249, 41);
            this.voltageValueTextBox.Name = "voltageValueTextBox";
            this.voltageValueTextBox.Size = new System.Drawing.Size(100, 20);
            this.voltageValueTextBox.TabIndex = 18;
            // 
            // calibrationGroupBox
            // 
            this.calibrationGroupBox.Controls.Add(this.voltageSetConstantCheckBox);
            this.calibrationGroupBox.Controls.Add(this.setVoltageOffsetButton);
            this.calibrationGroupBox.Controls.Add(this.setVoltageConstantButton);
            this.calibrationGroupBox.Controls.Add(this.voltageValueTextBox);
            this.calibrationGroupBox.Controls.Add(this.label7);
            this.calibrationGroupBox.Location = new System.Drawing.Point(12, 148);
            this.calibrationGroupBox.Name = "calibrationGroupBox";
            this.calibrationGroupBox.Size = new System.Drawing.Size(466, 77);
            this.calibrationGroupBox.TabIndex = 19;
            this.calibrationGroupBox.TabStop = false;
            this.calibrationGroupBox.Text = "Voltage Calibration";
            // 
            // setVoltageOffsetButton
            // 
            this.setVoltageOffsetButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.setVoltageOffsetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setVoltageOffsetButton.ForeColor = System.Drawing.Color.Red;
            this.setVoltageOffsetButton.Location = new System.Drawing.Point(8, 21);
            this.setVoltageOffsetButton.Name = "setVoltageOffsetButton";
            this.setVoltageOffsetButton.Size = new System.Drawing.Size(67, 43);
            this.setVoltageOffsetButton.TabIndex = 19;
            this.setVoltageOffsetButton.Text = "Offset";
            this.setVoltageOffsetButton.UseVisualStyleBackColor = true;
            this.setVoltageOffsetButton.Click += new System.EventHandler(this.setVoltageOffsetButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.currentSetConstantCheckBox);
            this.groupBox2.Controls.Add(this.setCurrentConstantButton);
            this.groupBox2.Controls.Add(this.setCurrentOffsetButton);
            this.groupBox2.Controls.Add(this.currentValueTextBox);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 231);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(466, 77);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Current Calibration";
            // 
            // setCurrentConstantButton
            // 
            this.setCurrentConstantButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.setCurrentConstantButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setCurrentConstantButton.ForeColor = System.Drawing.Color.Red;
            this.setCurrentConstantButton.Location = new System.Drawing.Point(81, 22);
            this.setCurrentConstantButton.Name = "setCurrentConstantButton";
            this.setCurrentConstantButton.Size = new System.Drawing.Size(159, 43);
            this.setCurrentConstantButton.TabIndex = 19;
            this.setCurrentConstantButton.Text = "Set";
            this.setCurrentConstantButton.UseVisualStyleBackColor = true;
            this.setCurrentConstantButton.Click += new System.EventHandler(this.setCurrentConstantButton_Click);
            // 
            // setCurrentOffsetButton
            // 
            this.setCurrentOffsetButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.setCurrentOffsetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setCurrentOffsetButton.ForeColor = System.Drawing.Color.Red;
            this.setCurrentOffsetButton.Location = new System.Drawing.Point(8, 22);
            this.setCurrentOffsetButton.Name = "setCurrentOffsetButton";
            this.setCurrentOffsetButton.Size = new System.Drawing.Size(67, 43);
            this.setCurrentOffsetButton.TabIndex = 15;
            this.setCurrentOffsetButton.Text = "Offset";
            this.setCurrentOffsetButton.UseVisualStyleBackColor = true;
            this.setCurrentOffsetButton.Click += new System.EventHandler(this.setCurrentOffsetButton_Click);
            // 
            // currentValueTextBox
            // 
            this.currentValueTextBox.Location = new System.Drawing.Point(249, 41);
            this.currentValueTextBox.Name = "currentValueTextBox";
            this.currentValueTextBox.Size = new System.Drawing.Size(100, 20);
            this.currentValueTextBox.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(246, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Value:";
            // 
            // currentRawDataLabel
            // 
            this.currentRawDataLabel.AutoSize = true;
            this.currentRawDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentRawDataLabel.Location = new System.Drawing.Point(98, 102);
            this.currentRawDataLabel.Name = "currentRawDataLabel";
            this.currentRawDataLabel.Size = new System.Drawing.Size(19, 20);
            this.currentRawDataLabel.TabIndex = 15;
            this.currentRawDataLabel.Text = "0";
            // 
            // voltageRawDataLabel
            // 
            this.voltageRawDataLabel.AutoSize = true;
            this.voltageRawDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.voltageRawDataLabel.Location = new System.Drawing.Point(98, 44);
            this.voltageRawDataLabel.Name = "voltageRawDataLabel";
            this.voltageRawDataLabel.Size = new System.Drawing.Size(19, 20);
            this.voltageRawDataLabel.TabIndex = 14;
            this.voltageRawDataLabel.Text = "0";
            // 
            // label1112
            // 
            this.label1112.AutoSize = true;
            this.label1112.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1112.Location = new System.Drawing.Point(98, 79);
            this.label1112.Name = "label1112";
            this.label1112.Size = new System.Drawing.Size(84, 20);
            this.label1112.TabIndex = 13;
            this.label1112.Text = "Raw Data:";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(98, 21);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(84, 20);
            this.label111.TabIndex = 12;
            this.label111.Text = "Raw Data:";
            // 
            // voltageSetConstantCheckBox
            // 
            this.voltageSetConstantCheckBox.AutoSize = true;
            this.voltageSetConstantCheckBox.Location = new System.Drawing.Point(363, 43);
            this.voltageSetConstantCheckBox.Name = "voltageSetConstantCheckBox";
            this.voltageSetConstantCheckBox.Size = new System.Drawing.Size(87, 17);
            this.voltageSetConstantCheckBox.TabIndex = 20;
            this.voltageSetConstantCheckBox.Text = "Set Constant";
            this.voltageSetConstantCheckBox.UseVisualStyleBackColor = true;
            // 
            // currentSetConstantCheckBox
            // 
            this.currentSetConstantCheckBox.AutoSize = true;
            this.currentSetConstantCheckBox.Location = new System.Drawing.Point(363, 43);
            this.currentSetConstantCheckBox.Name = "currentSetConstantCheckBox";
            this.currentSetConstantCheckBox.Size = new System.Drawing.Size(87, 17);
            this.currentSetConstantCheckBox.TabIndex = 21;
            this.currentSetConstantCheckBox.Text = "Set Constant";
            this.currentSetConstantCheckBox.UseVisualStyleBackColor = true;
            // 
            // BatteryAnalyzerCalibrationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 343);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.calibrationGroupBox);
            this.Controls.Add(this.deviceConnectedLabel);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BatteryAnalyzerCalibrationForm";
            this.Text = "CalibrationForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.calibrationGroupBox.ResumeLayout(false);
            this.calibrationGroupBox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label voltageLabel;
        private System.Windows.Forms.Label currentLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label voltageConstantlabel;
        private System.Windows.Forms.Label voltageOffsetlabel;
        private System.Windows.Forms.Label currentOffsetLabel;
        private System.Windows.Forms.Label currentConstantLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label deviceConnectedLabel;
        private System.Windows.Forms.Button setVoltageConstantButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox voltageValueTextBox;
        private System.Windows.Forms.GroupBox calibrationGroupBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button setCurrentOffsetButton;
        private System.Windows.Forms.TextBox currentValueTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button setCurrentConstantButton;
        private System.Windows.Forms.Button setVoltageOffsetButton;
        private System.Windows.Forms.Label currentRawDataLabel;
        private System.Windows.Forms.Label voltageRawDataLabel;
        private System.Windows.Forms.Label label1112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.CheckBox voltageSetConstantCheckBox;
        private System.Windows.Forms.CheckBox currentSetConstantCheckBox;
    }
}