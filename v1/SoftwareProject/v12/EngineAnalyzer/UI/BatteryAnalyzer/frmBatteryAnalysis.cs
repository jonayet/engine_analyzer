﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;
using INFRA.USB;
using RTF;
using ZedGraph;

// ReSharper disable CSharpWarnings::CS1591
// ReSharper disable SuggestUseVarKeywordEvident
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local
// ReSharper disable RedundantExplicitArrayCreation
// ReSharper disable RedundantDefaultFieldInitializer
// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable RedundantDelegateCreation
// ReSharper disable LocalizableElement
// ReSharper disable SpecifyACultureInStringConversionExplicitly

namespace EngineAnalyzer.UI.BatteryAnalyzer
{
    public partial class frmBatteryAnalysis : Form
    {
        #region local declarations
        private IPointListEdit plRPM;
        private IPointListEdit plVoltage;
        private IPointListEdit plCurrent;
        private readonly object _graphSync = new object();
        private readonly EmissionAnalyzerManager _emissionAnalyzerManager;
        private readonly OpacityAnalyzerManager _opacityAnalyzerManager;
        private readonly RTFBuilderbase _richTextBuilder;
        private readonly GraphHelper _graphHelper;
        private int ASYNC_DATA_LENGTH = 19;
        private float chargingPeakCurrent;
        private float dischargingPeakCurrent;
        private bool _isEmissionOpacityRunning = false;
        private readonly HidBatteryAnalyzerManager _batteryAnalyzer;
        
        #endregion

        #region constructor
        public frmBatteryAnalysis()
        {
            InitializeComponent();
            _richTextBuilder = new RTFBuilder();

            // settings for ZedGraph controls
            InitZedGraphControls();
            _graphHelper = new GraphHelper(zgcRPM, plRPM, zgcVoltage, plVoltage, zgcCurrent, plCurrent, _graphSync);

            // if Emission Analyzer is used as RPM source
            if (Settings.Default.__RPM_Emission)
            {
                // choose communication port
                if (Settings.Default.__EmissionUseHid)
                {
                    HidToSerialDevice hidToSerialDevice = new HidToSerialDevice(Settings.Default.__EmissionHidVendorId, Settings.Default.__EmissionHidProductId);
                    hidToSerialDevice.HidDevice.OnDeviceAttached += new EventHandler(_hidDevice_OnDeviceAttached);
                    hidToSerialDevice.HidDevice.OnDeviceRemoved += new EventHandler(_hidDevice_OnDeviceRemoved);
                    HidCommunication hidCom = new HidCommunication(hidToSerialDevice);
                    _emissionAnalyzerManager = new EmissionAnalyzerManager(hidCom);
                }
                else
                {
                    SerialComunication serCom = new SerialComunication(CommonResources.EmissionPortName, 9600);
                    _emissionAnalyzerManager = new EmissionAnalyzerManager(serCom);
                }

                // load data length. vary on model number
                if (Settings.Default.__NHA505)
                {
                    ASYNC_DATA_LENGTH = 19;
                }
                else if (Settings.Default.__NHA505_Touch)
                {
                    ASYNC_DATA_LENGTH = 18;
                }

                // asign event handler
                _emissionAnalyzerManager.RealtimeDataReceived += _emissionAnalyzerManager_RealtimeDataReceived;
            }
            else if (Settings.Default.__RPM_Opacity)   // if Opacity Analyzer is used as RPM source
            {
                // choose communication port
                if (Settings.Default.__OpacityUseHid)
                {
                    HidToSerialDevice hidToSerialDevice = new HidToSerialDevice(Settings.Default.__OpacityHidVendorId, Settings.Default.__OpacityHidProductId);
                    hidToSerialDevice.HidDevice.OnDeviceAttached += new EventHandler(_hidDevice_OnDeviceAttached);
                    hidToSerialDevice.HidDevice.OnDeviceRemoved += new EventHandler(_hidDevice_OnDeviceRemoved);
                    HidCommunication hidCom = new HidCommunication(hidToSerialDevice);
                    _opacityAnalyzerManager = new OpacityAnalyzerManager(hidCom);
                }
                else
                {
                    SerialComunication serCom = new SerialComunication(CommonResources.EmissionPortName, 9600);
                    _opacityAnalyzerManager = new OpacityAnalyzerManager(serCom);
                }

                // asign event handler
                _opacityAnalyzerManager.RealtimeTestDataReceived += _opacityAnalyzerManager_RealtimeTestDataReceived;
            }

            // init USB HID device
            HidDevice hidDevice = new HidDevice(Settings.Default.__BatteryHidVendorId, Settings.Default.__BatteryHidProductId);
            hidDevice.OnDeviceAttached += new EventHandler(_hidDevice_OnDeviceAttached);
            hidDevice.OnDeviceRemoved += new EventHandler(_hidDevice_OnDeviceRemoved);

            _batteryAnalyzer = new HidBatteryAnalyzerManager(hidDevice, 10, 12, 120);
            _batteryAnalyzer.OnAnalogDataReceived += batteryAnalyzer_OnAnalogDataReceived;
            hidDevice.Connect();
        }
        #endregion

        #region override function: OnLoad & OnClosing
        protected override void OnLoad(EventArgs e)
        {
            // load Total Capture
            if (CommonResources.BatteryTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.BatteryTotalCapture;
            }

            // load RichText
            _richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.BatteryCapturedRichText))
            {
                // add Emission Title
                _richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).AppendLine("Battery & Aletrnator Report");
                _richTextBuilder.AppendLine();
            }
            else
            {
                _richTextBuilder.AppendRTFDocument(CommonResources.BatteryCapturedRichText);
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // open communication port
            if (Settings.Default.__RPM_Emission)
            {
                _emissionAnalyzerManager.Open();
            }
            else if (Settings.Default.__RPM_Opacity)
            {
                _opacityAnalyzerManager.Open();
            }

            // start graph updater timer
            tmrGraphUpdater.Enabled = true;

            // pass to base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            tmrGraphUpdater.Enabled = false;
            _batteryAnalyzer.Close();
            if (Settings.Default.__RPM_Emission)
            {
                _emissionAnalyzerManager.Close();
            }
            else if (Settings.Default.__RPM_Opacity)
            {
                _opacityAnalyzerManager.Close();
            }
            base.OnClosing(e);
        } 
        #endregion

        #region ZedGraph
        private void InitZedGraphControls()
        {
            // common graph settings
            ZedGraphControl[] zGraphControls = new ZedGraphControl[3];
            zGraphControls[0] = zgcRPM;
            zGraphControls[1] = zgcVoltage;
            zGraphControls[2] = zgcCurrent;

            foreach (ZedGraphControl zGraph in zGraphControls)
            {
                // disable cursor tooltip values
                zGraph.IsShowPointValues = false;
                zGraph.IsShowCursorValues = false;

                // scrollbar
                zGraph.IsSynchronizeXAxes = true;
                zGraph.IsShowHScrollBar = false;
                zGraph.IsAutoScrollRange = true;
                zGraph.ScrollGrace = 100;


                // enable horizontal zoom and pan
                zGraph.IsZoomOnMouseCenter = false;
                zGraph.IsEnableHPan = true;
                zGraph.IsEnableHZoom = true;
                zGraph.IsEnableVPan = false;
                zGraph.IsEnableVZoom = false;

                // X axis title config
                zGraph.GraphPane.XAxis.Scale.FontSpec.Size = 20;
                zGraph.GraphPane.XAxis.Scale.FontSpec.FontColor = Color.Black;
                zGraph.GraphPane.XAxis.Title.FontSpec.Size = 16;
                zGraph.GraphPane.XAxis.Title.FontSpec.FontColor = Color.Black;
                zGraph.GraphPane.XAxis.Title.IsVisible = false;

                // X axis scale config
                zGraph.GraphPane.XAxis.Scale.Min = -500;
                zGraph.GraphPane.XAxis.Scale.Max = 0;
                zGraph.GraphPane.XAxis.Scale.IsVisible = true;
                zGraph.GraphPane.XAxis.IsVisible = true;
                zGraph.GraphPane.XAxis.Scale.MajorStepAuto = true;
                zGraph.GraphPane.XAxis.Scale.MinorStepAuto = false;
                zGraph.GraphPane.XAxis.Scale.MinorStep = double.MaxValue;
                zGraph.GraphPane.XAxis.Scale.Align = AlignP.Inside;
                //zGraph.GraphPane.XAxis.Scale.MaxAuto = true;

                // X axis grid config
                zGraph.GraphPane.XAxis.MajorGrid.IsVisible = true;
                zGraph.GraphPane.XAxis.MinorGrid.IsVisible = false;
                zGraph.GraphPane.XAxis.MajorGrid.DashOff = 5;
                zGraph.GraphPane.XAxis.MajorGrid.DashOn = 2;

                // set Y axis
                zGraph.GraphPane.YAxis.Scale.FontSpec.Size = 24;
                zGraph.GraphPane.XAxis.Scale.FontSpec.FontColor = Color.Black;
                zGraph.GraphPane.YAxis.Title.FontSpec.Size = 24;
                zGraph.GraphPane.XAxis.Title.FontSpec.FontColor = Color.Black;
                zGraph.GraphPane.YAxis.Title.IsVisible = true;

                // Y axis scale config
                zGraph.GraphPane.YAxis.Scale.Align = AlignP.Inside;

                // Y axis grid config
                zGraph.GraphPane.YAxis.MajorGrid.IsVisible = false;
                zGraph.GraphPane.YAxis.MinorGrid.IsVisible = false;
                zGraph.GraphPane.YAxis.MajorGrid.DashOff = 0;
                zGraph.GraphPane.YAxis.MajorGrid.DashOn = 0;
                zGraph.GraphPane.YAxis.MajorGrid.IsZeroLine = false;

                // title
                zGraph.GraphPane.Title.FontSpec.Size = 10;
                zGraph.GraphPane.Title.IsVisible = true;
            }

            // RPM Graph Specific setting
            zgcRPM.GraphPane.AddCurve("", new RollingPointPairList(2500), Color.Red, SymbolType.None);
            zgcRPM.GraphPane.YAxis.Scale.FontSpec.FontColor = Color.Red;
            zgcRPM.GraphPane.YAxis.Title.FontSpec.FontColor = Color.Red;
            zgcRPM.GraphPane.YAxis.Scale.Min = 0;
            zgcRPM.GraphPane.YAxis.Scale.Max = 40;
            zgcRPM.GraphPane.YAxis.Scale.MajorStep = 4;
            zgcRPM.GraphPane.YAxis.Scale.MinorStep = 4;
            zgcRPM.GraphPane.Title.Text = " "; //"RPM vs. Time";
            zgcRPM.GraphPane.YAxis.Title.Text = "RPM  (x100)";
            zgcRPM.GraphPane.XAxis.Title.Text = "Time";
            zgcRPM.AxisChange();

            // Voltage Graph Specific setting
            zgcVoltage.GraphPane.AddCurve("", new RollingPointPairList(2500), Color.Green, SymbolType.None);
            zgcVoltage.GraphPane.YAxis.Scale.FontSpec.FontColor = Color.Green;
            zgcVoltage.GraphPane.YAxis.Title.FontSpec.FontColor = Color.Green;
            zgcVoltage.GraphPane.YAxis.Scale.Min = CommonResources.MIN_VOLTAGE_12V_BATTERY;
            zgcVoltage.GraphPane.YAxis.Scale.Max = CommonResources.MAX_VOLTAGE_12V_BATTERY;
            zgcVoltage.GraphPane.YAxis.Scale.MajorStep = 1;
            zgcVoltage.GraphPane.YAxis.Scale.MinorStep = 1;
            zgcVoltage.GraphPane.Title.Text = " ";//"Voltage vs. Time";
            zgcVoltage.GraphPane.YAxis.Title.Text = "Voltage";
            zgcVoltage.GraphPane.XAxis.Title.Text = "Time";
            zgcVoltage.AxisChange();

            // Current Graph Specific setting
            zgcCurrent.GraphPane.AddCurve("", new RollingPointPairList(2500), Color.Blue, SymbolType.None);
            zgcCurrent.GraphPane.YAxis.Scale.FontSpec.FontColor = Color.Blue;
            zgcCurrent.GraphPane.YAxis.Title.FontSpec.FontColor = Color.Blue;
            zgcCurrent.GraphPane.YAxis.Scale.Min = 0;
            zgcCurrent.GraphPane.YAxis.Scale.Max = 30;
            //zgcCurrent.GraphPane.YAxis.Scale.MaxAuto = true;
            //zgcCurrent.GraphPane.YAxis.Scale.MinAuto = true;
            zgcCurrent.GraphPane.YAxis.Scale.MajorStep = 10;
            zgcCurrent.GraphPane.YAxis.Scale.MinorStep = 10;
            zgcCurrent.GraphPane.Title.Text = " "; //"Current vs. Time";
            zgcCurrent.GraphPane.YAxis.Title.Text = "Current";
            zgcCurrent.GraphPane.XAxis.Title.Text = "Time";
            zgcCurrent.AxisChange();

            // Get the PointPairList
            plRPM = zgcRPM.GraphPane.CurveList[0].Points as IPointListEdit;
            plVoltage = zgcVoltage.GraphPane.CurveList[0].Points as IPointListEdit;
            plCurrent = zgcCurrent.GraphPane.CurveList[0].Points as IPointListEdit;
        }

        private bool zgcRPM_DoubleClickEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            if (zgcRPM.Dock != DockStyle.Fill)
            {
                zgcRPM.Dock = DockStyle.Fill;
                zgcVoltage.Visible = false;
                zgcCurrent.Visible = false;
                lblOutOfScale.Visible = false;
            }
            else
            {
                zgcRPM.Dock = DockStyle.None;
                zgcVoltage.Visible = true;
                zgcCurrent.Visible = true;
            }
            return default(bool);
        }

        private bool zgcVoltage_DoubleClickEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            if (zgcVoltage.Dock != DockStyle.Fill)
            {
                zgcVoltage.Dock = DockStyle.Fill;
                zgcCurrent.Visible = false;
                zgcRPM.Visible = false;
            }
            else
            {
                zgcVoltage.Dock = DockStyle.None;
                zgcCurrent.Visible = true;
                zgcRPM.Visible = true;
            }
            return default(bool);
        }

        private bool zgcCurrent_DoubleClickEvent(ZedGraphControl sender, MouseEventArgs e)
        {
            if (zgcCurrent.Dock != DockStyle.Fill)
            {
                zgcCurrent.Dock = DockStyle.Fill;
                zgcRPM.Visible = false;
                zgcVoltage.Visible = false;
                lblOutOfScale.Visible = false;
            }
            else
            {
                zgcCurrent.Dock = DockStyle.None;
                zgcRPM.Visible = true;
                zgcVoltage.Visible = true;
            }
            return default(bool);
        }

        private void zgcRPM_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            _graphHelper.SetXScale(zgcRPM.GraphPane.XAxis.Scale.Max, zgcRPM.GraphPane.XAxis.Scale.Min);
        }

        private void zgcVoltage_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            _graphHelper.SetXScale(zgcVoltage.GraphPane.XAxis.Scale.Max, zgcVoltage.GraphPane.XAxis.Scale.Min);
        }

        private void zgcCurrent_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            _graphHelper.SetXScale(zgcCurrent.GraphPane.XAxis.Scale.Max, zgcCurrent.GraphPane.XAxis.Scale.Min);
        }

        private void tmrGraphUpdater_Tick(object sender, EventArgs e)
        {
            _graphHelper.ScrollGraph();
            _graphHelper.UpdateGraph();
        }
        #endregion

        #region Button Click Function
        private void btnMeasure_Click(object sender, EventArgs e)
        {
            // Emission Analyzer as RPM source
            if (Settings.Default.__RPM_Emission)
            {
                if (btnMeasure.Text == "MEASURE")
                {
                    if (_emissionAnalyzerManager.TurnPumpOn() == string.Empty)
                    {
                        _emissionAnalyzerManager.StartAsyncRealTimeData(500);
                        _isEmissionOpacityRunning = true;
                    }
                    else
                    {
                        MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    chargingPeakCurrent = 0;
                    dischargingPeakCurrent = 0;
                    btnMeasure.Text = "STOP";
                }
                else
                {
                    if (_isEmissionOpacityRunning)
                    {
                        _emissionAnalyzerManager.StopAsyncRealTimeData();
                        if (_emissionAnalyzerManager.TurnPumpOff() != string.Empty)
                        {
                            MessageBox.Show("Device is not responding, Plese check connections.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        _isEmissionOpacityRunning = false;
                    }
                    btnMeasure.Text = "MEASURE";
                }
            }
            else if (Settings.Default.__RPM_Opacity)
            {
                if (btnMeasure.Text == "MEASURE")
                {
                    string errorMessage;
                    if (!_opacityAnalyzerManager.IsDeviceWarmingUp(out errorMessage))
                    {
                        _opacityAnalyzerManager.StartRealtimeTest();
                        _opacityAnalyzerManager.ReadAsync_RealtimeTestData(500);
                        _isEmissionOpacityRunning = true;
                    }
                    else
                    {
                        MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    chargingPeakCurrent = 0;
                    dischargingPeakCurrent = 0;
                    btnMeasure.Text = "STOP";
                }
                else
                {
                    if (_isEmissionOpacityRunning)
                    {
                        _opacityAnalyzerManager.StopAsync_RealtimeTestData();
                        _isEmissionOpacityRunning = false;
                    }
                    btnMeasure.Text = "MEASURE";
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.BatteryTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.BatteryTotalCapture;

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                _richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            else
            {
                // richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            }

            // add RPM, Voltage and Current
            _richTextBuilder.AppendPara();
            _richTextBuilder.Reset();
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Engine speed", lblSpeed.Text + " RPM");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Battery Voltage", lblVoltage.Text + " V");
            CommonResources.RTF_AddRow(_richTextBuilder, new int[] { 40, 20 }, "Battery Current (" + lblCharging.Text + ")", lblCurrent.Text + " A");
            _richTextBuilder.AppendLine();

            if (cbxCaptureGraph.Checked)
            {
                //Stopwatch aStopwatch = new Stopwatch();
                //aStopwatch.Start();

                int width = 700, height = 200;
                Bitmap bmp = new Bitmap(width, height);
                Graphics g = Graphics.FromImage(bmp);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;

                // add RPM Graph
                if (zgcRPM.Visible)
                {
                    _richTextBuilder.AppendPara();
                    _richTextBuilder.Reset();
                    _richTextBuilder.AppendLine();
                    if (zgcRPM.Dock == DockStyle.Fill)
                    {
                        width = graphPanel.Width;
                        height = graphPanel.Height;
                        bmp.Dispose();
                        bmp = new Bitmap(width, height);
                        g.Dispose();
                        g = Graphics.FromImage(bmp);
                    }
                    g.DrawImage(zgcRPM.MasterPane.GetImage(), -1, -1, width + 2, height + 2);
                    _richTextBuilder.InsertImage(bmp);
                }

                // add Voltage Graph
                if (zgcVoltage.Visible)
                {
                    _richTextBuilder.AppendPara();
                    _richTextBuilder.Reset();
                    _richTextBuilder.AppendLine();
                    if (zgcVoltage.Dock == DockStyle.Fill)
                    {
                        width = graphPanel.Width;
                        height = graphPanel.Height;
                        bmp.Dispose();
                        bmp = new Bitmap(width, height);
                        g.Dispose();
                        g = Graphics.FromImage(bmp);
                    }
                    g.DrawImage(zgcVoltage.MasterPane.GetImage(), -1, -1, width + 2, height + 2);
                    _richTextBuilder.InsertImage(bmp);
                }

                // add Current Graph
                if (zgcCurrent.Visible)
                {
                    _richTextBuilder.AppendPara();
                    _richTextBuilder.Reset();
                    _richTextBuilder.AppendLine();
                    if (zgcCurrent.Dock == DockStyle.Fill)
                    {
                        width = graphPanel.Width;
                        height = graphPanel.Height;
                        bmp.Dispose();
                        bmp = new Bitmap(width, height);
                        g.Dispose();
                        g = Graphics.FromImage(bmp);
                    }
                    g.DrawImage(zgcCurrent.MasterPane.GetImage(), -1, -1, width + 2, height + 2);
                    _richTextBuilder.InsertImage(bmp);
                }

                g.Dispose();
                bmp.Dispose();

                //aStopwatch.Stop();
                //MessageBox.Show(aStopwatch.ElapsedMilliseconds.ToString());
            }


            _richTextBuilder.AppendPara();
            _richTextBuilder.Reset();
            _richTextBuilder.AppendLine();
            _richTextBuilder.AppendLine();

            // save new values
            CommonResources.BatteryCapturedRichText = _richTextBuilder.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            _richTextBuilder.Clear();
            CommonResources.BatteryTotalCapture = 0;
            CommonResources.BatteryCapturedRichText = "";
            btnCapture.Text = "CAPTURE";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            chargingPeakCurrent = 0;
            dischargingPeakCurrent = 0;
            plRPM.Clear();
            plVoltage.Clear();
            plCurrent.Clear();
            zgcRPM.Invalidate();
            zgcVoltage.Invalidate();
            zgcCurrent.Invalidate();
        } 
        #endregion

        #region data received
        void _emissionAnalyzerManager_RealtimeDataReceived(object sender, RealtimeEmissionDataEventArgs e)
        {
            if (btnMeasure.Text != "MEASURE")
            {
                ThreadHelperClass.SetText(this, lblSpeed, e.RPM.ToString());
                _graphHelper.SetRpmDta(e.RPM/100);
            }
        }

        void _opacityAnalyzerManager_RealtimeTestDataReceived(object sender, OpacityRealtimeTestDataEventArgs e)
        {
            if (btnMeasure.Text == "MEASURE") return;
            ThreadHelperClass.SetText(this, lblSpeed, e.Speed.ToString());
            _graphHelper.SetRpmDta(e.Speed/100);
        }

        void batteryAnalyzer_OnAnalogDataReceived(object sender, AnalogDataReceivedEventArgs e)
        {
            if (btnMeasure.Text == "MEASURE") { return; }
            
            ThreadHelperClass.SetText(this, lblVoltage, e.AverageVoltage.ToString("0.0"));
            ThreadHelperClass.SetText(this, lblCurrent, e.AverageCurrent.ToString("0"));
            if (e.AverageCurrent > 0)
            {
                ThreadHelperClass.SetText(this, lblCharging, "CHARGING");
                if (e.AverageCurrent > chargingPeakCurrent) { chargingPeakCurrent = e.AverageCurrent; }
            }
            else
            {
                ThreadHelperClass.SetText(this, lblCharging, "DISCHARGING  ");
                if (e.AverageCurrent < dischargingPeakCurrent) { dischargingPeakCurrent = e.AverageCurrent; }
            }
            ThreadHelperClass.SetText(this, lblPeakCurrent, chargingPeakCurrent.ToString("0") + ", " + dischargingPeakCurrent.ToString("0"));

            // show out of scale warning
            float avgVoltage = Math.Abs(e.AverageVoltage);
            if (avgVoltage < CommonResources.MIN_VOLTAGE_12V_BATTERY || avgVoltage > CommonResources.MAX_VOLTAGE_24V_BATTERY)
            {
                if(!lblOutOfScale.Visible && zgcVoltage.Visible)
                    ThreadHelperClass.SetVisibility(this, lblOutOfScale, true);
            }
            else
            {
                if (lblOutOfScale.Visible)
                    ThreadHelperClass.SetVisibility(this, lblOutOfScale, false);
            }

            _graphHelper.SetGraphDta(e.Voltages, e.Currents);
            //_graphHelper.SetGraphDta(e.Voltages, new float[] {e.AverageCurrent });
        }
        #endregion

        #region USB HID Library
        private void _hidDevice_OnDeviceAttached(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(_hidDevice_OnDeviceAttached), new object[] { sender, e });
            }
            else
            {
                lblUsbConnected.Text = "       Connected";
            }
        }

        private void _hidDevice_OnDeviceRemoved(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(_hidDevice_OnDeviceRemoved), new object[] { sender, e });
            }
            else
            {
                lblUsbConnected.Text = "Not Connected";
            }
        }
        #endregion
    }
}
