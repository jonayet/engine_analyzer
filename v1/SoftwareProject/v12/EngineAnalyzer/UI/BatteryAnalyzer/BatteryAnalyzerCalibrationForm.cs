﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;
using INFRA.USB;

// ReSharper disable CSharpWarnings::CS1591
// ReSharper disable SuggestUseVarKeywordEvident
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local
// ReSharper disable RedundantExplicitArrayCreation
// ReSharper disable RedundantDefaultFieldInitializer
// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable RedundantDelegateCreation
// ReSharper disable LocalizableElement

namespace EngineAnalyzer.UI.BatteryAnalyzer
{
    public partial class BatteryAnalyzerCalibrationForm : Form
    {
        private readonly HidDevice _hidDevice;
        private readonly HidBatteryAnalyzerManager _hidBatteryAnalyzerManager;
        private CalibrationStage _voltageCalibrationStage;
        private CalibrationStage _currentCalibrationStage;

        private float _actualVoltage1, _actualVoltage2;
        private float _actualCurrent1, _actualCurrent2;
        private double _deviceVoltageData1, _deviceVoltageData2;
        private double _deviceCurrentData1, _deviceCurrentData2;

        public BatteryAnalyzerCalibrationForm()
        {
            InitializeComponent();

            _hidDevice = new HidDevice(Settings.Default.__BatteryHidVendorId, Settings.Default.__BatteryHidProductId);
            _hidDevice.OnDeviceAttached += new EventHandler(hidPort_OnDeviceAttached);
            _hidDevice.OnDeviceRemoved += new EventHandler(hidPort_OnDeviceRemoved);
            _hidDevice.Connect();

            _hidBatteryAnalyzerManager = new HidBatteryAnalyzerManager(_hidDevice, 50, 600, 600);
            _hidBatteryAnalyzerManager.OnAnalogDataReceived += HidBatteryAnalyzerManagerOnAnalogDataReceived;
            _voltageCalibrationStage = CalibrationStage.Initial;
            _currentCalibrationStage = CalibrationStage.Initial;
        }

        void HidBatteryAnalyzerManagerOnAnalogDataReceived(object sender, AnalogDataReceivedEventArgs e)
        {
            // show voltage constant & offset
            ThreadHelperClass.SetText(this, voltageLabel, _hidBatteryAnalyzerManager.AverageVoltage.ToString("0.0 V"));
            ThreadHelperClass.SetText(this, voltageRawDataLabel, _hidBatteryAnalyzerManager.RawAverageVoltageReading.ToString("0"));
            ThreadHelperClass.SetText(this, voltageConstantlabel, _hidBatteryAnalyzerManager.VoltageConstant.ToString("0.0000000"));
            ThreadHelperClass.SetText(this, voltageOffsetlabel, _hidBatteryAnalyzerManager.VoltageOffset.ToString("0.000"));

            // show current constant & offset
            ThreadHelperClass.SetText(this, currentLabel, _hidBatteryAnalyzerManager.AverageCurrent.ToString("0 A"));
            ThreadHelperClass.SetText(this, currentRawDataLabel, _hidBatteryAnalyzerManager.RawAverageCurrentReading.ToString("0"));
            ThreadHelperClass.SetText(this, currentConstantLabel, _hidBatteryAnalyzerManager.CurrentConstant.ToString("0.0000000"));
            ThreadHelperClass.SetText(this, currentOffsetLabel, _hidBatteryAnalyzerManager.CurrentOffset.ToString("0.000"));
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _hidDevice.Disconnect();
            base.OnClosing(e);
        }

        private void hidPort_OnDeviceRemoved(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, deviceConnectedLabel, "Not Connected");
        }

        private void hidPort_OnDeviceAttached(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, deviceConnectedLabel, "Connected");
        }

        private void setVoltageConstantButton_Click(object sender, EventArgs e)
        {
            if (!_hidDevice.IsConnected) { return; }
            if (voltageSetConstantCheckBox.Checked)
            {
                var result = MessageBox.Show("Are you sure to Calibrate Voltage?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result != DialogResult.Yes) return;
                float constant;
                if (!float.TryParse(voltageValueTextBox.Text, out constant))
                {
                    MessageBox.Show("Enter value in correct format", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                _hidBatteryAnalyzerManager.WriteVoltageConstant(constant);
                MessageBox.Show("Voltage calibration was successfull!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            switch (_voltageCalibrationStage)
            {
                case CalibrationStage.Initial:
                    var result = MessageBox.Show("Are you sure to Calibrate Voltage?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result != DialogResult.Yes) return;
                    voltageValueTextBox.Text = "";
                    setVoltageConstantButton.Text = "Adjust Voltage1 (12 V)";
                    _voltageCalibrationStage = CalibrationStage.Value1;
                    break;

                case CalibrationStage.Value1:
                    if (!float.TryParse(voltageValueTextBox.Text, out _actualVoltage1))
                    {
                        MessageBox.Show("Enter value in correct format", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _deviceVoltageData1 = _hidBatteryAnalyzerManager.RawAverageVoltageReading;
                    voltageValueTextBox.Text = "";
                    setVoltageConstantButton.Text = "Adjust Voltage2 (24 V)";
                    _voltageCalibrationStage = CalibrationStage.Value2;
                    break;

                case CalibrationStage.Value2:
                    if (!float.TryParse(voltageValueTextBox.Text, out _actualVoltage2))
                    {
                        MessageBox.Show("Enter value in correct format", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _deviceVoltageData2 = _hidBatteryAnalyzerManager.RawAverageVoltageReading;
                    voltageValueTextBox.Text = "";
                    setVoltageConstantButton.Text = "Start Calibration";
                    _voltageCalibrationStage = CalibrationStage.Initial;

                    _hidBatteryAnalyzerManager.CalibrateVoltage(_actualVoltage1, _deviceVoltageData1, _actualVoltage2, _deviceVoltageData2);
                    MessageBox.Show("Voltage calibration was successfull!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        private void setVoltageOffsetButton_Click(object sender, EventArgs e)
        {
            if (!_hidDevice.IsConnected) { return; }
            var result = MessageBox.Show("Are you sure to Calibrate Voltage?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result != DialogResult.Yes) return;
            result = MessageBox.Show("Please disconnect the Voltage probe.\r\nPress OK when done.", "Offset Calibration", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result != DialogResult.OK) return;
            float voltageOffset = -_hidBatteryAnalyzerManager.RawAverageVoltageReading;
            _hidBatteryAnalyzerManager.WriteVoltageOffset(voltageOffset);
            MessageBox.Show("Current calibration was successfull!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void setCurrentConstantButton_Click(object sender, EventArgs e)
        {
            if (!_hidDevice.IsConnected) { return; }
            if (currentSetConstantCheckBox.Checked)
            {
                var result = MessageBox.Show("Are you sure to Calibrate Current?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result != DialogResult.Yes) return;
                float constant;
                if (!float.TryParse(currentValueTextBox.Text, out constant))
                {
                    MessageBox.Show("Enter value in correct format", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                _hidBatteryAnalyzerManager.WriteCurrentConstant(constant);
                MessageBox.Show("Current calibration was successfull!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            switch (_currentCalibrationStage)
            {
                case CalibrationStage.Initial:
                    var result = MessageBox.Show("Are you sure to Calibrate Current?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result != DialogResult.Yes) return;
                    currentValueTextBox.Text = "";
                    setCurrentConstantButton.Text = "Adjust Current1 (5 A)";
                    _currentCalibrationStage = CalibrationStage.Value1;
                    break;

                case CalibrationStage.Value1:
                    if (!float.TryParse(currentValueTextBox.Text, out _actualCurrent1))
                    {
                        MessageBox.Show("Enter value in correct format", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _deviceCurrentData1 = _hidBatteryAnalyzerManager.RawAverageCurrentReading;
                    currentValueTextBox.Text = "";
                    setCurrentConstantButton.Text = "Adjust Current2 (10 A)";
                    _currentCalibrationStage = CalibrationStage.Value2;
                    break;

                case CalibrationStage.Value2:
                    if (!float.TryParse(currentValueTextBox.Text, out _actualCurrent2))
                    {
                        MessageBox.Show("Enter value in correct format", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    _deviceCurrentData2 = _hidBatteryAnalyzerManager.RawAverageCurrentReading;
                    currentValueTextBox.Text = "";
                    setCurrentConstantButton.Text = "Start Calibration";
                    _currentCalibrationStage = CalibrationStage.Initial;

                    _hidBatteryAnalyzerManager.CalibrateCurrent(_actualCurrent1, _deviceCurrentData1, _actualCurrent2, _deviceCurrentData2);
                    MessageBox.Show("Current calibration was successfull!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        private void setCurrentOffsetButton_Click(object sender, EventArgs e)
        {
            if (!_hidDevice.IsConnected) { return; }
            var result = MessageBox.Show("Are you sure to Calibrate Current?", "Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result != DialogResult.Yes) return;
            result = MessageBox.Show("Please connect the Current probe.\r\nPress OK when done.", "Offset Calibration", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            if (result != DialogResult.OK) return;
            float currentOffset = -_hidBatteryAnalyzerManager.RawAverageCurrentReading;
            _hidBatteryAnalyzerManager.WriteCurrentOffset(currentOffset);
            MessageBox.Show("Current calibration was successfull!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

    enum CalibrationStage
    {
        Initial,
        Offset,
        Value1,
        Value2,
        Fixed
    }
}