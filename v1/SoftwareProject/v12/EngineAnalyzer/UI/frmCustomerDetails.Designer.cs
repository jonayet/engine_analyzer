﻿namespace EngineAnalyzer.UI
{
    partial class frmCustomerDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomerDetails));
            this.tbxCustomerName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxCustomerAddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxCustomerTelephone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxCustomerEmail = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxCustomerJobRef = new System.Windows.Forms.TextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbxCustomerName
            // 
            this.tbxCustomerName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxCustomerName.Location = new System.Drawing.Point(211, 29);
            this.tbxCustomerName.Margin = new System.Windows.Forms.Padding(7);
            this.tbxCustomerName.Name = "tbxCustomerName";
            this.tbxCustomerName.Size = new System.Drawing.Size(600, 35);
            this.tbxCustomerName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "Address:";
            // 
            // tbxCustomerAddress
            // 
            this.tbxCustomerAddress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxCustomerAddress.Location = new System.Drawing.Point(211, 80);
            this.tbxCustomerAddress.Margin = new System.Windows.Forms.Padding(7);
            this.tbxCustomerAddress.Multiline = true;
            this.tbxCustomerAddress.Name = "tbxCustomerAddress";
            this.tbxCustomerAddress.Size = new System.Drawing.Size(600, 245);
            this.tbxCustomerAddress.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 351);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 29);
            this.label3.TabIndex = 5;
            this.label3.Text = "Telephone:";
            // 
            // tbxCustomerTelephone
            // 
            this.tbxCustomerTelephone.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxCustomerTelephone.Location = new System.Drawing.Point(211, 348);
            this.tbxCustomerTelephone.Margin = new System.Windows.Forms.Padding(7);
            this.tbxCustomerTelephone.Name = "tbxCustomerTelephone";
            this.tbxCustomerTelephone.Size = new System.Drawing.Size(600, 35);
            this.tbxCustomerTelephone.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(105, 411);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 29);
            this.label4.TabIndex = 7;
            this.label4.Text = "Email:";
            // 
            // tbxCustomerEmail
            // 
            this.tbxCustomerEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxCustomerEmail.Location = new System.Drawing.Point(211, 408);
            this.tbxCustomerEmail.Margin = new System.Windows.Forms.Padding(7);
            this.tbxCustomerEmail.Name = "tbxCustomerEmail";
            this.tbxCustomerEmail.Size = new System.Drawing.Size(600, 35);
            this.tbxCustomerEmail.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(83, 473);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 29);
            this.label5.TabIndex = 9;
            this.label5.Text = "Job Ref:";
            // 
            // tbxCustomerJobRef
            // 
            this.tbxCustomerJobRef.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxCustomerJobRef.Location = new System.Drawing.Point(211, 470);
            this.tbxCustomerJobRef.Margin = new System.Windows.Forms.Padding(7);
            this.tbxCustomerJobRef.Name = "tbxCustomerJobRef";
            this.tbxCustomerJobRef.Size = new System.Drawing.Size(600, 35);
            this.tbxCustomerJobRef.TabIndex = 5;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Location = new System.Drawing.Point(288, 561);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(160, 48);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Location = new System.Drawing.Point(471, 561);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(160, 48);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Location = new System.Drawing.Point(651, 561);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(160, 48);
            this.btnExit.TabIndex = 12;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmCustomerDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(855, 663);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxCustomerJobRef);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxCustomerEmail);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbxCustomerTelephone);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxCustomerAddress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxCustomerName);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCustomerDetails";
            this.Text = "Customer Details";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCustomerDetails_FormClosing);
            this.Load += new System.EventHandler(this.frmCustomerDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxCustomerName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxCustomerAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxCustomerTelephone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxCustomerEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxCustomerJobRef;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
    }
}