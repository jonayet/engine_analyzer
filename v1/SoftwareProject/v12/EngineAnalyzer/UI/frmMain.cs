﻿using System;
using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.UI.BatteryAnalyzer;
using EngineAnalyzer.UI.EmissionAnalyzer;
using EngineAnalyzer.UI.OpacityAnalyzer;
using EngineAnalyzer.UI.VehicleDetails;
using EngineAnalyzer.Utilities;
using EngineAnalyzer.Utilities.Print;
using RTF;

namespace EngineAnalyzer.UI
{
    public partial class frmMain : Form
    {
        CommonResources cr = new CommonResources();
        frmEmissionMain emMain;
        frmOpacityMain opMain;
        frmBatteryAnalysis batteryAnalysis;
        frmCustomerDetails customerDettails;
        frmVehicleDetails vehicleDetails;
        frmSetup setup;

        public frmMain()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            lblCompanyName.Text = Settings.Default.__CompanyName;
            base.OnShown(e);
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            Application.Exit();
        }

        private void btnEmissionAnalysis_Click(object sender, EventArgs e)
        {
            // creat the form
            if (emMain == null || emMain.IsDisposed) { emMain = new frmEmissionMain(); }

            // show the form
            emMain.WindowState = FormWindowState.Maximized;
            emMain.Show();
        }

        private void btnOpacityAnalysis_Click(object sender, EventArgs e)
        {
            // creat the form
            if (opMain == null || opMain.IsDisposed) { opMain = new frmOpacityMain(); }

            // show the form
            opMain.WindowState = FormWindowState.Maximized;
            opMain.Show();
        }

        private void btnBatteryAnalysis_Click(object sender, EventArgs e)
        {
            // creat the form
            if (batteryAnalysis == null || batteryAnalysis.IsDisposed) { batteryAnalysis = new frmBatteryAnalysis(); }

            // show the form
            batteryAnalysis.WindowState = FormWindowState.Maximized;
            batteryAnalysis.Show();
        }

        private void btnCustomerDetails_Click(object sender, EventArgs e)
        {
            // creat the form
            if (customerDettails == null || customerDettails.IsDisposed) { customerDettails = new frmCustomerDetails(); }

            // show the form
            customerDettails.WindowState = FormWindowState.Maximized;
            customerDettails.Show();
        }

        private void btnVehicleDetails_Click(object sender, EventArgs e)
        {
            // creat the form
            if (vehicleDetails == null || vehicleDetails.IsDisposed) { vehicleDetails = new frmVehicleDetails(); }

            // show the form
            vehicleDetails.WindowState = FormWindowState.Maximized;
            vehicleDetails.Show();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            // create document
            RichTextBox fakeRichTextBox = new RichTextBox();
            fakeRichTextBox.Visible = false;

            // create a RichText Builder
            RTFBuilderbase mainRtBuilder = new RTFBuilder();

            // add Title
            mainRtBuilder.FontSize(24).Font(RTFFont.ArialBlack).FontStyle(FontStyle.Bold).Alignment(StringAlignment.Center).AppendLine("NANHUA-PEARL   COMPUTERIZED ENGINE ANALYZER");
            mainRtBuilder.AppendLine();

            // add Header
            if (Settings.Default.SetupPublishHeader)
            {
                mainRtBuilder.AppendRTFDocument(Settings.Default.SetupHeader);
                mainRtBuilder.AppendLine();
                mainRtBuilder.AppendLine();
            }

            // add Vehicle Details
            mainRtBuilder.FontSize(24).FontStyle(FontStyle.Bold).Append("Vehicle Details:");
            mainRtBuilder.AppendPara();
            mainRtBuilder.Reset();
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Date: " + Settings.Default.VehicleDetailsDate);
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Registration No: " + Settings.Default.VehicleRegNo, "Odometer(Mileage) K.M.: " + Settings.Default.VehicleOdometer);
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Make: " + Settings.Default.VehicleMake, "Model: " + Settings.Default.VehicleModel, "M. Year: " + Settings.Default.VehicleMYear);
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Engine Ref: " + Settings.Default.VehicleEngineRef, "Swift Volume: " + Settings.Default.VehicleSwiftVolume + " C.C.", "Fuel Type: " + Settings.Default.VehicleFuel);
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Others: " + Settings.Default.VehicleDetailsOthers);
            mainRtBuilder.Reset();
            mainRtBuilder.AppendLine();
            mainRtBuilder.AppendLine();

            // add Customer Details
            mainRtBuilder.FontSize(24).FontStyle(FontStyle.Bold).Append("Customer Details:");
            mainRtBuilder.AppendPara();
            mainRtBuilder.Reset();
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Name: " + Settings.Default.CustomerName);
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Address: " + Settings.Default.CustomerAddress);
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "Telephone: " + Settings.Default.CustomerTelephone);
            CommonResources.RTF_AddRow(mainRtBuilder, 88, "EMail: " + Settings.Default.CustomerEmail, "Job Ref.: " + Settings.Default.CustomerJobRef);
            mainRtBuilder.Reset();
            mainRtBuilder.AppendLine();
            mainRtBuilder.AppendLine();

            // add Emission captured data
            if (CommonResources.EmissionTotalCapture > 0)
                mainRtBuilder.AppendRTFDocument(CommonResources.EmissionCapturedRichText);
            mainRtBuilder.AppendLine();

            // add Opacity Realtime captured data
            if (CommonResources.OpacityRealtimeTotalCapture > 0)
                mainRtBuilder.AppendRTFDocument(CommonResources.OpacityRealtimeCapturedRichText);
            mainRtBuilder.AppendLine();

            // add Opacity Acceleration captured data
            if (CommonResources.OpacityAccelerationTotalCapture > 0)
                mainRtBuilder.AppendRTFDocument(CommonResources.OpacityAccelerationCapturedRichText);
            mainRtBuilder.AppendLine();

            // add Battery captured data
            if (CommonResources.BatteryTotalCapture > 0)
                mainRtBuilder.AppendRTFDocument(CommonResources.BatteryCapturedRichText);
            mainRtBuilder.AppendLine();

            // add Physical Observation
            mainRtBuilder.AppendRTFDocument(CommonResources.PhysicalInspectionRichText);

            // add Cylinder Compression Test
            if (Settings.Default.__CompressionTestPublish == true)
            {
                mainRtBuilder.AppendRTFDocument(Settings.Default.__CylinderCompressionRichText);
                mainRtBuilder.AppendLine();
            }

            // add Operator Signeture
            mainRtBuilder.FontSize(24).AppendLine();
            mainRtBuilder.FontSize(24).AppendLine();
            mainRtBuilder.FontSize(24).AppendLine();
            mainRtBuilder.FontSize(24).AppendLine("Operator:_________________________________________               ( Authorized Signature )");
            mainRtBuilder.FontSize(24).AppendLine();

            // add Remarks
            mainRtBuilder.FontSize(24).AppendLine();
            mainRtBuilder.FontSize(24).AppendLine("Remarks:");
            mainRtBuilder.FontSize(24).AppendLine("-------------------------------------------------------------------------------------------------------------------------------");
            mainRtBuilder.FontSize(24).AppendLine("-------------------------------------------------------------------------------------------------------------------------------");
            mainRtBuilder.FontSize(24).AppendLine("-------------------------------------------------------------------------------------------------------------------------------");
            mainRtBuilder.FontSize(24).AppendLine("-------------------------------------------------------------------------------------------------------------------------------");
            mainRtBuilder.FontSize(24).AppendLine("-------------------------------------------------------------------------------------------------------------------------------");
            mainRtBuilder.FontSize(24).AppendLine();

            // add Footer
            if (Settings.Default.SetupPublishFooter)
            {
                mainRtBuilder.FontSize(24).AppendLine();
                mainRtBuilder.AppendRTFDocument(Settings.Default.SetupFooter);
            }

            // load RichText to the real RichTextBox brfore printing
            fakeRichTextBox.Rtf = mainRtBuilder.ToString();

            // load RichTextBox fro printing, set the margins
            RichTextBoxDocument doc = new RichTextBoxDocument(fakeRichTextBox);
            doc.DefaultPageSettings.Margins.Top = 50;
            doc.DefaultPageSettings.Margins.Left = 50;
            doc.DefaultPageSettings.Margins.Right = 50;
            doc.DefaultPageSettings.Margins.Bottom = 70;

            // preview the document
            using (var dlg = new CoolPrintPreviewDialog())
            //using (var dlg = new PrintPreviewDialog())
            {
                dlg.Document = doc;
                dlg.ShowDialog(this);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {

        }

        private void btnSetup_Click(object sender, EventArgs e)
        {
            // creat the form
            if (setup == null || setup.IsDisposed) { setup = new frmSetup(); }

            // show the form
            setup.WindowState = FormWindowState.Maximized;
            setup.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tmrClock_Tick(object sender, EventArgs e)
        {
            lblClock.Text = DateTime.Now.ToLongTimeString();
        }
    }
}
