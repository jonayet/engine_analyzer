﻿namespace EngineAnalyzer.UI
{
    partial class frmSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetup));
            this.rtbHeader = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rtbFooter = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxEmissionSMin = new System.Windows.Forms.TextBox();
            this.tbxEmissionSMax = new System.Windows.Forms.TextBox();
            this.tbxEmissionCo2Max = new System.Windows.Forms.TextBox();
            this.tbxEmissionCo2Min = new System.Windows.Forms.TextBox();
            this.tbxEmissionHcMax = new System.Windows.Forms.TextBox();
            this.tbxEmissionHcMin = new System.Windows.Forms.TextBox();
            this.tbxEmissionTMax = new System.Windows.Forms.TextBox();
            this.tbxEmissionTMin = new System.Windows.Forms.TextBox();
            this.ckbxEmissionPublishLimits = new System.Windows.Forms.CheckBox();
            this.ckbxOpacityPublishLimits = new System.Windows.Forms.CheckBox();
            this.tbxOpacityTMax = new System.Windows.Forms.TextBox();
            this.tbxOpacityTMin = new System.Windows.Forms.TextBox();
            this.tbxOpacityKMax = new System.Windows.Forms.TextBox();
            this.tbxOpacityKMin = new System.Windows.Forms.TextBox();
            this.tbxOpacityNMax = new System.Windows.Forms.TextBox();
            this.tbxOpacityNMin = new System.Windows.Forms.TextBox();
            this.tbxOpacitySMax = new System.Windows.Forms.TextBox();
            this.tbxOpacitySMin = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ckbxPublishHeader = new System.Windows.Forms.CheckBox();
            this.ckbxPublishFooter = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbxEmissionO2Min = new System.Windows.Forms.TextBox();
            this.tbxEmissionO2Max = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbxEmissionNoMin = new System.Windows.Forms.TextBox();
            this.tbxEmissionNoMax = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbxEmissionCoMin = new System.Windows.Forms.TextBox();
            this.tbxEmissionCoMax = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbxEmissionComPort = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cmbxOpacityComPort = new System.Windows.Forms.ComboBox();
            this.btnDefault = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rbtNHA505_Touch = new System.Windows.Forms.RadioButton();
            this.rbtNHA505 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rbtOpacityRPM = new System.Windows.Forms.RadioButton();
            this.rbtEmissionRPM = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cmbxBatteryHidDevice = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbxOpacityUseHid = new System.Windows.Forms.CheckBox();
            this.cbxEmissionUseHid = new System.Windows.Forms.CheckBox();
            this.cmbxOpacityHidDevice = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbxEmissionHidDevice = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbxCompanyName = new System.Windows.Forms.TextBox();
            this.btnCalibrateBatteryModule = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtbHeader
            // 
            this.rtbHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtbHeader.Location = new System.Drawing.Point(84, 40);
            this.rtbHeader.Name = "rtbHeader";
            this.rtbHeader.Size = new System.Drawing.Size(842, 80);
            this.rtbHeader.TabIndex = 0;
            this.rtbHeader.Text = "";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(84, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Print Header:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(84, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(130, 25);
            this.label2.TabIndex = 3;
            this.label2.Text = "Print Footer:";
            // 
            // rtbFooter
            // 
            this.rtbFooter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtbFooter.Location = new System.Drawing.Point(84, 171);
            this.rtbFooter.Name = "rtbFooter";
            this.rtbFooter.Size = new System.Drawing.Size(842, 80);
            this.rtbFooter.TabIndex = 2;
            this.rtbFooter.Text = "";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "Idle RPM:";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 25);
            this.label5.TabIndex = 6;
            this.label5.Text = "CO2 (%):";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "HC (ppm):";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(53, 295);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 25);
            this.label7.TabIndex = 8;
            this.label7.Text = "Temp °C:";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(177, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 25);
            this.label8.TabIndex = 9;
            this.label8.Text = "Min";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(260, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 25);
            this.label9.TabIndex = 10;
            this.label9.Text = "Max";
            // 
            // tbxEmissionSMin
            // 
            this.tbxEmissionSMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionSMin.Location = new System.Drawing.Point(160, 69);
            this.tbxEmissionSMin.Name = "tbxEmissionSMin";
            this.tbxEmissionSMin.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionSMin.TabIndex = 11;
            // 
            // tbxEmissionSMax
            // 
            this.tbxEmissionSMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionSMax.Location = new System.Drawing.Point(246, 70);
            this.tbxEmissionSMax.Name = "tbxEmissionSMax";
            this.tbxEmissionSMax.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionSMax.TabIndex = 12;
            // 
            // tbxEmissionCo2Max
            // 
            this.tbxEmissionCo2Max.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionCo2Max.Location = new System.Drawing.Point(246, 144);
            this.tbxEmissionCo2Max.Name = "tbxEmissionCo2Max";
            this.tbxEmissionCo2Max.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionCo2Max.TabIndex = 14;
            // 
            // tbxEmissionCo2Min
            // 
            this.tbxEmissionCo2Min.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionCo2Min.Location = new System.Drawing.Point(160, 144);
            this.tbxEmissionCo2Min.Name = "tbxEmissionCo2Min";
            this.tbxEmissionCo2Min.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionCo2Min.TabIndex = 13;
            // 
            // tbxEmissionHcMax
            // 
            this.tbxEmissionHcMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionHcMax.Location = new System.Drawing.Point(246, 107);
            this.tbxEmissionHcMax.Name = "tbxEmissionHcMax";
            this.tbxEmissionHcMax.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionHcMax.TabIndex = 16;
            // 
            // tbxEmissionHcMin
            // 
            this.tbxEmissionHcMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionHcMin.Location = new System.Drawing.Point(160, 107);
            this.tbxEmissionHcMin.Name = "tbxEmissionHcMin";
            this.tbxEmissionHcMin.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionHcMin.TabIndex = 15;
            // 
            // tbxEmissionTMax
            // 
            this.tbxEmissionTMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionTMax.Location = new System.Drawing.Point(246, 292);
            this.tbxEmissionTMax.Name = "tbxEmissionTMax";
            this.tbxEmissionTMax.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionTMax.TabIndex = 18;
            // 
            // tbxEmissionTMin
            // 
            this.tbxEmissionTMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionTMin.Location = new System.Drawing.Point(160, 292);
            this.tbxEmissionTMin.Name = "tbxEmissionTMin";
            this.tbxEmissionTMin.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionTMin.TabIndex = 17;
            // 
            // ckbxEmissionPublishLimits
            // 
            this.ckbxEmissionPublishLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ckbxEmissionPublishLimits.AutoSize = true;
            this.ckbxEmissionPublishLimits.Location = new System.Drawing.Point(160, 329);
            this.ckbxEmissionPublishLimits.Name = "ckbxEmissionPublishLimits";
            this.ckbxEmissionPublishLimits.Size = new System.Drawing.Size(102, 29);
            this.ckbxEmissionPublishLimits.TabIndex = 19;
            this.ckbxEmissionPublishLimits.Text = "Publish";
            this.ckbxEmissionPublishLimits.UseVisualStyleBackColor = true;
            // 
            // ckbxOpacityPublishLimits
            // 
            this.ckbxOpacityPublishLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ckbxOpacityPublishLimits.AutoSize = true;
            this.ckbxOpacityPublishLimits.Location = new System.Drawing.Point(156, 193);
            this.ckbxOpacityPublishLimits.Name = "ckbxOpacityPublishLimits";
            this.ckbxOpacityPublishLimits.Size = new System.Drawing.Size(102, 29);
            this.ckbxOpacityPublishLimits.TabIndex = 35;
            this.ckbxOpacityPublishLimits.Text = "Publish";
            this.ckbxOpacityPublishLimits.UseVisualStyleBackColor = true;
            // 
            // tbxOpacityTMax
            // 
            this.tbxOpacityTMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacityTMax.Location = new System.Drawing.Point(240, 156);
            this.tbxOpacityTMax.Name = "tbxOpacityTMax";
            this.tbxOpacityTMax.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacityTMax.TabIndex = 34;
            // 
            // tbxOpacityTMin
            // 
            this.tbxOpacityTMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacityTMin.Location = new System.Drawing.Point(154, 156);
            this.tbxOpacityTMin.Name = "tbxOpacityTMin";
            this.tbxOpacityTMin.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacityTMin.TabIndex = 33;
            // 
            // tbxOpacityKMax
            // 
            this.tbxOpacityKMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacityKMax.Location = new System.Drawing.Point(240, 119);
            this.tbxOpacityKMax.Name = "tbxOpacityKMax";
            this.tbxOpacityKMax.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacityKMax.TabIndex = 32;
            // 
            // tbxOpacityKMin
            // 
            this.tbxOpacityKMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacityKMin.Location = new System.Drawing.Point(154, 119);
            this.tbxOpacityKMin.Name = "tbxOpacityKMin";
            this.tbxOpacityKMin.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacityKMin.TabIndex = 31;
            // 
            // tbxOpacityNMax
            // 
            this.tbxOpacityNMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacityNMax.Location = new System.Drawing.Point(240, 82);
            this.tbxOpacityNMax.Name = "tbxOpacityNMax";
            this.tbxOpacityNMax.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacityNMax.TabIndex = 30;
            // 
            // tbxOpacityNMin
            // 
            this.tbxOpacityNMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacityNMin.Location = new System.Drawing.Point(154, 82);
            this.tbxOpacityNMin.Name = "tbxOpacityNMin";
            this.tbxOpacityNMin.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacityNMin.TabIndex = 29;
            // 
            // tbxOpacitySMax
            // 
            this.tbxOpacitySMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacitySMax.Location = new System.Drawing.Point(240, 45);
            this.tbxOpacitySMax.Name = "tbxOpacitySMax";
            this.tbxOpacitySMax.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacitySMax.TabIndex = 28;
            // 
            // tbxOpacitySMin
            // 
            this.tbxOpacitySMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxOpacitySMin.Location = new System.Drawing.Point(154, 45);
            this.tbxOpacitySMin.Name = "tbxOpacitySMin";
            this.tbxOpacitySMin.Size = new System.Drawing.Size(80, 31);
            this.tbxOpacitySMin.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(254, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 25);
            this.label10.TabIndex = 26;
            this.label10.Text = "Max";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(171, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 25);
            this.label11.TabIndex = 25;
            this.label11.Text = "Min";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 159);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 25);
            this.label12.TabIndex = 24;
            this.label12.Text = "Temp (°C):";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(65, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 25);
            this.label13.TabIndex = 23;
            this.label13.Text = "K (/M):";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(130, 25);
            this.label14.TabIndex = 22;
            this.label14.Text = "Opacity (%):";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(105, 25);
            this.label15.TabIndex = 21;
            this.label15.Text = "Idle RPM:";
            // 
            // ckbxPublishHeader
            // 
            this.ckbxPublishHeader.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ckbxPublishHeader.AutoSize = true;
            this.ckbxPublishHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbxPublishHeader.Location = new System.Drawing.Point(824, 8);
            this.ckbxPublishHeader.Name = "ckbxPublishHeader";
            this.ckbxPublishHeader.Size = new System.Drawing.Size(102, 29);
            this.ckbxPublishHeader.TabIndex = 36;
            this.ckbxPublishHeader.Text = "Publish";
            this.ckbxPublishHeader.UseVisualStyleBackColor = true;
            // 
            // ckbxPublishFooter
            // 
            this.ckbxPublishFooter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ckbxPublishFooter.AutoSize = true;
            this.ckbxPublishFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckbxPublishFooter.Location = new System.Drawing.Point(824, 139);
            this.ckbxPublishFooter.Name = "ckbxPublishFooter";
            this.ckbxPublishFooter.Size = new System.Drawing.Size(102, 29);
            this.ckbxPublishFooter.TabIndex = 37;
            this.ckbxPublishFooter.Text = "Publish";
            this.ckbxPublishFooter.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.tbxEmissionO2Min);
            this.groupBox1.Controls.Add(this.tbxEmissionO2Max);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.tbxEmissionNoMin);
            this.groupBox1.Controls.Add(this.tbxEmissionNoMax);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.tbxEmissionSMin);
            this.groupBox1.Controls.Add(this.tbxEmissionCoMin);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbxEmissionCoMax);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbxEmissionSMax);
            this.groupBox1.Controls.Add(this.tbxEmissionHcMin);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbxEmissionHcMax);
            this.groupBox1.Controls.Add(this.tbxEmissionTMin);
            this.groupBox1.Controls.Add(this.tbxEmissionTMax);
            this.groupBox1.Controls.Add(this.tbxEmissionCo2Max);
            this.groupBox1.Controls.Add(this.tbxEmissionCo2Min);
            this.groupBox1.Controls.Add(this.ckbxEmissionPublishLimits);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(22, 309);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(382, 370);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Emission Analysis ";
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(69, 258);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 25);
            this.label20.TabIndex = 50;
            this.label20.Text = "O2 (%):";
            // 
            // tbxEmissionO2Min
            // 
            this.tbxEmissionO2Min.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionO2Min.Location = new System.Drawing.Point(160, 255);
            this.tbxEmissionO2Min.Name = "tbxEmissionO2Min";
            this.tbxEmissionO2Min.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionO2Min.TabIndex = 51;
            // 
            // tbxEmissionO2Max
            // 
            this.tbxEmissionO2Max.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionO2Max.Location = new System.Drawing.Point(246, 255);
            this.tbxEmissionO2Max.Name = "tbxEmissionO2Max";
            this.tbxEmissionO2Max.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionO2Max.TabIndex = 52;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(44, 184);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 25);
            this.label19.TabIndex = 47;
            this.label19.Text = "NO (ppm):";
            // 
            // tbxEmissionNoMin
            // 
            this.tbxEmissionNoMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionNoMin.Location = new System.Drawing.Point(160, 181);
            this.tbxEmissionNoMin.Name = "tbxEmissionNoMin";
            this.tbxEmissionNoMin.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionNoMin.TabIndex = 48;
            // 
            // tbxEmissionNoMax
            // 
            this.tbxEmissionNoMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionNoMax.Location = new System.Drawing.Point(246, 181);
            this.tbxEmissionNoMax.Name = "tbxEmissionNoMax";
            this.tbxEmissionNoMax.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionNoMax.TabIndex = 49;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(66, 221);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(88, 25);
            this.label18.TabIndex = 44;
            this.label18.Text = "CO (%):";
            // 
            // tbxEmissionCoMin
            // 
            this.tbxEmissionCoMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionCoMin.Location = new System.Drawing.Point(160, 218);
            this.tbxEmissionCoMin.Name = "tbxEmissionCoMin";
            this.tbxEmissionCoMin.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionCoMin.TabIndex = 45;
            // 
            // tbxEmissionCoMax
            // 
            this.tbxEmissionCoMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxEmissionCoMax.Location = new System.Drawing.Point(246, 218);
            this.tbxEmissionCoMax.Name = "tbxEmissionCoMax";
            this.tbxEmissionCoMax.Size = new System.Drawing.Size(80, 31);
            this.tbxEmissionCoMax.TabIndex = 46;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "COM Port (Emission):";
            // 
            // cmbxEmissionComPort
            // 
            this.cmbxEmissionComPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxEmissionComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxEmissionComPort.FormattingEnabled = true;
            this.cmbxEmissionComPort.Location = new System.Drawing.Point(122, 17);
            this.cmbxEmissionComPort.Name = "cmbxEmissionComPort";
            this.cmbxEmissionComPort.Size = new System.Drawing.Size(93, 21);
            this.cmbxEmissionComPort.TabIndex = 36;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox2.Controls.Add(this.tbxOpacityNMin);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.ckbxOpacityPublishLimits);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tbxOpacityTMax);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.tbxOpacityTMin);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.tbxOpacityKMax);
            this.groupBox2.Controls.Add(this.tbxOpacitySMin);
            this.groupBox2.Controls.Add(this.tbxOpacityKMin);
            this.groupBox2.Controls.Add(this.tbxOpacitySMax);
            this.groupBox2.Controls.Add(this.tbxOpacityNMax);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(419, 310);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(340, 228);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Opacity Analysis ";
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 53);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "COM Port (Opacity):";
            // 
            // cmbxOpacityComPort
            // 
            this.cmbxOpacityComPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxOpacityComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxOpacityComPort.FormattingEnabled = true;
            this.cmbxOpacityComPort.Location = new System.Drawing.Point(122, 49);
            this.cmbxOpacityComPort.Name = "cmbxOpacityComPort";
            this.cmbxOpacityComPort.Size = new System.Drawing.Size(93, 21);
            this.cmbxOpacityComPort.TabIndex = 36;
            // 
            // btnDefault
            // 
            this.btnDefault.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDefault.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDefault.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDefault.Location = new System.Drawing.Point(550, 640);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(127, 46);
            this.btnDefault.TabIndex = 41;
            this.btnDefault.Text = "Default";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(714, 640);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(127, 46);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(878, 640);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(127, 46);
            this.btnExit.TabIndex = 43;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox3.Controls.Add(this.cmbxOpacityComPort);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.cmbxEmissionComPort);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Location = new System.Drawing.Point(773, 457);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(227, 80);
            this.groupBox3.TabIndex = 44;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " COM Port ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox4.Controls.Add(this.rbtNHA505_Touch);
            this.groupBox4.Controls.Add(this.rbtNHA505);
            this.groupBox4.Location = new System.Drawing.Point(773, 316);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(227, 61);
            this.groupBox4.TabIndex = 55;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Product (Emission)";
            // 
            // rbtNHA505_Touch
            // 
            this.rbtNHA505_Touch.AutoSize = true;
            this.rbtNHA505_Touch.Location = new System.Drawing.Point(13, 37);
            this.rbtNHA505_Touch.Name = "rbtNHA505_Touch";
            this.rbtNHA505_Touch.Size = new System.Drawing.Size(143, 17);
            this.rbtNHA505_Touch.TabIndex = 1;
            this.rbtNHA505_Touch.Text = "NHA505 (Touch Screen)";
            this.rbtNHA505_Touch.UseVisualStyleBackColor = true;
            // 
            // rbtNHA505
            // 
            this.rbtNHA505.AutoSize = true;
            this.rbtNHA505.Checked = true;
            this.rbtNHA505.Location = new System.Drawing.Point(13, 17);
            this.rbtNHA505.Name = "rbtNHA505";
            this.rbtNHA505.Size = new System.Drawing.Size(66, 17);
            this.rbtNHA505.TabIndex = 0;
            this.rbtNHA505.TabStop = true;
            this.rbtNHA505.Text = "NHA505";
            this.rbtNHA505.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(85, 271);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 24);
            this.label3.TabIndex = 56;
            this.label3.Text = "Company Name:";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox5.Controls.Add(this.rbtOpacityRPM);
            this.groupBox5.Controls.Add(this.rbtEmissionRPM);
            this.groupBox5.Location = new System.Drawing.Point(774, 389);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(226, 58);
            this.groupBox5.TabIndex = 58;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "RPM Reading (Battery && Alternator)";
            // 
            // rbtOpacityRPM
            // 
            this.rbtOpacityRPM.AutoSize = true;
            this.rbtOpacityRPM.Location = new System.Drawing.Point(13, 34);
            this.rbtOpacityRPM.Name = "rbtOpacityRPM";
            this.rbtOpacityRPM.Size = new System.Drawing.Size(104, 17);
            this.rbtOpacityRPM.TabIndex = 1;
            this.rbtOpacityRPM.Text = "Opacity Analyzer";
            this.rbtOpacityRPM.UseVisualStyleBackColor = true;
            // 
            // rbtEmissionRPM
            // 
            this.rbtEmissionRPM.AutoSize = true;
            this.rbtEmissionRPM.Checked = true;
            this.rbtEmissionRPM.Location = new System.Drawing.Point(13, 17);
            this.rbtEmissionRPM.Name = "rbtEmissionRPM";
            this.rbtEmissionRPM.Size = new System.Drawing.Size(109, 17);
            this.rbtEmissionRPM.TabIndex = 0;
            this.rbtEmissionRPM.TabStop = true;
            this.rbtEmissionRPM.Text = "Emission Analyzer";
            this.rbtEmissionRPM.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox6.Controls.Add(this.cmbxBatteryHidDevice);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.cbxOpacityUseHid);
            this.groupBox6.Controls.Add(this.cbxEmissionUseHid);
            this.groupBox6.Controls.Add(this.cmbxOpacityHidDevice);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.cmbxEmissionHidDevice);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Location = new System.Drawing.Point(419, 545);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(581, 90);
            this.groupBox6.TabIndex = 59;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " HID Device Selection";
            // 
            // cmbxBatteryHidDevice
            // 
            this.cmbxBatteryHidDevice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxBatteryHidDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxBatteryHidDevice.FormattingEnabled = true;
            this.cmbxBatteryHidDevice.Location = new System.Drawing.Point(292, 12);
            this.cmbxBatteryHidDevice.Name = "cmbxBatteryHidDevice";
            this.cmbxBatteryHidDevice.Size = new System.Drawing.Size(239, 21);
            this.cmbxBatteryHidDevice.TabIndex = 45;
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(133, 15);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(157, 13);
            this.label23.TabIndex = 46;
            this.label23.Text = "Battery Analyzer (USB Device ):";
            // 
            // cbxOpacityUseHid
            // 
            this.cbxOpacityUseHid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxOpacityUseHid.AutoSize = true;
            this.cbxOpacityUseHid.Location = new System.Drawing.Point(48, 66);
            this.cbxOpacityUseHid.Name = "cbxOpacityUseHid";
            this.cbxOpacityUseHid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbxOpacityUseHid.Size = new System.Drawing.Size(120, 17);
            this.cbxOpacityUseHid.TabIndex = 42;
            this.cbxOpacityUseHid.Text = "Use USB in Opacity";
            this.cbxOpacityUseHid.UseVisualStyleBackColor = true;
            this.cbxOpacityUseHid.CheckedChanged += new System.EventHandler(this.cbxOpacityUseHid_CheckedChanged);
            // 
            // cbxEmissionUseHid
            // 
            this.cbxEmissionUseHid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxEmissionUseHid.AutoSize = true;
            this.cbxEmissionUseHid.Location = new System.Drawing.Point(43, 40);
            this.cbxEmissionUseHid.Name = "cbxEmissionUseHid";
            this.cbxEmissionUseHid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cbxEmissionUseHid.Size = new System.Drawing.Size(125, 17);
            this.cbxEmissionUseHid.TabIndex = 39;
            this.cbxEmissionUseHid.Text = "Use USB in Emission";
            this.cbxEmissionUseHid.UseVisualStyleBackColor = true;
            this.cbxEmissionUseHid.CheckedChanged += new System.EventHandler(this.cbxEmissionUseHid_CheckedChanged);
            // 
            // cmbxOpacityHidDevice
            // 
            this.cmbxOpacityHidDevice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxOpacityHidDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxOpacityHidDevice.FormattingEnabled = true;
            this.cmbxOpacityHidDevice.Location = new System.Drawing.Point(292, 64);
            this.cmbxOpacityHidDevice.Name = "cmbxOpacityHidDevice";
            this.cmbxOpacityHidDevice.Size = new System.Drawing.Size(239, 21);
            this.cmbxOpacityHidDevice.TabIndex = 36;
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(176, 67);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(114, 13);
            this.label21.TabIndex = 38;
            this.label21.Text = "USB Device (Opacity):";
            // 
            // cmbxEmissionHidDevice
            // 
            this.cmbxEmissionHidDevice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxEmissionHidDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxEmissionHidDevice.FormattingEnabled = true;
            this.cmbxEmissionHidDevice.Location = new System.Drawing.Point(292, 38);
            this.cmbxEmissionHidDevice.Name = "cmbxEmissionHidDevice";
            this.cmbxEmissionHidDevice.Size = new System.Drawing.Size(239, 21);
            this.cmbxEmissionHidDevice.TabIndex = 36;
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(171, 41);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(119, 13);
            this.label22.TabIndex = 37;
            this.label22.Text = "USB Device (Emission):";
            // 
            // tbxCompanyName
            // 
            this.tbxCompanyName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxCompanyName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompanyName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCompanyName.Location = new System.Drawing.Point(243, 268);
            this.tbxCompanyName.Name = "tbxCompanyName";
            this.tbxCompanyName.Size = new System.Drawing.Size(683, 29);
            this.tbxCompanyName.TabIndex = 57;
            this.tbxCompanyName.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompanyName;
            // 
            // btnCalibrateBatteryModule
            // 
            this.btnCalibrateBatteryModule.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCalibrateBatteryModule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalibrateBatteryModule.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalibrateBatteryModule.Location = new System.Drawing.Point(450, 123);
            this.btnCalibrateBatteryModule.Name = "btnCalibrateBatteryModule";
            this.btnCalibrateBatteryModule.Size = new System.Drawing.Size(148, 46);
            this.btnCalibrateBatteryModule.TabIndex = 60;
            this.btnCalibrateBatteryModule.Text = "Battery Calibration";
            this.btnCalibrateBatteryModule.UseVisualStyleBackColor = true;
            this.btnCalibrateBatteryModule.Click += new System.EventHandler(this.btnCalibrateBatteryModule_Click);
            // 
            // frmSetup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1018, 698);
            this.Controls.Add(this.btnCalibrateBatteryModule);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.tbxCompanyName);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnDefault);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ckbxPublishFooter);
            this.Controls.Add(this.ckbxPublishHeader);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtbFooter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtbHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "frmSetup";
            this.Text = "Setup";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSetup_FormClosing);
            this.Load += new System.EventHandler(this.frmSetup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbHeader;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtbFooter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxEmissionSMin;
        private System.Windows.Forms.TextBox tbxEmissionSMax;
        private System.Windows.Forms.TextBox tbxEmissionCo2Max;
        private System.Windows.Forms.TextBox tbxEmissionCo2Min;
        private System.Windows.Forms.TextBox tbxEmissionHcMax;
        private System.Windows.Forms.TextBox tbxEmissionHcMin;
        private System.Windows.Forms.TextBox tbxEmissionTMax;
        private System.Windows.Forms.TextBox tbxEmissionTMin;
        private System.Windows.Forms.CheckBox ckbxEmissionPublishLimits;
        private System.Windows.Forms.CheckBox ckbxOpacityPublishLimits;
        private System.Windows.Forms.TextBox tbxOpacityTMax;
        private System.Windows.Forms.TextBox tbxOpacityTMin;
        private System.Windows.Forms.TextBox tbxOpacityKMax;
        private System.Windows.Forms.TextBox tbxOpacityKMin;
        private System.Windows.Forms.TextBox tbxOpacityNMax;
        private System.Windows.Forms.TextBox tbxOpacityNMin;
        private System.Windows.Forms.TextBox tbxOpacitySMax;
        private System.Windows.Forms.TextBox tbxOpacitySMin;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox ckbxPublishHeader;
        private System.Windows.Forms.CheckBox ckbxPublishFooter;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbxEmissionComPort;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbxOpacityComPort;
        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbxEmissionO2Min;
        private System.Windows.Forms.TextBox tbxEmissionO2Max;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbxEmissionNoMin;
        private System.Windows.Forms.TextBox tbxEmissionNoMax;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbxEmissionCoMin;
        private System.Windows.Forms.TextBox tbxEmissionCoMax;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbxCompanyName;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbtNHA505_Touch;
        private System.Windows.Forms.RadioButton rbtNHA505;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton rbtOpacityRPM;
        private System.Windows.Forms.RadioButton rbtEmissionRPM;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox cbxEmissionUseHid;
        private System.Windows.Forms.ComboBox cmbxOpacityHidDevice;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmbxEmissionHidDevice;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.CheckBox cbxOpacityUseHid;
        private System.Windows.Forms.ComboBox cmbxBatteryHidDevice;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnCalibrateBatteryModule;
    }
}