﻿namespace EngineAnalyzer.UI.VehicleDetails
{
    partial class frmCylinderCompressionTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCylinderCompressionTest));
            this.smartLabel9 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel4 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel5 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel6 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel7 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel8 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnClear = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cbxPublish = new System.Windows.Forms.CheckBox();
            this.tbxLimit = new System.Windows.Forms.TextBox();
            this.tbxRemarks6 = new System.Windows.Forms.TextBox();
            this.tbxPressure6 = new System.Windows.Forms.TextBox();
            this.tbxRemarks5 = new System.Windows.Forms.TextBox();
            this.tbxPressure5 = new System.Windows.Forms.TextBox();
            this.tbxRemarks4 = new System.Windows.Forms.TextBox();
            this.tbxPressure4 = new System.Windows.Forms.TextBox();
            this.tbxRemarks3 = new System.Windows.Forms.TextBox();
            this.tbxPressure3 = new System.Windows.Forms.TextBox();
            this.tbxRemarks2 = new System.Windows.Forms.TextBox();
            this.tbxPressure2 = new System.Windows.Forms.TextBox();
            this.tbxRemarks1 = new System.Windows.Forms.TextBox();
            this.tbxPressure1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // smartLabel9
            // 
            this.smartLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel9.AutoSize = true;
            this.smartLabel9.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.BorderColor = System.Drawing.Color.Black;
            this.smartLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel9.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel9.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.Location = new System.Drawing.Point(9, 25);
            this.smartLabel9.Name = "smartLabel9";
            this.smartLabel9.Size = new System.Drawing.Size(83, 25);
            this.smartLabel9.TabIndex = 1;
            this.smartLabel9.Text = "Cy. No.";
            this.smartLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel9.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.Location = new System.Drawing.Point(109, 25);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(98, 25);
            this.smartLabel1.TabIndex = 2;
            this.smartLabel1.Text = "Pressure";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel1.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.Location = new System.Drawing.Point(298, 25);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(97, 25);
            this.smartLabel2.TabIndex = 3;
            this.smartLabel2.Text = "Remarks";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel2.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel3
            // 
            this.smartLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel3.AutoSize = true;
            this.smartLabel3.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.BorderColor = System.Drawing.Color.Black;
            this.smartLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.Location = new System.Drawing.Point(50, 73);
            this.smartLabel3.Name = "smartLabel3";
            this.smartLabel3.Size = new System.Drawing.Size(42, 25);
            this.smartLabel3.TabIndex = 4;
            this.smartLabel3.Text = "01.";
            this.smartLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel3.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel4
            // 
            this.smartLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel4.AutoSize = true;
            this.smartLabel4.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.BorderColor = System.Drawing.Color.Black;
            this.smartLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel4.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel4.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.Location = new System.Drawing.Point(50, 118);
            this.smartLabel4.Name = "smartLabel4";
            this.smartLabel4.Size = new System.Drawing.Size(42, 25);
            this.smartLabel4.TabIndex = 5;
            this.smartLabel4.Text = "02.";
            this.smartLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel4.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel5
            // 
            this.smartLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel5.AutoSize = true;
            this.smartLabel5.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.BorderColor = System.Drawing.Color.Black;
            this.smartLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.Location = new System.Drawing.Point(50, 163);
            this.smartLabel5.Name = "smartLabel5";
            this.smartLabel5.Size = new System.Drawing.Size(42, 25);
            this.smartLabel5.TabIndex = 6;
            this.smartLabel5.Text = "03.";
            this.smartLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel5.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel6
            // 
            this.smartLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel6.AutoSize = true;
            this.smartLabel6.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.BorderColor = System.Drawing.Color.Black;
            this.smartLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.Location = new System.Drawing.Point(50, 208);
            this.smartLabel6.Name = "smartLabel6";
            this.smartLabel6.Size = new System.Drawing.Size(42, 25);
            this.smartLabel6.TabIndex = 7;
            this.smartLabel6.Text = "04.";
            this.smartLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel6.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel7
            // 
            this.smartLabel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel7.AutoSize = true;
            this.smartLabel7.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.BorderColor = System.Drawing.Color.Black;
            this.smartLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel7.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel7.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.Location = new System.Drawing.Point(50, 253);
            this.smartLabel7.Name = "smartLabel7";
            this.smartLabel7.Size = new System.Drawing.Size(42, 25);
            this.smartLabel7.TabIndex = 8;
            this.smartLabel7.Text = "05.";
            this.smartLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel7.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel8
            // 
            this.smartLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel8.AutoSize = true;
            this.smartLabel8.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.BorderColor = System.Drawing.Color.Black;
            this.smartLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel8.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel8.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.Location = new System.Drawing.Point(50, 298);
            this.smartLabel8.Name = "smartLabel8";
            this.smartLabel8.Size = new System.Drawing.Size(42, 25);
            this.smartLabel8.TabIndex = 9;
            this.smartLabel8.Text = "06.";
            this.smartLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel8.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.AutoSize = true;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(50, 343);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(168, 25);
            this.smartLabel10.TabIndex = 23;
            this.smartLabel10.Text = "Limit (minimum):";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel10.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(114, 389);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(121, 39);
            this.btnClear.TabIndex = 41;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(454, 389);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(121, 39);
            this.btnExit.TabIndex = 40;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(285, 389);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(121, 39);
            this.btnSave.TabIndex = 42;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbxPublish
            // 
            this.cbxPublish.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxPublish.AutoSize = true;
            this.cbxPublish.Checked = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestPublish;
            this.cbxPublish.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestPublish", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxPublish.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPublish.Location = new System.Drawing.Point(473, 341);
            this.cbxPublish.Name = "cbxPublish";
            this.cbxPublish.Size = new System.Drawing.Size(102, 29);
            this.cbxPublish.TabIndex = 39;
            this.cbxPublish.Text = "Publish";
            this.cbxPublish.UseVisualStyleBackColor = true;
            // 
            // tbxLimit
            // 
            this.tbxLimit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxLimit.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestLimit", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxLimit.Location = new System.Drawing.Point(229, 340);
            this.tbxLimit.Margin = new System.Windows.Forms.Padding(7);
            this.tbxLimit.Name = "tbxLimit";
            this.tbxLimit.Size = new System.Drawing.Size(224, 31);
            this.tbxLimit.TabIndex = 24;
            this.tbxLimit.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestLimit;
            // 
            // tbxRemarks6
            // 
            this.tbxRemarks6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxRemarks6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestRemarks6", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxRemarks6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRemarks6.Location = new System.Drawing.Point(302, 295);
            this.tbxRemarks6.Margin = new System.Windows.Forms.Padding(7);
            this.tbxRemarks6.Name = "tbxRemarks6";
            this.tbxRemarks6.Size = new System.Drawing.Size(273, 31);
            this.tbxRemarks6.TabIndex = 22;
            this.tbxRemarks6.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestRemarks6;
            // 
            // tbxPressure6
            // 
            this.tbxPressure6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxPressure6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestPressure6", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxPressure6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPressure6.Location = new System.Drawing.Point(114, 295);
            this.tbxPressure6.Margin = new System.Windows.Forms.Padding(7);
            this.tbxPressure6.Name = "tbxPressure6";
            this.tbxPressure6.Size = new System.Drawing.Size(174, 31);
            this.tbxPressure6.TabIndex = 21;
            this.tbxPressure6.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestPressure6;
            // 
            // tbxRemarks5
            // 
            this.tbxRemarks5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxRemarks5.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestRemarks5", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxRemarks5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRemarks5.Location = new System.Drawing.Point(302, 250);
            this.tbxRemarks5.Margin = new System.Windows.Forms.Padding(7);
            this.tbxRemarks5.Name = "tbxRemarks5";
            this.tbxRemarks5.Size = new System.Drawing.Size(273, 31);
            this.tbxRemarks5.TabIndex = 19;
            this.tbxRemarks5.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestRemarks5;
            // 
            // tbxPressure5
            // 
            this.tbxPressure5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxPressure5.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestPressure5", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxPressure5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPressure5.Location = new System.Drawing.Point(114, 250);
            this.tbxPressure5.Margin = new System.Windows.Forms.Padding(7);
            this.tbxPressure5.Name = "tbxPressure5";
            this.tbxPressure5.Size = new System.Drawing.Size(174, 31);
            this.tbxPressure5.TabIndex = 18;
            this.tbxPressure5.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestPressure5;
            // 
            // tbxRemarks4
            // 
            this.tbxRemarks4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxRemarks4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestRemarks4", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxRemarks4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRemarks4.Location = new System.Drawing.Point(302, 205);
            this.tbxRemarks4.Margin = new System.Windows.Forms.Padding(7);
            this.tbxRemarks4.Name = "tbxRemarks4";
            this.tbxRemarks4.Size = new System.Drawing.Size(273, 31);
            this.tbxRemarks4.TabIndex = 17;
            this.tbxRemarks4.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestRemarks4;
            // 
            // tbxPressure4
            // 
            this.tbxPressure4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxPressure4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestPressure4", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxPressure4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPressure4.Location = new System.Drawing.Point(114, 205);
            this.tbxPressure4.Margin = new System.Windows.Forms.Padding(7);
            this.tbxPressure4.Name = "tbxPressure4";
            this.tbxPressure4.Size = new System.Drawing.Size(174, 31);
            this.tbxPressure4.TabIndex = 16;
            this.tbxPressure4.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestPressure4;
            // 
            // tbxRemarks3
            // 
            this.tbxRemarks3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxRemarks3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestRemarks3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxRemarks3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRemarks3.Location = new System.Drawing.Point(302, 160);
            this.tbxRemarks3.Margin = new System.Windows.Forms.Padding(7);
            this.tbxRemarks3.Name = "tbxRemarks3";
            this.tbxRemarks3.Size = new System.Drawing.Size(273, 31);
            this.tbxRemarks3.TabIndex = 15;
            this.tbxRemarks3.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestRemarks3;
            // 
            // tbxPressure3
            // 
            this.tbxPressure3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxPressure3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestPressure3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxPressure3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPressure3.Location = new System.Drawing.Point(114, 160);
            this.tbxPressure3.Margin = new System.Windows.Forms.Padding(7);
            this.tbxPressure3.Name = "tbxPressure3";
            this.tbxPressure3.Size = new System.Drawing.Size(174, 31);
            this.tbxPressure3.TabIndex = 14;
            this.tbxPressure3.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestPressure3;
            // 
            // tbxRemarks2
            // 
            this.tbxRemarks2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxRemarks2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestRemarks2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxRemarks2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRemarks2.Location = new System.Drawing.Point(302, 115);
            this.tbxRemarks2.Margin = new System.Windows.Forms.Padding(7);
            this.tbxRemarks2.Name = "tbxRemarks2";
            this.tbxRemarks2.Size = new System.Drawing.Size(273, 31);
            this.tbxRemarks2.TabIndex = 13;
            this.tbxRemarks2.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestRemarks2;
            // 
            // tbxPressure2
            // 
            this.tbxPressure2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxPressure2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestPressure2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxPressure2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPressure2.Location = new System.Drawing.Point(114, 115);
            this.tbxPressure2.Margin = new System.Windows.Forms.Padding(7);
            this.tbxPressure2.Name = "tbxPressure2";
            this.tbxPressure2.Size = new System.Drawing.Size(174, 31);
            this.tbxPressure2.TabIndex = 12;
            this.tbxPressure2.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestPressure2;
            // 
            // tbxRemarks1
            // 
            this.tbxRemarks1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxRemarks1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestRemarks1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxRemarks1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRemarks1.Location = new System.Drawing.Point(302, 70);
            this.tbxRemarks1.Margin = new System.Windows.Forms.Padding(7);
            this.tbxRemarks1.Name = "tbxRemarks1";
            this.tbxRemarks1.Size = new System.Drawing.Size(273, 31);
            this.tbxRemarks1.TabIndex = 11;
            this.tbxRemarks1.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestRemarks1;
            // 
            // tbxPressure1
            // 
            this.tbxPressure1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxPressure1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EngineAnalyzer.Properties.Settings.Default, "__CompressionTestPressure1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.tbxPressure1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPressure1.Location = new System.Drawing.Point(114, 70);
            this.tbxPressure1.Margin = new System.Windows.Forms.Padding(7);
            this.tbxPressure1.Name = "tbxPressure1";
            this.tbxPressure1.Size = new System.Drawing.Size(174, 31);
            this.tbxPressure1.TabIndex = 10;
            this.tbxPressure1.Text = global::EngineAnalyzer.Properties.Settings.Default.@__CompressionTestPressure1;
            // 
            // frmCylinderCompressionTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(611, 446);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.cbxPublish);
            this.Controls.Add(this.tbxLimit);
            this.Controls.Add(this.smartLabel10);
            this.Controls.Add(this.tbxRemarks6);
            this.Controls.Add(this.tbxPressure6);
            this.Controls.Add(this.tbxRemarks5);
            this.Controls.Add(this.tbxPressure5);
            this.Controls.Add(this.tbxRemarks4);
            this.Controls.Add(this.tbxPressure4);
            this.Controls.Add(this.tbxRemarks3);
            this.Controls.Add(this.tbxPressure3);
            this.Controls.Add(this.tbxRemarks2);
            this.Controls.Add(this.tbxPressure2);
            this.Controls.Add(this.tbxRemarks1);
            this.Controls.Add(this.tbxPressure1);
            this.Controls.Add(this.smartLabel8);
            this.Controls.Add(this.smartLabel7);
            this.Controls.Add(this.smartLabel6);
            this.Controls.Add(this.smartLabel5);
            this.Controls.Add(this.smartLabel4);
            this.Controls.Add(this.smartLabel3);
            this.Controls.Add(this.smartLabel2);
            this.Controls.Add(this.smartLabel1);
            this.Controls.Add(this.smartLabel9);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCylinderCompressionTest";
            this.Text = "Cylinder Compression Test";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel9;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel3;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel4;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel5;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel6;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel7;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel8;
        private System.Windows.Forms.TextBox tbxPressure1;
        private System.Windows.Forms.TextBox tbxRemarks1;
        private System.Windows.Forms.TextBox tbxRemarks2;
        private System.Windows.Forms.TextBox tbxPressure2;
        private System.Windows.Forms.TextBox tbxRemarks3;
        private System.Windows.Forms.TextBox tbxPressure3;
        private System.Windows.Forms.TextBox tbxRemarks4;
        private System.Windows.Forms.TextBox tbxPressure4;
        private System.Windows.Forms.TextBox tbxRemarks5;
        private System.Windows.Forms.TextBox tbxPressure5;
        private System.Windows.Forms.TextBox tbxRemarks6;
        private System.Windows.Forms.TextBox tbxPressure6;
        private System.Windows.Forms.TextBox tbxLimit;
        public InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnExit;
        public System.Windows.Forms.CheckBox cbxPublish;
        private System.Windows.Forms.Button btnSave;
    }
}