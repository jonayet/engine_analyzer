﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace EngineAnalyzer.UI.VehicleDetails
{
    public partial class frmPhysicalInspection : Form
    {
        public frmPhysicalInspection()
        {
            InitializeComponent();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            base.OnClosing(e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbxBEL_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxBEL_OK.Checked)
            {
                cbxBEL_NOK.Checked = false;
            }
        }

        private void cbxBEL_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxBEL_NOK.Checked)
            {
                cbxBEL_OK.Checked = false;
            }
        }

        private void cbxTC_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxTC_OK.Checked)
            {
                cbxTC_NOK.Checked = false;
            }
        }

        private void cbxTC_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxTC_NOK.Checked)
            {
                cbxTC_OK.Checked = false;
            }
        }

        private void cbxTP_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxTP_OK.Checked)
            {
                cbxTP_NOK.Checked = false;
            }
        }

        private void cbxTP_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxTP_NOK.Checked)
            {
                cbxTP_OK.Checked = false;
            }
        }

        private void cbxCS_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxCS_OK.Checked)
            {
                cbxCS_NOK.Checked = false;
            }
        }

        private void cbxCS_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxCS_NOK.Checked)
            {
                cbxCS_OK.Checked = false;
            }
        }

        private void cbxHL_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxHL_OK.Checked)
            {
                cbxHL_NOK.Checked = false;
            }
        }

        private void cbxHL_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxHL_NOK.Checked)
            {
                cbxHL_OK.Checked = false;
            }
        }

        private void cbxPL_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxPL_OK.Checked)
            {
                cbxPL_NOK.Checked = false;
            }
        }

        private void cbxPL_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxPL_NOK.Checked)
            {
                cbxPL_OK.Checked = false;
            }
        }

        private void cbxH_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxH_OK.Checked)
            {
                cbxH_NOK.Checked = false;
            }
        }

        private void cbxH_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxH_NOK.Checked)
            {
                cbxH_OK.Checked = false;
            }
        }

        private void cbxA_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxA_OK.Checked)
            {
                cbxA_NOK.Checked = false;
            }
        }

        private void cbxA_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxA_NOK.Checked)
            {
                cbxA_OK.Checked = false;
            }
        }

        private void cbxBO_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxBO_OK.Checked)
            {
                cbxBO_NOK.Checked = false;
            }
        }

        private void cbxBO_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxBO_NOK.Checked)
            {
                cbxBO_OK.Checked = false;
            }
        }

        private void cbxEO_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxEO_OK.Checked)
            {
                cbxEO_NOK.Checked = false;
            }
        }

        private void cbxEO_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxEO_NOK.Checked)
            {
                cbxEO_OK.Checked = false;
            }
        }

        private void cbxTO_OK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxTO_OK.Checked)
            {
                cbxTO_NOK.Checked = false;
            }
        }

        private void cbxTO_NOK_CheckedChanged(object sender, EventArgs e)
        {
            if (cbxTO_NOK.Checked)
            {
                cbxTO_OK.Checked = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cbxA_NOK.Checked = false;
            cbxA_OK.Checked = false;
            cbxBEL_NOK.Checked = false;
            cbxBEL_OK.Checked = false;
            cbxBO_NOK.Checked = false;
            cbxBO_OK.Checked = false;
            cbxCS_NOK.Checked = false;
            cbxCS_OK.Checked = false;
            cbxEO_NOK.Checked = false;
            cbxEO_OK.Checked = false;
            cbxH_NOK.Checked = false;
            cbxH_OK.Checked = false;
            cbxHL_NOK.Checked = false;
            cbxHL_OK.Checked = false;
            cbxPL_NOK.Checked = false;
            cbxPL_OK.Checked = false;
            cbxTC_NOK.Checked = false;
            cbxTC_OK.Checked = false;
            cbxTO_NOK.Checked = false;
            cbxTO_OK.Checked = false;
            cbxTP_NOK.Checked = false;
            cbxTP_OK.Checked = false;
        }
    }
}
