﻿using System;
using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using InterfaceLab.WinForm.Tools;
using RTF;

namespace EngineAnalyzer.UI.VehicleDetails
{
    public partial class frmVehicleDetails : Form
    {
        // our object for tracking the "dirty" status of the form
        private DirtyTracker _dirtyTracker;
        private RTFBuilderbase richTextBuilder = new RTFBuilder(RTFFont.Arial, 22);
        frmPhysicalInspection phyObs;
        frmCylinderCompressionTest compressionTest;

        public frmVehicleDetails()
        {
            InitializeComponent();
        }

        private void frmVehicleDetails_Load(object sender, EventArgs e)
        {
            // load previous settings at startup
            //tbxVehicleDetailsDate.Text = Properties.Settings.Default.VehicleDetailsDate;
            tbxVehicleDetailsDate.Text = DateTime.Now.ToLongDateString();
            tbxVehicleDetailsOthers.Text = Properties.Settings.Default.VehicleDetailsOthers;
            tbxVehicleEngineRef.Text = Properties.Settings.Default.VehicleEngineRef;
            tbxVehicleFuel.Text = Properties.Settings.Default.VehicleFuel;
            tbxVehicleMake.Text = Properties.Settings.Default.VehicleMake;
            tbxVehicleManufacturingYear.Text = Properties.Settings.Default.VehicleMYear;
            tbxVehicleModel.Text = Properties.Settings.Default.VehicleModel;
            tbxVehicleOdometer.Text = Properties.Settings.Default.VehicleOdometer;
            tbxVehicleRegNo.Text = Properties.Settings.Default.VehicleRegNo;
            tbxVehicleSwiftVolume.Text = Properties.Settings.Default.VehicleSwiftVolume;

            // in the Load event initialize our tracking object
            _dirtyTracker = new DirtyTracker(this);
            _dirtyTracker.SetAsClean();
            _dirtyTracker.DirtyTracked += new EventHandler(_dirtyTracker_DirtyTracked);
            _dirtyTracker.TrackCleaned += new EventHandler(_dirtyTracker_TrackCleaned);
        }

        private void frmVehicleDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_dirtyTracker.IsDirty)
            {
                DialogResult dRes = MessageBox.Show("Would you like to save changes before closing?", "Warning!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                // cancel the closing
                if (dRes == DialogResult.Cancel) { e.Cancel = true; return; }

                if (dRes == DialogResult.Yes)
                {
                    // save the new settings
                    btnSave_Click(this, EventArgs.Empty);
                }
            }
        }

        void _dirtyTracker_DirtyTracked(object sender, EventArgs e)
        {
            this.Text += "  * (unsaved)";
            CommonResources.PhysicalInspectionRichText = "";
        }

        void _dirtyTracker_TrackCleaned(object sender, EventArgs e)
        {
            this.Text = this.Text.Replace("  * (unsaved)", "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // save the new settings
            Properties.Settings.Default["VehicleDetailsDate"] = tbxVehicleDetailsDate.Text;
            Properties.Settings.Default["VehicleDetailsOthers"] = tbxVehicleDetailsOthers.Text;
            Properties.Settings.Default["VehicleEngineRef"] = tbxVehicleEngineRef.Text;
            Properties.Settings.Default["VehicleFuel"] = tbxVehicleFuel.Text;
            Properties.Settings.Default["VehicleMake"] = tbxVehicleMake.Text;
            Properties.Settings.Default["VehicleMYear"] = tbxVehicleManufacturingYear.Text;
            Properties.Settings.Default["VehicleModel"] = tbxVehicleModel.Text;
            Properties.Settings.Default["VehicleOdometer"] = tbxVehicleOdometer.Text;
            Properties.Settings.Default["VehicleRegNo"] = tbxVehicleRegNo.Text;
            Properties.Settings.Default["VehicleSwiftVolume"] = tbxVehicleSwiftVolume.Text;
            Properties.Settings.Default.Save();

            // make DirtyTracker clean
            _dirtyTracker.SetAsClean();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            // clear all text values
            tbxVehicleDetailsDate.Text = "";
            tbxVehicleDetailsOthers.Text = "";
            tbxVehicleEngineRef.Text = "";
            tbxVehicleFuel.Text = "";
            tbxVehicleMake.Text = "";
            tbxVehicleManufacturingYear.Text = "";
            tbxVehicleModel.Text = "";
            tbxVehicleOdometer.Text = "";
            tbxVehicleRegNo.Text = "";
            tbxVehicleSwiftVolume.Text = "";
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            // clos the form
            this.Close();
        }

        private void btnPhyObservation_Click(object sender, EventArgs e)
        {
            //frmPhysicalInspection phyObs = new frmPhysicalInspection();
            if (phyObs == null) { phyObs = new frmPhysicalInspection(); }
            phyObs.ShowDialog();

            // add title, if nesecery
            if (Settings.Default.A_NOK || Settings.Default.A_OK || Settings.Default.BEL_NOK || Settings.Default.BEL_OK || Settings.Default.BO_NOK || Settings.Default.BO_OK
                || Settings.Default.CS_NOK || Settings.Default.CS_OK || Settings.Default.EO_NOK || Settings.Default.EO_OK || Settings.Default.H_NOK || Settings.Default.H_OK
                || Settings.Default.HL_NOK || Settings.Default.HL_OK || Settings.Default.PL_NOK || Settings.Default.PL_OK || Settings.Default.TC_NOK || Settings.Default.TC_OK
                || Settings.Default.TO_NOK || Settings.Default.TO_OK || Settings.Default.TP_NOK || Settings.Default.TP_OK)
            {
                richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).Alignment(StringAlignment.Center).AppendLine("Visual Inspection Report");
            }

            richTextBuilder.AppendPara();
            richTextBuilder.Reset();

            // add BEL
            if(Settings.Default.BEL_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblBEL.Text, phyObs.cbxBEL_OK.Text);
            }
            else if(Settings.Default.BEL_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblBEL.Text, phyObs.cbxBEL_NOK.Text);
            }

            // add TC
            if (Settings.Default.TC_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblTC.Text, phyObs.cbxTC_OK.Text);
            }
            else if (Settings.Default.TC_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblTC.Text, phyObs.cbxTC_NOK.Text);
            }

            // add TP
            if (Settings.Default.TP_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblTP.Text, phyObs.cbxTP_OK.Text);
            }
            else if (Settings.Default.TP_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblTP.Text, phyObs.cbxTP_NOK.Text);
            }

            // add CS
            if (Settings.Default.CS_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblCS.Text, phyObs.cbxCS_OK.Text);
            }
            else if (Settings.Default.CS_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblCS.Text, phyObs.cbxCS_NOK.Text);
            }

            // add HL
            if (Settings.Default.HL_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblHL.Text, phyObs.cbxHL_OK.Text);
            }
            else if (Settings.Default.HL_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblHL.Text, phyObs.cbxHL_NOK.Text);
            }

            // add PL
            if (Settings.Default.PL_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblPL.Text, phyObs.cbxPL_OK.Text);
            }
            else if (Settings.Default.PL_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblPL.Text, phyObs.cbxPL_NOK.Text);
            }

            // add H
            if (Settings.Default.H_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblH.Text, phyObs.cbxH_OK.Text);
            }
            else if (Settings.Default.H_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblH.Text, phyObs.cbxH_NOK.Text);
            }

            // add A
            if (Settings.Default.A_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblA.Text, phyObs.cbxA_OK.Text);
            }
            else if (Settings.Default.A_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblA.Text, phyObs.cbxA_NOK.Text);
            }

            // add BO
            if (Settings.Default.BO_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblBO.Text, phyObs.cbxBO_OK.Text);
            }
            else if (Settings.Default.BO_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblBO.Text, phyObs.cbxBO_NOK.Text);
            }

            // add EO
            if (Settings.Default.EO_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblEO.Text, phyObs.cbxEO_OK.Text);
            }
            else if (Settings.Default.EO_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblEO.Text, phyObs.cbxEO_NOK.Text);
            }

            // add TO
            if (Settings.Default.TO_OK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblTO.Text, phyObs.cbxTO_OK.Text);
            }
            else if (Settings.Default.TO_NOK)
            {
                CommonResources.RTF_AddRow(richTextBuilder, new int[] { 50, 10 }, phyObs.lblTO.Text, phyObs.cbxTO_NOK.Text);
            }

            richTextBuilder.Reset();
            richTextBuilder.AppendLine();
            richTextBuilder.AppendLine();

            // save new values
            CommonResources.PhysicalInspectionRichText = richTextBuilder.ToString();
        }

        private void btnCylinderCompressionTest_Click(object sender, EventArgs e)
        {
            if (compressionTest == null) { compressionTest = new frmCylinderCompressionTest(); }
            compressionTest.ShowDialog();
        }
    }
}
