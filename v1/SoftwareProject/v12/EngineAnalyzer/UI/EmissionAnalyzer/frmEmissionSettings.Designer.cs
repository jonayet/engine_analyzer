﻿namespace EngineAnalyzer.UI.EmissionAnalyzer
{
    partial class frmEmissionSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmissionSettings));
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel4 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.cmbxCycle = new System.Windows.Forms.ComboBox();
            this.cmbxFuel = new System.Windows.Forms.ComboBox();
            this.cmbxSparkCoil = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.smartLabel5 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblUsbConnected = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.SuspendLayout();
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.Location = new System.Drawing.Point(75, 62);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(97, 33);
            this.smartLabel2.TabIndex = 1;
            this.smartLabel2.Text = "Cycle:";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel2.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel3
            // 
            this.smartLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel3.AutoSize = true;
            this.smartLabel3.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.BorderColor = System.Drawing.Color.Black;
            this.smartLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.Location = new System.Drawing.Point(92, 116);
            this.smartLabel3.Name = "smartLabel3";
            this.smartLabel3.Size = new System.Drawing.Size(80, 33);
            this.smartLabel3.TabIndex = 2;
            this.smartLabel3.Text = "Fuel:";
            this.smartLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel3.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel4
            // 
            this.smartLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel4.AutoSize = true;
            this.smartLabel4.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.BorderColor = System.Drawing.Color.Black;
            this.smartLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel4.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel4.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.Location = new System.Drawing.Point(14, 170);
            this.smartLabel4.Name = "smartLabel4";
            this.smartLabel4.Size = new System.Drawing.Size(158, 33);
            this.smartLabel4.TabIndex = 3;
            this.smartLabel4.Text = "Spark Coil:";
            this.smartLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel4.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // cmbxCycle
            // 
            this.cmbxCycle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxCycle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxCycle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCycle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxCycle.FormattingEnabled = true;
            this.cmbxCycle.Items.AddRange(new object[] {
            "4 Cycle",
            "2 Cycle"});
            this.cmbxCycle.Location = new System.Drawing.Point(182, 60);
            this.cmbxCycle.Name = "cmbxCycle";
            this.cmbxCycle.Size = new System.Drawing.Size(249, 37);
            this.cmbxCycle.TabIndex = 2;
            // 
            // cmbxFuel
            // 
            this.cmbxFuel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxFuel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxFuel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxFuel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxFuel.FormattingEnabled = true;
            this.cmbxFuel.Items.AddRange(new object[] {
            "Gasoline",
            "C.N.G."});
            this.cmbxFuel.Location = new System.Drawing.Point(182, 114);
            this.cmbxFuel.Name = "cmbxFuel";
            this.cmbxFuel.Size = new System.Drawing.Size(249, 37);
            this.cmbxFuel.TabIndex = 3;
            // 
            // cmbxSparkCoil
            // 
            this.cmbxSparkCoil.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxSparkCoil.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxSparkCoil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxSparkCoil.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxSparkCoil.FormattingEnabled = true;
            this.cmbxSparkCoil.Items.AddRange(new object[] {
            "Single",
            "Twin"});
            this.cmbxSparkCoil.Location = new System.Drawing.Point(182, 168);
            this.cmbxSparkCoil.Name = "cmbxSparkCoil";
            this.cmbxSparkCoil.Size = new System.Drawing.Size(249, 37);
            this.cmbxSparkCoil.TabIndex = 4;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(250, 226);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(181, 54);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(46, 226);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(181, 54);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // smartLabel5
            // 
            this.smartLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel5.AutoSize = true;
            this.smartLabel5.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.BorderColor = System.Drawing.Color.Black;
            this.smartLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.Location = new System.Drawing.Point(113, 10);
            this.smartLabel5.Name = "smartLabel5";
            this.smartLabel5.Size = new System.Drawing.Size(266, 33);
            this.smartLabel5.TabIndex = 10;
            this.smartLabel5.Text = "Emission - Settings";
            this.smartLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel5.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblUsbConnected
            // 
            this.lblUsbConnected.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblUsbConnected.BackColor = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblUsbConnected.BackShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.BorderColor = System.Drawing.Color.Black;
            this.lblUsbConnected.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.lblUsbConnected.ForeColor = System.Drawing.Color.White;
            this.lblUsbConnected.ForeGradientColor2 = System.Drawing.Color.White;
            this.lblUsbConnected.ForeShadowAmount = 1;
            this.lblUsbConnected.ForeShadowColor = System.Drawing.Color.Black;
            this.lblUsbConnected.ForeShadowOffset = 1;
            this.lblUsbConnected.Location = new System.Drawing.Point(295, 288);
            this.lblUsbConnected.Name = "lblUsbConnected";
            this.lblUsbConnected.Size = new System.Drawing.Size(136, 24);
            this.lblUsbConnected.TabIndex = 260;
            this.lblUsbConnected.Text = " Not Conected";
            this.lblUsbConnected.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblUsbConnected.TextBorderColor = System.Drawing.Color.Black;
            // 
            // frmEmissionSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(454, 321);
            this.Controls.Add(this.lblUsbConnected);
            this.Controls.Add(this.smartLabel5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.cmbxSparkCoil);
            this.Controls.Add(this.cmbxFuel);
            this.Controls.Add(this.cmbxCycle);
            this.Controls.Add(this.smartLabel4);
            this.Controls.Add(this.smartLabel3);
            this.Controls.Add(this.smartLabel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(460, 350);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(460, 350);
            this.Name = "frmEmissionSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel3;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel4;
        private System.Windows.Forms.ComboBox cmbxCycle;
        private System.Windows.Forms.ComboBox cmbxFuel;
        private System.Windows.Forms.ComboBox cmbxSparkCoil;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel5;
        private InterfaceLab.WinForm.Controls.SmartLabel lblUsbConnected;
    }
}