﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;
using INFRA.USB;
using RTF;

// ReSharper disable CSharpWarnings::CS1591
// ReSharper disable SuggestUseVarKeywordEvident
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local
// ReSharper disable RedundantExplicitArrayCreation
// ReSharper disable RedundantDefaultFieldInitializer
// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable RedundantDelegateCreation
// ReSharper disable LocalizableElement
// ReSharper disable SpecifyACultureInStringConversionExplicitly

namespace EngineAnalyzer.UI.EmissionAnalyzer
{
    public partial class frmEmissionRealTimeTest : Form
    {
        private readonly EmissionAnalyzerManager _emissionAnalyzerManager;
        private readonly RTFBuilderbase _richTextBuilder;
        private const float AFR_CONSTANT = 14.7f;
        //private int _asyncDataLength;

        public frmEmissionRealTimeTest()
        {
            InitializeComponent();

            // load data length. vary on model number
            if (Settings.Default.__NHA505)
            {
                //_asyncDataLength = 19;
            }
            else if (Settings.Default.__NHA505_Touch)
            {
                //_asyncDataLength = 18;
            }

            // choose communication port
            if (Settings.Default.__EmissionUseHid)
            {
                HidToSerialDevice hidToSerialDevice = new HidToSerialDevice(Settings.Default.__EmissionHidVendorId, Settings.Default.__EmissionHidProductId);
                hidToSerialDevice.HidDevice.OnDeviceAttached += new EventHandler(hidToSerialDevice_OnDeviceAttached);
                hidToSerialDevice.HidDevice.OnDeviceRemoved += new EventHandler(hidToSerialDevice_OnDeviceRemoved);
                HidCommunication hidCom = new HidCommunication(hidToSerialDevice);
                _emissionAnalyzerManager = new EmissionAnalyzerManager(hidCom);
            }
            else
            {
                SerialComunication serCom = new SerialComunication(CommonResources.EmissionPortName, 9600);
                _emissionAnalyzerManager = new EmissionAnalyzerManager(serCom);
            }

            // asign event handler
            _emissionAnalyzerManager.RealtimeDataReceived += _emissionAnalyzerManager_RealtimeDataReceived;
            _richTextBuilder = new RTFBuilder();
        }

        protected override void OnLoad(EventArgs e)
        {
            // get limits
            lblCO2Max.Text = Settings.Default.EmissionCO2Max.ToString();
            lblCO2Min.Text = Settings.Default.EmissionCO2Min.ToString();
            lblCOMax.Text = Settings.Default.EmissionCOMax.ToString();
            lblCOMin.Text = Settings.Default.EmissionCOMin.ToString();
            lblNOMax.Text = Settings.Default.EmissionNOMax.ToString();
            lblNOMin.Text = Settings.Default.EmissionNOMin.ToString();
            lblHCMax.Text = Settings.Default.EmissionHCMax.ToString();
            lblHCMin.Text = Settings.Default.EmissionHCMin.ToString();
            lblO2Max.Text = Settings.Default.EmissionO2Max.ToString();
            lblO2Min.Text = Settings.Default.EmissionO2Min.ToString();

            // show limits
            if (!Settings.Default.EmissionPublishLimits)
            {
                pnlCO2Limits.Visible = false;
                pnlCOLimits.Visible = false;
                pnlHCLimits.Visible = false;
                pnlNOLimits.Visible = false;
                pnlO2Limits.Visible = false;
            }

            // load Total Capture
            if (CommonResources.EmissionTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.EmissionTotalCapture.ToString();
            }

            // load RichText
            _richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.EmissionCapturedRichText))
            {
                // add Emission Title
                _richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).Alignment(StringAlignment.Center).AppendLine("Emission Analysis Report");
                _richTextBuilder.AppendLine();
                _richTextBuilder.AppendLine();

                // add Emission Limits
                if (Settings.Default.EmissionPublishLimits)
                {
                    _richTextBuilder.FontStyle(FontStyle.Bold).Append("Limits");
                    _richTextBuilder.AppendPara();
                    _richTextBuilder.Reset();
                    CommonResources.RTF_AddRow(_richTextBuilder, new[] { 40, 10, 10 }, "Diagnosis Limit", "Min", "Max");
                    CommonResources.RTF_AddRow(_richTextBuilder, new[] { 40, 10, 10 }, "Idle RPM", Settings.Default.EmissionSMin.ToString(), Settings.Default.EmissionSMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new[] { 40, 10, 10 }, "CO %", Settings.Default.EmissionCOMin.ToString(), Settings.Default.EmissionCOMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new[] { 40, 10, 10 }, "HC PPM", Settings.Default.EmissionHCMin.ToString(), Settings.Default.EmissionHCMax.ToString());
                    CommonResources.RTF_AddRow(_richTextBuilder, new[] { 40, 10, 10 }, "Engine Temp", Settings.Default.EmissionTMin.ToString(), Settings.Default.EmissionTMax.ToString());
                    _richTextBuilder.Reset();
                    _richTextBuilder.AppendLine();
                    _richTextBuilder.AppendLine();
                }
            }
            else
            {
                _richTextBuilder.AppendRTFDocument(CommonResources.EmissionCapturedRichText);
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // open communication port
            _emissionAnalyzerManager.Open();

            // pass to base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _emissionAnalyzerManager.TurnPumpOff();
            _emissionAnalyzerManager.Close();
            base.OnClosing(e);
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.EmissionTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.EmissionTotalCapture.ToString();

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                _richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            //else
            //{
            //    _richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            //}

            // add HC, CO, CO2, O2, NO, n, Lamda and T
            _richTextBuilder.AppendPara();
            _richTextBuilder.Reset();
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "Engine speed", lblSpeed.Text + " RPM");
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "HC (Un-burn Hydrocarbon)", lblHC.Text + " ppm");
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "CO (Carbon Monoxide)", lblCO.Text + " %");
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "CO2 (Carbon Dioxide)", lblCO2.Text + " %");
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "O2 (Oxygen)", lblO2.Text + " %");
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "NO (Nitric Oxide)", lblNO.Text + " ppm");
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "Air Fuel Ratio", lblAFR.Text );
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "Calculated Lambda", lblLamda.Text);
            CommonResources.RTF_AddRow(_richTextBuilder, new[] {40, 20}, "Engine Temperature", lblT.Text + " °C");
            _richTextBuilder.Reset();
            _richTextBuilder.AppendLine();
            _richTextBuilder.AppendLine();

            // save new values
            CommonResources.EmissionCapturedRichText = _richTextBuilder.ToString();
        }

        private void btnMeasure_Click(object sender, EventArgs e)
        {
            if (btnMeasure.Text == "MEASURE")
            {
                if (_emissionAnalyzerManager.TurnPumpOn() == string.Empty)
                {
                    btnMeasure.Text = "STOP";
                    _emissionAnalyzerManager.StartAsyncRealTimeData(500);
                }
                else
                {
                    MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                _emissionAnalyzerManager.StopAsyncRealTimeData();
                if (_emissionAnalyzerManager.TurnPumpOff() == string.Empty)
                {
                    btnMeasure.Text = "MEASURE";
                }
                else
                {

                    MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        void _emissionAnalyzerManager_RealtimeDataReceived(object sender, RealtimeEmissionDataEventArgs args)
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new RealtimeEmissionDataReceivedEventHandler(_emissionAnalyzerManager_RealtimeDataReceived),
                        new[] {sender, args});
                }
                else
                {
                    // show the values
                    lblHC.Text = args.HC.ToString();
                    lblCO.Text = args.CO.ToString("0.00");
                    lblCO2.Text = args.CO2.ToString("0.00");
                    lblO2.Text = args.O2.ToString("0.00");
                    lblNO.Text = args.NO.ToString();
                    lblSpeed.Text = args.RPM.ToString();
                    lblT.Text = args.Temperature.ToString();
                    lblLamda.Text = args.Lamda.ToString();
                    lblAFR.Text = (args.Lamda*AFR_CONSTANT).ToString("0.00") + " : 1";
                    int pos = args.RPM/40;
                    if (pos > 100)
                    {
                        pos = 100;
                    }
                    pbrSpped.Position = pos;
                }
            }
            catch { }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            _richTextBuilder.Clear();
            CommonResources.EmissionTotalCapture = 0;
            CommonResources.EmissionCapturedRichText = "";
            ThreadHelperClass.SetText(this, btnCapture, "CAPTURE");
        }

        void hidToSerialDevice_OnDeviceRemoved(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Not Connected");
        }

        void hidToSerialDevice_OnDeviceAttached(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Connected");
        }
    }
}
