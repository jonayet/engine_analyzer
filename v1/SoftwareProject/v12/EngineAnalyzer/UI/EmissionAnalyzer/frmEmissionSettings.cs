﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using EngineAnalyzer.LogicLayer;
using EngineAnalyzer.Properties;
using EngineAnalyzer.Utilities;
using INFRA.USB;

// ReSharper disable InconsistentNaming
namespace EngineAnalyzer.UI.EmissionAnalyzer
{
    public partial class frmEmissionSettings : Form
    {
        private readonly LogicLayer.EmissionAnalyzerManager _eAnalyzerManager;
        private const int SET_FOUR_STROKE = 0x04;
        private const int SET_TWO_STROKE = 0x05;
        private const int SET_AS_GAS = 0x06;
        private const int SET_AS_LPG = 0x07;
        private const int SET_IGNITION_SINGLE = 0x0A;
        private const int SET_IGNITION_TWIN = 0x0B;

        public frmEmissionSettings()
        {
            InitializeComponent();

            // choose communication port
            if (Settings.Default.__EmissionUseHid)
            {
                var hidToSerialDevice = new HidToSerialDevice(Settings.Default.__EmissionHidVendorId, Settings.Default.__EmissionHidProductId);
                hidToSerialDevice.HidDevice.OnDeviceAttached += new EventHandler(hidPort_OnDeviceAttached);
                hidToSerialDevice.HidDevice.OnDeviceRemoved += new EventHandler(hidPort_OnDeviceRemoved);
                var hidCom = new HidCommunication(hidToSerialDevice);
                _eAnalyzerManager = new LogicLayer.EmissionAnalyzerManager(hidCom);
            }
            else
            {
                var serCom = new SerialComunication(CommonResources.EmissionPortName, 9600);
                _eAnalyzerManager = new LogicLayer.EmissionAnalyzerManager(serCom);
            }
        }

        protected override void  OnLoad(EventArgs e)
        {
            cmbxCycle.SelectedIndex = Settings.Default.EmissionSettingsCycleIndex;
            cmbxFuel.SelectedIndex = Settings.Default.EmissionSettingsFuelIndex;
            cmbxSparkCoil.SelectedIndex = Settings.Default.EmissionSettingsSparkCoilIndex;

            // open communication port
            _eAnalyzerManager.Open();

            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _eAnalyzerManager.Close();
            base.OnClosing(e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var engineStrokeType = (byte)(cmbxCycle.SelectedIndex == 0 ? SET_FOUR_STROKE : SET_TWO_STROKE);
            var fuelType = (byte)(cmbxFuel.SelectedIndex == 0 ? SET_AS_GAS : SET_AS_LPG);
            var sparkCoilType = (byte)(cmbxSparkCoil.SelectedIndex == 0 ? SET_IGNITION_SINGLE : SET_IGNITION_TWIN);

            var error = _eAnalyzerManager.SetNewSettings(engineStrokeType, fuelType, sparkCoilType);
            if (error == "")
            {
                Settings.Default["EmissionSettingsCycleIndex"] = cmbxCycle.SelectedIndex;
                Settings.Default["EmissionSettingsFuelIndex"] = cmbxFuel.SelectedIndex;
                Settings.Default["EmissionSettingsSparkCoilIndex"] = cmbxSparkCoil.SelectedIndex;
                Settings.Default.Save();
                MessageBox.Show("Saved successfully!");
            }
            else
            {
                MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        void hidPort_OnDeviceAttached(object sender, EventArgs e)
        {
            lblUsbConnected.Text = "Connected";
            ThreadHelperClass.SetText(this, lblUsbConnected, "Connected");
        }

        void hidPort_OnDeviceRemoved(object sender, EventArgs e)
        {
            ThreadHelperClass.SetText(this, lblUsbConnected, "Not Connected");
        }
    }
}
