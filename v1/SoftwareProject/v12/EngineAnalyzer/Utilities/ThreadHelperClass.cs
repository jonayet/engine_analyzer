﻿using System.Windows.Forms;

namespace EngineAnalyzer.Utilities
{
    public static class ThreadHelperClass
    {
        delegate void SetTextCallback(Form f, Control ctrl, string text);
        delegate void SetVisibilityCallback(Form f, Control ctrl, bool visible);

        /// <summary>
        /// Set text property of various controls
        /// </summary>
        /// <param name="form">The calling form</param>
        /// <param name="ctrl"></param>
        /// <param name="text"></param>
        public static void SetText(Form form, Control ctrl, string text)
        {
            // InvokeRequired required compares the thread ID of the 
            // calling thread to the thread ID of the creating thread. 
            // If these threads are different, it returns true. 
            try
            {
                if (ctrl.InvokeRequired)
                {
                    SetTextCallback d = SetText;
                    form.Invoke(d, new object[] {form, ctrl, text});
                }
                else
                {
                    ctrl.Text = text;
                }
            }
            catch { }
        }

        public static void SetVisibility(Form form, Control ctrl, bool visible)
        {
            // InvokeRequired required compares the thread ID of the 
            // calling thread to the thread ID of the creating thread. 
            // If these threads are different, it returns true. 
            try
            {
                if (ctrl.InvokeRequired)
                {
                    SetVisibilityCallback d = SetVisibility;
                    form.Invoke(d, new object[] { form, ctrl, visible });
                }
                else
                {
                    ctrl.Visible = visible;
                }
            }
            catch { }
        }
    }
}
