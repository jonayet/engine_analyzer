﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EngineAnalyzer.Utilities
{
    public static class Converter
    {
        public static Int16 GetInt16Value(byte[] data, int startIndex)
        {
            return (Int16)((data[startIndex] << 8) | data[startIndex + 1]);
        }
    }
}
