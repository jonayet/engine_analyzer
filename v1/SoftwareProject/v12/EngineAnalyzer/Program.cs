﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using EngineAnalyzer.UI;
using EngineAnalyzer.UI.BatteryAnalyzer;
using EngineAnalyzer.Utilities;

namespace EngineAnalyzer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // make settings portable
            PortableSettingsProvider portableSettingsProvider = new PortableSettingsProvider();
            Properties.Settings.Default.Providers.Add(portableSettingsProvider);
            foreach (System.Configuration.SettingsProperty property in Properties.Settings.Default.Properties)
            {
                property.Provider = portableSettingsProvider;
            }
            // make settings portable

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmRegistrationChecker());
            if (!RegistrationModule.IsRegistered) { return; }
            Application.Run(new frmMain());
            //Application.Run(new frmBatteryAnalysis());
            //Application.Run(new BatteryAnalyzerCalibrationForm());
        }
    }
}