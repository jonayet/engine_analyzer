﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace RegistrationPanel
{
    public partial class frmMain : Form
    {
        const string Password = "engine123";

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            tbxRegCode.Text = RegistrationModule.GenerateRegistrationCode(tbxProductId.Text, tbxMachineId.Text, tbxSharedKey.Text);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbxPassword_TextChanged(object sender, EventArgs e)
        {
            if (tbxPassword.Text.Equals(Password))
            {
                tbxPassword.Visible = false;
                this.Width = 500;
                this.Height = 380;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.CenterToScreen();
                groupBox1.Visible = true;
            }
        }
    }
}
