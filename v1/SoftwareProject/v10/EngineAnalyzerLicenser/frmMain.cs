﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace EmissionAnalyzerLicenser
{
    public partial class frmMain : Form
    {
        string SecretKey = "1FBDE5D6";
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string CompID = ComputerID.GetUniqueID();
                string InstallDate = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString();

                using (XmlWriter writer = XmlWriter.Create(saveFileDialog1.FileName))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("EmissionAnalyzerKeyFile");
                    writer.WriteElementString("OwnerName", tbxCompanyName.Text);
                    writer.WriteElementString("OwnerAddress", tbxAddress.Text);
                    writer.WriteElementString("InstallDate", Encryption.Encrypt(InstallDate, SecretKey));
                    writer.WriteElementString("License", Encryption.Encrypt(CompID, SecretKey));
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }

                MessageBox.Show("License is succussfully created. : " + saveFileDialog1.FileName);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // Create an XML reader for this file.
                using (XmlTextReader reader = new XmlTextReader(openFileDialog1.FileName))
                {
                    while (reader.Read())
                    {
                        // Only detect start elements.
                        if (reader.IsStartElement())
                        {
                            switch (reader.Name)
                            {
                                case "OwnerName":
                                    reader.Read();
                                    tbxCompanyName.Text = reader.Value;
                                    break;
                                case "OwnerAddress":
                                    reader.Read();
                                    tbxAddress.Text = reader.Value;
                                    break;
                                case "InstallDate":
                                    reader.Read();
                                    tbxInstallDate.Text = Encryption.Decrypt(reader.Value, SecretKey);
                                    break;
                                //case "License":
                                //    reader.Read();
                                //    tbxInstallDate.Text += " ---- " + Encryption.Decrypt(reader.Value, SecretKey);
                                //    break;
                            }
                        }
                    }
                }
            }
        }
    }
}
