﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using EngineAnalyzer.Properties;
using RTF;

namespace EngineAnalyzer
{
    public partial class frmOpacityRealTimeTest : Form
    {
        private RTFBuilderbase richTextBuilder = new RTFBuilder();
        private SerialComunication sCom = new SerialComunication(CommonResources.OpacityPortName, 9600);
        bool IsRealTimeData = true;

        public frmOpacityRealTimeTest()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            // load limits
            lblKMax.Text = Settings.Default.OpacityKMax.ToString("0.00");
            lblKMin.Text = Settings.Default.OpacityKMin.ToString("0.00");
            lblNMax.Text = Settings.Default.OpacityNMax.ToString("0.0");
            lblNMin.Text = Settings.Default.OpacityNMin.ToString("0.0");
            lblSMax.Text = Settings.Default.OpacitySMax.ToString("0");
            lblSMin.Text = Settings.Default.OpacitySMin.ToString("0");
            lblTMax.Text = Settings.Default.OpacityTMax.ToString("0");
            lblTMin.Text = Settings.Default.OpacityTMin.ToString("0");
            if (Settings.Default.OpacityPublishLimits == false)
            {
                pnlKLimits.Visible = false;
                pnlNLimits.Visible = false;
                pnlSLimits.Visible = false;
                pnlTLimits.Visible = false;
            }

            // load saved data
            richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.OpacityCapturedRichText))
            {
                // add Opacity Title
                richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).Alignment(StringAlignment.Center).AppendLine("Diesel Opacity and Smoke Analysis  Report");
                richTextBuilder.AppendLine();
                richTextBuilder.AppendLine();

                // add Opacity Limits
                if (Settings.Default.OpacityPublishLimits)
                {
                    richTextBuilder.FontStyle(FontStyle.Bold).Append("Limits");
                    richTextBuilder.AppendPara();
                    richTextBuilder.Reset();
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Diagnosis Limit", "Min", "Max");
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Idle RPM", Settings.Default.OpacitySMin.ToString(), Settings.Default.OpacitySMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Opacity %", Settings.Default.OpacityNMin.ToString(), Settings.Default.OpacityNMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Opacity %", Settings.Default.OpacityKMin.ToString(), Settings.Default.OpacityKMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Engine Temp", Settings.Default.OpacityTMin.ToString(), Settings.Default.OpacityTMax.ToString());
                    richTextBuilder.Reset();
                    richTextBuilder.AppendLine();
                    richTextBuilder.AppendLine();
                }
            }
            else
            {
                richTextBuilder.AppendRTFDocument(CommonResources.OpacityCapturedRichText);
            }

            // get capture number
            if (CommonResources.OpacityTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.OpacityTotalCapture.ToString();
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // configure Communication
            sCom.AsyncReceived += new EventHandler<AsyncReceived_EventArgs>(sCom_AsyncReceived);
            sCom.Open();

            // pass the base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            sCom.Close();
            base.OnClosing(e);
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.OpacityTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.OpacityTotalCapture.ToString();

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            else
            {
//                richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            }

            // add S, N, K and T
            richTextBuilder.AppendPara();
            richTextBuilder.Reset();
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Engine speed, S", lblSpped.Text + " RPM");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Opacity, N", lblMaxN.Text + " %");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient, K", lblMaxK.Text + " /M");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Engine Temperature, T", lblT.Text + " °C");
            richTextBuilder.Reset();
            richTextBuilder.AppendLine();
            richTextBuilder.AppendLine();

            // save new values
            CommonResources.OpacityCapturedRichText = richTextBuilder.ToString();
        }

        private void btnClearMax_Click(object sender, EventArgs e)
        {
            // send A7H command (clear real-time max data)
            sCom.Send(CommonResources.Opacity_GetDataPacket(0xA7));

            // clear Max value
            lblMaxK.Text = "0.00";
            lblMaxN.Text = "0.0";
            lblMaxS.Text = "0";
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            byte[] Response = new byte[3];

            // stop the real time measurement
            if(btnStart.Text == "STOP")
            {
                tmrMeasure.Enabled = false;
                btnStart.Text = "START";
                return;
            }

            // send A1H command
            bool IsSendSuccess = sCom.Send(CommonResources.Opacity_GetDataPacket(0xA1), Response, 3, 100);

            if (!IsSendSuccess)
            {
                MessageBox.Show("Error occured while sending data. Please check conection.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // check Mode Code
            if (Response[1] == 0x00)
            {
                MessageBox.Show("Device is under Warming up condition. Please start the test later.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // send A0H and RealTime Test mode command (01H)
            sCom.Send(CommonResources.Opacity_GetDataPacket(0xA0, 0x01), Response, 2, 100);

            // wait 2 second
            frmPleaseWait wait = new frmPleaseWait(2000, "Please wait....", "Please wait for a while....");
            //wait.ShowDialog();

            // need calibration?
            DialogResult dRes = MessageBox.Show("Do you want to Calibrate the instrument?", "Calibration", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dRes == System.Windows.Forms.DialogResult.Yes)
            {
                // prompt to take out the probe
                MessageBox.Show("Please take out the Probe and then click OK button.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                // send A4H command (calibration)
                sCom.Send(CommonResources.Opacity_GetDataPacket(0xA4), Response, 3, 100);

                // wait 3 second
                wait = new frmPleaseWait(3000, "Calibrating...", "Please wait....");
                wait.ShowDialog();
            }

            // prompt to insert the probe
            MessageBox.Show("Please insert the Probe and then click OK button.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            // wait 2 second
            wait = new frmPleaseWait(2000, "Please wait....", "Please wait for a while....");
            //wait.ShowDialog();

            // start the measuremnt through tmrMeasure tick
            btnStart.Text = "STOP";
            tmrMeasure.Enabled = true;
        }

        private void btnClearCapture_Click(object sender, EventArgs e)
        {
            richTextBuilder.Clear();
            CommonResources.OpacityTotalCapture = 0;
            CommonResources.OpacityCapturedRichText = "";
            btnCapture.Text = "CAPTURE";
        }

        void ShowRealTimeData(byte[] RtData)
        {
            // get measurement
            float Opacity_N = ((RtData[1] << 8) | RtData[2]);
            float LightCoefficient_K = ((RtData[3] << 8) | RtData[4]);
            int Speed_S = ((RtData[5] << 8) | RtData[6]);
            int Temp_T = ((RtData[7] << 8) | RtData[8]) - 273;

            // shape the result
            Opacity_N /= 10;
            LightCoefficient_K /= 100;

            // show the values
            lblN.Text = Opacity_N.ToString("0.0");
            lblK.Text = LightCoefficient_K.ToString("0.00");
            lblS.Text = Speed_S.ToString();
            lblSpped.Text = Speed_S.ToString();
            int pos = Speed_S / 50;
            if (pos > 100) { pos = 100; }
            pbrSpped.Position = pos;
            lblT.Text = Temp_T.ToString();
        }

        void ShowMaxData(byte[] MxData)
        {
            // get measurement
            float Opacity_N_Max = ((MxData[1] << 8) | MxData[2]);
            float LightCoefficient_K_Max = ((MxData[3] << 8) | MxData[4]);
            int Speed_S_Max = ((MxData[5] << 8) | MxData[6]);

            // shape the result
            Opacity_N_Max /= 10;
            LightCoefficient_K_Max /= 100;

            // show the values
            lblMaxN.Text = Opacity_N_Max.ToString("0.0");
            lblMaxK.Text = LightCoefficient_K_Max.ToString("0.00");
            lblMaxS.Text = Speed_S_Max.ToString();
        }

        void sCom_AsyncReceived(object sender, AsyncReceived_EventArgs e)
        {
            if (e.ReceivedSuccess)
            {
                if (IsRealTimeData)
                {
                    ShowRealTimeData(e.ReceivedData);
                    IsRealTimeData = false;
                }
                else
                {
                    ShowMaxData(e.ReceivedData);
                    IsRealTimeData = true;
                }
            }
        }

        private void tmrMeasure_Tick(object sender, EventArgs e)
        {
            if (IsRealTimeData)
            {
                // send A5H command (real time data)
                sCom.SendAsync(CommonResources.Opacity_GetDataPacket(0xA5), 10, 50);
            }
            else
            {
                // send A6H command (real time max data)
                sCom.SendAsync(CommonResources.Opacity_GetDataPacket(0xA6), 8, 50);
            }
        }
    }
}
