﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EngineAnalyzer.Properties;
using RTF;

namespace EngineAnalyzer
{
    public partial class frmOpacityAccelerationTest : Form
    {
        private RTFBuilderbase richTextBuilder = new RTFBuilder();
        private SerialComunication sCom = new SerialComunication(CommonResources.OpacityPortName, 9600);
        BackgroundWorker bwNetworkTester = new BackgroundWorker();
        delegate void ShowInfo(string text, Color color);
        bool IsNetworkTestSucceed = false;
        int TotalTest = 0;

        public frmOpacityAccelerationTest()
        {
            InitializeComponent();

            // tune bwNetworkTester
            bwNetworkTester.WorkerReportsProgress = false;
            bwNetworkTester.WorkerSupportsCancellation = true;
            bwNetworkTester.DoWork += new DoWorkEventHandler(bwNetworkTester_DoWork);
            bwNetworkTester.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwNetworkTester_RunWorkerCompleted);
        }

        protected override void OnLoad(EventArgs e)
        {
            lblKMax.Text = Settings.Default.OpacityKMax.ToString("0.00");
            lblKMin.Text = Settings.Default.OpacityKMin.ToString("0.00");
            if (Settings.Default.OpacityPublishLimits == false)
            {
                pnlKLimits.Visible = false;
            }

            // load saved data
            richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.OpacityCapturedRichText))
            {
                // add Opacity Title
                richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).AppendLine("Diesel Opacity and Smoke Analysis  Report");
                richTextBuilder.AppendLine();
                richTextBuilder.AppendLine();

                // add Opacity Limits
                if (Settings.Default.OpacityPublishLimits)
                {
                    richTextBuilder.FontStyle(FontStyle.Bold).Append("Limits");
                    richTextBuilder.AppendPara();
                    richTextBuilder.Reset();
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Diagnosis Limit", "Min", "Max");
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Idle RPM", Settings.Default.OpacitySMin.ToString(), Settings.Default.OpacitySMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Opacity %", Settings.Default.OpacityNMin.ToString(), Settings.Default.OpacityNMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "K /M", Settings.Default.OpacityKMin.ToString(), Settings.Default.OpacityKMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Engine Temp", Settings.Default.OpacityTMin.ToString(), Settings.Default.OpacityTMax.ToString());
                    richTextBuilder.Reset();
                    richTextBuilder.AppendLine();
                    richTextBuilder.AppendLine();
                }
            }
            else
            {
                richTextBuilder.AppendRTFDocument(CommonResources.OpacityCapturedRichText);
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // load capture number
            if (CommonResources.OpacityTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.OpacityTotalCapture.ToString();
            }

            // configure Communication
            sCom.Open();
            
            // pass to base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            if (bwNetworkTester.IsBusy) { bwNetworkTester.CancelAsync(); }
            base.OnClosing(e);
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClearCapture_Click(object sender, EventArgs e)
        {
            richTextBuilder.Clear();
            CommonResources.OpacityTotalCapture = 0;
            CommonResources.OpacityCapturedRichText = "";
            lblK_Peak1.Text = "0.00";
            lblK_Peak2.Text = "0.00";
            lblK_Peak3.Text = "0.00";
            lblK_Peak4.Text = "0.00";
            lblK_Average.Text = "0.00";
            lblTimes.Text = "0";
            TotalTest = 0;
            btnCapture.Text = "CAPTURE";
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.OpacityTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.OpacityTotalCapture.ToString();

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            else
            {
//                richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            }

            // add S, N, K and T
            richTextBuilder.AppendPara();
            richTextBuilder.Reset();
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 1", lblK_Peak1.Text + "  /M");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 2", lblK_Peak2.Text + "  /M");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 3", lblK_Peak3.Text + "  /M");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - 4", lblK_Peak4.Text + "  /M");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Light Absorption Coefficient - Average", lblK_Average.Text + "  /M");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 20 }, "Times", lblTimes.Text);
            richTextBuilder.Reset();
            richTextBuilder.AppendLine();
            richTextBuilder.AppendLine();

            // save new values
            CommonResources.OpacityCapturedRichText = richTextBuilder.ToString();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!bwNetworkTester.IsBusy)
            {
                // start the accleration test
                btnStart.Text = "STOP";

                // start Network test
                bwNetworkTester.RunWorkerAsync();
            }
            else
            {
                bwNetworkTester.CancelAsync();
            }
        }

        void bwNetworkTester_DoWork(object sender, DoWorkEventArgs e)
        {
            byte[] Response = new byte[3];

            // show start info text
            ShowInfoText("Acceleration Test has been started...", Color.Blue);
            System.Threading.Thread.Sleep(2000);

            // clear success flag
            IsNetworkTestSucceed = false;

            // send A1H command
            if (!sCom.Send(CommonResources.Opacity_GetDataPacket(0xA1), Response, 3, 100)) { ShowInfoText("Device is not responding.", Color.Red); System.Threading.Thread.Sleep(1000); return; }

            // check Mode Code
            if (Response[1] == 0x00)
            {
                ShowInfoText("Device is under Warming up condition.", Color.Red);
                System.Threading.Thread.Sleep(2000);
                return;
            }

            // send A0H and Acceleration Test(Networking) mode command (02H)
            sCom.Send(CommonResources.Opacity_GetDataPacket(0xA0, 0x02), Response, 2, 100);

            // wait 2 second
            ShowInfoText("Please wait....", Color.Blue);
            System.Threading.Thread.Sleep(2000);

            while (true)
            {
                // get Status code, A9H command
                if (!sCom.Send(CommonResources.Opacity_GetDataPacket(0xA9), Response, 3, 100)) { ShowInfoText("Device is not responding.", Color.Red); System.Threading.Thread.Sleep(1000); return; }

                // check Status Code
                if (Response[1] == 0x06 || Response[1] == 0x07 || Response[1] == 0x08) { break; }
                else
                {
                    // send Stop test, ABH command
                    if (!sCom.Send(CommonResources.Opacity_GetDataPacket(0xAB), Response, 2, 100)) { ShowInfoText("Device is not responding.", Color.Red); System.Threading.Thread.Sleep(1000); return; }

                    // wait 1 second
                    ShowInfoText("Waiting for device response. Please wait....", Color.Blue);
                    System.Threading.Thread.Sleep(1000);
                }

                // is cancel process?
                if (bwNetworkTester.CancellationPending)
                {
                    IsNetworkTestSucceed = false;
                    return;
                }

                // wait sometimes
                System.Threading.Thread.Sleep(100);
            }

            // set number of test = 4, A8H command
            if (!sCom.Send(CommonResources.Opacity_GetDataPacket(0xA8, 4), Response, 2, 100)) { ShowInfoText("Device is not responding.", Color.Red); System.Threading.Thread.Sleep(1000); return; }

            byte[] AccelerationData = new byte[12];
            int LastCommand = 0;
            TotalTest = 0;

            while (true)
            {
                // get Status code, A9H command
                if (!sCom.Send(CommonResources.Opacity_GetDataPacket(0xA9), Response, 3, 100)) { ShowInfoText("Device is not responding.", Color.Red); System.Threading.Thread.Sleep(1000); return; }

                if (Response[1] != LastCommand)
                {
                    switch (Response[1])
                    {
                        case 0x01:
                            // prompt to take out the probe
                            ShowInfoText("Please take out the Probe.", Color.Blue);
                            break;

                        case 0x02:
                            // show calibration message
                            ShowInfoText("Calibrating....", Color.Blue);
                            System.Threading.Thread.Sleep(2000);
                            break;

                        case 0x03:
                            // prompt to insert the probe
                            ShowInfoText("Please insert the Probe, keep engine into Idle speed.", Color.Blue);

                            // confirm probe insertion, AAH command
                            if (!sCom.Send(CommonResources.Opacity_GetDataPacket(0xAA), Response, 2, 100)) { ShowInfoText("Device is not responding.", Color.Red); System.Threading.Thread.Sleep(1000); return; }
                            break;

                        case 0x04:
                            // prompt to accelerate
                            ShowInfoText("Please accelerate.", Color.Blue);
                            TotalTest++;
                            break;

                        case 0x05:
                            // prompt to deccelerate
                            ShowInfoText("Please resume to Idle speed.", Color.Blue);
                            break;

                        case 0x06:
                            // get test result, ACH command
                            if (sCom.Send(CommonResources.Opacity_GetDataPacket(0xAC), AccelerationData, 12, 100))
                            {
                                Show_NetworkTestResult(AccelerationData);
                            }
                            IsNetworkTestSucceed = true;
                            return;

                        case 0x07:
                            // get test result, ACH command
                            if (sCom.Send(CommonResources.Opacity_GetDataPacket(0xAC), AccelerationData, 12, 100))
                            {
                                Show_NetworkTestResult(AccelerationData);
                            }
                            IsNetworkTestSucceed = true;
                            return;

                        case 0x08:
                            // process error
                            return;

                        default:
                            // send Stop test, ABH command
                            sCom.Send(CommonResources.Opacity_GetDataPacket(0xAB), Response, 2, 100);
                            return;
                    }

                    // update last command
                    LastCommand = Response[1];
                }

                // is cancel process?
                if (bwNetworkTester.CancellationPending)
                {
                    // send Stop test, ABH command
                    sCom.Send(CommonResources.Opacity_GetDataPacket(0xAB), Response, 2, 100);
                    IsNetworkTestSucceed = false;
                    return;
                }

                // wait sometimes
                System.Threading.Thread.Sleep(100);
            }
        }

        void bwNetworkTester_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // reset bunnon text
            btnStart.Text = "START";

            // show result
            if (IsNetworkTestSucceed)
            {
                ShowInfoText("Acceleration Test completed successfully!", Color.Green);
            }
            else
            {
                ShowInfoText("Acceleration Test was not successfully.", Color.Red);
            }
        }

        void Show_NetworkTestResult(byte[] AccelerationData)
        {
            // get measurement
            float LightCoefficient_K1 = ((AccelerationData[1] << 8) | AccelerationData[2]);
            float LightCoefficient_K2 = ((AccelerationData[3] << 8) | AccelerationData[4]);
            float LightCoefficient_K3 = ((AccelerationData[5] << 8) | AccelerationData[6]);
            float LightCoefficient_K4 = ((AccelerationData[7] << 8) | AccelerationData[8]);
            float LightCoefficient_Average = ((AccelerationData[9] << 8) | AccelerationData[10]);

            // shape the result
            LightCoefficient_K1 /= 100;
            LightCoefficient_K2 /= 100;
            LightCoefficient_K3 /= 100;
            LightCoefficient_K4 /= 100;
            LightCoefficient_Average /= 100;

            // show the peak values
            lblK_Peak1.Text = LightCoefficient_K1.ToString("0.00");
            lblK_Peak2.Text = LightCoefficient_K2.ToString("0.00");
            lblK_Peak3.Text = LightCoefficient_K3.ToString("0.00");
            lblK_Peak4.Text = LightCoefficient_K4.ToString("0.00");

            // show average value
            lblK_Average.Text = LightCoefficient_Average.ToString("0.00");

            // update counter
            lblTimes.Text = TotalTest.ToString();
        }

        // for cross thread operation
        private void ShowInfoText(string text, Color color)
        {
            if (lblInfoBox.InvokeRequired)
            {
                lblInfoBox.BeginInvoke(new ShowInfo(ShowInfoText), text, color);
            }
            else
            {
                lblInfoBox.Text = text;
                lblInfoBox.ForeColor = color;
                lblInfoBox.ForeGradientColor2 = color;
            }
        }
    }
}