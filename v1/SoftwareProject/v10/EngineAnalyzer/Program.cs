﻿using System;
using System.Collections.Generic;

using System.Windows.Forms;

namespace EngineAnalyzer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLicenseChecker());
            if (!frmLicenseChecker.IsLicensed) { return; }
            Application.Run(new frmMain());
        }
    }
}
