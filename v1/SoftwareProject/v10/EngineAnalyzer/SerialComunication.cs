﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;

namespace EngineAnalyzer
{
    class SerialComunication
    {
        /// <summary>
        /// This event will fire  whenever Response is received or a Timeout is occured.
        /// </summary>
        public event EventHandler<AsyncReceived_EventArgs> AsyncReceived;

        /// <summary>
        /// Rise the AsysncReceived event.
        /// </summary>
        /// <param name="e">Event Arguments</param>
        protected void OnAsyncReceived(AsyncReceived_EventArgs e)
        {
            if (AsyncReceived != null) { AsyncReceived(this, e); }
        }

        /// <summary>
        /// Get the Port status.
        /// </summary>
        public bool IsPortBusy
        {
            get { return _IsPortBusy; }
        }

        // Private properties
        private SerialPort ComPort = new SerialPort("COM1", 9600);
        private BackgroundWorker bgWorker = new BackgroundWorker();
        private byte[] Async_ReceivedBytes = new byte[1024];
        private bool Async_IsReceivedSuccess = false;
        private bool Async_TriggerEvent = false;
        private int Async_ResponseLength = 0;
        private int Async_WaitTime = 0;
        private bool _IsPortBusy = false;
        

        /// <summary>
        /// Initalize the SerialCommunication class.
        /// </summary>
        /// <param name="PortName">Name of the Com Port. i.e. COM1.</param>
        /// <param name="BaudRate">set the Baud Rate in bps, i.e. 9600.</param>
        public SerialComunication(string PortName, int BaudRate)
        {
            // initialize the port
            try
            {
                ComPort = new SerialPort(PortName, BaudRate);
            }
            catch { }
            ComPort.ReceivedBytesThreshold = 1;
            ComPort.ReadBufferSize = 1024;
            ComPort.WriteTimeout = 5000;
            ComPort.ReadTimeout = 5000;

            // configure bgWorker
            bgWorker.WorkerSupportsCancellation = true;

            // event handle for bgWorker
            bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
        }


        // destructor
        ~SerialComunication()
        {
            bgWorker.Dispose();
            ComPort.Dispose();
        }

        /// <summary>
        /// Open the selected Port.
        /// </summary>
        /// <returns>true: if opening is success, false: if error occured while opening the port.</returns>
        public bool Open()
        {
            if (ComPort.IsOpen) { return true; }
            try
            {
                // open com port
                ComPort.Open();
                return true;
            }
            catch
            {
                MessageBox.Show("Selected port: " + ComPort.PortName + " can't be opened!\r\nPlease check Settings menu.", "Port configuration error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        /// <summary>
        /// Close the Port and terminate all communication.
        /// </summary>
        public void Close()
        {
            try
            {
                if (bgWorker.IsBusy) { bgWorker.CancelAsync(); }
                if (ComPort.IsOpen) { ComPort.Close(); }
                _IsPortBusy = false;
            }
            catch { }
        }

        /// <summary>
        /// Send a single Byte through the Port.
        /// </summary>
        /// <param name="Data">Byte to send.</param>
        /// <returns>true: if success, false: if Port is close or is busy or Timeout occures.</returns>
        public bool Send(byte Data)
        {
            // is port close?
            if (!ComPort.IsOpen) { return false; }

            // is port busy? wait 100ms to be free.
            for (int i = 1; _IsPortBusy; i++ )
            {
                System.Threading.Thread.Sleep(1);
                if (i == 100) { return false; }
            }

            // set busy flag
            _IsPortBusy = true;

            try
            {
                // clear the out buffer
                ComPort.DiscardOutBuffer();

                // send the data byte
                ComPort.Write(new byte[] { Data }, 0, 1);
                return true;
            }
            catch { _IsPortBusy = false; return false; }
        }

        /// <summary>
        /// Send a Byte array through the port..
        /// </summary>
        /// <param name="Data">Byte Array to send.</param>
        /// <returns>true: if succeed, false: if error occured.</returns>
        public bool Send(byte[] Data)
        {
            // is port close?
            if (!ComPort.IsOpen) { return false; }

            // is port busy? wait 100ms to be free.
            for (int i = 1; _IsPortBusy; i++)
            {
                System.Threading.Thread.Sleep(1);
                if (i == 100) { return false; }
            }

            try
            {
                // clear buffer
                ComPort.DiscardOutBuffer();

                // send the data byte
                ComPort.Write(Data, 0, Data.Length);
                return true;
            }
            catch { _IsPortBusy = false; return false; }
        }

        /// <summary>
        /// Send a single byte and wait until Response byte found or Timeout occured.
        /// </summary>
        /// <param name="Data">Byte to send.</param>
        /// <param name="ResponseToMatch">Response to be matched.</param>
        /// <param name="WaitTime">waiting time before timeout occured in milliseconds.</param>
        /// <returns>true: if sent success and response matches, false: Response doesn't match or Timeout occured.</returns>
        public bool Send(byte Data, byte ResponseToMatch, int WaitTime)
        {
            int ReceivedByte = -1;
            bool IsReceiveComplete = false;

            // is port close?
            if (!ComPort.IsOpen) { return false; }

            // is port busy? wait 100ms to be free.
            for (int i = 1; _IsPortBusy; i++)
            {
                System.Threading.Thread.Sleep(1);
                if (i == 100) { return false; }
            }

            // set busy flag
            _IsPortBusy = true;

            try
            {
                // clear in/out buffers
                ComPort.DiscardInBuffer();
                ComPort.DiscardOutBuffer();

                // send the data byte
                ComPort.Write(new byte[] { Data }, 0, 1);

                // wait for response found
                for(int i = 0; i < WaitTime; i++)
                {
                    if (ComPort.BytesToRead >= 1) { IsReceiveComplete = true; break; }
                    System.Threading.Thread.Sleep(1);
                }

                // read the reponse
                if (IsReceiveComplete) { ReceivedByte = ComPort.ReadByte(); }
            }
            catch { _IsPortBusy = false; return false; }

            // clear busy flag
            _IsPortBusy = false;

            // return the result
            if (ReceivedByte == ResponseToMatch) { return true; }
            return false;
        }

        /// <summary>
        /// Send a single byte and wait until Response byte found or Timeout occured.
        /// </summary>
        /// <param name="Data">Byte to send.</param>
        /// /// <param name="WaitTime">waiting time before timeout occured in milliseconds.</param>
        /// <param name="Response">Response byte to save.</param>
        /// <returns>true: if sent success and response received, false: Timeout occured.</returns>
        public bool Send(byte Data, int WaitTime, byte Response)
        {
            bool IsReceiveComplete = false;

            // is port close?
            if (!ComPort.IsOpen) { return false; }

            // is port busy? wait 100ms to be free.
            for (int i = 1; _IsPortBusy; i++)
            {
                System.Threading.Thread.Sleep(1);
                if (i == 100) { return false; }
            }

            // set busy flag
            _IsPortBusy = true;

            try
            {
                // clear in/out buffers
                ComPort.DiscardInBuffer();
                ComPort.DiscardOutBuffer();

                // send the data byte
                ComPort.Write(new byte[] { Data }, 0, 1);

                // wait for response found
                for (int i = 0; i < WaitTime; i++)
                {
                    if (ComPort.BytesToRead >= 1) { IsReceiveComplete = true; break; }
                    System.Threading.Thread.Sleep(1);
                }

                // read the reponse
                if (IsReceiveComplete) { Response = (byte) ComPort.ReadByte(); }
            }
            catch { _IsPortBusy = false; return false; }

            // clear busy flag
            _IsPortBusy = false;

            // return the result
            return true;
        }

        /// <summary>
        /// Send a Byte array and wait for a Response byte array.
        /// </summary>
        /// <param name="Data">Byte arry to send.</param>
        /// <param name="Response">Response Byte array.</param>
        /// <param name="ResponseLength">Length of the Response byte array.</param>
        /// <param name="WaitTime">Time in milliseconds before timeout occured.</param>
        /// <returns>true: send success, false: send was not successfull.</returns>
        public bool Send(byte[] Data, byte[] Response, int ResponseLength, int WaitTime)
        {
            bool IsReceiveComplete = false;

            // is port close?
            if (!ComPort.IsOpen) { return false; }

            // is port busy? wait 100ms to be free.
            for (int i = 1; _IsPortBusy; i++)
            {
                System.Threading.Thread.Sleep(1);
                if (i == 100) { return false; }
            }

            // set busy flag
            _IsPortBusy = true;

            // is bgWorker busy?
            if (bgWorker.IsBusy) { bgWorker.CancelAsync(); while (bgWorker.IsBusy) { } }

            try
            {
                // clear in/out buffers
                ComPort.DiscardInBuffer();
                ComPort.DiscardOutBuffer();

                // send the data byte
                ComPort.Write(Data, 0, Data.Length);

                // wait for response found
                for(int i = 0; i < WaitTime; i++)
                {
                    if (ComPort.BytesToRead >= ResponseLength) { IsReceiveComplete = true; break; }
                    System.Threading.Thread.Sleep(1);
                }

                // clear busy flag
                _IsPortBusy = false;

                // is there valid data received?
                if (IsReceiveComplete) { ComPort.Read(Response, 0, ResponseLength); } else { _IsPortBusy = false; return false; }
            }
            catch { _IsPortBusy = false; return false; }
            
            // return the result
            return true;
        }

        /// <summary>
        /// Send a Byte, register a event to received the response.
        /// </summary>
        /// <param name="Data">Byte to send.</param>
        /// <param name="ResponseLength">Number of response bytes.</param>
        /// <param name="WaitTime">Time in milliseconds before timeout occured.</param>
        /// <returns>true: send success and waiting Responses to received, false: error occured while sending data</returns>
        public bool SendAsync(byte Data, int ResponseLength, int WaitTime)
        {
            // is port close?
            if (!ComPort.IsOpen) { return false; }

            // is port busy? wait 100ms to be free.
            for (int i = 1; _IsPortBusy; i++)
            {
                System.Threading.Thread.Sleep(1);
                if (i == 100) { return false; }
            }

            // is bgWorker busy?
            if (bgWorker.IsBusy) { bgWorker.CancelAsync(); while (bgWorker.IsBusy) { } }

            // set parameters
            Async_ResponseLength = ResponseLength;
            Async_WaitTime = WaitTime;

            // start receiving
            Async_TriggerEvent = true;
            bgWorker.RunWorkerAsync();

            // set busy flag
            _IsPortBusy = true;

            try
            {
                // clear in/out buffers
                ComPort.DiscardInBuffer();
                ComPort.DiscardOutBuffer();

                // send the data byte
                ComPort.Write(new byte[] { Data }, 0, 1);
            }
            catch { Async_TriggerEvent = false; bgWorker.CancelAsync(); return false; }

            // clear busy flag
            _IsPortBusy = false;

            // return the result
            return true;
        }

        /// <summary>
        /// Send a Array of Byte through the Port.
        /// </summary>
        /// <param name="Data">Byte Array to send.</param>
        /// <param name="ResponseLength">Number of byte in response data.</param>
        /// <param name="WaitTime">Time in milliseconds before Timeout occure.</param>
        /// <returns>true: send success and waiting Responses to received, false: error occured while sending data</returns>
        public bool SendAsync(byte[] Data, int ResponseLength, int WaitTime)
        {
            // is port close?
            if (!ComPort.IsOpen) { return false; }

            // is port busy? wait 100ms to be free.
            for (int i = 1; _IsPortBusy; i++)
            {
                System.Threading.Thread.Sleep(1);
                if (i == 100) { return false; }
            }

            // is bgWorker busy?
            if (bgWorker.IsBusy) { bgWorker.CancelAsync(); while (bgWorker.IsBusy) { } }

            // set parameters
            Async_ResponseLength = ResponseLength;
            Async_WaitTime = WaitTime;

            // start receiving
            Async_TriggerEvent = true;
            bgWorker.RunWorkerAsync();

            // set busy flag
            _IsPortBusy = true;

            try
            {
                // clear in/out buffers
                ComPort.DiscardInBuffer();
                ComPort.DiscardOutBuffer();

                // send the data byte
                ComPort.Write(Data, 0, Data.Length);
            }
            catch { bgWorker.CancelAsync(); return false; }

            // clear busy flag
            _IsPortBusy = false;

            // return the result
            return true;
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool IsReceiveComplete = false;

            // do nothing if parameters are empty
            if ((Async_ResponseLength == 0) || (Async_ResponseLength == 0)) { Async_TriggerEvent = false; return; }

            // set flag to false
            Async_IsReceivedSuccess = false;

            // wait till timeout
            for(int i = 0; i < Async_WaitTime; i++)
            {
                // is all data received?
                if (ComPort.BytesToRead >= Async_ResponseLength) { IsReceiveComplete = true; break; }

                // is cancel method is triggred?
                if (bgWorker.CancellationPending) { Async_TriggerEvent = false; return; }

                // wait
                System.Threading.Thread.Sleep(1);
            }

            // is received was successfull?
            if (IsReceiveComplete)
            {
                try
                {
                    // read received data Data
                    ComPort.Read(Async_ReceivedBytes, 0, Async_ResponseLength);
                    Async_IsReceivedSuccess = true;
                }
                catch { }
            }            
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            AsyncReceived_EventArgs eArgs = new AsyncReceived_EventArgs();
            eArgs.ReceivedSuccess = Async_IsReceivedSuccess;

            // is received was successfull?
            if (Async_IsReceivedSuccess)
            {
                // load Event data
                eArgs.ReceivedData = new byte[Async_ResponseLength];
                for (int i = 0; i < Async_ResponseLength; i++) { eArgs.ReceivedData[i] = Async_ReceivedBytes[i]; }
            }

            // reset all the parameters
            Async_IsReceivedSuccess = false;
            Async_ResponseLength = 0;
            Async_WaitTime = 0;

            // fire the event
            if (Async_TriggerEvent) { OnAsyncReceived(eArgs); }
        }
    }

    public class AsyncReceived_EventArgs : EventArgs
    {
        public bool ReceivedSuccess = false;
        public byte[] ReceivedData;
    }
}