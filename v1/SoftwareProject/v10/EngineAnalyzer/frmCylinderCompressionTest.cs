﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using InterfaceLab.WinForm.Tools;
using EngineAnalyzer.Properties;
using RTF;

namespace EngineAnalyzer
{
    public partial class frmCylinderCompressionTest : Form
    {
        private DirtyTracker _dirtyTracker;
        private RTFBuilderbase richTextBuilder = new RTFBuilder(RTFFont.Arial, 22);

        public frmCylinderCompressionTest()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // in the Load event initialize our tracking object
            _dirtyTracker = new DirtyTracker(this);
            _dirtyTracker.SetAsClean();
            _dirtyTracker.DirtyTracked += new EventHandler(_dirtyTracker_DirtyTracked);
            _dirtyTracker.TrackCleaned += new EventHandler(_dirtyTracker_TrackCleaned);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (_dirtyTracker.IsDirty)
            {
                DialogResult dRes = MessageBox.Show("Would you like to save changes before closing?", "Warning!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                // cancel the closing
                if (dRes == DialogResult.Cancel) { e.Cancel = true; return; }

                if (dRes == DialogResult.Yes)
                {
                    // save the new settings
                    btnSave_Click(this, EventArgs.Empty);
                }
            }

            base.OnFormClosing(e);
        }

        void _dirtyTracker_DirtyTracked(object sender, EventArgs e)
        {
            this.Text += "  * (unsaved)";
        }

        void _dirtyTracker_TrackCleaned(object sender, EventArgs e)
        {
            this.Text = this.Text.Replace("  * (unsaved)", "");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // add text
            richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).Alignment(StringAlignment.Center).AppendLine("Engine Cylinder Compression Test");
            richTextBuilder.AppendPara();
            richTextBuilder.Reset();
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 25, 30 }, "Cylinder No.", "Compression Pressure", "Remarks");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 25, 30 }, "01.", tbxPressure1.Text, tbxRemarks1.Text);
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 25, 30 }, "02.", tbxPressure2.Text, tbxRemarks2.Text);
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 25, 30 }, "03.", tbxPressure3.Text, tbxRemarks3.Text);
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 25, 30 }, "04.", tbxPressure4.Text, tbxRemarks4.Text);
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 25, 30 }, "05.", tbxPressure5.Text, tbxRemarks5.Text);
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 25, 30 }, "06.", tbxPressure6.Text, tbxRemarks6.Text);
            CommonResources.RTF_AddRow(richTextBuilder, new int[] { 15, 55 }, "Limit (minimum)", tbxLimit.Text);
            richTextBuilder.Reset();
            richTextBuilder.AppendLine();
            richTextBuilder.AppendLine();

            // save the new settings
            Settings.Default["__CylinderCompressionRichText"] = richTextBuilder.ToString();
            Properties.Settings.Default.Save();

            // make DirtyTracker clean
            _dirtyTracker.SetAsClean();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            // clear all text values
            tbxPressure1.Text = "";
            tbxPressure2.Text = "";
            tbxPressure3.Text = "";
            tbxPressure4.Text = "";
            tbxPressure5.Text = "";
            tbxPressure6.Text = "";
            tbxRemarks1.Text = "";
            tbxRemarks2.Text = "";
            tbxRemarks3.Text = "";
            tbxRemarks4.Text = "";
            tbxRemarks5.Text = "";
            tbxRemarks6.Text = "";
            tbxLimit.Text = "";
            cbxPublish.Checked = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            // clos the form
            this.Close();
        }
    }
}
