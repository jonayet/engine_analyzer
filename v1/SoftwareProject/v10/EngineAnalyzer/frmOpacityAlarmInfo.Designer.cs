﻿namespace EngineAnalyzer
{
    partial class frmOpacityAlarmInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel4 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel5 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel6 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel7 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel8 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel9 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel11 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblBoardTemp = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblDetectorTemp = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTubeTemp = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblPowerVoltage = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblLedTemp = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblOpaity = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblFanOverCurrent = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblFanCurrentImbalance = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblFullLightIntensity = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblEeprom = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblAmbLightIntensity = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel23 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnCheckAgain = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.Location = new System.Drawing.Point(76, 51);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(236, 29);
            this.smartLabel1.TabIndex = 0;
            this.smartLabel1.Text = "Board Temperature: ";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel1.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.Location = new System.Drawing.Point(50, 92);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(262, 29);
            this.smartLabel2.TabIndex = 1;
            this.smartLabel2.Text = "Detector Temperature: ";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel2.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel3
            // 
            this.smartLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel3.AutoSize = true;
            this.smartLabel3.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.BorderColor = System.Drawing.Color.Black;
            this.smartLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.Location = new System.Drawing.Point(84, 133);
            this.smartLabel3.Name = "smartLabel3";
            this.smartLabel3.Size = new System.Drawing.Size(228, 29);
            this.smartLabel3.TabIndex = 2;
            this.smartLabel3.Text = "Tube Temperature: ";
            this.smartLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel3.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel4
            // 
            this.smartLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel4.AutoSize = true;
            this.smartLabel4.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel4.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.BorderColor = System.Drawing.Color.Black;
            this.smartLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel4.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel4.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel4.Location = new System.Drawing.Point(129, 174);
            this.smartLabel4.Name = "smartLabel4";
            this.smartLabel4.Size = new System.Drawing.Size(183, 29);
            this.smartLabel4.TabIndex = 3;
            this.smartLabel4.Text = "Power Voltage: ";
            this.smartLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel4.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel5
            // 
            this.smartLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel5.AutoSize = true;
            this.smartLabel5.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel5.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.BorderColor = System.Drawing.Color.Black;
            this.smartLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel5.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel5.Location = new System.Drawing.Point(95, 215);
            this.smartLabel5.Name = "smartLabel5";
            this.smartLabel5.Size = new System.Drawing.Size(217, 29);
            this.smartLabel5.TabIndex = 4;
            this.smartLabel5.Text = "LED Temperature: ";
            this.smartLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel5.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel6
            // 
            this.smartLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel6.AutoSize = true;
            this.smartLabel6.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.BorderColor = System.Drawing.Color.Black;
            this.smartLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.Location = new System.Drawing.Point(206, 256);
            this.smartLabel6.Name = "smartLabel6";
            this.smartLabel6.Size = new System.Drawing.Size(106, 29);
            this.smartLabel6.TabIndex = 5;
            this.smartLabel6.Text = "Opacity: ";
            this.smartLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel6.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel7
            // 
            this.smartLabel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel7.AutoSize = true;
            this.smartLabel7.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.BorderColor = System.Drawing.Color.Black;
            this.smartLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel7.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel7.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.Location = new System.Drawing.Point(103, 297);
            this.smartLabel7.Name = "smartLabel7";
            this.smartLabel7.Size = new System.Drawing.Size(209, 29);
            this.smartLabel7.TabIndex = 6;
            this.smartLabel7.Text = "Fan Over Current: ";
            this.smartLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel7.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel8
            // 
            this.smartLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel8.AutoSize = true;
            this.smartLabel8.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.BorderColor = System.Drawing.Color.Black;
            this.smartLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel8.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel8.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.Location = new System.Drawing.Point(44, 338);
            this.smartLabel8.Name = "smartLabel8";
            this.smartLabel8.Size = new System.Drawing.Size(268, 29);
            this.smartLabel8.TabIndex = 7;
            this.smartLabel8.Text = "Fan Current Imbalance: ";
            this.smartLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel8.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel9
            // 
            this.smartLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel9.AutoSize = true;
            this.smartLabel9.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.BorderColor = System.Drawing.Color.Black;
            this.smartLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel9.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel9.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.Location = new System.Drawing.Point(96, 379);
            this.smartLabel9.Name = "smartLabel9";
            this.smartLabel9.Size = new System.Drawing.Size(216, 29);
            this.smartLabel9.TabIndex = 8;
            this.smartLabel9.Text = "Full Light Intensity: ";
            this.smartLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel9.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.AutoSize = true;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(75, 420);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(237, 29);
            this.smartLabel10.TabIndex = 9;
            this.smartLabel10.Text = "Amb. Light Intensity:  ";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel10.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel11
            // 
            this.smartLabel11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel11.AutoSize = true;
            this.smartLabel11.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel11.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel11.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel11.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel11.BorderColor = System.Drawing.Color.Black;
            this.smartLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel11.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel11.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel11.Location = new System.Drawing.Point(183, 461);
            this.smartLabel11.Name = "smartLabel11";
            this.smartLabel11.Size = new System.Drawing.Size(129, 29);
            this.smartLabel11.TabIndex = 10;
            this.smartLabel11.Text = "EEPROM: ";
            this.smartLabel11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel11.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblBoardTemp
            // 
            this.lblBoardTemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblBoardTemp.AutoSize = true;
            this.lblBoardTemp.BackColor = System.Drawing.Color.Transparent;
            this.lblBoardTemp.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblBoardTemp.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblBoardTemp.BackShadowColor = System.Drawing.Color.Black;
            this.lblBoardTemp.BorderColor = System.Drawing.Color.Black;
            this.lblBoardTemp.BorderWidth = 1;
            this.lblBoardTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBoardTemp.ForeColor = System.Drawing.Color.Green;
            this.lblBoardTemp.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblBoardTemp.ForeShadowColor = System.Drawing.Color.Black;
            this.lblBoardTemp.Location = new System.Drawing.Point(321, 51);
            this.lblBoardTemp.Name = "lblBoardTemp";
            this.lblBoardTemp.Size = new System.Drawing.Size(48, 29);
            this.lblBoardTemp.TabIndex = 11;
            this.lblBoardTemp.Text = "OK";
            this.lblBoardTemp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblBoardTemp.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblDetectorTemp
            // 
            this.lblDetectorTemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDetectorTemp.AutoSize = true;
            this.lblDetectorTemp.BackColor = System.Drawing.Color.Transparent;
            this.lblDetectorTemp.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblDetectorTemp.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblDetectorTemp.BackShadowColor = System.Drawing.Color.Black;
            this.lblDetectorTemp.BorderColor = System.Drawing.Color.Black;
            this.lblDetectorTemp.BorderWidth = 1;
            this.lblDetectorTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetectorTemp.ForeColor = System.Drawing.Color.Green;
            this.lblDetectorTemp.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblDetectorTemp.ForeShadowColor = System.Drawing.Color.Black;
            this.lblDetectorTemp.Location = new System.Drawing.Point(321, 92);
            this.lblDetectorTemp.Name = "lblDetectorTemp";
            this.lblDetectorTemp.Size = new System.Drawing.Size(48, 29);
            this.lblDetectorTemp.TabIndex = 12;
            this.lblDetectorTemp.Text = "OK";
            this.lblDetectorTemp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDetectorTemp.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblTubeTemp
            // 
            this.lblTubeTemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTubeTemp.AutoSize = true;
            this.lblTubeTemp.BackColor = System.Drawing.Color.Transparent;
            this.lblTubeTemp.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTubeTemp.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTubeTemp.BackShadowColor = System.Drawing.Color.Black;
            this.lblTubeTemp.BorderColor = System.Drawing.Color.Black;
            this.lblTubeTemp.BorderWidth = 1;
            this.lblTubeTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTubeTemp.ForeColor = System.Drawing.Color.Green;
            this.lblTubeTemp.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblTubeTemp.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTubeTemp.Location = new System.Drawing.Point(321, 133);
            this.lblTubeTemp.Name = "lblTubeTemp";
            this.lblTubeTemp.Size = new System.Drawing.Size(48, 29);
            this.lblTubeTemp.TabIndex = 13;
            this.lblTubeTemp.Text = "OK";
            this.lblTubeTemp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblTubeTemp.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblPowerVoltage
            // 
            this.lblPowerVoltage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPowerVoltage.AutoSize = true;
            this.lblPowerVoltage.BackColor = System.Drawing.Color.Transparent;
            this.lblPowerVoltage.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblPowerVoltage.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblPowerVoltage.BackShadowColor = System.Drawing.Color.Black;
            this.lblPowerVoltage.BorderColor = System.Drawing.Color.Black;
            this.lblPowerVoltage.BorderWidth = 1;
            this.lblPowerVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPowerVoltage.ForeColor = System.Drawing.Color.Green;
            this.lblPowerVoltage.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblPowerVoltage.ForeShadowColor = System.Drawing.Color.Black;
            this.lblPowerVoltage.Location = new System.Drawing.Point(321, 174);
            this.lblPowerVoltage.Name = "lblPowerVoltage";
            this.lblPowerVoltage.Size = new System.Drawing.Size(48, 29);
            this.lblPowerVoltage.TabIndex = 14;
            this.lblPowerVoltage.Text = "OK";
            this.lblPowerVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblPowerVoltage.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblLedTemp
            // 
            this.lblLedTemp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblLedTemp.AutoSize = true;
            this.lblLedTemp.BackColor = System.Drawing.Color.Transparent;
            this.lblLedTemp.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblLedTemp.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblLedTemp.BackShadowColor = System.Drawing.Color.Black;
            this.lblLedTemp.BorderColor = System.Drawing.Color.Black;
            this.lblLedTemp.BorderWidth = 1;
            this.lblLedTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLedTemp.ForeColor = System.Drawing.Color.Green;
            this.lblLedTemp.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblLedTemp.ForeShadowColor = System.Drawing.Color.Black;
            this.lblLedTemp.Location = new System.Drawing.Point(321, 215);
            this.lblLedTemp.Name = "lblLedTemp";
            this.lblLedTemp.Size = new System.Drawing.Size(48, 29);
            this.lblLedTemp.TabIndex = 15;
            this.lblLedTemp.Text = "OK";
            this.lblLedTemp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLedTemp.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblOpaity
            // 
            this.lblOpaity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblOpaity.AutoSize = true;
            this.lblOpaity.BackColor = System.Drawing.Color.Transparent;
            this.lblOpaity.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblOpaity.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblOpaity.BackShadowColor = System.Drawing.Color.Black;
            this.lblOpaity.BorderColor = System.Drawing.Color.Black;
            this.lblOpaity.BorderWidth = 1;
            this.lblOpaity.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOpaity.ForeColor = System.Drawing.Color.Green;
            this.lblOpaity.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblOpaity.ForeShadowColor = System.Drawing.Color.Black;
            this.lblOpaity.Location = new System.Drawing.Point(321, 256);
            this.lblOpaity.Name = "lblOpaity";
            this.lblOpaity.Size = new System.Drawing.Size(48, 29);
            this.lblOpaity.TabIndex = 16;
            this.lblOpaity.Text = "OK";
            this.lblOpaity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblOpaity.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblFanOverCurrent
            // 
            this.lblFanOverCurrent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblFanOverCurrent.AutoSize = true;
            this.lblFanOverCurrent.BackColor = System.Drawing.Color.Transparent;
            this.lblFanOverCurrent.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblFanOverCurrent.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblFanOverCurrent.BackShadowColor = System.Drawing.Color.Black;
            this.lblFanOverCurrent.BorderColor = System.Drawing.Color.Black;
            this.lblFanOverCurrent.BorderWidth = 1;
            this.lblFanOverCurrent.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFanOverCurrent.ForeColor = System.Drawing.Color.Green;
            this.lblFanOverCurrent.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblFanOverCurrent.ForeShadowColor = System.Drawing.Color.Black;
            this.lblFanOverCurrent.Location = new System.Drawing.Point(321, 297);
            this.lblFanOverCurrent.Name = "lblFanOverCurrent";
            this.lblFanOverCurrent.Size = new System.Drawing.Size(48, 29);
            this.lblFanOverCurrent.TabIndex = 17;
            this.lblFanOverCurrent.Text = "OK";
            this.lblFanOverCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFanOverCurrent.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblFanCurrentImbalance
            // 
            this.lblFanCurrentImbalance.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblFanCurrentImbalance.AutoSize = true;
            this.lblFanCurrentImbalance.BackColor = System.Drawing.Color.Transparent;
            this.lblFanCurrentImbalance.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblFanCurrentImbalance.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblFanCurrentImbalance.BackShadowColor = System.Drawing.Color.Black;
            this.lblFanCurrentImbalance.BorderColor = System.Drawing.Color.Black;
            this.lblFanCurrentImbalance.BorderWidth = 1;
            this.lblFanCurrentImbalance.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFanCurrentImbalance.ForeColor = System.Drawing.Color.Green;
            this.lblFanCurrentImbalance.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblFanCurrentImbalance.ForeShadowColor = System.Drawing.Color.Black;
            this.lblFanCurrentImbalance.Location = new System.Drawing.Point(321, 338);
            this.lblFanCurrentImbalance.Name = "lblFanCurrentImbalance";
            this.lblFanCurrentImbalance.Size = new System.Drawing.Size(48, 29);
            this.lblFanCurrentImbalance.TabIndex = 18;
            this.lblFanCurrentImbalance.Text = "OK";
            this.lblFanCurrentImbalance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFanCurrentImbalance.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblFullLightIntensity
            // 
            this.lblFullLightIntensity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblFullLightIntensity.AutoSize = true;
            this.lblFullLightIntensity.BackColor = System.Drawing.Color.Transparent;
            this.lblFullLightIntensity.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblFullLightIntensity.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblFullLightIntensity.BackShadowColor = System.Drawing.Color.Black;
            this.lblFullLightIntensity.BorderColor = System.Drawing.Color.Black;
            this.lblFullLightIntensity.BorderWidth = 1;
            this.lblFullLightIntensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFullLightIntensity.ForeColor = System.Drawing.Color.Green;
            this.lblFullLightIntensity.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblFullLightIntensity.ForeShadowColor = System.Drawing.Color.Black;
            this.lblFullLightIntensity.Location = new System.Drawing.Point(321, 379);
            this.lblFullLightIntensity.Name = "lblFullLightIntensity";
            this.lblFullLightIntensity.Size = new System.Drawing.Size(48, 29);
            this.lblFullLightIntensity.TabIndex = 19;
            this.lblFullLightIntensity.Text = "OK";
            this.lblFullLightIntensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblFullLightIntensity.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblEeprom
            // 
            this.lblEeprom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblEeprom.AutoSize = true;
            this.lblEeprom.BackColor = System.Drawing.Color.Transparent;
            this.lblEeprom.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblEeprom.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblEeprom.BackShadowColor = System.Drawing.Color.Black;
            this.lblEeprom.BorderColor = System.Drawing.Color.Black;
            this.lblEeprom.BorderWidth = 1;
            this.lblEeprom.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEeprom.ForeColor = System.Drawing.Color.Green;
            this.lblEeprom.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblEeprom.ForeShadowColor = System.Drawing.Color.Black;
            this.lblEeprom.Location = new System.Drawing.Point(321, 461);
            this.lblEeprom.Name = "lblEeprom";
            this.lblEeprom.Size = new System.Drawing.Size(48, 29);
            this.lblEeprom.TabIndex = 20;
            this.lblEeprom.Text = "OK";
            this.lblEeprom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEeprom.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // lblAmbLightIntensity
            // 
            this.lblAmbLightIntensity.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAmbLightIntensity.AutoSize = true;
            this.lblAmbLightIntensity.BackColor = System.Drawing.Color.Transparent;
            this.lblAmbLightIntensity.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblAmbLightIntensity.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblAmbLightIntensity.BackShadowColor = System.Drawing.Color.Black;
            this.lblAmbLightIntensity.BorderColor = System.Drawing.Color.Black;
            this.lblAmbLightIntensity.BorderWidth = 1;
            this.lblAmbLightIntensity.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAmbLightIntensity.ForeColor = System.Drawing.Color.Green;
            this.lblAmbLightIntensity.ForeGradientColor2 = System.Drawing.Color.Green;
            this.lblAmbLightIntensity.ForeShadowColor = System.Drawing.Color.Black;
            this.lblAmbLightIntensity.Location = new System.Drawing.Point(321, 420);
            this.lblAmbLightIntensity.Name = "lblAmbLightIntensity";
            this.lblAmbLightIntensity.Size = new System.Drawing.Size(48, 29);
            this.lblAmbLightIntensity.TabIndex = 21;
            this.lblAmbLightIntensity.Text = "OK";
            this.lblAmbLightIntensity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAmbLightIntensity.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // smartLabel23
            // 
            this.smartLabel23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel23.AutoSize = true;
            this.smartLabel23.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.BorderColor = System.Drawing.Color.Black;
            this.smartLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.Location = new System.Drawing.Point(37, 3);
            this.smartLabel23.Name = "smartLabel23";
            this.smartLabel23.Size = new System.Drawing.Size(405, 29);
            this.smartLabel23.TabIndex = 22;
            this.smartLabel23.Text = "OPACITY - ALARM INFORMATION  ";
            this.smartLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel23.TextBorderColor = System.Drawing.Color.Empty;
            // 
            // btnCheckAgain
            // 
            this.btnCheckAgain.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheckAgain.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckAgain.Location = new System.Drawing.Point(125, 512);
            this.btnCheckAgain.Name = "btnCheckAgain";
            this.btnCheckAgain.Size = new System.Drawing.Size(149, 50);
            this.btnCheckAgain.TabIndex = 23;
            this.btnCheckAgain.Text = "Check Again";
            this.btnCheckAgain.UseVisualStyleBackColor = true;
            this.btnCheckAgain.Click += new System.EventHandler(this.btnCheckAgain_Click);
            // 
            // btnExit
            // 
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(295, 512);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(119, 50);
            this.btnExit.TabIndex = 24;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmOpacityAlarmInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(494, 574);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnCheckAgain);
            this.Controls.Add(this.smartLabel23);
            this.Controls.Add(this.lblAmbLightIntensity);
            this.Controls.Add(this.lblEeprom);
            this.Controls.Add(this.lblFullLightIntensity);
            this.Controls.Add(this.lblFanCurrentImbalance);
            this.Controls.Add(this.lblFanOverCurrent);
            this.Controls.Add(this.lblOpaity);
            this.Controls.Add(this.lblLedTemp);
            this.Controls.Add(this.lblPowerVoltage);
            this.Controls.Add(this.lblTubeTemp);
            this.Controls.Add(this.lblDetectorTemp);
            this.Controls.Add(this.lblBoardTemp);
            this.Controls.Add(this.smartLabel11);
            this.Controls.Add(this.smartLabel10);
            this.Controls.Add(this.smartLabel9);
            this.Controls.Add(this.smartLabel8);
            this.Controls.Add(this.smartLabel7);
            this.Controls.Add(this.smartLabel6);
            this.Controls.Add(this.smartLabel5);
            this.Controls.Add(this.smartLabel4);
            this.Controls.Add(this.smartLabel3);
            this.Controls.Add(this.smartLabel2);
            this.Controls.Add(this.smartLabel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmOpacityAlarmInfo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel3;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel4;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel5;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel6;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel7;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel8;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel9;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel11;
        private InterfaceLab.WinForm.Controls.SmartLabel lblBoardTemp;
        private InterfaceLab.WinForm.Controls.SmartLabel lblDetectorTemp;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTubeTemp;
        private InterfaceLab.WinForm.Controls.SmartLabel lblPowerVoltage;
        private InterfaceLab.WinForm.Controls.SmartLabel lblLedTemp;
        private InterfaceLab.WinForm.Controls.SmartLabel lblOpaity;
        private InterfaceLab.WinForm.Controls.SmartLabel lblFanOverCurrent;
        private InterfaceLab.WinForm.Controls.SmartLabel lblFanCurrentImbalance;
        private InterfaceLab.WinForm.Controls.SmartLabel lblFullLightIntensity;
        private InterfaceLab.WinForm.Controls.SmartLabel lblEeprom;
        private InterfaceLab.WinForm.Controls.SmartLabel lblAmbLightIntensity;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel23;
        private System.Windows.Forms.Button btnCheckAgain;
        private System.Windows.Forms.Button btnExit;
    }
}