﻿namespace EngineAnalyzer
{
    partial class frmOpacitySettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnExit = new System.Windows.Forms.Button();
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.tmrTime = new System.Windows.Forms.Timer(this.components);
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.cmbxEngineStoke = new System.Windows.Forms.ComboBox();
            this.cmbxSpeedSensor = new System.Windows.Forms.ComboBox();
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.cmbxGasComp = new System.Windows.Forms.ComboBox();
            this.smartLabel3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(410, 217);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(138, 40);
            this.btnExit.TabIndex = 21;
            this.btnExit.Text = "EXIT";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.AutoSize = true;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(37, 6);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(501, 33);
            this.smartLabel10.TabIndex = 17;
            this.smartLabel10.Text = "OPACITY - PAREMETER SETTINGS";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel10.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // tmrTime
            // 
            this.tmrTime.Enabled = true;
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.Location = new System.Drawing.Point(37, 61);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(203, 33);
            this.smartLabel1.TabIndex = 22;
            this.smartLabel1.Text = "Engine Stoke: ";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel1.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // cmbxEngineStoke
            // 
            this.cmbxEngineStoke.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxEngineStoke.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxEngineStoke.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxEngineStoke.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxEngineStoke.FormattingEnabled = true;
            this.cmbxEngineStoke.Items.AddRange(new object[] {
            "Two Stroke",
            "Four Stroke"});
            this.cmbxEngineStoke.Location = new System.Drawing.Point(245, 59);
            this.cmbxEngineStoke.Name = "cmbxEngineStoke";
            this.cmbxEngineStoke.Size = new System.Drawing.Size(300, 37);
            this.cmbxEngineStoke.TabIndex = 23;
            // 
            // cmbxSpeedSensor
            // 
            this.cmbxSpeedSensor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxSpeedSensor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxSpeedSensor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxSpeedSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxSpeedSensor.FormattingEnabled = true;
            this.cmbxSpeedSensor.Items.AddRange(new object[] {
            "Piezoelectric",
            "Optical",
            "Battery"});
            this.cmbxSpeedSensor.Location = new System.Drawing.Point(245, 109);
            this.cmbxSpeedSensor.Name = "cmbxSpeedSensor";
            this.cmbxSpeedSensor.Size = new System.Drawing.Size(300, 37);
            this.cmbxSpeedSensor.TabIndex = 25;
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.Location = new System.Drawing.Point(26, 109);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(214, 33);
            this.smartLabel2.TabIndex = 24;
            this.smartLabel2.Text = "Speed Sensor: ";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel2.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // cmbxGasComp
            // 
            this.cmbxGasComp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxGasComp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxGasComp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxGasComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxGasComp.FormattingEnabled = true;
            this.cmbxGasComp.Items.AddRange(new object[] {
            "On",
            "Off"});
            this.cmbxGasComp.Location = new System.Drawing.Point(245, 159);
            this.cmbxGasComp.Name = "cmbxGasComp";
            this.cmbxGasComp.Size = new System.Drawing.Size(300, 37);
            this.cmbxGasComp.TabIndex = 27;
            // 
            // smartLabel3
            // 
            this.smartLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel3.AutoSize = true;
            this.smartLabel3.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.BorderColor = System.Drawing.Color.Black;
            this.smartLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel3.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.Location = new System.Drawing.Point(69, 161);
            this.smartLabel3.Name = "smartLabel3";
            this.smartLabel3.Size = new System.Drawing.Size(171, 33);
            this.smartLabel3.TabIndex = 26;
            this.smartLabel3.Text = "Gas Comp: ";
            this.smartLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel3.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSave.Location = new System.Drawing.Point(248, 217);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(138, 40);
            this.btnSave.TabIndex = 30;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmOpacitySettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(574, 276);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbxGasComp);
            this.Controls.Add(this.smartLabel3);
            this.Controls.Add(this.cmbxSpeedSensor);
            this.Controls.Add(this.smartLabel2);
            this.Controls.Add(this.cmbxEngineStoke);
            this.Controls.Add(this.smartLabel1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.smartLabel10);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(580, 300);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(580, 300);
            this.Name = "frmOpacitySettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private System.Windows.Forms.Timer tmrTime;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
        private System.Windows.Forms.ComboBox cmbxEngineStoke;
        private System.Windows.Forms.ComboBox cmbxSpeedSensor;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        private System.Windows.Forms.ComboBox cmbxGasComp;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel3;
        private System.Windows.Forms.Button btnSave;
    }
}