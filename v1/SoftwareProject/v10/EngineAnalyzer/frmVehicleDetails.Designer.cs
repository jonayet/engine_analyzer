﻿namespace EngineAnalyzer
{
    partial class frmVehicleDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.tbxVehicleDetailsOthers = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbxVehicleMake = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbxVehicleOdometer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxVehicleDetailsDate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxVehicleRegNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxVehicleModel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxVehicleManufacturingYear = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbxVehicleEngineRef = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbxVehicleSwiftVolume = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxVehicleFuel = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnPhyObservation = new System.Windows.Forms.Button();
            this.btnCylinderCompressionTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(215, 556);
            this.label5.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 29);
            this.label5.TabIndex = 21;
            this.label5.Text = "Others:";
            // 
            // tbxVehicleDetailsOthers
            // 
            this.tbxVehicleDetailsOthers.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleDetailsOthers.Location = new System.Drawing.Point(332, 549);
            this.tbxVehicleDetailsOthers.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleDetailsOthers.Name = "tbxVehicleDetailsOthers";
            this.tbxVehicleDetailsOthers.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleDetailsOthers.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(225, 206);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 29);
            this.label4.TabIndex = 19;
            this.label4.Text = "Make:";
            // 
            // tbxVehicleMake
            // 
            this.tbxVehicleMake.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleMake.Location = new System.Drawing.Point(332, 201);
            this.tbxVehicleMake.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleMake.Name = "tbxVehicleMake";
            this.tbxVehicleMake.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleMake.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 148);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(285, 29);
            this.label3.TabIndex = 17;
            this.label3.Text = "Odometer(Mileage) K.M.:";
            // 
            // tbxVehicleOdometer
            // 
            this.tbxVehicleOdometer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleOdometer.Location = new System.Drawing.Point(332, 143);
            this.tbxVehicleOdometer.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleOdometer.Name = "tbxVehicleOdometer";
            this.tbxVehicleOdometer.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleOdometer.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(234, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 29);
            this.label1.TabIndex = 13;
            this.label1.Text = "Date:";
            // 
            // tbxVehicleDetailsDate
            // 
            this.tbxVehicleDetailsDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleDetailsDate.Location = new System.Drawing.Point(332, 22);
            this.tbxVehicleDetailsDate.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleDetailsDate.Name = "tbxVehicleDetailsDate";
            this.tbxVehicleDetailsDate.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleDetailsDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(117, 88);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(186, 29);
            this.label2.TabIndex = 25;
            this.label2.Text = "Registration No:";
            // 
            // tbxVehicleRegNo
            // 
            this.tbxVehicleRegNo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleRegNo.Location = new System.Drawing.Point(332, 80);
            this.tbxVehicleRegNo.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleRegNo.Name = "tbxVehicleRegNo";
            this.tbxVehicleRegNo.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleRegNo.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(220, 264);
            this.label6.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 29);
            this.label6.TabIndex = 27;
            this.label6.Text = "Model:";
            // 
            // tbxVehicleModel
            // 
            this.tbxVehicleModel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleModel.Location = new System.Drawing.Point(332, 259);
            this.tbxVehicleModel.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleModel.Name = "tbxVehicleModel";
            this.tbxVehicleModel.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleModel.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(75, 320);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(226, 29);
            this.label7.TabIndex = 29;
            this.label7.Text = "Manufacturing Year:";
            // 
            // tbxVehicleManufacturingYear
            // 
            this.tbxVehicleManufacturingYear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleManufacturingYear.Location = new System.Drawing.Point(332, 317);
            this.tbxVehicleManufacturingYear.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleManufacturingYear.Name = "tbxVehicleManufacturingYear";
            this.tbxVehicleManufacturingYear.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleManufacturingYear.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(157, 378);
            this.label8.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 29);
            this.label8.TabIndex = 31;
            this.label8.Text = "Engine Ref.:";
            // 
            // tbxVehicleEngineRef
            // 
            this.tbxVehicleEngineRef.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleEngineRef.Location = new System.Drawing.Point(332, 370);
            this.tbxVehicleEngineRef.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleEngineRef.Name = "tbxVehicleEngineRef";
            this.tbxVehicleEngineRef.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleEngineRef.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(94, 436);
            this.label9.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(215, 29);
            this.label9.TabIndex = 33;
            this.label9.Text = "Swift Volume(C.C):";
            // 
            // tbxVehicleSwiftVolume
            // 
            this.tbxVehicleSwiftVolume.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleSwiftVolume.Location = new System.Drawing.Point(332, 428);
            this.tbxVehicleSwiftVolume.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleSwiftVolume.Name = "tbxVehicleSwiftVolume";
            this.tbxVehicleSwiftVolume.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleSwiftVolume.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(181, 494);
            this.label10.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 29);
            this.label10.TabIndex = 35;
            this.label10.Text = "Fuel Type:";
            // 
            // tbxVehicleFuel
            // 
            this.tbxVehicleFuel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxVehicleFuel.Location = new System.Drawing.Point(332, 491);
            this.tbxVehicleFuel.Margin = new System.Windows.Forms.Padding(7);
            this.tbxVehicleFuel.Name = "tbxVehicleFuel";
            this.tbxVehicleFuel.Size = new System.Drawing.Size(461, 35);
            this.tbxVehicleFuel.TabIndex = 9;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Location = new System.Drawing.Point(657, 605);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(135, 51);
            this.btnExit.TabIndex = 38;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.Location = new System.Drawing.Point(494, 605);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(135, 51);
            this.btnSave.TabIndex = 37;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Location = new System.Drawing.Point(331, 605);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(135, 51);
            this.btnClear.TabIndex = 36;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPhyObservation
            // 
            this.btnPhyObservation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPhyObservation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPhyObservation.Location = new System.Drawing.Point(44, 605);
            this.btnPhyObservation.Name = "btnPhyObservation";
            this.btnPhyObservation.Size = new System.Drawing.Size(259, 51);
            this.btnPhyObservation.TabIndex = 39;
            this.btnPhyObservation.Text = "Physical Inspection";
            this.btnPhyObservation.UseVisualStyleBackColor = true;
            this.btnPhyObservation.Click += new System.EventHandler(this.btnPhyObservation_Click);
            // 
            // btnCylinderCompressionTest
            // 
            this.btnCylinderCompressionTest.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCylinderCompressionTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCylinderCompressionTest.Location = new System.Drawing.Point(44, 672);
            this.btnCylinderCompressionTest.Name = "btnCylinderCompressionTest";
            this.btnCylinderCompressionTest.Size = new System.Drawing.Size(328, 51);
            this.btnCylinderCompressionTest.TabIndex = 40;
            this.btnCylinderCompressionTest.Text = "Cylinder Compression Test";
            this.btnCylinderCompressionTest.UseVisualStyleBackColor = true;
            this.btnCylinderCompressionTest.Click += new System.EventHandler(this.btnCylinderCompressionTest_Click);
            // 
            // frmVehicleDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(854, 738);
            this.Controls.Add(this.btnCylinderCompressionTest);
            this.Controls.Add(this.btnPhyObservation);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbxVehicleFuel);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbxVehicleSwiftVolume);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbxVehicleEngineRef);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxVehicleManufacturingYear);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxVehicleModel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxVehicleRegNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxVehicleDetailsOthers);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxVehicleMake);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbxVehicleOdometer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxVehicleDetailsDate);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(860, 700);
            this.Name = "frmVehicleDetails";
            this.Text = "Vehicle Details";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVehicleDetails_FormClosing);
            this.Load += new System.EventHandler(this.frmVehicleDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxVehicleDetailsOthers;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxVehicleMake;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbxVehicleOdometer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxVehicleDetailsDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxVehicleRegNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxVehicleModel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxVehicleManufacturingYear;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbxVehicleEngineRef;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbxVehicleSwiftVolume;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbxVehicleFuel;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnPhyObservation;
        private System.Windows.Forms.Button btnCylinderCompressionTest;
    }
}