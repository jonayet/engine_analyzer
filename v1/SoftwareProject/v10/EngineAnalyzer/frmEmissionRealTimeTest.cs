﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EngineAnalyzer.Properties;
using RTF;

namespace EngineAnalyzer
{
    public partial class frmEmissionRealTimeTest : Form
    {
        private RTFBuilderbase richTextBuilder = new RTFBuilder();
        private SerialComunication sCom = new SerialComunication(CommonResources.EmissionPortName, 9600);
        private const float AFR_Constant = 14.7f;

        public frmEmissionRealTimeTest()
        {
            InitializeComponent();
            sCom.AsyncReceived += new EventHandler<AsyncReceived_EventArgs>(sCom_AsyncReceived);
        }

        protected override void OnLoad(EventArgs e)
        {
            // get limits
            lblCO2Max.Text = Settings.Default.EmissionCO2Max.ToString();
            lblCO2Min.Text = Settings.Default.EmissionCO2Min.ToString();
            lblCOMax.Text = Settings.Default.EmissionCOMax.ToString();
            lblCOMin.Text = Settings.Default.EmissionCOMin.ToString();
            lblNOMax.Text = Settings.Default.EmissionNOMax.ToString();
            lblNOMin.Text = Settings.Default.EmissionNOMin.ToString();
            lblHCMax.Text = Settings.Default.EmissionHCMax.ToString();
            lblHCMin.Text = Settings.Default.EmissionHCMin.ToString();
            lblO2Max.Text = Settings.Default.EmissionO2Max.ToString();
            lblO2Min.Text = Settings.Default.EmissionO2Min.ToString();

            // show limits
            if (!Settings.Default.EmissionPublishLimits)
            {
                pnlCO2Limits.Visible = false;
                pnlCOLimits.Visible = false;
                pnlHCLimits.Visible = false;
                pnlNOLimits.Visible = false;
                pnlO2Limits.Visible = false;
            }

            // load Total Capture
            if (CommonResources.EmissionTotalCapture > 0)
            {
                btnCapture.Text = "CAPTURE - " + CommonResources.EmissionTotalCapture.ToString();
            }

            // load RichText
            richTextBuilder.Clear();
            if (string.IsNullOrEmpty(CommonResources.EmissionCapturedRichText))
            {
                // add Emission Title
                richTextBuilder.FontSize(30).FontStyle(FontStyle.Bold).AppendLine("Emission Analysis Report");
                richTextBuilder.AppendLine();
                richTextBuilder.AppendLine();

                // add Emission Limits
                if (Settings.Default.EmissionPublishLimits)
                {
                    richTextBuilder.FontStyle(FontStyle.Bold).Append("Limits");
                    richTextBuilder.AppendPara();
                    richTextBuilder.Reset();
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Diagnosis Limit", "Min", "Max");
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Idle RPM", Settings.Default.EmissionSMin.ToString(), Settings.Default.EmissionSMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "CO %", Settings.Default.EmissionCOMin.ToString(), Settings.Default.EmissionCOMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "HC PPM", Settings.Default.EmissionHCMin.ToString(), Settings.Default.EmissionHCMax.ToString());
                    CommonResources.RTF_AddRow(richTextBuilder, new int[] { 40, 10, 10 }, "Engine Temp", Settings.Default.EmissionTMin.ToString(), Settings.Default.EmissionTMax.ToString());
                    richTextBuilder.Reset();
                    richTextBuilder.AppendLine();
                    richTextBuilder.AppendLine();
                }
            }
            else
            {
                richTextBuilder.AppendRTFDocument(CommonResources.EmissionCapturedRichText);
            }

            // set default item
            cmbxCaption.SelectedIndex = 0;

            // open COM port
            sCom.Open();

            // pass to base method
            base.OnLoad(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            sCom.Send(0x02);
            tmrMeasure.Enabled = false;
            sCom.Close();
            base.OnClosing(e);
        }

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            // add number
            CommonResources.EmissionTotalCapture++;
            btnCapture.Text = "CAPTURE - " + CommonResources.EmissionTotalCapture.ToString();

            // add caption
            if (cmbxCaption.SelectedIndex > 0)
            {
                richTextBuilder.FontStyle(FontStyle.Bold).Append((string)cmbxCaption.SelectedItem + ":");
            }
            else
            {
//                richTextBuilder.FontStyle(FontStyle.Bold).Append("Date: " + DateTime.Now.ToLongDateString() + " Time: " + DateTime.Now.ToLongTimeString());
            }

            // add HC, CO, CO2, O2, NO, n, Lamda and T
            richTextBuilder.AppendPara();
            richTextBuilder.Reset();
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "Engine speed", lblSpped.Text + " RPM");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "HC (Un-burn Hydrocarbon)", lblHC.Text + " ppm");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "CO (Carbon Monoxide)", lblCO.Text + " %");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "CO2 (Carbon Dioxide)", lblCO2.Text + " %");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "O2 (Oxygen)", lblO2.Text + " %");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "NO (Nitric Oxide)", lblNO.Text + " ppm");
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "Air Fuel Ratio", lblAFR.Text );
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "Calculated Lambda", lblLamda.Text);
            CommonResources.RTF_AddRow(richTextBuilder, new int[] {40, 20}, "Engine Temperature", lblT.Text + " °C");
            richTextBuilder.Reset();
            richTextBuilder.AppendLine();
            richTextBuilder.AppendLine();

            // save new values
            CommonResources.EmissionCapturedRichText = richTextBuilder.ToString();
        }

        private void btnMeasure_Click(object sender, EventArgs e)
        {
            if (btnMeasure.Text == "MEASURE")
            {
                if (sCom.Send(0x01, 0x06, 2000))
                {
                    tmrMeasure.Enabled = true;
                    btnMeasure.Text = "STOP";
                }
                else
                {
                    MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (sCom.Send(0x02, 0x06, 2000))
                {
                    tmrMeasure.Enabled = false;
                    btnMeasure.Text = "MEASURE";
                }
                else
                {

                    MessageBox.Show("Device is not responding, Plese check connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        void sCom_AsyncReceived(object sender, AsyncReceived_EventArgs e)
        {
            if (e.ReceivedSuccess)
            {
                // get data from EventArgs
                int Ack = e.ReceivedData[0];
                int HC_Value = (e.ReceivedData[1] << 8) | e.ReceivedData[2];
                float CO_Value = (e.ReceivedData[3] << 8) | e.ReceivedData[4];
                float CO2_Value = (e.ReceivedData[5] << 8) | e.ReceivedData[6];
                float O2_Value = (e.ReceivedData[7] << 8) | e.ReceivedData[8];
                int NO_Value = (e.ReceivedData[9] << 8) | e.ReceivedData[10];
                int Speed_Value = (e.ReceivedData[11] << 8) | e.ReceivedData[12];
                int Temperature_Value = (e.ReceivedData[13] << 8) | e.ReceivedData[14];
                float Lamda_Value = (e.ReceivedData[15] << 8) | e.ReceivedData[16];
                int CheckSum_Value = (e.ReceivedData[17] << 8) | e.ReceivedData[18];

                // check limits
                if (HC_Value > 10000) { HC_Value = 0; }
                if (CO_Value > 10000) { CO2_Value = 0; }
                if (CO2_Value > 10000) { CO2_Value = 0; }
                if (O2_Value > 10000) { O2_Value = 0; }
                if (NO_Value > 10000) { NO_Value = 0; }
                if (Speed_Value > 10000) { Speed_Value = 0; }
                if (Temperature_Value > 10000) { Temperature_Value = 0; }
                if (Lamda_Value > 10000) { Lamda_Value = 0; }

                // shape the results
                CO_Value /= 100;
                CO2_Value /= 100;
                O2_Value /= 100;
                Lamda_Value /= 100;

                // show the values
                lblHC.Text = HC_Value.ToString();
                lblCO.Text = CO_Value.ToString("0.00");
                lblCO2.Text = CO2_Value.ToString("0.00");
                lblO2.Text = O2_Value.ToString("0.00");
                lblNO.Text = NO_Value.ToString();
                lblSpped.Text = Speed_Value.ToString();
                lblT.Text = Temperature_Value.ToString();
                lblLamda.Text = Lamda_Value.ToString();
                lblAFR.Text = (Lamda_Value * AFR_Constant).ToString("0.00") + " : 1";
                int pos = Speed_Value / 50;
                if (pos > 100) { pos = 100; }
                pbrSpped.Position = pos;
            }
        }

        private void tmrMeasGeneral_Tick(object sender, EventArgs e)
        {
            sCom.SendAsync(0x03, 19, 300);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            richTextBuilder.Clear();
            CommonResources.EmissionTotalCapture = 0;
            CommonResources.EmissionCapturedRichText = "";
            btnCapture.Text = "CAPTURE";
        }
    }
}
