﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

public partial class frmLicenseChecker : Form
{
	static public string OwnerName = "";
	static public string OwnerAddress = "";
	static public string InstallDate = "";
	static public string CurrentComputerID = "";
    static public string LicenseComputerID = "";
    static public bool WaitWhileReading = true;
    static public bool IsLicensed = false;
    private string LicenseFile = "License.eal";
    string SecretKey = "1FBDE5D6";
	BackgroundWorker bgWorker = new BackgroundWorker();

	public frmLicenseChecker()
	{
		InitializeComponent();

		// start background task
		bgWorker.DoWork += new DoWorkEventHandler(bgWorker_DoWork);
		bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
		bgWorker.RunWorkerAsync();
	}

	void bgWorker_DoWork(object sender, DoWorkEventArgs e)
	{
		CurrentComputerID = ComputerID.GetUniqueID();
	}

	void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
	{
        ReadLicense();
        if (CurrentComputerID == LicenseComputerID)
        {
            WaitWhileReading = false;
            IsLicensed = true;
            this.Close();
        }
        else
        {
            WaitWhileReading = false;
            IsLicensed = false;
            tbxMachineId.Text = CurrentComputerID;
            owfProgressControl1.Visible = false;
            pnlNoLicense.Visible = true;
        }
	}

	private bool ReadLicense()
	{
		if (!File.Exists(LicenseFile)) { return false; }

		try
		{
			// Create an XML reader for this file.
			using (XmlTextReader reader = new XmlTextReader(LicenseFile))
			{
				while (reader.Read())
				{
					// Only detect start elements.
					if (reader.IsStartElement())
					{
						switch (reader.Name)
						{
							case "OwnerName":
								reader.Read();
								OwnerName = reader.Value;
								break;
							case "OwnerAddress":
								reader.Read();
								OwnerAddress = reader.Value;
								break;
							case "InstallDate":
								reader.Read();
								InstallDate = Encryption.Decrypt(reader.Value, SecretKey);
								break;
							case "License":
								reader.Read();
								LicenseComputerID = Encryption.Decrypt(reader.Value, SecretKey);
								break;
						}
					}
				}
			}
			return true;
		}
		catch { return false; }
	}

    private void btnClose_Click(object sender, EventArgs e)
    {
        Application.Exit();
    }
}