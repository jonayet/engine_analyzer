﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    public partial class frmOpacityAlarmInfo : Form
    {
        private SerialComunication sCom = new SerialComunication(CommonResources.OpacityPortName, 9600);

        public frmOpacityAlarmInfo()
        {
            InitializeComponent();
        }

        protected override void OnShown(EventArgs e)
        {
            bool IsSendSuccess = false;
            byte[] AlarmFlag = new byte[2];
            sCom.Open();
            IsSendSuccess = sCom.Send(CommonResources.Opacity_GetDataPacket(0xA3), AlarmFlag, 2, 100);
            if (IsSendSuccess)
            {
                ShowAlarm(AlarmFlag);
            }
            else
            {
                MessageBox.Show("Device is not responding. Please chek the connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            base.OnShown(e);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            sCom.Close();
            base.OnClosing(e);
        }

        private void btnCheckAgain_Click(object sender, EventArgs e)
        {
            bool IsSendSuccess = false;
            byte[] AlarmFlag = new byte[2];
            IsSendSuccess = sCom.Send(CommonResources.Opacity_GetDataPacket(0xA3), AlarmFlag, 2, 100);
            if (IsSendSuccess)
            {
                ShowAlarm(AlarmFlag);
            }
            else
            {
                MessageBox.Show("Device is not responding. Please chek the connections.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void ShowAlarm(byte[] Flag)
        {
            // get the alarm flag
            byte AlarmFlag_Lo = Flag[0];
            byte AlarmFlag_Hi = Flag[1];

            // Full Light Intensity
            if ((AlarmFlag_Hi & 0x02) != 0)
            {
                lblFullLightIntensity.Text = "ERROR";
                lblFullLightIntensity.ForeColor = Color.Red;
                lblFullLightIntensity.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblFullLightIntensity.Text = "OK";
                lblFullLightIntensity.ForeColor = Color.Green;
                lblFullLightIntensity.ForeGradientColor2 = Color.Green;
            }

            // Amb. Light Intensity
            if ((AlarmFlag_Hi & 0x04) != 0)
            {
                lblAmbLightIntensity.Text = "ERROR";
                lblAmbLightIntensity.ForeColor = Color.Red;
                lblAmbLightIntensity.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblAmbLightIntensity.Text = "OK";
                lblAmbLightIntensity.ForeColor = Color.Green;
                lblAmbLightIntensity.ForeGradientColor2 = Color.Green;
            }

            // EEPROM
            if ((AlarmFlag_Hi & 0x80) != 0)
            {
                lblEeprom.Text = "ERROR";
                lblEeprom.ForeColor = Color.Red;
                lblEeprom.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblEeprom.Text = "OK";
                lblEeprom.ForeColor = Color.Green;
                lblEeprom.ForeGradientColor2 = Color.Green;
            }

            // Board Temp
            if ((AlarmFlag_Lo & 0x01) != 0)
            {
                lblBoardTemp.Text = "ERROR";
                lblBoardTemp.ForeColor = Color.Red;
                lblBoardTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblBoardTemp.Text = "OK";
                lblBoardTemp.ForeColor = Color.Green;
                lblBoardTemp.ForeGradientColor2 = Color.Green;
            }

            // Detector Temp
            if ((AlarmFlag_Lo & 0x02) != 0)
            {
                lblDetectorTemp.Text = "ERROR";
                lblDetectorTemp.ForeColor = Color.Red;
                lblDetectorTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblDetectorTemp.Text = "OK";
                lblDetectorTemp.ForeColor = Color.Green;
                lblDetectorTemp.ForeGradientColor2 = Color.Green;
            }

            // Tube Temp
            if ((AlarmFlag_Lo & 0x04) != 0)
            {
                lblTubeTemp.Text = "ERROR";
                lblTubeTemp.ForeColor = Color.Red;
                lblTubeTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblTubeTemp.Text = "OK";
                lblTubeTemp.ForeColor = Color.Green;
                lblTubeTemp.ForeGradientColor2 = Color.Green;
            }

            // Power Voltage
            if ((AlarmFlag_Lo & 0x08) != 0)
            {
                lblPowerVoltage.Text = "ERROR";
                lblPowerVoltage.ForeColor = Color.Red;
                lblPowerVoltage.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblPowerVoltage.Text = "OK";
                lblPowerVoltage.ForeColor = Color.Green;
                lblPowerVoltage.ForeGradientColor2 = Color.Green;
            }

            // LED Temp
            if ((AlarmFlag_Lo & 0x10) != 0)
            {
                lblLedTemp.Text = "ERROR";
                lblLedTemp.ForeColor = Color.Red;
                lblLedTemp.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblLedTemp.Text = "OK";
                lblLedTemp.ForeColor = Color.Green;
                lblLedTemp.ForeGradientColor2 = Color.Green;
            }

            // Opacity
            if ((AlarmFlag_Lo & 0x20) != 0)
            {
                lblOpaity.Text = "ERROR";
                lblOpaity.ForeColor = Color.Red;
                lblOpaity.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblOpaity.Text = "OK";
                lblOpaity.ForeColor = Color.Green;
                lblOpaity.ForeGradientColor2 = Color.Green;
            }

            // Fan Over Current
            if ((AlarmFlag_Lo & 0x40) != 0)
            {
                lblFanOverCurrent.Text = "ERROR";
                lblFanOverCurrent.ForeColor = Color.Red;
                lblFanOverCurrent.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblFanOverCurrent.Text = "OK";
                lblFanOverCurrent.ForeColor = Color.Green;
                lblFanOverCurrent.ForeGradientColor2 = Color.Green;
            }

            // Fan Current Imbalance
            if ((AlarmFlag_Lo & 0x80) != 0)
            {
                lblFanCurrentImbalance.Text = "ERROR";
                lblFanCurrentImbalance.ForeColor = Color.Red;
                lblFanCurrentImbalance.ForeGradientColor2 = Color.Red;
            }
            else
            {
                lblFanCurrentImbalance.Text = "OK";
                lblFanCurrentImbalance.ForeColor = Color.Green;
                lblFanCurrentImbalance.ForeGradientColor2 = Color.Green;
            }
        }
    }
}
