﻿namespace EngineAnalyzer
{
    partial class frmEmissionRealTimeTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmissionRealTimeTest));
            this.tmrTime = new System.Windows.Forms.Timer(this.components);
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCapture = new System.Windows.Forms.Button();
            this.lblDate = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTime = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel10 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel22 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblSpped = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel9 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlNOLimits = new System.Windows.Forms.Panel();
            this.lblNOMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblNOMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel23 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlCO2Limits = new System.Windows.Forms.Panel();
            this.lblCO2Min = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCO2Max = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel16 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlHCLimits = new System.Windows.Forms.Panel();
            this.lblHCMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblHCMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel6 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel19 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel20 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel15 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel17 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel8 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel12 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel7 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblT = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblNO = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCO2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblHC = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel1 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlO2Limits = new System.Windows.Forms.Panel();
            this.lblO2Min = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblO2Max = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel13 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.pnlCOLimits = new System.Windows.Forms.Panel();
            this.lblCOMin = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCOMax = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel36 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel39 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel40 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel41 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblO2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCO = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel3 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblLamda = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnMeasure = new System.Windows.Forms.Button();
            this.tmrMeasure = new System.Windows.Forms.Timer(this.components);
            this.smartLabel2 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblAFR = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel34 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.cmbxCaption = new System.Windows.Forms.ComboBox();
            this.smartLabel35 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel33 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel32 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel31 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel30 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel29 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel28 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel25 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel24 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel21 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.smartLabel18 = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnClear = new System.Windows.Forms.Button();
            this.pbrSpped = new AdvancedProgressBarControl.AdvancedProgressBar();
            this.pnlNOLimits.SuspendLayout();
            this.pnlCO2Limits.SuspendLayout();
            this.pnlHCLimits.SuspendLayout();
            this.pnlO2Limits.SuspendLayout();
            this.pnlCOLimits.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrTime
            // 
            this.tmrTime.Enabled = true;
            this.tmrTime.Tick += new System.EventHandler(this.tmrTime_Tick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(577, 643);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(138, 48);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "CLOSE";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCapture
            // 
            this.btnCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCapture.AutoSize = true;
            this.btnCapture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapture.ForeColor = System.Drawing.Color.Blue;
            this.btnCapture.Location = new System.Drawing.Point(176, 642);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(197, 48);
            this.btnCapture.TabIndex = 180;
            this.btnCapture.Text = "CAPTURE";
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDate.AutoSize = true;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblDate.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblDate.BackShadowColor = System.Drawing.Color.Black;
            this.lblDate.BorderColor = System.Drawing.Color.Black;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 31F);
            this.lblDate.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblDate.ForeShadowAmount = 2;
            this.lblDate.ForeShadowColor = System.Drawing.Color.Black;
            this.lblDate.ForeShadowOffset = 1;
            this.lblDate.Location = new System.Drawing.Point(778, 9);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(228, 48);
            this.lblDate.TabIndex = 184;
            this.lblDate.Text = "01/01/2013";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDate.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTime.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTime.BackShadowColor = System.Drawing.Color.Black;
            this.lblTime.BorderColor = System.Drawing.Color.Black;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 29.5F);
            this.lblTime.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblTime.ForeShadowAmount = 2;
            this.lblTime.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTime.ForeShadowOffset = 1;
            this.lblTime.Location = new System.Drawing.Point(761, 54);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(245, 46);
            this.lblTime.TabIndex = 183;
            this.lblTime.Text = "00:00:00 AM";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTime.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel10
            // 
            this.smartLabel10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel10.AutoSize = true;
            this.smartLabel10.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel10.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.BorderColor = System.Drawing.Color.Black;
            this.smartLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.5F);
            this.smartLabel10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel10.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel10.Location = new System.Drawing.Point(4, 4);
            this.smartLabel10.Name = "smartLabel10";
            this.smartLabel10.Size = new System.Drawing.Size(325, 40);
            this.smartLabel10.TabIndex = 182;
            this.smartLabel10.Text = "EMISSION - TEST ";
            this.smartLabel10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel10.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel22
            // 
            this.smartLabel22.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel22.AutoSize = true;
            this.smartLabel22.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel22.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel22.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel22.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel22.BorderColor = System.Drawing.Color.Black;
            this.smartLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel22.ForeColor = System.Drawing.Color.Black;
            this.smartLabel22.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel22.ForeShadowAmount = 1;
            this.smartLabel22.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel22.ForeShadowOffset = 1;
            this.smartLabel22.Location = new System.Drawing.Point(23, 129);
            this.smartLabel22.Name = "smartLabel22";
            this.smartLabel22.Size = new System.Drawing.Size(63, 37);
            this.smartLabel22.TabIndex = 152;
            this.smartLabel22.Text = "HC";
            this.smartLabel22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel22.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblSpped
            // 
            this.lblSpped.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblSpped.BackColor = System.Drawing.Color.Transparent;
            this.lblSpped.BackGradientAngle = 150;
            this.lblSpped.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblSpped.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblSpped.BackShadowColor = System.Drawing.Color.Black;
            this.lblSpped.BorderColor = System.Drawing.Color.Black;
            this.lblSpped.BorderCornerRadius = 10;
            this.lblSpped.BorderWidth = 1;
            this.lblSpped.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.5F, System.Drawing.FontStyle.Bold);
            this.lblSpped.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblSpped.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblSpped.ForeShadowAmount = 3;
            this.lblSpped.ForeShadowColor = System.Drawing.Color.Black;
            this.lblSpped.ForeShadowOffset = 1;
            this.lblSpped.Location = new System.Drawing.Point(827, 648);
            this.lblSpped.Name = "lblSpped";
            this.lblSpped.Size = new System.Drawing.Size(93, 45);
            this.lblSpped.TabIndex = 169;
            this.lblSpped.Text = "0";
            this.lblSpped.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSpped.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel9
            // 
            this.smartLabel9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel9.AutoSize = true;
            this.smartLabel9.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel9.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.BorderColor = System.Drawing.Color.Black;
            this.smartLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel9.ForeColor = System.Drawing.Color.Black;
            this.smartLabel9.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel9.ForeShadowAmount = 1;
            this.smartLabel9.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel9.ForeShadowOffset = 1;
            this.smartLabel9.Location = new System.Drawing.Point(747, 654);
            this.smartLabel9.Name = "smartLabel9";
            this.smartLabel9.Size = new System.Drawing.Size(73, 37);
            this.smartLabel9.TabIndex = 168;
            this.smartLabel9.Text = "rpm";
            this.smartLabel9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel9.TextBorderColor = System.Drawing.Color.Black;
            // 
            // pnlNOLimits
            // 
            this.pnlNOLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlNOLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlNOLimits.Controls.Add(this.lblNOMin);
            this.pnlNOLimits.Controls.Add(this.lblNOMax);
            this.pnlNOLimits.Controls.Add(this.smartLabel23);
            this.pnlNOLimits.Location = new System.Drawing.Point(288, 323);
            this.pnlNOLimits.Name = "pnlNOLimits";
            this.pnlNOLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlNOLimits.TabIndex = 165;
            // 
            // lblNOMin
            // 
            this.lblNOMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNOMin.BackColor = System.Drawing.Color.Transparent;
            this.lblNOMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblNOMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblNOMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblNOMin.BorderColor = System.Drawing.Color.Black;
            this.lblNOMin.BorderWidth = 1;
            this.lblNOMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblNOMin.ForeColor = System.Drawing.Color.Red;
            this.lblNOMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblNOMin.ForeShadowAmount = 1;
            this.lblNOMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblNOMin.ForeShadowOffset = 1;
            this.lblNOMin.Location = new System.Drawing.Point(5, 46);
            this.lblNOMin.Name = "lblNOMin";
            this.lblNOMin.Size = new System.Drawing.Size(80, 32);
            this.lblNOMin.TabIndex = 7;
            this.lblNOMin.Text = "0";
            this.lblNOMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNOMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblNOMax
            // 
            this.lblNOMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNOMax.BackColor = System.Drawing.Color.Transparent;
            this.lblNOMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblNOMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblNOMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblNOMax.BorderColor = System.Drawing.Color.Black;
            this.lblNOMax.BorderWidth = 1;
            this.lblNOMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblNOMax.ForeColor = System.Drawing.Color.Red;
            this.lblNOMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblNOMax.ForeShadowAmount = 1;
            this.lblNOMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblNOMax.ForeShadowOffset = 1;
            this.lblNOMax.Location = new System.Drawing.Point(5, 15);
            this.lblNOMax.Name = "lblNOMax";
            this.lblNOMax.Size = new System.Drawing.Size(80, 32);
            this.lblNOMax.TabIndex = 9;
            this.lblNOMax.Text = "0";
            this.lblNOMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblNOMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel23
            // 
            this.smartLabel23.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel23.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel23.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.BorderColor = System.Drawing.Color.Black;
            this.smartLabel23.BorderWidth = 1;
            this.smartLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel23.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel23.Location = new System.Drawing.Point(5, 2);
            this.smartLabel23.Name = "smartLabel23";
            this.smartLabel23.Size = new System.Drawing.Size(80, 14);
            this.smartLabel23.TabIndex = 8;
            this.smartLabel23.Text = "LIMITS";
            this.smartLabel23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel23.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // pnlCO2Limits
            // 
            this.pnlCO2Limits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlCO2Limits.BackColor = System.Drawing.Color.Transparent;
            this.pnlCO2Limits.Controls.Add(this.lblCO2Min);
            this.pnlCO2Limits.Controls.Add(this.lblCO2Max);
            this.pnlCO2Limits.Controls.Add(this.smartLabel16);
            this.pnlCO2Limits.Location = new System.Drawing.Point(288, 229);
            this.pnlCO2Limits.Name = "pnlCO2Limits";
            this.pnlCO2Limits.Size = new System.Drawing.Size(90, 80);
            this.pnlCO2Limits.TabIndex = 164;
            // 
            // lblCO2Min
            // 
            this.lblCO2Min.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCO2Min.BackColor = System.Drawing.Color.Transparent;
            this.lblCO2Min.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCO2Min.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCO2Min.BackShadowColor = System.Drawing.Color.Black;
            this.lblCO2Min.BorderColor = System.Drawing.Color.Black;
            this.lblCO2Min.BorderWidth = 1;
            this.lblCO2Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblCO2Min.ForeColor = System.Drawing.Color.Red;
            this.lblCO2Min.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblCO2Min.ForeShadowAmount = 1;
            this.lblCO2Min.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCO2Min.ForeShadowOffset = 1;
            this.lblCO2Min.Location = new System.Drawing.Point(5, 46);
            this.lblCO2Min.Name = "lblCO2Min";
            this.lblCO2Min.Size = new System.Drawing.Size(80, 32);
            this.lblCO2Min.TabIndex = 7;
            this.lblCO2Min.Text = "0";
            this.lblCO2Min.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCO2Min.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblCO2Max
            // 
            this.lblCO2Max.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCO2Max.BackColor = System.Drawing.Color.Transparent;
            this.lblCO2Max.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCO2Max.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCO2Max.BackShadowColor = System.Drawing.Color.Black;
            this.lblCO2Max.BorderColor = System.Drawing.Color.Black;
            this.lblCO2Max.BorderWidth = 1;
            this.lblCO2Max.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblCO2Max.ForeColor = System.Drawing.Color.Red;
            this.lblCO2Max.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblCO2Max.ForeShadowAmount = 1;
            this.lblCO2Max.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCO2Max.ForeShadowOffset = 1;
            this.lblCO2Max.Location = new System.Drawing.Point(5, 15);
            this.lblCO2Max.Name = "lblCO2Max";
            this.lblCO2Max.Size = new System.Drawing.Size(80, 32);
            this.lblCO2Max.TabIndex = 9;
            this.lblCO2Max.Text = "0";
            this.lblCO2Max.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCO2Max.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel16
            // 
            this.smartLabel16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel16.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel16.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel16.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel16.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel16.BorderColor = System.Drawing.Color.Black;
            this.smartLabel16.BorderWidth = 1;
            this.smartLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel16.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel16.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel16.Location = new System.Drawing.Point(5, 2);
            this.smartLabel16.Name = "smartLabel16";
            this.smartLabel16.Size = new System.Drawing.Size(80, 14);
            this.smartLabel16.TabIndex = 8;
            this.smartLabel16.Text = "LIMITS";
            this.smartLabel16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel16.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // pnlHCLimits
            // 
            this.pnlHCLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlHCLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlHCLimits.Controls.Add(this.lblHCMin);
            this.pnlHCLimits.Controls.Add(this.lblHCMax);
            this.pnlHCLimits.Controls.Add(this.smartLabel6);
            this.pnlHCLimits.Location = new System.Drawing.Point(288, 135);
            this.pnlHCLimits.Name = "pnlHCLimits";
            this.pnlHCLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlHCLimits.TabIndex = 163;
            // 
            // lblHCMin
            // 
            this.lblHCMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHCMin.BackColor = System.Drawing.Color.Transparent;
            this.lblHCMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblHCMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblHCMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblHCMin.BorderColor = System.Drawing.Color.Black;
            this.lblHCMin.BorderWidth = 1;
            this.lblHCMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblHCMin.ForeColor = System.Drawing.Color.Red;
            this.lblHCMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblHCMin.ForeShadowAmount = 1;
            this.lblHCMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblHCMin.ForeShadowOffset = 1;
            this.lblHCMin.Location = new System.Drawing.Point(5, 46);
            this.lblHCMin.Name = "lblHCMin";
            this.lblHCMin.Size = new System.Drawing.Size(80, 32);
            this.lblHCMin.TabIndex = 7;
            this.lblHCMin.Text = "0";
            this.lblHCMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHCMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblHCMax
            // 
            this.lblHCMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHCMax.BackColor = System.Drawing.Color.Transparent;
            this.lblHCMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblHCMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblHCMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblHCMax.BorderColor = System.Drawing.Color.Black;
            this.lblHCMax.BorderWidth = 1;
            this.lblHCMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblHCMax.ForeColor = System.Drawing.Color.Red;
            this.lblHCMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblHCMax.ForeShadowAmount = 1;
            this.lblHCMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblHCMax.ForeShadowOffset = 1;
            this.lblHCMax.Location = new System.Drawing.Point(5, 15);
            this.lblHCMax.Name = "lblHCMax";
            this.lblHCMax.Size = new System.Drawing.Size(80, 32);
            this.lblHCMax.TabIndex = 9;
            this.lblHCMax.Text = "0";
            this.lblHCMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHCMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel6
            // 
            this.smartLabel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel6.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel6.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.BorderColor = System.Drawing.Color.Black;
            this.smartLabel6.BorderWidth = 1;
            this.smartLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel6.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel6.Location = new System.Drawing.Point(5, 2);
            this.smartLabel6.Name = "smartLabel6";
            this.smartLabel6.Size = new System.Drawing.Size(80, 14);
            this.smartLabel6.TabIndex = 8;
            this.smartLabel6.Text = "LIMITS";
            this.smartLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel6.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel19
            // 
            this.smartLabel19.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel19.AutoSize = true;
            this.smartLabel19.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel19.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel19.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel19.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel19.BorderColor = System.Drawing.Color.Black;
            this.smartLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel19.ForeColor = System.Drawing.Color.Black;
            this.smartLabel19.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel19.ForeShadowAmount = 1;
            this.smartLabel19.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel19.ForeShadowOffset = 1;
            this.smartLabel19.Location = new System.Drawing.Point(437, 480);
            this.smartLabel19.Name = "smartLabel19";
            this.smartLabel19.Size = new System.Drawing.Size(73, 37);
            this.smartLabel19.TabIndex = 162;
            this.smartLabel19.Text = "(°C)";
            this.smartLabel19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel19.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel20
            // 
            this.smartLabel20.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel20.AutoSize = true;
            this.smartLabel20.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel20.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel20.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel20.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel20.BorderColor = System.Drawing.Color.Black;
            this.smartLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel20.ForeColor = System.Drawing.Color.Black;
            this.smartLabel20.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel20.ForeShadowAmount = 1;
            this.smartLabel20.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel20.ForeShadowOffset = 1;
            this.smartLabel20.Location = new System.Drawing.Point(446, 431);
            this.smartLabel20.Name = "smartLabel20";
            this.smartLabel20.Size = new System.Drawing.Size(53, 55);
            this.smartLabel20.TabIndex = 161;
            this.smartLabel20.Tag = "";
            this.smartLabel20.Text = "T";
            this.smartLabel20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel20.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel15
            // 
            this.smartLabel15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel15.AutoSize = true;
            this.smartLabel15.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel15.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel15.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel15.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel15.BorderColor = System.Drawing.Color.Black;
            this.smartLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel15.ForeColor = System.Drawing.Color.Black;
            this.smartLabel15.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel15.ForeShadowAmount = 1;
            this.smartLabel15.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel15.ForeShadowOffset = 1;
            this.smartLabel15.Location = new System.Drawing.Point(3, 368);
            this.smartLabel15.Name = "smartLabel15";
            this.smartLabel15.Size = new System.Drawing.Size(102, 37);
            this.smartLabel15.TabIndex = 160;
            this.smartLabel15.Text = "(ppm)";
            this.smartLabel15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel15.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel17
            // 
            this.smartLabel17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel17.AutoSize = true;
            this.smartLabel17.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel17.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel17.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel17.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel17.BorderColor = System.Drawing.Color.Black;
            this.smartLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel17.ForeColor = System.Drawing.Color.Black;
            this.smartLabel17.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel17.ForeShadowAmount = 1;
            this.smartLabel17.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel17.ForeShadowOffset = 1;
            this.smartLabel17.Location = new System.Drawing.Point(21, 334);
            this.smartLabel17.Name = "smartLabel17";
            this.smartLabel17.Size = new System.Drawing.Size(66, 37);
            this.smartLabel17.TabIndex = 159;
            this.smartLabel17.Text = "NO";
            this.smartLabel17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel17.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel8
            // 
            this.smartLabel8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel8.AutoSize = true;
            this.smartLabel8.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel8.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.BorderColor = System.Drawing.Color.Black;
            this.smartLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel8.ForeColor = System.Drawing.Color.Black;
            this.smartLabel8.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel8.ForeShadowAmount = 1;
            this.smartLabel8.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel8.ForeShadowOffset = 1;
            this.smartLabel8.Location = new System.Drawing.Point(21, 269);
            this.smartLabel8.Name = "smartLabel8";
            this.smartLabel8.Size = new System.Drawing.Size(67, 37);
            this.smartLabel8.TabIndex = 158;
            this.smartLabel8.Text = "(%)";
            this.smartLabel8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel8.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel12
            // 
            this.smartLabel12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel12.AutoSize = true;
            this.smartLabel12.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel12.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel12.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel12.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel12.BorderColor = System.Drawing.Color.Black;
            this.smartLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel12.ForeColor = System.Drawing.Color.Black;
            this.smartLabel12.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel12.ForeShadowAmount = 1;
            this.smartLabel12.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel12.ForeShadowOffset = 1;
            this.smartLabel12.Location = new System.Drawing.Point(13, 232);
            this.smartLabel12.Name = "smartLabel12";
            this.smartLabel12.Size = new System.Drawing.Size(83, 37);
            this.smartLabel12.TabIndex = 157;
            this.smartLabel12.Text = "CO2";
            this.smartLabel12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel12.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel7
            // 
            this.smartLabel7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel7.AutoSize = true;
            this.smartLabel7.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel7.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.BorderColor = System.Drawing.Color.Black;
            this.smartLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel7.ForeColor = System.Drawing.Color.Black;
            this.smartLabel7.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel7.ForeShadowAmount = 1;
            this.smartLabel7.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel7.ForeShadowOffset = 1;
            this.smartLabel7.Location = new System.Drawing.Point(3, 165);
            this.smartLabel7.Name = "smartLabel7";
            this.smartLabel7.Size = new System.Drawing.Size(102, 37);
            this.smartLabel7.TabIndex = 156;
            this.smartLabel7.Text = "(ppm)";
            this.smartLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel7.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblT
            // 
            this.lblT.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblT.BackColor = System.Drawing.Color.Transparent;
            this.lblT.BackGradientAngle = 150;
            this.lblT.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblT.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblT.BackShadowColor = System.Drawing.Color.Black;
            this.lblT.BorderColor = System.Drawing.Color.Black;
            this.lblT.BorderCornerRadius = 10;
            this.lblT.BorderWidth = 1;
            this.lblT.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblT.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblT.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblT.ForeShadowAmount = 3;
            this.lblT.ForeShadowColor = System.Drawing.Color.Black;
            this.lblT.ForeShadowOffset = 1;
            this.lblT.Location = new System.Drawing.Point(514, 439);
            this.lblT.Name = "lblT";
            this.lblT.Size = new System.Drawing.Size(163, 68);
            this.lblT.TabIndex = 155;
            this.lblT.Text = "0";
            this.lblT.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblT.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblNO
            // 
            this.lblNO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblNO.BackColor = System.Drawing.Color.Transparent;
            this.lblNO.BackGradientAngle = 150;
            this.lblNO.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblNO.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblNO.BackShadowColor = System.Drawing.Color.Black;
            this.lblNO.BorderColor = System.Drawing.Color.Black;
            this.lblNO.BorderCornerRadius = 10;
            this.lblNO.BorderWidth = 1;
            this.lblNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblNO.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblNO.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblNO.ForeShadowAmount = 3;
            this.lblNO.ForeShadowColor = System.Drawing.Color.Black;
            this.lblNO.ForeShadowOffset = 1;
            this.lblNO.Location = new System.Drawing.Point(122, 329);
            this.lblNO.Name = "lblNO";
            this.lblNO.Size = new System.Drawing.Size(163, 68);
            this.lblNO.TabIndex = 154;
            this.lblNO.Text = "0";
            this.lblNO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblNO.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblCO2
            // 
            this.lblCO2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCO2.BackColor = System.Drawing.Color.Transparent;
            this.lblCO2.BackGradientAngle = 150;
            this.lblCO2.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblCO2.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblCO2.BackShadowColor = System.Drawing.Color.Black;
            this.lblCO2.BorderColor = System.Drawing.Color.Black;
            this.lblCO2.BorderCornerRadius = 10;
            this.lblCO2.BorderWidth = 1;
            this.lblCO2.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblCO2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblCO2.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblCO2.ForeShadowAmount = 3;
            this.lblCO2.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCO2.ForeShadowOffset = 1;
            this.lblCO2.Location = new System.Drawing.Point(122, 235);
            this.lblCO2.Name = "lblCO2";
            this.lblCO2.Size = new System.Drawing.Size(163, 68);
            this.lblCO2.TabIndex = 153;
            this.lblCO2.Text = "0.00";
            this.lblCO2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCO2.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblHC
            // 
            this.lblHC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHC.BackColor = System.Drawing.Color.Transparent;
            this.lblHC.BackGradientAngle = 150;
            this.lblHC.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblHC.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblHC.BackShadowColor = System.Drawing.Color.Black;
            this.lblHC.BorderColor = System.Drawing.Color.Black;
            this.lblHC.BorderCornerRadius = 10;
            this.lblHC.BorderWidth = 1;
            this.lblHC.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblHC.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblHC.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblHC.ForeShadowAmount = 3;
            this.lblHC.ForeShadowColor = System.Drawing.Color.Black;
            this.lblHC.ForeShadowOffset = 1;
            this.lblHC.Location = new System.Drawing.Point(122, 141);
            this.lblHC.Name = "lblHC";
            this.lblHC.Size = new System.Drawing.Size(163, 68);
            this.lblHC.TabIndex = 151;
            this.lblHC.Text = "0";
            this.lblHC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblHC.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel1
            // 
            this.smartLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel1.AutoSize = true;
            this.smartLabel1.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel1.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.BorderColor = System.Drawing.Color.Black;
            this.smartLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel1.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel1.ForeShadowAmount = 1;
            this.smartLabel1.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel1.ForeShadowOffset = 1;
            this.smartLabel1.Location = new System.Drawing.Point(415, 126);
            this.smartLabel1.Name = "smartLabel1";
            this.smartLabel1.Size = new System.Drawing.Size(65, 37);
            this.smartLabel1.TabIndex = 188;
            this.smartLabel1.Text = "CO";
            this.smartLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel1.TextBorderColor = System.Drawing.Color.Black;
            // 
            // pnlO2Limits
            // 
            this.pnlO2Limits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlO2Limits.BackColor = System.Drawing.Color.Transparent;
            this.pnlO2Limits.Controls.Add(this.lblO2Min);
            this.pnlO2Limits.Controls.Add(this.lblO2Max);
            this.pnlO2Limits.Controls.Add(this.smartLabel13);
            this.pnlO2Limits.Location = new System.Drawing.Point(680, 226);
            this.pnlO2Limits.Name = "pnlO2Limits";
            this.pnlO2Limits.Size = new System.Drawing.Size(90, 80);
            this.pnlO2Limits.TabIndex = 197;
            // 
            // lblO2Min
            // 
            this.lblO2Min.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblO2Min.BackColor = System.Drawing.Color.Transparent;
            this.lblO2Min.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblO2Min.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblO2Min.BackShadowColor = System.Drawing.Color.Black;
            this.lblO2Min.BorderColor = System.Drawing.Color.Black;
            this.lblO2Min.BorderWidth = 1;
            this.lblO2Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblO2Min.ForeColor = System.Drawing.Color.Red;
            this.lblO2Min.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblO2Min.ForeShadowAmount = 1;
            this.lblO2Min.ForeShadowColor = System.Drawing.Color.Black;
            this.lblO2Min.ForeShadowOffset = 1;
            this.lblO2Min.Location = new System.Drawing.Point(5, 46);
            this.lblO2Min.Name = "lblO2Min";
            this.lblO2Min.Size = new System.Drawing.Size(80, 32);
            this.lblO2Min.TabIndex = 7;
            this.lblO2Min.Text = "0";
            this.lblO2Min.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblO2Min.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblO2Max
            // 
            this.lblO2Max.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblO2Max.BackColor = System.Drawing.Color.Transparent;
            this.lblO2Max.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblO2Max.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblO2Max.BackShadowColor = System.Drawing.Color.Black;
            this.lblO2Max.BorderColor = System.Drawing.Color.Black;
            this.lblO2Max.BorderWidth = 1;
            this.lblO2Max.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblO2Max.ForeColor = System.Drawing.Color.Red;
            this.lblO2Max.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblO2Max.ForeShadowAmount = 1;
            this.lblO2Max.ForeShadowColor = System.Drawing.Color.Black;
            this.lblO2Max.ForeShadowOffset = 1;
            this.lblO2Max.Location = new System.Drawing.Point(5, 15);
            this.lblO2Max.Name = "lblO2Max";
            this.lblO2Max.Size = new System.Drawing.Size(80, 32);
            this.lblO2Max.TabIndex = 9;
            this.lblO2Max.Text = "0";
            this.lblO2Max.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblO2Max.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel13
            // 
            this.smartLabel13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel13.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel13.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel13.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel13.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel13.BorderColor = System.Drawing.Color.Black;
            this.smartLabel13.BorderWidth = 1;
            this.smartLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel13.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel13.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel13.Location = new System.Drawing.Point(5, 2);
            this.smartLabel13.Name = "smartLabel13";
            this.smartLabel13.Size = new System.Drawing.Size(80, 14);
            this.smartLabel13.TabIndex = 8;
            this.smartLabel13.Text = "LIMITS";
            this.smartLabel13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel13.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // pnlCOLimits
            // 
            this.pnlCOLimits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlCOLimits.BackColor = System.Drawing.Color.Transparent;
            this.pnlCOLimits.Controls.Add(this.lblCOMin);
            this.pnlCOLimits.Controls.Add(this.lblCOMax);
            this.pnlCOLimits.Controls.Add(this.smartLabel36);
            this.pnlCOLimits.Location = new System.Drawing.Point(680, 132);
            this.pnlCOLimits.Name = "pnlCOLimits";
            this.pnlCOLimits.Size = new System.Drawing.Size(90, 80);
            this.pnlCOLimits.TabIndex = 196;
            // 
            // lblCOMin
            // 
            this.lblCOMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCOMin.BackColor = System.Drawing.Color.Transparent;
            this.lblCOMin.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCOMin.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCOMin.BackShadowColor = System.Drawing.Color.Black;
            this.lblCOMin.BorderColor = System.Drawing.Color.Black;
            this.lblCOMin.BorderWidth = 1;
            this.lblCOMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblCOMin.ForeColor = System.Drawing.Color.Red;
            this.lblCOMin.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblCOMin.ForeShadowAmount = 1;
            this.lblCOMin.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCOMin.ForeShadowOffset = 1;
            this.lblCOMin.Location = new System.Drawing.Point(5, 46);
            this.lblCOMin.Name = "lblCOMin";
            this.lblCOMin.Size = new System.Drawing.Size(80, 32);
            this.lblCOMin.TabIndex = 7;
            this.lblCOMin.Text = "0";
            this.lblCOMin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCOMin.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblCOMax
            // 
            this.lblCOMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCOMax.BackColor = System.Drawing.Color.Transparent;
            this.lblCOMax.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCOMax.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCOMax.BackShadowColor = System.Drawing.Color.Black;
            this.lblCOMax.BorderColor = System.Drawing.Color.Black;
            this.lblCOMax.BorderWidth = 1;
            this.lblCOMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.lblCOMax.ForeColor = System.Drawing.Color.Red;
            this.lblCOMax.ForeGradientColor2 = System.Drawing.Color.Red;
            this.lblCOMax.ForeShadowAmount = 1;
            this.lblCOMax.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCOMax.ForeShadowOffset = 1;
            this.lblCOMax.Location = new System.Drawing.Point(5, 15);
            this.lblCOMax.Name = "lblCOMax";
            this.lblCOMax.Size = new System.Drawing.Size(80, 32);
            this.lblCOMax.TabIndex = 9;
            this.lblCOMax.Text = "0";
            this.lblCOMax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCOMax.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel36
            // 
            this.smartLabel36.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel36.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel36.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel36.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel36.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel36.BorderColor = System.Drawing.Color.Black;
            this.smartLabel36.BorderWidth = 1;
            this.smartLabel36.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.5F);
            this.smartLabel36.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel36.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel36.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel36.Location = new System.Drawing.Point(5, 2);
            this.smartLabel36.Name = "smartLabel36";
            this.smartLabel36.Size = new System.Drawing.Size(80, 14);
            this.smartLabel36.TabIndex = 8;
            this.smartLabel36.Text = "LIMITS";
            this.smartLabel36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.smartLabel36.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel39
            // 
            this.smartLabel39.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel39.AutoSize = true;
            this.smartLabel39.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel39.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel39.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel39.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel39.BorderColor = System.Drawing.Color.Black;
            this.smartLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel39.ForeColor = System.Drawing.Color.Black;
            this.smartLabel39.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel39.ForeShadowAmount = 1;
            this.smartLabel39.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel39.ForeShadowOffset = 1;
            this.smartLabel39.Location = new System.Drawing.Point(414, 273);
            this.smartLabel39.Name = "smartLabel39";
            this.smartLabel39.Size = new System.Drawing.Size(67, 37);
            this.smartLabel39.TabIndex = 193;
            this.smartLabel39.Text = "(%)";
            this.smartLabel39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel39.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel40
            // 
            this.smartLabel40.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel40.AutoSize = true;
            this.smartLabel40.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel40.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel40.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel40.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel40.BorderColor = System.Drawing.Color.Black;
            this.smartLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel40.ForeColor = System.Drawing.Color.Black;
            this.smartLabel40.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel40.ForeShadowAmount = 1;
            this.smartLabel40.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel40.ForeShadowOffset = 1;
            this.smartLabel40.Location = new System.Drawing.Point(417, 229);
            this.smartLabel40.Name = "smartLabel40";
            this.smartLabel40.Size = new System.Drawing.Size(60, 37);
            this.smartLabel40.TabIndex = 192;
            this.smartLabel40.Text = "O2";
            this.smartLabel40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel40.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel41
            // 
            this.smartLabel41.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel41.AutoSize = true;
            this.smartLabel41.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel41.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel41.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel41.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel41.BorderColor = System.Drawing.Color.Black;
            this.smartLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel41.ForeColor = System.Drawing.Color.Black;
            this.smartLabel41.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel41.ForeShadowAmount = 1;
            this.smartLabel41.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel41.ForeShadowOffset = 1;
            this.smartLabel41.Location = new System.Drawing.Point(414, 172);
            this.smartLabel41.Name = "smartLabel41";
            this.smartLabel41.Size = new System.Drawing.Size(67, 37);
            this.smartLabel41.TabIndex = 191;
            this.smartLabel41.Text = "(%)";
            this.smartLabel41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel41.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblO2
            // 
            this.lblO2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblO2.BackColor = System.Drawing.Color.Transparent;
            this.lblO2.BackGradientAngle = 150;
            this.lblO2.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblO2.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblO2.BackShadowColor = System.Drawing.Color.Black;
            this.lblO2.BorderColor = System.Drawing.Color.Black;
            this.lblO2.BorderCornerRadius = 10;
            this.lblO2.BorderWidth = 1;
            this.lblO2.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblO2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblO2.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblO2.ForeShadowAmount = 3;
            this.lblO2.ForeShadowColor = System.Drawing.Color.Black;
            this.lblO2.ForeShadowOffset = 1;
            this.lblO2.Location = new System.Drawing.Point(514, 232);
            this.lblO2.Name = "lblO2";
            this.lblO2.Size = new System.Drawing.Size(163, 68);
            this.lblO2.TabIndex = 189;
            this.lblO2.Text = "0.00";
            this.lblO2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblO2.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblCO
            // 
            this.lblCO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCO.BackColor = System.Drawing.Color.Transparent;
            this.lblCO.BackGradientAngle = 150;
            this.lblCO.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblCO.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblCO.BackShadowColor = System.Drawing.Color.Black;
            this.lblCO.BorderColor = System.Drawing.Color.Black;
            this.lblCO.BorderCornerRadius = 10;
            this.lblCO.BorderWidth = 1;
            this.lblCO.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblCO.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblCO.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblCO.ForeShadowAmount = 3;
            this.lblCO.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCO.ForeShadowOffset = 1;
            this.lblCO.Location = new System.Drawing.Point(514, 138);
            this.lblCO.Name = "lblCO";
            this.lblCO.Size = new System.Drawing.Size(163, 68);
            this.lblCO.TabIndex = 187;
            this.lblCO.Text = "0.00";
            this.lblCO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCO.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel3
            // 
            this.smartLabel3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel3.AutoSize = true;
            this.smartLabel3.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel3.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.BorderColor = System.Drawing.Color.Black;
            this.smartLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel3.ForeColor = System.Drawing.Color.Black;
            this.smartLabel3.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel3.ForeShadowAmount = 1;
            this.smartLabel3.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel3.ForeShadowOffset = 1;
            this.smartLabel3.Location = new System.Drawing.Point(423, 332);
            this.smartLabel3.Name = "smartLabel3";
            this.smartLabel3.Size = new System.Drawing.Size(48, 55);
            this.smartLabel3.TabIndex = 200;
            this.smartLabel3.Tag = "";
            this.smartLabel3.Text = "λ";
            this.smartLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel3.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblLamda
            // 
            this.lblLamda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblLamda.BackColor = System.Drawing.Color.Transparent;
            this.lblLamda.BackGradientAngle = 150;
            this.lblLamda.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblLamda.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblLamda.BackShadowColor = System.Drawing.Color.Black;
            this.lblLamda.BorderColor = System.Drawing.Color.Black;
            this.lblLamda.BorderCornerRadius = 10;
            this.lblLamda.BorderWidth = 1;
            this.lblLamda.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblLamda.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblLamda.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblLamda.ForeShadowAmount = 3;
            this.lblLamda.ForeShadowColor = System.Drawing.Color.Black;
            this.lblLamda.ForeShadowOffset = 1;
            this.lblLamda.Location = new System.Drawing.Point(514, 323);
            this.lblLamda.Name = "lblLamda";
            this.lblLamda.Size = new System.Drawing.Size(163, 68);
            this.lblLamda.TabIndex = 199;
            this.lblLamda.Text = "0";
            this.lblLamda.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblLamda.TextBorderColor = System.Drawing.Color.Black;
            // 
            // btnMeasure
            // 
            this.btnMeasure.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnMeasure.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMeasure.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMeasure.Location = new System.Drawing.Point(391, 642);
            this.btnMeasure.Name = "btnMeasure";
            this.btnMeasure.Size = new System.Drawing.Size(154, 48);
            this.btnMeasure.TabIndex = 202;
            this.btnMeasure.Text = "MEASURE";
            this.btnMeasure.UseVisualStyleBackColor = true;
            this.btnMeasure.Click += new System.EventHandler(this.btnMeasure_Click);
            // 
            // tmrMeasure
            // 
            this.tmrMeasure.Interval = 1000;
            this.tmrMeasure.Tick += new System.EventHandler(this.tmrMeasGeneral_Tick);
            // 
            // smartLabel2
            // 
            this.smartLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel2.AutoSize = true;
            this.smartLabel2.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel2.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.BorderColor = System.Drawing.Color.Black;
            this.smartLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel2.ForeColor = System.Drawing.Color.Black;
            this.smartLabel2.ForeGradientColor2 = System.Drawing.Color.Black;
            this.smartLabel2.ForeShadowAmount = 1;
            this.smartLabel2.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel2.ForeShadowOffset = 1;
            this.smartLabel2.Location = new System.Drawing.Point(5, 446);
            this.smartLabel2.Name = "smartLabel2";
            this.smartLabel2.Size = new System.Drawing.Size(159, 55);
            this.smartLabel2.TabIndex = 204;
            this.smartLabel2.Tag = "";
            this.smartLabel2.Text = "A.F.R.";
            this.smartLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.smartLabel2.TextBorderColor = System.Drawing.Color.Black;
            // 
            // lblAFR
            // 
            this.lblAFR.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblAFR.BackColor = System.Drawing.Color.Transparent;
            this.lblAFR.BackGradientAngle = 150;
            this.lblAFR.BackGredientColor1 = System.Drawing.Color.ForestGreen;
            this.lblAFR.BackGredientColor2 = System.Drawing.Color.LimeGreen;
            this.lblAFR.BackShadowColor = System.Drawing.Color.Black;
            this.lblAFR.BorderColor = System.Drawing.Color.Black;
            this.lblAFR.BorderCornerRadius = 10;
            this.lblAFR.BorderWidth = 1;
            this.lblAFR.Font = new System.Drawing.Font("Microsoft Sans Serif", 39.5F, System.Drawing.FontStyle.Bold);
            this.lblAFR.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblAFR.ForeGradientColor2 = System.Drawing.Color.WhiteSmoke;
            this.lblAFR.ForeShadowAmount = 3;
            this.lblAFR.ForeShadowColor = System.Drawing.Color.Black;
            this.lblAFR.ForeShadowOffset = 1;
            this.lblAFR.Location = new System.Drawing.Point(170, 439);
            this.lblAFR.Name = "lblAFR";
            this.lblAFR.Size = new System.Drawing.Size(240, 68);
            this.lblAFR.TabIndex = 203;
            this.lblAFR.Text = "0.00 : 1";
            this.lblAFR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblAFR.TextBorderColor = System.Drawing.Color.Black;
            // 
            // smartLabel34
            // 
            this.smartLabel34.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel34.AutoSize = true;
            this.smartLabel34.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel34.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel34.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel34.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel34.BorderColor = System.Drawing.Color.Black;
            this.smartLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel34.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel34.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel34.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel34.Location = new System.Drawing.Point(48, 558);
            this.smartLabel34.Name = "smartLabel34";
            this.smartLabel34.Size = new System.Drawing.Size(127, 20);
            this.smartLabel34.TabIndex = 206;
            this.smartLabel34.Text = "Test Conditions: ";
            this.smartLabel34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel34.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // cmbxCaption
            // 
            this.cmbxCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cmbxCaption.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbxCaption.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbxCaption.FormattingEnabled = true;
            this.cmbxCaption.Items.AddRange(new object[] {
            "",
            "With AC Load",
            "Without AC Load",
            "With Acceleration",
            "Before Adjustment",
            "After Adjustment"});
            this.cmbxCaption.Location = new System.Drawing.Point(177, 555);
            this.cmbxCaption.Name = "cmbxCaption";
            this.cmbxCaption.Size = new System.Drawing.Size(395, 28);
            this.cmbxCaption.TabIndex = 205;
            // 
            // smartLabel35
            // 
            this.smartLabel35.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel35.AutoSize = true;
            this.smartLabel35.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel35.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel35.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel35.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel35.BorderColor = System.Drawing.Color.Black;
            this.smartLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel35.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel35.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel35.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel35.Location = new System.Drawing.Point(920, 601);
            this.smartLabel35.Name = "smartLabel35";
            this.smartLabel35.Size = new System.Drawing.Size(37, 25);
            this.smartLabel35.TabIndex = 218;
            this.smartLabel35.Text = "- 0";
            this.smartLabel35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel35.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel33
            // 
            this.smartLabel33.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel33.AutoSize = true;
            this.smartLabel33.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel33.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel33.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel33.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel33.BorderColor = System.Drawing.Color.Black;
            this.smartLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel33.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel33.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel33.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel33.Location = new System.Drawing.Point(920, 178);
            this.smartLabel33.Name = "smartLabel33";
            this.smartLabel33.Size = new System.Drawing.Size(73, 25);
            this.smartLabel33.TabIndex = 217;
            this.smartLabel33.Text = "- 4500";
            this.smartLabel33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel33.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel32
            // 
            this.smartLabel32.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel32.AutoSize = true;
            this.smartLabel32.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel32.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel32.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel32.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel32.BorderColor = System.Drawing.Color.Black;
            this.smartLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel32.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel32.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel32.Location = new System.Drawing.Point(920, 554);
            this.smartLabel32.Name = "smartLabel32";
            this.smartLabel32.Size = new System.Drawing.Size(61, 25);
            this.smartLabel32.TabIndex = 216;
            this.smartLabel32.Text = "- 500";
            this.smartLabel32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel32.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel31
            // 
            this.smartLabel31.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel31.AutoSize = true;
            this.smartLabel31.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel31.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel31.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel31.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel31.BorderColor = System.Drawing.Color.Black;
            this.smartLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel31.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel31.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel31.Location = new System.Drawing.Point(920, 507);
            this.smartLabel31.Name = "smartLabel31";
            this.smartLabel31.Size = new System.Drawing.Size(73, 25);
            this.smartLabel31.TabIndex = 215;
            this.smartLabel31.Text = "- 1000";
            this.smartLabel31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel31.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel30
            // 
            this.smartLabel30.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel30.AutoSize = true;
            this.smartLabel30.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel30.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel30.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel30.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel30.BorderColor = System.Drawing.Color.Black;
            this.smartLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel30.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel30.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel30.Location = new System.Drawing.Point(920, 460);
            this.smartLabel30.Name = "smartLabel30";
            this.smartLabel30.Size = new System.Drawing.Size(73, 25);
            this.smartLabel30.TabIndex = 214;
            this.smartLabel30.Text = "- 1500";
            this.smartLabel30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel30.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel29
            // 
            this.smartLabel29.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel29.AutoSize = true;
            this.smartLabel29.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel29.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel29.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel29.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel29.BorderColor = System.Drawing.Color.Black;
            this.smartLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel29.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel29.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel29.Location = new System.Drawing.Point(920, 413);
            this.smartLabel29.Name = "smartLabel29";
            this.smartLabel29.Size = new System.Drawing.Size(73, 25);
            this.smartLabel29.TabIndex = 213;
            this.smartLabel29.Text = "- 2000";
            this.smartLabel29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel29.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel28
            // 
            this.smartLabel28.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel28.AutoSize = true;
            this.smartLabel28.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel28.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel28.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel28.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel28.BorderColor = System.Drawing.Color.Black;
            this.smartLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel28.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel28.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel28.Location = new System.Drawing.Point(920, 366);
            this.smartLabel28.Name = "smartLabel28";
            this.smartLabel28.Size = new System.Drawing.Size(73, 25);
            this.smartLabel28.TabIndex = 212;
            this.smartLabel28.Text = "- 2500";
            this.smartLabel28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel28.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel25
            // 
            this.smartLabel25.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel25.AutoSize = true;
            this.smartLabel25.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel25.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel25.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel25.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel25.BorderColor = System.Drawing.Color.Black;
            this.smartLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel25.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel25.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel25.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel25.Location = new System.Drawing.Point(920, 319);
            this.smartLabel25.Name = "smartLabel25";
            this.smartLabel25.Size = new System.Drawing.Size(73, 25);
            this.smartLabel25.TabIndex = 211;
            this.smartLabel25.Text = "- 3000";
            this.smartLabel25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel25.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel24
            // 
            this.smartLabel24.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel24.AutoSize = true;
            this.smartLabel24.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel24.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel24.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel24.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel24.BorderColor = System.Drawing.Color.Black;
            this.smartLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel24.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel24.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel24.Location = new System.Drawing.Point(920, 272);
            this.smartLabel24.Name = "smartLabel24";
            this.smartLabel24.Size = new System.Drawing.Size(73, 25);
            this.smartLabel24.TabIndex = 210;
            this.smartLabel24.Text = "- 3500";
            this.smartLabel24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel24.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel21
            // 
            this.smartLabel21.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel21.AutoSize = true;
            this.smartLabel21.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel21.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel21.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel21.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel21.BorderColor = System.Drawing.Color.Black;
            this.smartLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel21.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel21.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel21.Location = new System.Drawing.Point(920, 225);
            this.smartLabel21.Name = "smartLabel21";
            this.smartLabel21.Size = new System.Drawing.Size(73, 25);
            this.smartLabel21.TabIndex = 209;
            this.smartLabel21.Text = "- 4000";
            this.smartLabel21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel21.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // smartLabel18
            // 
            this.smartLabel18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.smartLabel18.AutoSize = true;
            this.smartLabel18.BackColor = System.Drawing.Color.Transparent;
            this.smartLabel18.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.smartLabel18.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.smartLabel18.BackShadowColor = System.Drawing.Color.Black;
            this.smartLabel18.BorderColor = System.Drawing.Color.Black;
            this.smartLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smartLabel18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.smartLabel18.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.smartLabel18.ForeShadowColor = System.Drawing.Color.Black;
            this.smartLabel18.Location = new System.Drawing.Point(920, 131);
            this.smartLabel18.Name = "smartLabel18";
            this.smartLabel18.Size = new System.Drawing.Size(73, 25);
            this.smartLabel18.TabIndex = 208;
            this.smartLabel18.Text = "- 5000";
            this.smartLabel18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.smartLabel18.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.AutoSize = true;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(28, 642);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(137, 48);
            this.btnClear.TabIndex = 219;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // pbrSpped
            // 
            this.pbrSpped.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbrSpped.BackColor = System.Drawing.Color.Transparent;
            this.pbrSpped.BorderColor = System.Drawing.Color.Black;
            this.pbrSpped.BorderDisplay = true;
            this.pbrSpped.BorderSize = 1;
            this.pbrSpped.Color1 = System.Drawing.Color.DeepSkyBlue;
            this.pbrSpped.Color2 = System.Drawing.Color.Blue;
            this.pbrSpped.FillingMethod = AdvancedProgressBarControl.AdvancedProgressBar.FillingType.BottomToTop;
            this.pbrSpped.Font = new System.Drawing.Font("Tahoma", 8F);
            this.pbrSpped.ForeColor = System.Drawing.Color.Green;
            this.pbrSpped.GradientMethod = AdvancedProgressBarControl.AdvancedProgressBar.FillDirection.Vertical;
            this.pbrSpped.Location = new System.Drawing.Point(835, 139);
            this.pbrSpped.Name = "pbrSpped";
            this.pbrSpped.PercentDisplay = false;
            this.pbrSpped.Position = 0;
            this.pbrSpped.ProgressBarBackColor = System.Drawing.Color.Gainsboro;
            this.pbrSpped.Shape = AdvancedProgressBarControl.AdvancedProgressBar.ProgressBarShape.RightTriangle;
            this.pbrSpped.Size = new System.Drawing.Size(76, 480);
            this.pbrSpped.TabIndex = 220;
            // 
            // frmEmissionRealTimeTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1018, 740);
            this.Controls.Add(this.pbrSpped);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.smartLabel35);
            this.Controls.Add(this.smartLabel33);
            this.Controls.Add(this.smartLabel32);
            this.Controls.Add(this.smartLabel31);
            this.Controls.Add(this.smartLabel30);
            this.Controls.Add(this.smartLabel29);
            this.Controls.Add(this.smartLabel28);
            this.Controls.Add(this.smartLabel25);
            this.Controls.Add(this.smartLabel24);
            this.Controls.Add(this.smartLabel21);
            this.Controls.Add(this.smartLabel18);
            this.Controls.Add(this.smartLabel34);
            this.Controls.Add(this.cmbxCaption);
            this.Controls.Add(this.smartLabel2);
            this.Controls.Add(this.lblAFR);
            this.Controls.Add(this.btnMeasure);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.smartLabel3);
            this.Controls.Add(this.lblLamda);
            this.Controls.Add(this.smartLabel1);
            this.Controls.Add(this.pnlO2Limits);
            this.Controls.Add(this.pnlCOLimits);
            this.Controls.Add(this.smartLabel39);
            this.Controls.Add(this.smartLabel40);
            this.Controls.Add(this.smartLabel41);
            this.Controls.Add(this.lblO2);
            this.Controls.Add(this.lblCO);
            this.Controls.Add(this.btnCapture);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.smartLabel10);
            this.Controls.Add(this.smartLabel22);
            this.Controls.Add(this.lblSpped);
            this.Controls.Add(this.smartLabel9);
            this.Controls.Add(this.pnlNOLimits);
            this.Controls.Add(this.pnlCO2Limits);
            this.Controls.Add(this.pnlHCLimits);
            this.Controls.Add(this.smartLabel19);
            this.Controls.Add(this.smartLabel20);
            this.Controls.Add(this.smartLabel15);
            this.Controls.Add(this.smartLabel17);
            this.Controls.Add(this.smartLabel8);
            this.Controls.Add(this.smartLabel12);
            this.Controls.Add(this.smartLabel7);
            this.Controls.Add(this.lblT);
            this.Controls.Add(this.lblNO);
            this.Controls.Add(this.lblCO2);
            this.Controls.Add(this.lblHC);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 726);
            this.Name = "frmEmissionRealTimeTest";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.pnlNOLimits.ResumeLayout(false);
            this.pnlCO2Limits.ResumeLayout(false);
            this.pnlHCLimits.ResumeLayout(false);
            this.pnlO2Limits.ResumeLayout(false);
            this.pnlCOLimits.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrTime;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCapture;
        private InterfaceLab.WinForm.Controls.SmartLabel lblDate;
        private InterfaceLab.WinForm.Controls.SmartLabel lblTime;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel10;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel22;
        private InterfaceLab.WinForm.Controls.SmartLabel lblSpped;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel9;
        private System.Windows.Forms.Panel pnlNOLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblNOMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblNOMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel23;
        private System.Windows.Forms.Panel pnlCO2Limits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCO2Min;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCO2Max;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel16;
        private System.Windows.Forms.Panel pnlHCLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblHCMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblHCMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel6;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel19;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel20;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel15;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel17;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel8;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel12;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel7;
        private InterfaceLab.WinForm.Controls.SmartLabel lblT;
        private InterfaceLab.WinForm.Controls.SmartLabel lblNO;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCO2;
        private InterfaceLab.WinForm.Controls.SmartLabel lblHC;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel1;
        private System.Windows.Forms.Panel pnlO2Limits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblO2Min;
        private InterfaceLab.WinForm.Controls.SmartLabel lblO2Max;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel13;
        private System.Windows.Forms.Panel pnlCOLimits;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCOMin;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCOMax;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel36;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel39;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel40;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel41;
        private InterfaceLab.WinForm.Controls.SmartLabel lblO2;
        private InterfaceLab.WinForm.Controls.SmartLabel lblCO;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel3;
        private InterfaceLab.WinForm.Controls.SmartLabel lblLamda;
        private System.Windows.Forms.Button btnMeasure;
        private System.Windows.Forms.Timer tmrMeasure;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel2;
        private InterfaceLab.WinForm.Controls.SmartLabel lblAFR;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel34;
        private System.Windows.Forms.ComboBox cmbxCaption;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel35;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel33;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel32;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel31;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel30;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel29;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel28;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel25;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel24;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel21;
        private InterfaceLab.WinForm.Controls.SmartLabel smartLabel18;
        private System.Windows.Forms.Button btnClear;
        private AdvancedProgressBarControl.AdvancedProgressBar pbrSpped;
    }
}