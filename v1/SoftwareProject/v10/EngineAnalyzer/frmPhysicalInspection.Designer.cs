﻿namespace EngineAnalyzer
{
    partial class frmPhysicalInspection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPhysicalInspection));
            this.lblBEL = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTC = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTP = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblCS = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblHL = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblPL = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblH = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblA = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblBO = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblEO = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.lblTO = new InterfaceLab.WinForm.Controls.SmartLabel(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.cbxPL_OK = new System.Windows.Forms.CheckBox();
            this.cbxTO_OK = new System.Windows.Forms.CheckBox();
            this.cbxTO_NOK = new System.Windows.Forms.CheckBox();
            this.cbxEO_OK = new System.Windows.Forms.CheckBox();
            this.cbxEO_NOK = new System.Windows.Forms.CheckBox();
            this.cbxBO_OK = new System.Windows.Forms.CheckBox();
            this.cbxBO_NOK = new System.Windows.Forms.CheckBox();
            this.cbxA_OK = new System.Windows.Forms.CheckBox();
            this.cbxA_NOK = new System.Windows.Forms.CheckBox();
            this.cbxH_OK = new System.Windows.Forms.CheckBox();
            this.cbxH_NOK = new System.Windows.Forms.CheckBox();
            this.cbxPL_NOK = new System.Windows.Forms.CheckBox();
            this.cbxHL_OK = new System.Windows.Forms.CheckBox();
            this.cbxHL_NOK = new System.Windows.Forms.CheckBox();
            this.cbxCS_OK = new System.Windows.Forms.CheckBox();
            this.cbxCS_NOK = new System.Windows.Forms.CheckBox();
            this.cbxTP_OK = new System.Windows.Forms.CheckBox();
            this.cbxTP_NOK = new System.Windows.Forms.CheckBox();
            this.cbxTC_OK = new System.Windows.Forms.CheckBox();
            this.cbxTC_NOK = new System.Windows.Forms.CheckBox();
            this.cbxBEL_OK = new System.Windows.Forms.CheckBox();
            this.cbxBEL_NOK = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblBEL
            // 
            this.lblBEL.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblBEL.AutoSize = true;
            this.lblBEL.BackColor = System.Drawing.Color.Transparent;
            this.lblBEL.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblBEL.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblBEL.BackShadowColor = System.Drawing.Color.Black;
            this.lblBEL.BorderColor = System.Drawing.Color.Black;
            this.lblBEL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBEL.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblBEL.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblBEL.ForeShadowColor = System.Drawing.Color.Black;
            this.lblBEL.Location = new System.Drawing.Point(24, 38);
            this.lblBEL.Name = "lblBEL";
            this.lblBEL.Size = new System.Drawing.Size(271, 25);
            this.lblBEL.TabIndex = 0;
            this.lblBEL.Text = "Battery Electrolyte & Load    ";
            this.lblBEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBEL.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblTC
            // 
            this.lblTC.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTC.AutoSize = true;
            this.lblTC.BackColor = System.Drawing.Color.Transparent;
            this.lblTC.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTC.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTC.BackShadowColor = System.Drawing.Color.Black;
            this.lblTC.BorderColor = System.Drawing.Color.Black;
            this.lblTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTC.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTC.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblTC.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTC.Location = new System.Drawing.Point(131, 74);
            this.lblTC.Name = "lblTC";
            this.lblTC.Size = new System.Drawing.Size(164, 25);
            this.lblTC.TabIndex = 1;
            this.lblTC.Text = "Tyre Condition  ";
            this.lblTC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTC.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblTP
            // 
            this.lblTP.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTP.AutoSize = true;
            this.lblTP.BackColor = System.Drawing.Color.Transparent;
            this.lblTP.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTP.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTP.BackShadowColor = System.Drawing.Color.Black;
            this.lblTP.BorderColor = System.Drawing.Color.Black;
            this.lblTP.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTP.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTP.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblTP.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTP.Location = new System.Drawing.Point(136, 110);
            this.lblTP.Name = "lblTP";
            this.lblTP.Size = new System.Drawing.Size(159, 25);
            this.lblTP.TabIndex = 2;
            this.lblTP.Text = "Tyre Pressure  ";
            this.lblTP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTP.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblCS
            // 
            this.lblCS.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCS.AutoSize = true;
            this.lblCS.BackColor = System.Drawing.Color.Transparent;
            this.lblCS.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblCS.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblCS.BackShadowColor = System.Drawing.Color.Black;
            this.lblCS.BorderColor = System.Drawing.Color.Black;
            this.lblCS.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCS.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblCS.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblCS.ForeShadowColor = System.Drawing.Color.Black;
            this.lblCS.Location = new System.Drawing.Point(121, 146);
            this.lblCS.Name = "lblCS";
            this.lblCS.Size = new System.Drawing.Size(174, 25);
            this.lblCS.TabIndex = 3;
            this.lblCS.Text = "Cooling System  ";
            this.lblCS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCS.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblHL
            // 
            this.lblHL.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblHL.AutoSize = true;
            this.lblHL.BackColor = System.Drawing.Color.Transparent;
            this.lblHL.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblHL.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblHL.BackShadowColor = System.Drawing.Color.Black;
            this.lblHL.BorderColor = System.Drawing.Color.Black;
            this.lblHL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHL.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblHL.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblHL.ForeShadowColor = System.Drawing.Color.Black;
            this.lblHL.Location = new System.Drawing.Point(167, 182);
            this.lblHL.Name = "lblHL";
            this.lblHL.Size = new System.Drawing.Size(128, 25);
            this.lblHL.TabIndex = 4;
            this.lblHL.Text = "Head Light  ";
            this.lblHL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblHL.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblPL
            // 
            this.lblPL.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPL.AutoSize = true;
            this.lblPL.BackColor = System.Drawing.Color.Transparent;
            this.lblPL.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblPL.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblPL.BackShadowColor = System.Drawing.Color.Black;
            this.lblPL.BorderColor = System.Drawing.Color.Black;
            this.lblPL.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPL.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblPL.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblPL.ForeShadowColor = System.Drawing.Color.Black;
            this.lblPL.Location = new System.Drawing.Point(145, 218);
            this.lblPL.Name = "lblPL";
            this.lblPL.Size = new System.Drawing.Size(150, 25);
            this.lblPL.TabIndex = 5;
            this.lblPL.Text = "Parking Light  ";
            this.lblPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPL.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblH
            // 
            this.lblH.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblH.AutoSize = true;
            this.lblH.BackColor = System.Drawing.Color.Transparent;
            this.lblH.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblH.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblH.BackShadowColor = System.Drawing.Color.Black;
            this.lblH.BorderColor = System.Drawing.Color.Black;
            this.lblH.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblH.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblH.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblH.ForeShadowColor = System.Drawing.Color.Black;
            this.lblH.Location = new System.Drawing.Point(225, 254);
            this.lblH.Name = "lblH";
            this.lblH.Size = new System.Drawing.Size(70, 25);
            this.lblH.TabIndex = 6;
            this.lblH.Text = "Horn  ";
            this.lblH.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblH.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblA
            // 
            this.lblA.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblA.AutoSize = true;
            this.lblA.BackColor = System.Drawing.Color.Transparent;
            this.lblA.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblA.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblA.BackShadowColor = System.Drawing.Color.Black;
            this.lblA.BorderColor = System.Drawing.Color.Black;
            this.lblA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblA.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblA.ForeShadowColor = System.Drawing.Color.Black;
            this.lblA.Location = new System.Drawing.Point(178, 290);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(117, 25);
            this.lblA.TabIndex = 7;
            this.lblA.Text = "Alternator  ";
            this.lblA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblA.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblBO
            // 
            this.lblBO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblBO.AutoSize = true;
            this.lblBO.BackColor = System.Drawing.Color.Transparent;
            this.lblBO.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblBO.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblBO.BackShadowColor = System.Drawing.Color.Black;
            this.lblBO.BorderColor = System.Drawing.Color.Black;
            this.lblBO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblBO.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblBO.ForeShadowColor = System.Drawing.Color.Black;
            this.lblBO.Location = new System.Drawing.Point(183, 326);
            this.lblBO.Name = "lblBO";
            this.lblBO.Size = new System.Drawing.Size(112, 25);
            this.lblBO.TabIndex = 8;
            this.lblBO.Text = "Brake Oil  ";
            this.lblBO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblBO.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblEO
            // 
            this.lblEO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblEO.AutoSize = true;
            this.lblEO.BackColor = System.Drawing.Color.Transparent;
            this.lblEO.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblEO.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblEO.BackShadowColor = System.Drawing.Color.Black;
            this.lblEO.BorderColor = System.Drawing.Color.Black;
            this.lblEO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblEO.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblEO.ForeShadowColor = System.Drawing.Color.Black;
            this.lblEO.Location = new System.Drawing.Point(172, 362);
            this.lblEO.Name = "lblEO";
            this.lblEO.Size = new System.Drawing.Size(123, 25);
            this.lblEO.TabIndex = 9;
            this.lblEO.Text = "Engine Oil  ";
            this.lblEO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblEO.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // lblTO
            // 
            this.lblTO.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTO.AutoSize = true;
            this.lblTO.BackColor = System.Drawing.Color.Transparent;
            this.lblTO.BackGredientColor1 = System.Drawing.Color.Transparent;
            this.lblTO.BackGredientColor2 = System.Drawing.Color.Transparent;
            this.lblTO.BackShadowColor = System.Drawing.Color.Black;
            this.lblTO.BorderColor = System.Drawing.Color.Black;
            this.lblTO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTO.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblTO.ForeGradientColor2 = System.Drawing.SystemColors.ControlText;
            this.lblTO.ForeShadowColor = System.Drawing.Color.Black;
            this.lblTO.Location = new System.Drawing.Point(111, 398);
            this.lblTO.Name = "lblTO";
            this.lblTO.Size = new System.Drawing.Size(184, 25);
            this.lblTO.TabIndex = 10;
            this.lblTO.Text = "Transmission Oil  ";
            this.lblTO.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblTO.TextBorderColor = System.Drawing.SystemColors.ControlLight;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(359, 438);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(121, 39);
            this.btnExit.TabIndex = 37;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(216, 438);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(121, 39);
            this.btnClear.TabIndex = 38;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // cbxPL_OK
            // 
            this.cbxPL_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxPL_OK.AutoSize = true;
            this.cbxPL_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.PL_OK;
            this.cbxPL_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "PL_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxPL_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPL_OK.Location = new System.Drawing.Point(310, 216);
            this.cbxPL_OK.Name = "cbxPL_OK";
            this.cbxPL_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxPL_OK.TabIndex = 24;
            this.cbxPL_OK.Text = "OK";
            this.cbxPL_OK.UseVisualStyleBackColor = true;
            this.cbxPL_OK.CheckedChanged += new System.EventHandler(this.cbxPL_OK_CheckedChanged);
            // 
            // cbxTO_OK
            // 
            this.cbxTO_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxTO_OK.AutoSize = true;
            this.cbxTO_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.TO_OK;
            this.cbxTO_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "TO_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxTO_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTO_OK.Location = new System.Drawing.Point(310, 396);
            this.cbxTO_OK.Name = "cbxTO_OK";
            this.cbxTO_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxTO_OK.TabIndex = 36;
            this.cbxTO_OK.Text = "OK";
            this.cbxTO_OK.UseVisualStyleBackColor = true;
            this.cbxTO_OK.CheckedChanged += new System.EventHandler(this.cbxTO_OK_CheckedChanged);
            // 
            // cbxTO_NOK
            // 
            this.cbxTO_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxTO_NOK.AutoSize = true;
            this.cbxTO_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.TO_NOK;
            this.cbxTO_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "TO_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxTO_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTO_NOK.Location = new System.Drawing.Point(380, 396);
            this.cbxTO_NOK.Name = "cbxTO_NOK";
            this.cbxTO_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxTO_NOK.TabIndex = 35;
            this.cbxTO_NOK.Text = "Not OK";
            this.cbxTO_NOK.UseVisualStyleBackColor = true;
            this.cbxTO_NOK.CheckedChanged += new System.EventHandler(this.cbxTO_NOK_CheckedChanged);
            // 
            // cbxEO_OK
            // 
            this.cbxEO_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxEO_OK.AutoSize = true;
            this.cbxEO_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.EO_OK;
            this.cbxEO_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "EO_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxEO_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEO_OK.Location = new System.Drawing.Point(310, 360);
            this.cbxEO_OK.Name = "cbxEO_OK";
            this.cbxEO_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxEO_OK.TabIndex = 34;
            this.cbxEO_OK.Text = "OK";
            this.cbxEO_OK.UseVisualStyleBackColor = true;
            this.cbxEO_OK.CheckedChanged += new System.EventHandler(this.cbxEO_OK_CheckedChanged);
            // 
            // cbxEO_NOK
            // 
            this.cbxEO_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxEO_NOK.AutoSize = true;
            this.cbxEO_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.EO_NOK;
            this.cbxEO_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "EO_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxEO_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxEO_NOK.Location = new System.Drawing.Point(380, 360);
            this.cbxEO_NOK.Name = "cbxEO_NOK";
            this.cbxEO_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxEO_NOK.TabIndex = 33;
            this.cbxEO_NOK.Text = "Not OK";
            this.cbxEO_NOK.UseVisualStyleBackColor = true;
            this.cbxEO_NOK.CheckedChanged += new System.EventHandler(this.cbxEO_NOK_CheckedChanged);
            // 
            // cbxBO_OK
            // 
            this.cbxBO_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxBO_OK.AutoSize = true;
            this.cbxBO_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.BO_OK;
            this.cbxBO_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "BO_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxBO_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxBO_OK.Location = new System.Drawing.Point(310, 324);
            this.cbxBO_OK.Name = "cbxBO_OK";
            this.cbxBO_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxBO_OK.TabIndex = 32;
            this.cbxBO_OK.Text = "OK";
            this.cbxBO_OK.UseVisualStyleBackColor = true;
            this.cbxBO_OK.CheckedChanged += new System.EventHandler(this.cbxBO_OK_CheckedChanged);
            // 
            // cbxBO_NOK
            // 
            this.cbxBO_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxBO_NOK.AutoSize = true;
            this.cbxBO_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.BO_NOK;
            this.cbxBO_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "BO_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxBO_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxBO_NOK.Location = new System.Drawing.Point(380, 324);
            this.cbxBO_NOK.Name = "cbxBO_NOK";
            this.cbxBO_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxBO_NOK.TabIndex = 31;
            this.cbxBO_NOK.Text = "Not OK";
            this.cbxBO_NOK.UseVisualStyleBackColor = true;
            this.cbxBO_NOK.CheckedChanged += new System.EventHandler(this.cbxBO_NOK_CheckedChanged);
            // 
            // cbxA_OK
            // 
            this.cbxA_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxA_OK.AutoSize = true;
            this.cbxA_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.A_OK;
            this.cbxA_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "A_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxA_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxA_OK.Location = new System.Drawing.Point(310, 288);
            this.cbxA_OK.Name = "cbxA_OK";
            this.cbxA_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxA_OK.TabIndex = 30;
            this.cbxA_OK.Text = "OK";
            this.cbxA_OK.UseVisualStyleBackColor = true;
            this.cbxA_OK.CheckedChanged += new System.EventHandler(this.cbxA_OK_CheckedChanged);
            // 
            // cbxA_NOK
            // 
            this.cbxA_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxA_NOK.AutoSize = true;
            this.cbxA_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.A_NOK;
            this.cbxA_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "A_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxA_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxA_NOK.Location = new System.Drawing.Point(380, 288);
            this.cbxA_NOK.Name = "cbxA_NOK";
            this.cbxA_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxA_NOK.TabIndex = 29;
            this.cbxA_NOK.Text = "Not OK";
            this.cbxA_NOK.UseVisualStyleBackColor = true;
            this.cbxA_NOK.CheckedChanged += new System.EventHandler(this.cbxA_NOK_CheckedChanged);
            // 
            // cbxH_OK
            // 
            this.cbxH_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxH_OK.AutoSize = true;
            this.cbxH_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.H_OK;
            this.cbxH_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "H_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxH_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxH_OK.Location = new System.Drawing.Point(310, 252);
            this.cbxH_OK.Name = "cbxH_OK";
            this.cbxH_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxH_OK.TabIndex = 28;
            this.cbxH_OK.Text = "OK";
            this.cbxH_OK.UseVisualStyleBackColor = true;
            this.cbxH_OK.CheckedChanged += new System.EventHandler(this.cbxH_OK_CheckedChanged);
            // 
            // cbxH_NOK
            // 
            this.cbxH_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxH_NOK.AutoSize = true;
            this.cbxH_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.H_NOK;
            this.cbxH_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "H_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxH_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxH_NOK.Location = new System.Drawing.Point(380, 252);
            this.cbxH_NOK.Name = "cbxH_NOK";
            this.cbxH_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxH_NOK.TabIndex = 27;
            this.cbxH_NOK.Text = "Not OK";
            this.cbxH_NOK.UseVisualStyleBackColor = true;
            this.cbxH_NOK.CheckedChanged += new System.EventHandler(this.cbxH_NOK_CheckedChanged);
            // 
            // cbxPL_NOK
            // 
            this.cbxPL_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxPL_NOK.AutoSize = true;
            this.cbxPL_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.PL_NOK;
            this.cbxPL_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "PL_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxPL_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxPL_NOK.Location = new System.Drawing.Point(380, 216);
            this.cbxPL_NOK.Name = "cbxPL_NOK";
            this.cbxPL_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxPL_NOK.TabIndex = 23;
            this.cbxPL_NOK.Text = "Not OK";
            this.cbxPL_NOK.UseVisualStyleBackColor = true;
            this.cbxPL_NOK.CheckedChanged += new System.EventHandler(this.cbxPL_NOK_CheckedChanged);
            // 
            // cbxHL_OK
            // 
            this.cbxHL_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxHL_OK.AutoSize = true;
            this.cbxHL_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.HL_OK;
            this.cbxHL_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "HL_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxHL_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxHL_OK.Location = new System.Drawing.Point(310, 180);
            this.cbxHL_OK.Name = "cbxHL_OK";
            this.cbxHL_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxHL_OK.TabIndex = 22;
            this.cbxHL_OK.Text = "OK";
            this.cbxHL_OK.UseVisualStyleBackColor = true;
            this.cbxHL_OK.CheckedChanged += new System.EventHandler(this.cbxHL_OK_CheckedChanged);
            // 
            // cbxHL_NOK
            // 
            this.cbxHL_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxHL_NOK.AutoSize = true;
            this.cbxHL_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.HL_NOK;
            this.cbxHL_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "HL_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxHL_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxHL_NOK.Location = new System.Drawing.Point(380, 180);
            this.cbxHL_NOK.Name = "cbxHL_NOK";
            this.cbxHL_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxHL_NOK.TabIndex = 21;
            this.cbxHL_NOK.Text = "Not OK";
            this.cbxHL_NOK.UseVisualStyleBackColor = true;
            this.cbxHL_NOK.CheckedChanged += new System.EventHandler(this.cbxHL_NOK_CheckedChanged);
            // 
            // cbxCS_OK
            // 
            this.cbxCS_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxCS_OK.AutoSize = true;
            this.cbxCS_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.CS_OK;
            this.cbxCS_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "CS_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCS_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCS_OK.Location = new System.Drawing.Point(310, 144);
            this.cbxCS_OK.Name = "cbxCS_OK";
            this.cbxCS_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxCS_OK.TabIndex = 20;
            this.cbxCS_OK.Text = "OK";
            this.cbxCS_OK.UseVisualStyleBackColor = true;
            this.cbxCS_OK.CheckedChanged += new System.EventHandler(this.cbxCS_OK_CheckedChanged);
            // 
            // cbxCS_NOK
            // 
            this.cbxCS_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxCS_NOK.AutoSize = true;
            this.cbxCS_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.CS_NOK;
            this.cbxCS_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "CS_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxCS_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxCS_NOK.Location = new System.Drawing.Point(380, 144);
            this.cbxCS_NOK.Name = "cbxCS_NOK";
            this.cbxCS_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxCS_NOK.TabIndex = 19;
            this.cbxCS_NOK.Text = "Not OK";
            this.cbxCS_NOK.UseVisualStyleBackColor = true;
            this.cbxCS_NOK.CheckedChanged += new System.EventHandler(this.cbxCS_NOK_CheckedChanged);
            // 
            // cbxTP_OK
            // 
            this.cbxTP_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxTP_OK.AutoSize = true;
            this.cbxTP_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.TP_OK;
            this.cbxTP_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "TP_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxTP_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTP_OK.Location = new System.Drawing.Point(310, 108);
            this.cbxTP_OK.Name = "cbxTP_OK";
            this.cbxTP_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxTP_OK.TabIndex = 18;
            this.cbxTP_OK.Text = "OK";
            this.cbxTP_OK.UseVisualStyleBackColor = true;
            this.cbxTP_OK.CheckedChanged += new System.EventHandler(this.cbxTP_OK_CheckedChanged);
            // 
            // cbxTP_NOK
            // 
            this.cbxTP_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxTP_NOK.AutoSize = true;
            this.cbxTP_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.TP_NOK;
            this.cbxTP_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "TP_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxTP_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTP_NOK.Location = new System.Drawing.Point(380, 108);
            this.cbxTP_NOK.Name = "cbxTP_NOK";
            this.cbxTP_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxTP_NOK.TabIndex = 17;
            this.cbxTP_NOK.Text = "Not OK";
            this.cbxTP_NOK.UseVisualStyleBackColor = true;
            this.cbxTP_NOK.CheckedChanged += new System.EventHandler(this.cbxTP_NOK_CheckedChanged);
            // 
            // cbxTC_OK
            // 
            this.cbxTC_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxTC_OK.AutoSize = true;
            this.cbxTC_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.TC_OK;
            this.cbxTC_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "TC_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxTC_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTC_OK.Location = new System.Drawing.Point(310, 72);
            this.cbxTC_OK.Name = "cbxTC_OK";
            this.cbxTC_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxTC_OK.TabIndex = 16;
            this.cbxTC_OK.Text = "OK";
            this.cbxTC_OK.UseVisualStyleBackColor = true;
            this.cbxTC_OK.CheckedChanged += new System.EventHandler(this.cbxTC_OK_CheckedChanged);
            // 
            // cbxTC_NOK
            // 
            this.cbxTC_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxTC_NOK.AutoSize = true;
            this.cbxTC_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.TC_NOK;
            this.cbxTC_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "TC_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxTC_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxTC_NOK.Location = new System.Drawing.Point(380, 72);
            this.cbxTC_NOK.Name = "cbxTC_NOK";
            this.cbxTC_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxTC_NOK.TabIndex = 15;
            this.cbxTC_NOK.Text = "Not OK";
            this.cbxTC_NOK.UseVisualStyleBackColor = true;
            this.cbxTC_NOK.CheckedChanged += new System.EventHandler(this.cbxTC_NOK_CheckedChanged);
            // 
            // cbxBEL_OK
            // 
            this.cbxBEL_OK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxBEL_OK.AutoSize = true;
            this.cbxBEL_OK.Checked = global::EngineAnalyzer.Properties.Settings.Default.BEL_OK;
            this.cbxBEL_OK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "BEL_OK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxBEL_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxBEL_OK.Location = new System.Drawing.Point(310, 36);
            this.cbxBEL_OK.Name = "cbxBEL_OK";
            this.cbxBEL_OK.Size = new System.Drawing.Size(61, 29);
            this.cbxBEL_OK.TabIndex = 14;
            this.cbxBEL_OK.Text = "OK";
            this.cbxBEL_OK.UseVisualStyleBackColor = true;
            this.cbxBEL_OK.CheckedChanged += new System.EventHandler(this.cbxBEL_OK_CheckedChanged);
            // 
            // cbxBEL_NOK
            // 
            this.cbxBEL_NOK.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbxBEL_NOK.AutoSize = true;
            this.cbxBEL_NOK.Checked = global::EngineAnalyzer.Properties.Settings.Default.BEL_NOK;
            this.cbxBEL_NOK.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::EngineAnalyzer.Properties.Settings.Default, "BEL_NOK", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.cbxBEL_NOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxBEL_NOK.Location = new System.Drawing.Point(380, 36);
            this.cbxBEL_NOK.Name = "cbxBEL_NOK";
            this.cbxBEL_NOK.Size = new System.Drawing.Size(100, 29);
            this.cbxBEL_NOK.TabIndex = 13;
            this.cbxBEL_NOK.Text = "Not OK";
            this.cbxBEL_NOK.UseVisualStyleBackColor = true;
            this.cbxBEL_NOK.CheckedChanged += new System.EventHandler(this.cbxBEL_NOK_CheckedChanged);
            // 
            // frmPhysicalInspection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(514, 492);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.cbxTO_OK);
            this.Controls.Add(this.cbxTO_NOK);
            this.Controls.Add(this.cbxEO_OK);
            this.Controls.Add(this.cbxEO_NOK);
            this.Controls.Add(this.cbxBO_OK);
            this.Controls.Add(this.cbxBO_NOK);
            this.Controls.Add(this.cbxA_OK);
            this.Controls.Add(this.cbxA_NOK);
            this.Controls.Add(this.cbxH_OK);
            this.Controls.Add(this.cbxH_NOK);
            this.Controls.Add(this.cbxPL_OK);
            this.Controls.Add(this.cbxPL_NOK);
            this.Controls.Add(this.cbxHL_OK);
            this.Controls.Add(this.cbxHL_NOK);
            this.Controls.Add(this.cbxCS_OK);
            this.Controls.Add(this.cbxCS_NOK);
            this.Controls.Add(this.cbxTP_OK);
            this.Controls.Add(this.cbxTP_NOK);
            this.Controls.Add(this.cbxTC_OK);
            this.Controls.Add(this.cbxTC_NOK);
            this.Controls.Add(this.cbxBEL_OK);
            this.Controls.Add(this.cbxBEL_NOK);
            this.Controls.Add(this.lblTO);
            this.Controls.Add(this.lblEO);
            this.Controls.Add(this.lblBO);
            this.Controls.Add(this.lblA);
            this.Controls.Add(this.lblH);
            this.Controls.Add(this.lblPL);
            this.Controls.Add(this.lblHL);
            this.Controls.Add(this.lblCS);
            this.Controls.Add(this.lblTP);
            this.Controls.Add(this.lblTC);
            this.Controls.Add(this.lblBEL);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(520, 520);
            this.Name = "frmPhysicalInspection";
            this.Text = "Physical Observation";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        public InterfaceLab.WinForm.Controls.SmartLabel lblBEL;
        public InterfaceLab.WinForm.Controls.SmartLabel lblTC;
        public InterfaceLab.WinForm.Controls.SmartLabel lblTP;
        public InterfaceLab.WinForm.Controls.SmartLabel lblCS;
        public InterfaceLab.WinForm.Controls.SmartLabel lblHL;
        public InterfaceLab.WinForm.Controls.SmartLabel lblPL;
        public InterfaceLab.WinForm.Controls.SmartLabel lblH;
        public InterfaceLab.WinForm.Controls.SmartLabel lblA;
        public InterfaceLab.WinForm.Controls.SmartLabel lblBO;
        public InterfaceLab.WinForm.Controls.SmartLabel lblEO;
        public InterfaceLab.WinForm.Controls.SmartLabel lblTO;
        public System.Windows.Forms.CheckBox cbxBEL_NOK;
        public System.Windows.Forms.CheckBox cbxBEL_OK;
        public System.Windows.Forms.CheckBox cbxTC_OK;
        public System.Windows.Forms.CheckBox cbxTC_NOK;
        public System.Windows.Forms.CheckBox cbxTP_OK;
        public System.Windows.Forms.CheckBox cbxTP_NOK;
        public System.Windows.Forms.CheckBox cbxCS_OK;
        public System.Windows.Forms.CheckBox cbxCS_NOK;
        public System.Windows.Forms.CheckBox cbxHL_OK;
        public System.Windows.Forms.CheckBox cbxHL_NOK;
        public System.Windows.Forms.CheckBox cbxPL_OK;
        public System.Windows.Forms.CheckBox cbxPL_NOK;
        public System.Windows.Forms.CheckBox cbxH_OK;
        public System.Windows.Forms.CheckBox cbxH_NOK;
        public System.Windows.Forms.CheckBox cbxA_OK;
        public System.Windows.Forms.CheckBox cbxA_NOK;
        public System.Windows.Forms.CheckBox cbxBO_OK;
        public System.Windows.Forms.CheckBox cbxBO_NOK;
        public System.Windows.Forms.CheckBox cbxEO_OK;
        public System.Windows.Forms.CheckBox cbxEO_NOK;
        public System.Windows.Forms.CheckBox cbxTO_OK;
        public System.Windows.Forms.CheckBox cbxTO_NOK;
        private System.Windows.Forms.Button btnClear;
    }
}