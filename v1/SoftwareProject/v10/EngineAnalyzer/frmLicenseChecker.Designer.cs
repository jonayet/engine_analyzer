﻿
partial class frmLicenseChecker
{
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	/// <summary>
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing)
	{
		if (disposing && (components != null))
		{
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Windows Form Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent()
	{
            this.components = new System.ComponentModel.Container();
            this.owfProgressControl1 = new Owf.Controls.OwfProgressControl(this.components);
            this.tbxMachineId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlNoLicense = new System.Windows.Forms.Panel();
            this.pnlNoLicense.SuspendLayout();
            this.SuspendLayout();
            // 
            // owfProgressControl1
            // 
            this.owfProgressControl1.AnimationSpeed = ((short)(75));
            this.owfProgressControl1.BackColor = System.Drawing.Color.Transparent;
            this.owfProgressControl1.CirclesColor = System.Drawing.Color.Lime;
            this.owfProgressControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.owfProgressControl1.Location = new System.Drawing.Point(130, 16);
            this.owfProgressControl1.Name = "owfProgressControl1";
            this.owfProgressControl1.Size = new System.Drawing.Size(319, 153);
            this.owfProgressControl1.TabIndex = 0;
            this.owfProgressControl1.Text = "owfProgressControl1";
            this.owfProgressControl1.TitileText = "  Please wait...";
            // 
            // tbxMachineId
            // 
            this.tbxMachineId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbxMachineId.Location = new System.Drawing.Point(199, 94);
            this.tbxMachineId.Name = "tbxMachineId";
            this.tbxMachineId.ReadOnly = true;
            this.tbxMachineId.Size = new System.Drawing.Size(239, 20);
            this.tbxMachineId.TabIndex = 40;
            this.tbxMachineId.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(128, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 41;
            this.label3.Tag = "";
            this.label3.Text = "Machine ID:";
            // 
            // btnClose
            // 
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(214, 129);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(130, 42);
            this.btnClose.TabIndex = 42;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(90, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(386, 31);
            this.label1.TabIndex = 43;
            this.label1.Tag = "";
            this.label1.Text = "There is no License registered.";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(109, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(352, 31);
            this.label2.TabIndex = 44;
            this.label2.Tag = "";
            this.label2.Text = "This Program will be closed.";
            // 
            // pnlNoLicense
            // 
            this.pnlNoLicense.Controls.Add(this.label1);
            this.pnlNoLicense.Controls.Add(this.label2);
            this.pnlNoLicense.Controls.Add(this.tbxMachineId);
            this.pnlNoLicense.Controls.Add(this.label3);
            this.pnlNoLicense.Controls.Add(this.btnClose);
            this.pnlNoLicense.Location = new System.Drawing.Point(0, 0);
            this.pnlNoLicense.Name = "pnlNoLicense";
            this.pnlNoLicense.Size = new System.Drawing.Size(552, 183);
            this.pnlNoLicense.TabIndex = 45;
            this.pnlNoLicense.Visible = false;
            // 
            // frmLicenseChecker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(552, 183);
            this.ControlBox = false;
            this.Controls.Add(this.pnlNoLicense);
            this.Controls.Add(this.owfProgressControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(552, 183);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(552, 183);
            this.Name = "frmLicenseChecker";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.pnlNoLicense.ResumeLayout(false);
            this.pnlNoLicense.PerformLayout();
            this.ResumeLayout(false);

	}

	#endregion

	private Owf.Controls.OwfProgressControl owfProgressControl1;
    private System.Windows.Forms.TextBox tbxMachineId;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel pnlNoLicense;
}